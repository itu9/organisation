package package extract.marque;;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


@CrossOrigin("*")
@RequestMapping("/marque")
@RestController 
public class Marque extends CrudController<MarqueController, Integer, MarqueRepository, MarqueService,MarqueSearch,MarqueUtils> {
    @Autowired 
    public Marque(MarqueService service) {
        super(service);
    }
}