alter table produit add column pvente double precision not null default 0;
alter table vente add column prix double precision not null default 0;
-- TODO Benefit --
-- sum vente.pvente-vente.pu per product per year,month,week
-- create table benefitRecord
-- fit the benefit view with the benefitRecord

create table benefitRecord (
    id serial primary key,
    produitid integer,
    vente double precision not null default 0.0,
    depense double precision not null default 0.0,
    benefit double precision not null default 0.0,
    date date not null
);
alter table benefitRecord add foreign key(produitid) references produit(id);

create or replace view vbenefitRecord as
    select br.produitid,
           max(br.date) date
        from benefitRecord br
        group by br.produitid;

create or replace view vventewithbenef as
    select v.id,
           v.produitid,
           v.date,
           v.quantite,
           v.prix
    from vbenefitRecord br
    left join vente v on br.produitid = v.produitid
    and v.date > br.date;

create or replace view vventenobenef as
    select v.id,
           v.produitid,
           v.date,
           v.quantite,
           v.prix
    from vente v
    join vbenefitrecord br on br.produitid != v.produitid;

create or replace view vvente as
    select vwb.id,
           vwb.produitid,
           vwb.date,
           vwb.quantite,
           vwb.prix
        from vventewithbenef vwb
    union
    select vnb.id,
           vnb.produitid,
           vnb.date,
           vnb.quantite,
           vnb.prix
        from vventenobenef vnb;

create or replace view benefit as
select v.produitid,
       sum(v.prix) vente,
       sum(p.pu*v.quantite) depense,
       sum(v.prix) - sum(p.pu*v.quantite) benefit,
       row_number() over () id,
       extract(year from v.date) annee,
       extract(month from v.date) mois,
       extract(week from v.date) semaine
    from vvente v
    left join produit p on v.produitid = p.id
    group by v.produitid,
             extract(year from v.date),
             extract(month from v.date),
             extract(week from v.date);

create or replace view benefitmonth as
    select b.produitid,
           sum(b.vente) vente,
           sum(b.depense) depense,
           sum(b.benefit) benefit,
           row_number() over () id,
           b.annee,
           b.mois
    from benefit b
    group by b.produitid,
             b.annee,
             b.mois;

create or replace view benefityear as
    select b.produitid,
    sum(b.vente) vente,
    sum(b.depense) depense,
    sum(b.benefit) benefit,
    row_number() over () id,
    b.annee
    from benefit b
    group by b.produitid,
    b.annee;