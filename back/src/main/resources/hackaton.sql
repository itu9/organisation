create table client (
    id serial primary key,
    email varchar(50),
    mdp varchar(50),
    nom varchar(50),
    prenom varchar(50),
    etat int not null default 0,
    taille double precision not null default 0,
    naissance date
);

create table personne (
    id serial primary key,
    nom varchar(50),
    prenom varchar(50),
    taille double precision not null default 0
);

CREATE TABLE public.produit (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    type integer DEFAULT 50 NOT NULL
);


CREATE TABLE public.sousprod (
    id integer NOT NULL,
    nombre double precision DEFAULT '0'::numeric NOT NULL,
    pointure double precision NOT NULL,
    taille integer NOT NULL,
    produitid integer NOT NULL,
    dateins timestamp without time zone NOT NULL
);

CREATE TABLE public.hist (
    id integer NOT NULL,
    nombre double precision DEFAULT '0'::numeric NOT NULL,
    dateins timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    produitid integer NOT NULL
);

create table taille (
    id serial primary key not null,
    title varchar(50),
    type double precision
);

ALTER TABLE ONLY public.hist
    ADD CONSTRAINT fkhist219959 FOREIGN KEY (produitid) REFERENCES public.produit(id);

ALTER TABLE ONLY public.sousprod
    ADD CONSTRAINT fksousprod125207 FOREIGN KEY (produitid) REFERENCES public.produit(id);

ALTER TABLE ONLY public.token
    ADD CONSTRAINT fktj3cyyavmpxp32tkhjnu39bo2 FOREIGN KEY (loginid) REFERENCES public.client(id);



----------------------------------------------------------------
----------------------------Data--------------------------------
----------------------------------------------------------------

insert into taille(title,type) values 
('S',50),
('SM',100),
('M',150);

insert into client(email,mdp,nom,prenom,etat,taille,naissance) values 
    ('hasinaclient@gmail.com','motdepasse','Rivonandrasana','Hasina',50,1.5,'2002-06-24'),
    ('heryclient@gmail.com','motdepasse','Ravelojaona','Hery',50,1.62,'2003-06-24'),
    ('lazaclient@gmail.com','motdepasse','Ramanandahy','Laza',50,1.70,'2004-06-24'),
    ('faniryclient@gmail.com','motdepasse','Rainala','Faniry',50,1.8,'2005-06-24'),
    ('sahazaclient@gmail.com','motdepasse','Rabesandratana','Sahaza',50,1.42,'2006-06-24'),
    ('harilalaclient@gmail.com','motdepasse','Rainiavo','Harilala',0,1.65,'2002-06-24');

insert into personne(nom,prenom,taille) values
    ('Rivonandrasana','Hasina',1.72),
    ('Rainarivelo','Hery',1.65),
    ('Rivonandrasana','Hasina',1.72),
    ('Rivonandrasana','Hasina',1.72);

insert into produit(title,type) values('Nike 1',50);
insert into produit(title,type) values('Nike 10',50);
insert into produit(title,type) values('Nike 100',50);
insert into produit(title,type) values('Nike 1000',50);
insert into produit(title,type) values('Vetement 1',100);
insert into produit(title,type) values('Vetement 10',100);
insert into produit(title,type) values('Vetement 100',100);
insert into produit(title,type) values('Vetement 1000',100);


insert into sousprod(produitid,pointure,nombre) values
    (1,20,10),
    (1,21,15),
    (1,25,20),
    (2,23,5),
    (2,25,10),
    (2,23,7);
    
insert into sousprod(produitid,taille,nombre) values
    (5,50,5),
    (5,100,4),
    (5,50,10),
    (6,100,5),
    (6,100,10),
    (6,150,7);

create or replace view vstock as 
    select sum(nombre) nombre,
        produitid,
        pointure,
        null taille,
        prix,
        coleur
    from sousprod
    where pointure is not null
    group by produitid,pointure,prix,coleur
    union
    select sum(nombre) nombre,
        produitid,
        null pointure,
        taille,
        prix,
        coleur
    from sousprod
    where taille is not null
    group by produitid,taille,prix,coleur;

create or replace view vhist as 
    select id,
    nombre,
    dateins,
    produitid,
    coalesce(pointure,0) pointure,
    coalesce(taille,0) taille,
    coleur,
    prix
    from hist;

create or replace view vhist1 as 
    select 
    sum(nombre) nombre,
    produitid,
    pointure,
    taille,
    coleur,
    prix
    from vhist
    group by produitid,pointure,taille,coleur,prix;

create or replace view vstock14 as 
select s.nombre-coalesce(h.nombre,0) nombre,
            s.produitid,
            s.pointure,
            s.taille,
            row_number() over() id,
            s.coleur,
            s.prix
        from vstock s left join vhist1 h
        on s.produitid = h.produitid
        and (s.pointure = h.pointure or (s.pointure is null))
        and (s.taille = h.taille or (s.taille is null))
        and (s.coleur = h.coleur )
        and (s.prix = h.prix )
        order by s.produitid;

    create or replace view stock as 
        select s.*,
            t.title ltaille,
            c.title couleur
        from vstock14 s left join taille t 
        on s.taille = t.type
        left join couleur c 
        on s.coleur = c.id
        where nombre != 0;

alter table sousprod add column coleur integer not null default 1;
alter table sousprod add column prix double precision not null default 0.;

alter table hist add column coleur integer not null default 1;
alter table hist add column prix double precision not null default 0.;

create table couleur (
    id serial primary key not null,
    title varchar(50),
    type double precision
);
insert into couleur (title,type) values ('Rouge',1);
insert into couleur (title,type) values ('Bleu',10);
insert into couleur (title,type) values ('Noire',20);
insert into couleur (title,type) values ('Rose',30);
insert into couleur (title,type) values ('Jaune',40);
insert into couleur (title,type) values ('Blanc',50);

create table couleurcomp (
    id serial primary  key not null,
    couleur1 int,
    couleur2 int
);
alter table couleurcomp add FOREIGN KEY (couleur1 ) REFERENCES couleur(id);
alter table couleurcomp add FOREIGN KEY (couleur2 ) REFERENCES couleur(id);

create or replace view vcouleurcomp as
select id,
    couleur1,
    couleur2
from couleurcomp
union
select id,
    couleur2 couleur1,
    couleur1 couleur2
from couleurcomp
;

insert into couleurcomp (couleur1,couleur2) values(1,2);
insert into couleurcomp (couleur1,couleur2) values(1,3);
insert into couleurcomp (couleur1,couleur2) values(4,2);
insert into couleurcomp (couleur1,couleur2) values(4,5);
insert into couleurcomp (couleur1,couleur2) values(6,1);

create table taillecomp (
    id serial primary key not null,
    taille double precision,
    debut double precision,
    fin double precision
);

insert into taillecomp(taille,debut,fin) values
(50,0,25),
(100,20,40),
(150,35,60)
;
insert into sousprod (nombre,pointure,taille,produitid,coleur,prix) values
    (1,35,null,20,1,50000),
    (1,36,null,20,1,50000),
    (1,37,null,20,1,50000),
    (1,38,null,20,1,50000),
    (1,39,null,20,1,50000),
    (1,40,null,20,1,50000),
    (1,41,null,20,1,50000),
    (1,42,null,20,1,50000),
    (1,43,null,20,1,50000),
    
    (1,35,null,21,2,55000),
    (1,36,null,21,2,55000),
    (1,37,null,21,2,55000),
    (1,38,null,21,2,55000),
    (1,39,null,21,2,55000),
    (1,40,null,21,2,55000),
    (1,41,null,21,2,55000),
    (1,42,null,21,2,55000),
    (1,43,null,21,2,55000),
    
    (1,35,null,22,3,60000),
    (1,36,null,22,3,60000),
    (1,37,null,22,3,60000),
    (1,38,null,22,3,60000),
    (1,39,null,22,3,60000),
    (1,40,null,22,3,60000),
    (1,41,null,22,3,60000),
    (1,42,null,22,3,60000),
    (1,43,null,22,3,60000),
    
    (1,35,null,23,4,65000),
    (1,36,null,23,4,65000),
    (1,37,null,23,4,65000),
    (1,38,null,23,4,65000),
    (1,39,null,23,4,65000),
    (1,40,null,23,4,65000),
    (1,41,null,23,4,65000),
    (1,42,null,23,4,65000),
    (1,43,null,23,4,65000),
    
    (1,null,0,24,3,50000),
    (1,null,50,24,3,50000),
    (1,null,100,24,3,50000),
    (1,null,150,24,3,50000),

    (1,null,0,25,5,45000),
    (1,null,50,25,5,45000),
    (1,null,100,25,5,45000),
    (1,null,150,25,5,45000),

    (1,null,0,26,2,40000),
    (1,null,50,26,2,40000),
    (1,null,100,26,2,40000),
    (1,null,150,26,2,40000),

    (1,null,0,27,6,40000),
    (1,null,50,27,6,40000),
    (1,null,100,27,6,40000),
    (1,null,150,27,6,40000)
    ;