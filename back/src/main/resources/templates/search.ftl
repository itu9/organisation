${packageName}

${import}
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class ${className} extends Search<${classMapped}> {
    ${attribute}
    public ${className}() {
        super(${classMapped}.class);
    }
};