${packageName}

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;
${import}

@Service 
public class ${className} extends HCrud<${classMapped}, ${id}, ${repository},${search},${utils}> {
    @Autowired 
    @Override 
    public void setRepos(${repository} repos) {
        super.setRepos(repos);
    }
}
