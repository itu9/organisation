import { useParams } from "react-router-dom";

import ${importClass} from "./${importClass}";

export default function ${className}() {
    const {id} = useParams();
    return (
        <${importClass} id={id}>
        </${importClass}>
    );
}