import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class ${className}  extends GeneralForm  {
    constructor(params) {
        super(params);
        ${constructor}
        this.urlSend = "${urlSend}";
        this.urlUtils = "${urlUtils}";
        this.afterValidation = "${urlSend}";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            ${preparedData}
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">${formTitle}</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            ${data}
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}