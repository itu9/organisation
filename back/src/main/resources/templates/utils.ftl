${packageName}

${import}
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class ${className} extends FormUtils<${classMapped}> {
    ${attribute}
    public ${className}() {
        super(${classMapped}.class);
    }
};