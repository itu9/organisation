${packageName}

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
${import}
import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("${urlMapping}")
@RestController 
public class ${className} extends CrudController<${classMapped}, ${id}, ${repository}, ${service},${search},${utils}> {
    @Autowired 
    public ${className}(${service} service) {
        super(service);
    }
}