<#-- Component-->
<Route path="${path}" element={ <${compListe} /> }></Route>
<Route path="${path}/:id" element={ <${compFiche} /> }></Route>
<Route path="${path}/form" element={ <${compForm} /> }></Route>
<Route path="${path}/update/:id" element={ <${compUpdate} /> }></Route>