<h3 className="text-info mt-3">${label}</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={${refmin}} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={${refmax}} />
    </div>
</div>