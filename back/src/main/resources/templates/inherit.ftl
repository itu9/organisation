<h3 className="text-info mt-3" >${label}</h3>
<select className="form-control" ref={${reference}}>
    <option value=""></option>
    {
        ${variable} !== undefined && ${variable} !== null?
        ${variableMap}.map((${data},index) => (
            <option key={index} value={${value}} ${default}>{${optLabel}}</option>
        ))
        :<></>
    }
</select>