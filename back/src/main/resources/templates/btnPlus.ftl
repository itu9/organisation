 <td>
    <button className="btn" onClick={()=> {
        window.location.replace("${urlUpdate}"+data.id)
    }}>
        <i className="fas fa-pencil-alt text-warning"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        this.delete(data)
    }}>
        <i className="fas fa-trash-alt text-danger"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        window.location.replace("${urlInfo}"+data.id)
    }}>
        <i className="fas fa-plus text-info"></i>
    </button>
</td>