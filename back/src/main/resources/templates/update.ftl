import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class ${className}  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        ${constructor}
        this.urlSend = "${urlSend}";
        this.urlUtils = "${urlUtils}";
        this.afterValidation = "${after}";
        this.id = React.createRef(null);
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        ${loading}
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            ${preparedData}
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">${updateTitle}</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            ${data}
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}