package ${packageName};

import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;
${import}

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class ${className} extends HElement<Integer> {
    ${attribute}
}