<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Liste générée par H-istant</title>
    
    <style type="text/css">
        table{
            border-collapse: collapse;
            width: 400px;
        }
        th, td{
            border: 1px solid black;
            padding: 10px;
            vertical-align: top;
        }
    </style>
</head>
<body>
     <div>
        <h1>Gestion evenement</h1>
        <h2>${titre}</h2>
        <table>
            <tr>
                ${head}
            </tr>
            ${body}
        </table>
     </div>
</body>
</html>