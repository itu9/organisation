create table type (
    id serial primary key,
    titre varchar(30)
);

create table marque (
    id serial primary key,
    titre varchar(30)
);

create table modele (
    id serial primary key,
    typeid integer,
    marqueid integer
);
ALTER TABLE modele add foreign key(marqueid) references marque(id);
ALTER TABLE modele add foreign key(typeid) references type(id);

create table vehicule (
    id serial primary key,
    numero varchar(15),
    modeleid integer
);
ALTER TABLE vehicule add foreign key(modeleid) references modele(id);

create table lieu (
    id serial primary key,
    titre varchar(15)
);

create table Trajet (
    id serial primary key,
    hdepart timestamp not null,
    lieudep integer,
    kilodep double precision,
    harrive timestamp,
    lieuarr integer,
    kiloarr double precision,
    vehiculeid integer,
    motif varchar(255)
);
ALTER table Trajet ADD foreign key(lieudep) references lieu(id);
ALTER table Trajet ADD foreign key(lieuarr) references lieu(id);
ALTER table Trajet ADD foreign key(vehiculeid) references vehicule(id);

insert into trajet(hdepart,lieudep,kilodep,motif) values
    ('2023-05-04 10:47:17',1,1500,'Bus');

create table Echeance (
    id serial primary key,
    titre varchar(30),
    nbj double precision default 0
);

create table EcheanceVoiture (
    id serial primary key,
    vehiculeid integer,
    date date not null,
    echeanceid integer
);
ALTER TABLE EcheanceVoiture add foreign key(vehiculeid) references vehicule(id);
ALTER TABLE EcheanceVoiture add foreign key(echeanceid) references echeance(id);

create table maintenance (
    id serial primary key,
    titre varchar(30),
    valable double precision
);

create table maintenancevoiture (
    id serial primary key,
    vehiculeid integer,
    date date,
    maintenanceid integer
);
ALTER TABLE maintenancevoiture add foreign key(vehiculeid) references vehicule(id);
ALTER TABLE maintenancevoiture add foreign key(maintenanceid) references maintenance(id);

create or replace view v_vehicule_echeance as 
    select v.id vehiculeid,
        e.id echeanceid
    from vehicule v
    cross join echeance e;

create or replace view v_echeance as 
    select ve.vehiculeid,
        ve.echeanceid,
        coalesce( ecv.date,current_date-e.nbj::int) date,
        coalesce( ecv.date,current_date-e.nbj::int)+e.nbj::int-current_date reste,
        row_number() over() id
    from v_vehicule_echeance ve 
    left join EcheanceVoiture ecv 
    on ve.echeanceid = ecv.echeanceid
    and ve.vehiculeid = ecv.vehiculeid
    left join Echeance e 
    on ve.echeanceid = e.id;

create or replace view v_maxtrajet as
select max(t.hdepart) hdepart,
    t.vehiculeid
from trajet t
group by t.vehiculeid;

create or replace view v_last_trajet as 
    select t.id,
        t.hdepart,
        t.harrive,
        t.vehiculeid
    from v_maxtrajet mt 
    left join trajet t 
    on mt.hdepart = t.hdepart
    and mt.vehiculeid = t.vehiculeid;

create or replace view disponible as 
    select v.id,
        v.numero,
        v.modeleid
    from vehicule v
    left join v_last_trajet lt 
    on v.id = lt.vehiculeid
    where (lt.hdepart is not null and lt.harrive is not null) 
    or (lt.hdepart is null and lt.harrive is null);

create or replace view v_vehicule_maintenance as 
    select v.id vehiculeid,
        m.id maintenanceid
    from vehicule v 
    cross join maintenance m;

create or replace view v_max_maintenance as 
    select max(mv.date) date,
        mv.vehiculeid
    from maintenancevoiture mv
    group by mv.vehiculeid;

create or replace view v_last_maintenance as 
    select mv.id,
        mv.vehiculeid,
        mv.date,
        mv.maintenanceid
    from v_max_maintenance mm 
    left join maintenancevoiture mv 
    on mm.vehiculeid = mv.vehiculeid
    and mm.date = mv.date;

create or replace view v_trajet_maintenance as 
    select t.id,
        t.vehiculeid,
        coalesce(t.kiloarr,t.kilodep)-t.kilodep kilometrage,
        lm.maintenanceid
    from v_last_maintenance lm
    left join trajet t
    on lm.vehiculeid = t.vehiculeid
    and t.hdepart >= lm.date;

create or replace view v_trajet_somme as 
    select tm.vehiculeid,
        tm.maintenanceid,
        sum(tm.kilometrage) kilometrage
    from v_trajet_maintenance tm
    group by tm.vehiculeid,tm.maintenanceid;

create or replace view maintenance_status as 
    select vm.vehiculeid,
        vm.maintenanceid, 
        coalesce(ts.kilometrage,m.valable) kilometrage,
        m.valable -coalesce(ts.kilometrage,m.valable) reste,
        row_number() over() id
    from v_vehicule_maintenance vm
    left join v_trajet_somme ts
    on vm.maintenanceid = ts.maintenanceid
    and vm.vehiculeid = ts.vehiculeid
    left join maintenance m 
    on vm.maintenanceid = m.id;
