package extract.percent;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/percent")
@RestController 
public class PercentController extends CrudController<Percent, Integer, PercentRepository, PercentService,PercentSearch,PercentUtils> {
    @Autowired 
    public PercentController(PercentService service) {
        super(service);
    }
}