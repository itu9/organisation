package extract.percent;


import org.springframework.data.jpa.repository.JpaRepository;
public interface PercentRepository extends JpaRepository<Percent,Integer> {

}
