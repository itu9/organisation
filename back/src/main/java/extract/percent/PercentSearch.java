package extract.percent;

import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class PercentSearch extends Search<Percent> {
    Double valuehmin;Double valuehmax;
    public PercentSearch() {
        super(Percent.class);
    }
};