package extract.percent;

import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;


@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Percent extends HElement<Integer> {
    Double value;

    public Double eval(Double benefbrut) {
        return this.getValue()*benefbrut / 100.;
    }
}