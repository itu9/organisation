package extract.percent;

import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class PercentUtils extends FormUtils<Percent> {
    
    public PercentUtils() {
        super(Percent.class);
    }
};