package extract.percent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;


@Service 
public class PercentService extends HCrud<Percent, Integer, PercentRepository,PercentSearch,PercentUtils> {
    @Autowired 
    @Override 
    public void setRepos(PercentRepository repos) {
        super.setRepos(repos);
    }
}
