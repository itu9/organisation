package extract.type;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/type")
@RestController 
public class TypeController extends CrudController<Type, Integer, TypeRepository, TypeService,TypeSearch,TypeUtils> {
    @Autowired 
    public TypeController(TypeService service) {
        super(service);
    }
}