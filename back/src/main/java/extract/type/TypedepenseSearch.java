package extract.type;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class TypedepenseSearch extends Search<Typedepense> {
    
    public TypedepenseSearch() {
        super(Typedepense.class);
    }
};