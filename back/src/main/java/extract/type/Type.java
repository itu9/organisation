package extract.type;

import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;


@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Type extends HElement<Integer> {
    String titre;
}