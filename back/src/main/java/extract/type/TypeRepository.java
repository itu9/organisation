package extract.type;


import org.springframework.data.jpa.repository.JpaRepository;
public interface TypeRepository extends JpaRepository<Type,Integer> {

}
