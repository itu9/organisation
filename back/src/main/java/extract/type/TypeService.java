package extract.type;

import extract.controller.LoginCnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class TypeService extends HCrud<Type, Integer, TypeRepository,TypeSearch,TypeUtils> {
    @Autowired 
    @Override 
    public void setRepos(TypeRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public List<Type> create(List<Type> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Type create(Type obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Type update(Type obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        super.delete(obj, idUser);
    }
}
