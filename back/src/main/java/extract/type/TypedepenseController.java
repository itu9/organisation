package extract.type;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/typedepense")
@RestController 
public class TypedepenseController extends CrudController<Typedepense, Integer, TypedepenseRepository, TypedepenseService,TypedepenseSearch,TypedepenseUtils> {
    @Autowired 
    public TypedepenseController(TypedepenseService service) {
        super(service);
    }
}