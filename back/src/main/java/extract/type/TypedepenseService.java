package extract.type;

import extract.controller.LoginCnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class TypedepenseService extends HCrud<Typedepense, Integer, TypedepenseRepository,TypedepenseSearch,TypedepenseUtils> {
    @Autowired 
    @Override 
    public void setRepos(TypedepenseRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public List<Typedepense> create(List<Typedepense> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Typedepense create(Typedepense obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Typedepense update(Typedepense obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        super.delete(obj, idUser);
    }
}
