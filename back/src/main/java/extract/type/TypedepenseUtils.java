package extract.type;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class TypedepenseUtils extends FormUtils<Typedepense> {
    
    public TypedepenseUtils() {
        super(Typedepense.class);
    }
};