package extract.type;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class TypeUtils extends FormUtils<Type> {
    
    public TypeUtils() {
        super(Type.class);
    }
};