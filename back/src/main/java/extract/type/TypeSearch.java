package extract.type;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class TypeSearch extends Search<Type> {
    
    public TypeSearch() {
        super(Type.class);
    }
};