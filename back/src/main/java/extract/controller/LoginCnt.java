package extract.controller;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import extract.auth.Login;
import extract.auth.LoginParam;
import extract.auth.LoginSer;
import extract.auth.NoLogin;
import extract.auth.Token;
import lombok.Data;
import lombok.EqualsAndHashCode;

@CrossOrigin(value = "*")
@Data
@RestController
@EqualsAndHashCode(callSuper = true)
public class LoginCnt extends Cnt {
    @Autowired
    LoginSer loginSer;
    public static final int EMPLOYE = 25;
    public static final int ADMIN = 50;

    public ResponseEntity<?> login(LoginParam login, int etat) {
        try {
            Token l = this.getLoginSer().login(login, etat);
            return this.returnSuccess(l, HttpStatus.OK);
        } catch (NoLogin e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        } catch (NoSuchAlgorithmException e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/login/client")
    public ResponseEntity<?> loginClient(@RequestBody LoginParam login) {
        return this.login(login, LoginCnt.EMPLOYE);
    }

    @PostMapping("/login/admin")
    public ResponseEntity<?> loginAdmin(@RequestBody LoginParam login) {
        return this.login(login, LoginCnt.ADMIN);
    }

    public ResponseEntity<?> signup(Login login, int etat) {
        try {
            login.setAcreditation(etat);
            this.getLoginSer().signUp(login);
            return this.returnSuccess(login, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/incription/client")
    public ResponseEntity<?> createClient(@RequestBody Login login) {
        return this.signup(login, LoginCnt.EMPLOYE);

    }

    @PostMapping("/incription/admin")
    public ResponseEntity<?> createAdmin(@RequestBody Login login) {
        return this.signup(login, LoginCnt.ADMIN);
    }
}
