package extract.controller;

import java.util.List;

import extract.hmodel.HCrud;
import extract.hmodel.HElement;
import extract.hmodel.PdfParams;
import extract.hmodel.SearchObj;
import extract.tool.HMaker;
import extract.tool.OwnTools;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;

public class Search<T extends HElement<?>> {
    String keys = "";
    Class<T> classes;
    PdfParams pdf;
    int page = 1;

    public Search(Class<T> classes) {
        this.setClasses(classes);
    }

    public void toPdf(HMaker pdfMaker, List<?> data) {
        String content = "";
        for (Object object : data) {
            ((HElement<?>) object).pdfContent();
            content += this.getBody(object);
        }
        pdfMaker.put("head", this.getHead());
        pdfMaker.put("body", content);
    }

    public String getBody(Object data) {
        String[] answer = { "" };
        OwnTools.onField(this.getClasses(), (field) -> {
            if (HCrud.inHerit(field.getType())) {
                try {
                    answer[0] += this.getPdf().getInHerit(field, data);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                }
            } else {
                try {
                    answer[0] += this.getPdf().getSimple(field, data);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                }
            }
        });
        return "<tr>" + answer[0] + "</tr>";
    }

    public String getHead() {
        String[] answer = { "" };
        OwnTools.onField(this.getClasses(), (field) -> {
            if (HCrud.inHerit(field.getType())) {
                answer[0] += this.getPdf().getSimpleHead(field);
            } else {
                answer[0] += this.getPdf().getSimpleHead(field);
            }
        });
        return answer[0];
    }

    public void verifPdf() throws NoPdfParam {
        if (this.getPdf() == null) {
            throw new NoPdfParam();
        }
    }

    public Class<T> getClasses() {
        return classes;
    }

    public void setClasses(Class<T> classes) {
        this.classes = classes;
    }

    public String getKeys() {
        return this.keys == null ? "" : this.keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public PdfParams getPdf() {
        return pdf;
    }

    public void setPdf(PdfParams pdf) {
        this.pdf = pdf;
    }

    public Predicate getTrue(CriteriaBuilder criteria) {
        return criteria.isTrue(criteria.literal(Boolean.TRUE));
    }

    public Predicate where(SearchObj searchMap) {
        System.out.println("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
        return this.getTrue(searchMap.getCriteria());
    }
}
