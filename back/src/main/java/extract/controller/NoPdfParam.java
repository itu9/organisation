package extract.controller;

public class NoPdfParam extends Exception {

    public NoPdfParam() {
        super("Envoyer le parametre pour le pdf");
    }

}
