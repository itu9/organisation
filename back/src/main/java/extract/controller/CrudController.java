package extract.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import extract.hmodel.HCrud;
import extract.hmodel.HElement;
import extract.hmodel.SearchObj;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class CrudController<E extends HElement<ID>, ID, R extends JpaRepository<E, ID>, S extends HCrud<E, ID, R, Sh, U>, Sh extends Search<E>, U extends FormUtils<E>>
        extends Cnt {

    public static String UTILS = "/utils";
    public static String FILTER = "/filter";
    public S service;

    @PostMapping("/utils")
    public ResponseEntity<?> utils(@RequestBody U map, @RequestParam(name = "idUser", required = false) ID id) {
        try {
            return this.returnSuccess(this.service.getUtils(map, id));
        } catch (Exception e) {
            return this.returnError(e);
        }
    }

    @PostMapping("/filter")
    public ResponseEntity<?> search(@RequestBody Sh key,
            @RequestParam(name = "idUser", required = false) ID id) {
        try {
            if (key.getPdf() != null) {
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
                        .body(this.service.toPdf(key, key.getClasses(), id));
            } else {
                SearchObj response = this.service.search(key, key.getClasses(), id);
                SuccessResponse ans = new SuccessResponse(response.getData());
                ans.setSize(response.getSize());
                ans.setPagesize(response.getPageSize());
                ans.setPage(response.getPage());
                return this.returnResponse(ans, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e);
        }

    }

    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody E obj,
            @RequestParam(name = "idUser", required = false) ID id) throws Exception {
        try {
            return this.returnSuccess(service.create(obj, id), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") ID id,
            @RequestParam(name = "idUser", required = false) ID idUsers) {
        try {
            return this.returnSuccess(service.findById(id, idUsers), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable ID id,
            @RequestParam(name = "idUser", required = false) ID idUsers) {
        try {
            this.getService().delete(id, idUsers);
            return this.returnSuccess(idUsers, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/reload/{id}")
    public ResponseEntity<?> reload(@PathVariable ID id, @RequestParam(name = "idUser", required = false) ID idUser) {
        try {
            this.getService().reload(id, idUser);
            return this.findById(id, idUser);
        } catch (Exception e) {
            return this.returnError(e);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(name = "idUser", required = false) ID id) {
        try {
            return returnSuccess(service.getAll(id), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody E obj,
            @RequestParam(name = "idUser", required = false) ID id) throws Exception {
        try {
            return this.returnSuccess(service.update(obj, id), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e, HttpStatus.NOT_FOUND);
        }
    }

}
