package extract.controller;

import extract.hmodel.HElement;

public class SuccessResponse {
    private Object data;
    private long size;
    private int pagesize;
    private int page;

    public SuccessResponse(Object data) throws Exception {
        this.data = data;
        if (data instanceof HElement) {
            ((HElement<?>) this.data).prepareSend();
        }
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public long getSize() {
        return size;
    }

    public int getNbPage() {
        if (this.getPagesize() == 0) {
            return 0;
        }
        int ans = (int) (this.getSize() / this.getPagesize());
        ans = (this.getSize() - (ans * this.getPagesize()) > 0) ? ans + 1 : ans;
        return ans;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
