package extract.controller;

import java.util.List;

import extract.hmodel.HElement;
import extract.tool.OwnTools;

public class FormUtils<T extends HElement<?>> {
    Class<T> clazz;

    public FormUtils(Class<T> clazz) {
        this.setClazz(clazz);
    }

    public void set(List<?> data) {
        if (data.size() == 0) 
            return;
        OwnTools.onFieldWithList(this, (field) -> {
            Class<?> clazz = OwnTools.getClass(field);
            if (clazz.equals(data.get(0).getClass())) {
                try {
                    field.set(this, data);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }


}
