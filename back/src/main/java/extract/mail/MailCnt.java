package extract.mail;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.Cnt;
import extract.tool.HMail;

@RestController
public class MailCnt extends Cnt {
    // mkrandriamizara@gmail.com
    @GetMapping("/test/mail")
    public ResponseEntity<?> testMail() {
        String mail = "hasinajuunii123@gmail.com";
        String subject = "Test de fonctionnalité";
        String corps = "<p>Mande lesy io ah</p>";
        try {
            HMail hMail = new HMail(this.getSender(), mail, subject, corps, null);
            hMail.send();
            return this.returnSuccess("ok");
        } catch (Exception e) {
            return this.returnError(e);
        }

    }
}
