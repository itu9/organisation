package extract.usualexception;

public class QuantiteException extends Exception {

    public QuantiteException(Integer quantite) throws QuantiteException {
        if(quantite < 0) {
            throw this;
        }
    }

}
