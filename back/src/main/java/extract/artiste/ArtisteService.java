package extract.artiste;

import extract.controller.LoginCnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class ArtisteService extends HCrud<Artiste, Integer, ArtisteRepository,ArtisteSearch,ArtisteUtils> {
    @Autowired 
    @Override 
    public void setRepos(ArtisteRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public List<Artiste> create(List<Artiste> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Artiste create(Artiste obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Artiste update(Artiste obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        super.delete(obj, idUser);
    }
}
