package extract.artiste;

import java.lang.Double;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class ArtisteUtils extends FormUtils<Artiste> {
    
    public ArtisteUtils() {
        super(Artiste.class);
    }
};