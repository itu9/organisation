package extract.artiste;

import java.lang.Double;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class ArtisteSearch extends Search<Artiste> {
    Double tarifhmin;Double tarifhmax;Double dureehmin;Double dureehmax;
    public ArtisteSearch() {
        super(Artiste.class);
    }
};