package extract.artiste;

import javax.persistence.Entity;

import extract.usualexception.NegativePriceException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;


@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Artiste extends HElement<Integer> {
    String nom;
    Double tarif;
    Double duree;
    String photo;

    @Override
    public void verif() throws Exception {
        if (this.getTarif() == null || this.getTarif() < 0.)
            throw new NegativePriceException();
    }
}