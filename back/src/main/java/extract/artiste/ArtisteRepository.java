package extract.artiste;


import org.springframework.data.jpa.repository.JpaRepository;
public interface ArtisteRepository extends JpaRepository<Artiste,Integer> {

}
