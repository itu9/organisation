package extract.artiste;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/artiste")
@RestController 
public class ArtisteController extends CrudController<Artiste, Integer, ArtisteRepository, ArtisteService,ArtisteSearch,ArtisteUtils> {
    @Autowired 
    public ArtisteController(ArtisteService service) {
        super(service);
    }
}