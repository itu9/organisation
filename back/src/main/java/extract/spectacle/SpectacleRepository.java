package extract.spectacle;


import org.springframework.data.jpa.repository.JpaRepository;
public interface SpectacleRepository extends JpaRepository<Spectacle,Integer> {

}
