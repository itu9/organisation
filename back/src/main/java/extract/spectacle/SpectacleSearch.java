package extract.spectacle;

import extract.lieu.Lieu;import java.lang.Double;import java.sql.Date;import java.sql.Time;import java.lang.Integer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class SpectacleSearch extends Search<Spectacle> {
    Lieu lieuid;Double prixlieuhmin;Double prixlieuhmax;Date datehmin;Date datehmax;Time hourhmin;Time hourhmax;Integer etathmin;Integer etathmax;
    public SpectacleSearch() {
        super(Spectacle.class);
    }
};