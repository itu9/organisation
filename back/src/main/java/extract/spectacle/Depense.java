package extract.spectacle;

public interface Depense {
    public String getLabel();
    public Double getValue();
}
