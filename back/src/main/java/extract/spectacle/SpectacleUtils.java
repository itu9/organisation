package extract.spectacle;

import extract.lieu.Lieu;import java.lang.Double;import java.sql.Date;import java.sql.Time;import java.lang.Integer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class SpectacleUtils extends FormUtils<Spectacle> {
    List<Lieu> lieuid = new ArrayList<Lieu>();
    public SpectacleUtils() {
        super(Spectacle.class);
    }
};