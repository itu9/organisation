package extract.spectacle;

import javax.persistence.Entity;
import javax.persistence.Transient;

import extract.devis.DevisMere;
import extract.devis.artiste.Devisartiste;
import extract.devis.autre.Devisautre;
import extract.devis.fixe.Devisfixe;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Spectacle extends DevisMere {
    @Transient
    List<Depense> depenses = new ArrayList<>();

    @Override
    public void setDevisautres(List<Devisautre> autres) {
        for (Devisautre dep : autres)
            if (dep.getSum() < 25)
                this.depenses.add(dep);
        super.setDevisautres(autres);
    }

    @Override
    public void setDevisartistes(List<Devisartiste> devis) {
        for (Devisartiste dep : devis)
            if (dep.getSum() < 25)
                this.depenses.add(dep);
        super.setDevisartistes(devis);
    }

    @Override
    public void setDevisfixes(List<Devisfixe> devis) {
        for (Devisfixe dep : devis)
            if (dep.getSum() < 25)
                this.depenses.add(dep);
        super.setDevisfixes(devis);
    }
}