package extract.spectacle;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/spectacle")
@RestController 
public class SpectacleController extends CrudController<Spectacle, Integer, SpectacleRepository, SpectacleService,SpectacleSearch,SpectacleUtils> {
    @Autowired 
    public SpectacleController(SpectacleService service) {
        super(service);
    }
}