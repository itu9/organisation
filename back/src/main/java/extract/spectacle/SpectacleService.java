package extract.spectacle;

import extract.billet.DevisbilletService;
import extract.devis.artiste.DevisartisteService;
import extract.devis.autre.DevisautreService;
import extract.devis.fixe.DevisfixeService;
import extract.percent.Percent;
import extract.percent.PercentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class SpectacleService extends HCrud<Spectacle, Integer, SpectacleRepository,SpectacleSearch,SpectacleUtils> {
    @Autowired
    DevisartisteService devisartisteService;

    @Autowired
    DevisfixeService devisfixeService;

    @Autowired
    DevisautreService devisautreService;
    @Autowired
    PercentService percentService;
    @Autowired
    DevisbilletService devisbilletService;
    @Autowired
    @Override 
    public void setRepos(SpectacleRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public Spectacle findById(Integer obj, Integer idUser) throws Exception {
        Spectacle devis = super.findById(obj, idUser);
        List<Percent> taxes = this.percentService.getAll(idUser);
        if (taxes == null || taxes.size() == 0) {
            throw new Exception("Ajouter une taxe dans la base");
        }
        devis.setDevisartistes(this.devisartisteService.getByDevis(devis.getId(), idUser));
        devis.setDevisfixes(this.devisfixeService.getByDevis(devis, idUser));
        devis.setDevisautres(this.devisautreService.getByDevis(devis, idUser));
        devis.setDevisbillets(this.devisbilletService.getByDevis(devis,idUser));
        devis.eval(taxes.get(0));
        return devis;
    }
}
