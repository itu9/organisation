package extract.depensefixe;

import extract.controller.LoginCnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class DepensefixeService extends HCrud<Depensefixe, Integer, DepensefixeRepository,DepensefixeSearch,DepensefixeUtils> {
    @Autowired 
    @Override 
    public void setRepos(DepensefixeRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public List<Depensefixe> create(List<Depensefixe> obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Depensefixe create(Depensefixe obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Depensefixe update(Depensefixe obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        super.delete(obj, idUser);
    }
}
