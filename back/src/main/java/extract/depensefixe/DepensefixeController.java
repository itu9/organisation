package extract.depensefixe;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/depensefixe")
@RestController 
public class DepensefixeController extends CrudController<Depensefixe, Integer, DepensefixeRepository, DepensefixeService,DepensefixeSearch,DepensefixeUtils> {
    @Autowired 
    public DepensefixeController(DepensefixeService service) {
        super(service);
    }
}