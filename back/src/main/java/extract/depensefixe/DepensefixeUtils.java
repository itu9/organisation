package extract.depensefixe;

import java.lang.Double;import extract.type.Type;import extract.type.Typedepense;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DepensefixeUtils extends FormUtils<Depensefixe> {
    List<Type> typeid = new ArrayList<Type>();List<Typedepense> typedepenseid = new ArrayList<Typedepense>();
    public DepensefixeUtils() {
        super(Depensefixe.class);
    }
};