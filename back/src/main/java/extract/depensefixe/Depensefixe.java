package extract.depensefixe;

import javax.persistence.Entity;

import extract.usualexception.NegativePriceException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.type.Type;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.type.Typedepense;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Depensefixe extends HElement<Integer> {
    String titre;
    Double tarif;
    @OneToOne
    @JoinColumn(name = "typeid", nullable = false)
    Type typeid;
    @OneToOne
    @JoinColumn(name = "typedepenseid", nullable = false)
    Typedepense typedepenseid;
    Double duree;

    @Override
    public void verif() throws Exception {
        if (this.getTarif() == null || this.getTarif() < 0.)
            throw new NegativePriceException();
    }
}