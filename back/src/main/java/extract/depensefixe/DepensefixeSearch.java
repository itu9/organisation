package extract.depensefixe;

import java.lang.Double;import extract.type.Type;import extract.type.Typedepense;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DepensefixeSearch extends Search<Depensefixe> {
    Double tarifhmin;Double tarifhmax;Type typeid;Typedepense typedepenseid;Double dureehmin;Double dureehmax;
    public DepensefixeSearch() {
        super(Depensefixe.class);
    }
};