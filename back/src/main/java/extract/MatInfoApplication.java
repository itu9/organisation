package extract;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class })
@SpringBootApplication(scanBasePackages = "extract.*",exclude = { ErrorMvcAutoConfiguration.class })
public class MatInfoApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MatInfoApplication.class)
		.properties("spring.resources.static-locations=classpath:/static/");
	}

	public static void main(String[] args) {
		SpringApplication.run(MatInfoApplication.class, args);
	}
}
