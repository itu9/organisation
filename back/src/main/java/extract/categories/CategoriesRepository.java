package extract.categories;


import org.springframework.data.jpa.repository.JpaRepository;
public interface CategoriesRepository extends JpaRepository<Categories,Integer> {

}
