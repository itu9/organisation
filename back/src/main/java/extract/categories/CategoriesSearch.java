package extract.categories;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class CategoriesSearch extends Search<Categories> {
    
    public CategoriesSearch() {
        super(Categories.class);
    }
};