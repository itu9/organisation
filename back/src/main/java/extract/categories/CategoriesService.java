package extract.categories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;


@Service 
public class CategoriesService extends HCrud<Categories, Integer, CategoriesRepository,CategoriesSearch,CategoriesUtils> {
    @Autowired 
    @Override 
    public void setRepos(CategoriesRepository repos) {
        super.setRepos(repos);
    }
}
