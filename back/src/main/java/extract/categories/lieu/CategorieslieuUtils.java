package extract.categories.lieu;

import extract.lieu.Lieu;import extract.categories.Categories;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class CategorieslieuUtils extends FormUtils<Categorieslieu> {
    List<Lieu> lieuid = new ArrayList<Lieu>();List<Categories> categoriesid = new ArrayList<Categories>();
    public CategorieslieuUtils() {
        super(Categorieslieu.class);
    }
};