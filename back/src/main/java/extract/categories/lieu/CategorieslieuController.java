package extract.categories.lieu;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/categories/lieu")
@RestController 
public class CategorieslieuController extends CrudController<Categorieslieu, Integer, CategorieslieuRepository, CategorieslieuService,CategorieslieuSearch,CategorieslieuUtils> {
    @Autowired 
    public CategorieslieuController(CategorieslieuService service) {
        super(service);
    }
}