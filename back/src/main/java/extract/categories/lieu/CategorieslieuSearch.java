package extract.categories.lieu;

import extract.lieu.Lieu;
import extract.categories.Categories;

import java.lang.Double;

import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data
@EqualsAndHashCode(callSuper = false)
public class CategorieslieuSearch extends Search<Categorieslieu> {
    Lieu lieuid;
    Categories categoriesid;
    Double nbplacehmin;
    Double nbplacehmax;

    public CategorieslieuSearch() {
        super(Categorieslieu.class);
    }
};