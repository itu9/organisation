package extract.categories.lieu;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.lieu.Lieu;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.categories.Categories;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Categorieslieu extends HElement<Integer> {
    @OneToOne
    @JoinColumn(name = "lieuid", nullable = false)
    Lieu lieuid;
    @OneToOne
    @JoinColumn(name = "categoriesid", nullable = false)
    Categories categoriesid;
    Double nbplace;
}