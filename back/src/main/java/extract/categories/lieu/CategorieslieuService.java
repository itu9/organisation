package extract.categories.lieu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;


@Service 
public class CategorieslieuService extends HCrud<Categorieslieu, Integer, CategorieslieuRepository,CategorieslieuSearch,CategorieslieuUtils> {
    @Autowired 
    @Override 
    public void setRepos(CategorieslieuRepository repos) {
        super.setRepos(repos);
    }
}
