package extract.categories;

import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;


@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Categories extends HElement<Integer> {
    String titre;
}