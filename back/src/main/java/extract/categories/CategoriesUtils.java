package extract.categories;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class CategoriesUtils extends FormUtils<Categories> {
    
    public CategoriesUtils() {
        super(Categories.class);
    }
};