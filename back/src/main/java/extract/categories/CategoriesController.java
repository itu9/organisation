package extract.categories;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/categories")
@RestController 
public class CategoriesController extends CrudController<Categories, Integer, CategoriesRepository, CategoriesService,CategoriesSearch,CategoriesUtils> {
    @Autowired 
    public CategoriesController(CategoriesService service) {
        super(service);
    }
}