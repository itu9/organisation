package extract.lieu;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/lieu")
@RestController 
public class LieuController extends CrudController<Lieu, Integer, LieuRepository, LieuService,LieuSearch,LieuUtils> {
    @Autowired 
    public LieuController(LieuService service) {
        super(service);
    }
}