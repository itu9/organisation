package extract.lieu;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;


@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Lieu extends HElement<Integer> {
    String titre;
    Double nbplace;
    String photo;
}