package extract.lieu;

import java.lang.Double;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class LieuUtils extends FormUtils<Lieu> {
    
    public LieuUtils() {
        super(Lieu.class);
    }
};