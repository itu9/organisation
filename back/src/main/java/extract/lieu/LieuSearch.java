package extract.lieu;

import java.lang.Double;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class LieuSearch extends Search<Lieu> {
    Double nbplacehmin;Double nbplacehmax;
    public LieuSearch() {
        super(Lieu.class);
    }
};