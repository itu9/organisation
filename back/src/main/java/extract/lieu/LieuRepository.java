package extract.lieu;


import org.springframework.data.jpa.repository.JpaRepository;
public interface LieuRepository extends JpaRepository<Lieu,Integer> {

}
