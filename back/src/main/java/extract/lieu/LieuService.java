package extract.lieu;

import extract.controller.LoginCnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class LieuService extends HCrud<Lieu, Integer, LieuRepository,LieuSearch,LieuUtils> {
    @Autowired 
    @Override 
    public void setRepos(LieuRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public List<Lieu> create(List<Lieu> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Lieu create(Lieu obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Lieu update(Lieu obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        super.delete(obj, idUser);
    }
}
