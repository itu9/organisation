package extract;

import java.io.IOException;

import extract.tool.HWriter;

public class Import {
    public static void main(String[] args) throws IOException {
        HWriter writer = new HWriter();
        String[] data = writer.intoFile("Commande.txt");
        String res = "";
        String pattern = "insert into commandes(societe,employe,ville,pays,date,numero,produit,total,num_societe,num_ville,num_pays,num_produit) values"
                +
                "('%s','%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,%s);\n";
        String[] splt = {};
        for (String string : data) {
            splt = string.split(";");
            normalize(splt);
            res += String.format(pattern, splt[0], splt[1], splt[2], splt[3], splt[4], splt[5], splt[6], splt[7],
                    splt[8], splt[9], splt[10], splt[11]);
        }
        writer.write_data("data.sql", true, false, res);
    }

    public static void normalize(String[] data) {
        for (int i = 0; i < data.length; i++) {
            data[i] = data[i].replace("'", "''");
        }
        data[7] = remove(data[7]);
    }

    public static String remove(String str) {
        String ans = "";
        char[] chrs = str.toCharArray();
        for (char c : chrs) {
            if (((int) c) <= 256) {
                ans += c;
            }
        }
        return ans;
    }
}
