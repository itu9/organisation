package extract.hmodel;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import extract.auth.Login;
import extract.auth.LoginSer;
import extract.auth.NoAuth;
import extract.auth.TokenSer;
import extract.controller.Estimable;
import extract.controller.FormUtils;
import extract.controller.Search;
import extract.scafold.creator.Creator;
import extract.tool.HMaker;
import extract.tool.HPdf;
import extract.tool.OwnTools;
import freemarker.template.TemplateException;
import lombok.Data;

@Data
public class HCrud<T extends HElement<ID>, ID, R extends JpaRepository<T, ID>, Sh extends Search<T>, U extends FormUtils<T>>
        implements HModel<T, ID, Sh, U> {
    static int pageSize = 10;
    R repos;
    @Autowired
    EntityManager entityManager;
    @Autowired
    TokenSer tokenSer;

    @Autowired
    LoginSer loginSer;

    @Override
    public U getUtils(U map, ID idUser) throws Exception {
        OwnTools.onFieldWithList(map, (field) -> {
            Class<?> clazz = OwnTools.getClass(field);
            SearchObj response = this.find((searchMap) -> {
                searchMap.where(this.getTrue(searchMap.getCriteria()));
            }, clazz, -1);
            map.set(response.getData());
        });
        return map;
    }

    public byte[] toPdf(Sh search, Class<T> clazz, ID idUser) throws IllegalAccessException, Exception {
        search.verifPdf();
        HPdf hPdf = new HPdf();
        SearchObj searchMap = this.search(search, clazz, idUser);
        String content = this.getContentPdf(searchMap.getData(), search);
        return hPdf.toPdf(content, "liste.pdf");
    }

    public String getContentPdf(List<?> data, Sh search) throws TemplateException, IOException {
        HMaker pdfMaker = new HMaker("pdfListe.ftl");
        pdfMaker.put("titre", search.getPdf().getTitre());
        search.toPdf(pdfMaker, data);
        return pdfMaker.getContent();
    }

    @Override
    public SearchObj search(Sh search, Class<T> classes, ID idUser) throws Exception, IllegalAccessException {
        String key = search.getKeys();
        return this.find((searchMap) -> {
            String[] keys = this.separate(key);
            Predicate[] predicate = new Predicate[keys.length];
            for (int i = 0; i < predicate.length; i++) {
                predicate[i] = this.toPredicate(searchMap.getCriteria(), searchMap.getCritSearch().getQuery(),
                        searchMap.getCritSearch().getRoot(), classes, keys[i]);
            }
            try {
                searchMap.where(
                        searchMap.getCriteria().and(
                                searchMap.getCriteria().and(predicate),
                                this.otherCrit(searchMap.getCriteria(), searchMap.getCritSearch().getQuery(),
                                        searchMap.getCritSearch().getRoot(), search),
                                search.where(searchMap)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, classes, search.getPage());
    }

    public Predicate otherCrit(CriteriaBuilder criteria, CriteriaQuery<?> query, Root<?> root, Sh search)
            throws IllegalArgumentException, IllegalAccessException {
        List<Field> fields = fieldsNoTransient(search.getClass());
        List<Predicate> pred = this.extract(criteria, root, fields, search);
        if (pred.size() == 0)
            return this.getTrue(criteria);
        return criteria.and(pred.toArray(new Predicate[0]));
    }

    public List<Predicate> extract(CriteriaBuilder criteria, Root<?> root, List<Field> fields, Sh search)
            throws IllegalArgumentException, IllegalAccessException {
        List<Predicate> pred = new ArrayList<Predicate>();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(search);
            if (value == null || field.getName().equals("classes") || field.getName().equals("keys"))
                continue;
            if (HCrud.inHerit(field.getType())) {
                pred.add(criteria.equal(root.get(field.getName()), value));
            } else if (field.getName().endsWith(Creator.hmin)) {
                pred.add(this.getMinPred(criteria, root, field, value));
            } else if (field.getName().endsWith(Creator.hmax)) {
                pred.add(this.getMaxPred(criteria, root, field, value));
            }
        }
        return pred;
    }

    public Predicate getMaxPred(CriteriaBuilder criteria, Root<?> root, Field field, Object value) {
        String col = field.getName().replace(Creator.hmax, "");
        try {
            Double val = Double.parseDouble(value.toString());
            return criteria.lessThanOrEqualTo(root.get(col), val);
        } catch (Exception e) {
            Date date = Date.valueOf(value.toString());
            return criteria.lessThanOrEqualTo(root.get(col), date);
        }
    }

    public Predicate getMinPred(CriteriaBuilder criteria, Root<?> root, Field field, Object value) {
        String col = field.getName().replace(Creator.hmin, "");
        try {
            Double val = Double.parseDouble(value.toString());
            return criteria.greaterThanOrEqualTo(root.get(col), val);
        } catch (Exception e) {
            Date date = Date.valueOf(value.toString());
            return criteria.greaterThanOrEqualTo(root.get(col), date);
        }
    }

    public Predicate toPredicate(CriteriaBuilder criteria, CriteriaQuery<?> query, Root<?> root, Class<?> classes,
            String key) {
        try {
            double value = Double.parseDouble(key);
            return this.toPredicate(criteria, query, root, classes, value);
        } catch (NumberFormatException e) {
            try {
                Date date = Date.valueOf(key);
                return this.toPredicate(criteria, query, root, classes, date);
            } catch (Exception ex) {
                return this.toPredicateStr(criteria, query, root, classes, key);
            }
        }
    }

    public Predicate getTrue(CriteriaBuilder criteria) {
        return criteria.isTrue(criteria.literal(Boolean.TRUE));
    }

    public Predicate toPredicateStr(CriteriaBuilder criteria, CriteriaQuery<?> query, Root<?> root, Class<?> classes,
            String key) {
        List<String> fields = this.filterFields(classes, String.class);
        if (fields.size() == 0)
            return this.getTrue(criteria);
        Predicate[] predicates = new Predicate[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            predicates[i] = this.toPredicate(criteria, root, fields.get(i), key);
        }
        return criteria.or(predicates);
    }

    public Predicate toPredicate(CriteriaBuilder criteria, Root<?> root, String field, String key) {
        String[] keys = field.split("\\.");
        Path<String> nicknames = root.get(keys[0]);
        for (int i = 1; i < keys.length; i++) {
            nicknames = nicknames.get(keys[i]);
        }
        return criteria.like(criteria.lower(nicknames), this.like(key));
    }

    public Predicate toPredicate(CriteriaBuilder criteria, CriteriaQuery<?> query, Root<?> root, Class<?> classes,
            Object key) {
        List<String> fields = this.filterFields(classes, Double.class, Integer.class, Float.class, Date.class);
        if (fields.size() == 0)
            return this.getTrue(criteria);
        Predicate[] predicates = new Predicate[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            predicates[i] = this.toPredicate(criteria, root, fields.get(i), key);
        }
        return criteria.or(predicates);
    }

    public Predicate toPredicate(CriteriaBuilder criteria, Root<?> root, String field, Object key) {
        String[] keys = field.split("\\.");
        Path<String> nicknames = root.get(keys[0]);
        for (int i = 1; i < keys.length; i++) {
            nicknames = nicknames.get(keys[i]);
        }
        return criteria.equal((nicknames), key);
    }

    public static boolean compare(Class<?> clazz1, Class<?> clazz2) {
        return clazz1.getName().equals(clazz2.getName());
    }

    public static boolean inHerit(Class<?> cls) {
        if (HCrud.compare(cls, HElement.class)) {
            return true;
        }
        if (cls.getSuperclass() != null) {
            return inHerit(cls.getSuperclass());
        } else {
            return false;
        }
    }

    public boolean skypes(Field field, Class<?>... skypes) {
        for (Class<?> cls : skypes) {
            if (field.getType().equals(cls)) {
                return true;
            }
        }
        return false;
    }

    public void fieldsString(List<String> answer, List<Field> fields, String pre, Class<?>... skypes) {
        for (Field field : fields) {
            if (skypes(field, skypes)) {
                answer.add(pre + field.getName());
            } else if (!field.getType().isPrimitive() && inHerit(field.getType())) {
                List<Field> nFields = fieldsNoTransient(field.getType());
                fieldsString(answer, nFields, pre + field.getName() + ".", skypes);
            }
        }
    }

    public static boolean skype(Field field) {
        boolean verif = field.getAnnotation(Transient.class) == null;
        verif &= field.getAnnotation(OneToMany.class) == null;
        verif &= field.getAnnotation(ManyToMany.class) == null;
        return verif;
    }

    public static List<Field> fieldsNoTransient(Class<?> cls) {
        List<Field> fields = new ArrayList<Field>();
        getFields(cls, fields);
        List<Field> answer = new ArrayList<Field>();
        for (int i = 0; i < fields.size(); i++) {
            if (skype(fields.get(i))) {
                answer.add(fields.get(i));
            }
        }
        return answer;
    }

    public static void getFields(Class<?> cls, List<Field> result) {
        Field[] fields = cls.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
        }
        result.addAll(Arrays.asList(fields));
        if (cls.getSuperclass() != null)
            getFields(cls.getSuperclass(), result);
    }

    public String like(String str) {
        return "%" + str.toLowerCase() + "%";
    }

    public List<String> filterFields(Class<?> classes, Class<?>... skypes) {
        List<Field> fields = fieldsNoTransient(classes);
        List<String> names = new ArrayList<>();
        fieldsString(names, fields, "", skypes);
        return names;
    }

    public String[] separate(String keys) {
        return keys.split("\\ ");
    }

    public Login getLogin(Integer id) throws NoAuth {
        Optional<Login> res = null;
        if ((res = this.getLoginSer().getLoginRepos().findById(id)).isPresent())
            return res.get();
        throw new NoAuth();
    }

    @Override
    public List<T> create(List<T> obj, ID idUser) throws Exception {
        this.getRepos().saveAll(obj);
        return obj;
    }

    @Override
    public T create(T obj, ID idUser) throws Exception {
        obj.verif();
        if (obj instanceof Estimable) {
            ((Estimable) obj).estimer();
        }
        return this.getRepos().save(obj);
    }

    @Override
    public List<T> getAll(ID idUser) throws Exception {
        return this.getRepos().findAll();
    }

    @Override
    public T update(T obj, ID idUser) throws Exception {
        return this.getRepos().save(obj);
    }

    @Override
    public void delete(ID obj, ID idUser) throws Exception {
        this.getRepos().deleteById(obj);
    }

    @Override
    public T findById(ID obj, ID idUser) throws Exception {
        return this.getRepos().findById(obj).get();
    }

    public void reload(T obj, ID idUser) throws Exception {
        this.reload(obj.getId(), idUser);
    }

    public void actBeforeReload(T obj) {

    }

    public void reload(ID id, ID idUser) throws Exception {
        T obj = this.findById(id, idUser);
        this.actBeforeReload(obj);
        this.create(obj, idUser);
    }

    public SearchObj find(Consumer<SearchObj> where, Class<?> classes, int page) {
        SearchObj searchObj = new SearchObj(this.getEntityManager(), classes);
        searchObj.setPage(page);
        searchObj.setPageSize(HCrud.pageSize);
        where.accept(searchObj);
        searchObj.init();
        return searchObj;
    }

}
