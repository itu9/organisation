package extract.hmodel;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class HCriteria {
    CriteriaQuery<?> query;
    Root root;

    public HCriteria(CriteriaBuilder builder,Class<?> created ,Class<?> from) {
        this.setQuery(builder.createQuery(created));
        this.setRoot(this.getQuery().from(from));
    }

    public CriteriaQuery<?> getQuery() {
        return query;
    }

    public void setQuery(CriteriaQuery<?> query) {
        this.query = query;
    }

    public Root getRoot() {
        return root;
    }

    public void setRoot(Root root) {
        this.root = root;
    }

}
