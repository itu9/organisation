package extract.hmodel;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;

/**
 * Objet pour regler le double root count
 */
public class SearchObj {
    EntityManager manager;
    CriteriaBuilder criteria;
    Class<?> classes;
    HCriteria critSearch;
    HCriteria critCount;
    TypedQuery<?> typedQuery;
    int page = -1;
    int pageSize = 0;
    List<Object> data;
    long size;

    public SearchObj(EntityManager manager, Class<?> classes) {
        this.setManager(manager);
        this.setCriteria(this.getManager().getCriteriaBuilder());
        this.setClasses(classes);
        this.setCritSearch(new HCriteria(this.getCriteria(), classes, classes));
        this.setCritCount(new HCriteria(this.getCriteria(), Long.class, classes));
    }

    public void limit() {
        if (this.getPage() >= 0) {
            int firstPage = (this.getPage() - 1) * this.getPageSize();
            this.getTypedQuery().setFirstResult(firstPage);
            this.getTypedQuery().setMaxResults(this.getPageSize());
        }
    }

    public CriteriaQuery<Long> getCountQuery() {
        CriteriaQuery<Long> crit = ((CriteriaQuery<Long>)this.getCritCount().getQuery());
        return crit;
    }

    public void where(Predicate predicate) {
        this.getCritSearch().getQuery().where(predicate);
        this.getCountQuery().select(this.getCriteria().count(this.getCritCount().getRoot()));
        this.getCountQuery().where(predicate);
    }

    public void init() {
        this.initData();
        this.setSize(this.getManager().createQuery(this.getCountQuery()).getSingleResult());
    }

    public void initData() {
        this.limit();
        this.setData((List<Object>)this.getTypedQuery().getResultList());
    }

    public TypedQuery<?> getTypedQuery() {
        if (this.typedQuery == null) {
            this.setTypedQuery(this.getManager().createQuery(this.getCritSearch().getQuery()));
        }
        return this.typedQuery;
    }

    public CriteriaBuilder getCriteria() {
        return criteria;
    }

    public void setCriteria(CriteriaBuilder criteria) {
        this.criteria = criteria;
    }

    public Class<?> getClasses() {
        return classes;
    }

    public void setClasses(Class<?> classes) {
        this.classes = classes;
    }

    public HCriteria getCritSearch() {
        return critSearch;
    }

    public void setCritSearch(HCriteria critSearch) {
        this.critSearch = critSearch;
    }

    public HCriteria getCritCount() {
        return critCount;
    }

    public void setCritCount(HCriteria critCount) {
        this.critCount = critCount;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

    public void setTypedQuery(TypedQuery<?> typedQuery) {
        this.typedQuery = typedQuery;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

}
