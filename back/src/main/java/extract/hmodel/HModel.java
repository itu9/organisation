package extract.hmodel;

import java.util.List;

import extract.controller.FormUtils;
import extract.controller.Search;

public interface HModel<T extends HElement<?>, Id, Sh extends Search<T>, U extends FormUtils<T>> {
    public byte[] toPdf(Sh search, Class<T> clazz, Id idUser) throws IllegalAccessException, Exception;

    public U getUtils(U map, Id idUser) throws Exception;

    public T create(T obj, Id idUser) throws Exception;

    public List<T> create(List<T> obj, Id idUser) throws Exception;

    public SearchObj search(Sh keys, Class<T> classes, Id idUser) throws Exception;

    public List<T> getAll(Id idUser) throws Exception;

    public T update(T obj, Id idUser) throws Exception;

    public void delete(Id obj, Id idUser) throws Exception;

    public T findById(Id id, Id idUser) throws Exception;
}
