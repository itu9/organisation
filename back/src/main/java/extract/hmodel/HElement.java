package extract.hmodel;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public class HElement<ID> {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    ID id;
    @Getter
    @Setter
    @Transient
    int sum = 0;

    public void prepareSend() throws Exception {

    }

    public void verif() throws Exception {

    }

    public String gethValue() {
        return "";
    }

    public String pdfContent() {
        return "";
    }
}
