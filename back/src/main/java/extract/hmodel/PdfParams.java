package extract.hmodel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import extract.scafold.creator.front.param.InheritParam;
import extract.scafold.creator.front.param.ListeParam;
import extract.scafold.creator.front.param.NotMatched;
import extract.tool.OwnTools;

public class PdfParams extends ListeParam {
    List<PdfField> fields = new ArrayList<>();
    String titre;

    public List<PdfField> getFields() {
        return fields;
    }

    public String getInHeritHead(Field field) {
        try {
            InheritParam param = this.match(field.getName());
            return String.format("<th>%s</th>", param.getTitle());
        } catch (NotMatched e) {
            return "";
        }
    }

    public PdfField matchField(Field field) {
        for (PdfField pdfField : fields) {
            if (pdfField.getField().equals(field.getName())) {
                return pdfField;
            }
        }
        return null;
    }

    public String getSimple(Field field, Object object) throws IllegalArgumentException, IllegalAccessException {
        PdfField param = this.matchField(field);
        field.setAccessible(true);
        return param != null ? String.format("<td>%s</td>", String.valueOf(field.get(object))) : "";
    }

    public String getSimpleHead(Field field) {
        PdfField param = this.matchField(field);
        return param != null ? String.format("<th>%s</th>", param.getTitre()) : "";
    }

    public void setFields(List<PdfField> fields) {
        this.fields = fields;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getInHerit(Field field, Object data) throws IllegalArgumentException, IllegalAccessException {
        try {
            InheritParam param = this.match(field.getName());
            Object[] datas = { field.get(data) };
            String[] result = { "" };
            OwnTools.onField(field.getType(), (nField) -> {
                if (nField.getName().equals(param.getPureLabel())) {
                    try {
                        result[0] = String.format("<td>%s</td>", String.valueOf(nField.get(datas[0])));
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                    }
                }
            });
            return result[0];
        } catch (NotMatched e) {
            return "";
        }
    }

}
