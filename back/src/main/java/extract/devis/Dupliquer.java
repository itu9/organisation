package extract.devis;

import lombok.Data;

import java.sql.Date;
import java.sql.Time;

@Data
public class Dupliquer {
    Integer id;
    Date date;
    Time heur;
}
