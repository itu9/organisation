package extract.devis;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/devis")
@RestController 
public class DevisController extends CrudController<Devis, Integer, DevisRepository, DevisService,DevisSearch,DevisUtils> {
    @Autowired 
    public DevisController(DevisService service) {
        super(service);
    }

    @GetMapping("/{id}/spectacle")
    public ResponseEntity<?> toSpectacle(@PathVariable("id") Integer id, @RequestParam(name = "idUser", required = false) Integer idUser) {
        try {
            this.getService().addToSpectacle(id, idUser);
            return this.returnSuccess("ok");
        } catch (Exception e) {
            return this.returnError(e);
        }
    }

    @PostMapping("/bis")
    public ResponseEntity<?> dupliquer(@RequestBody Dupliquer duplicParam, @RequestParam(name = "idUser", required = false) Integer idUser) {
        try {
            this.getService().dupliquer(duplicParam,idUser);
            return this.returnSuccess("ok");
        } catch (Exception e) {
            return this.returnError(e);
        }
    }
}