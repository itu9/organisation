package extract.devis;

import extract.lieu.Lieu;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisUtils extends FormUtils<Devis> {
    List<Lieu> lieuid = new ArrayList<Lieu>();
    public DevisUtils() {
        super(Devis.class);
    }
};