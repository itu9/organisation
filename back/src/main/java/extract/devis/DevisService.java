package extract.devis;

import extract.billet.Devisbillet;
import extract.billet.DevisbilletService;
import extract.controller.LoginCnt;
import extract.devis.artiste.Devisartiste;
import extract.devis.artiste.DevisartisteService;
import extract.devis.autre.Devisautre;
import extract.devis.autre.DevisautreService;
import extract.devis.fixe.Devisfixe;
import extract.devis.fixe.DevisfixeService;
import extract.hmodel.SearchObj;
import extract.percent.Percent;
import extract.percent.PercentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class DevisService extends HCrud<Devis, Integer, DevisRepository, DevisSearch, DevisUtils> {
    @Autowired
    DevisartisteService devisartisteService;

    @Autowired
    DevisfixeService devisfixeService;

    @Autowired
    DevisautreService devisautreService;

    @Autowired
    DevisbilletService devisbilletService;

    @Autowired
    PercentService percentService;

    @Autowired
    @Override
    public void setRepos(DevisRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public Devis findById(Integer obj, Integer idUser) throws Exception {
        Devis devis = super.findById(obj, idUser);
        List<Percent> taxes = this.percentService.getAll(idUser);
        if (taxes == null || taxes.size() == 0) {
            throw new Exception("Ajouter une taxe dans la base");
        }
        devis.setDevisartistes(this.devisartisteService.getByDevis(devis.getId(), idUser));
        devis.setDevisfixes(this.devisfixeService.getByDevis(devis, idUser));
        devis.setDevisautres(this.devisautreService.getByDevis(devis, idUser));
        devis.setDevisbillets(this.devisbilletService.getByDevis(devis,idUser));
        devis.eval(taxes.get(0));
        return devis;
    }

    public void addToSpectacle(Integer devisid,Integer idUser) throws Exception {
        Devis devis = super.findById(devisid,idUser);
        devis.setEtat(Devis.SPECTACLE);
        this.update(devis,idUser);
    }

    @Override
    public byte[] toPdf(DevisSearch search, Class<Devis> clazz, Integer idUser) throws IllegalAccessException, Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.toPdf(search, clazz, idUser);
    }

    @Override
    public SearchObj search(DevisSearch search, Class<Devis> classes, Integer idUser) throws Exception, IllegalAccessException {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.search(search, classes, idUser);
    }

    @Override
    public List<Devis> create(List<Devis> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.create(obj, idUser);
    }

    @Override
    public Devis create(Devis obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.create(obj, idUser);
    }

    @Override
    public Devis update(Devis obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        super.delete(obj, idUser);
    }

    @Transactional(rollbackFor = {Exception.class})
    public void dupliquer(Dupliquer duplicParam,Integer idUser) throws Exception {
        Devis devis = super.findById(duplicParam.getId(),idUser);
        List <Devisartiste> devisartistes = this.devisartisteService.getByDevis(devis.getId(), idUser);
        List <Devisfixe> devisfixes = this.devisfixeService.getByDevis(devis, idUser);
        List<Devisautre> devisautres = this.devisautreService.getByDevis(devis, idUser);
        List<Devisbillet> devisbillets = this.devisbilletService.getByDevis(devis,idUser);
        devis = devis.generateBis(duplicParam);
        this.create(devis, idUser);
        this.loadArtiste(devisartistes,devis,idUser);
        this.loadFixe(devisfixes,devis,idUser);
        this.loadAutres(devisautres,devis,idUser);
        this.loadBillet(devisbillets,devis,idUser);

    }
    public void loadBillet(List<Devisbillet> devisbillets,Devis devis,Integer idUser) throws Exception {
        List<Devisbillet> nDevis = new ArrayList<>();
        for (Devisbillet devisbillet : devisbillets) {
            nDevis.add(devisbillet.generate(devis));
        }
        this.devisbilletService.create(nDevis,idUser);
    }

    public void loadAutres(List<Devisautre> devisautres,Devis devis,Integer idUser) throws Exception {
        List<Devisautre> nDevis = new ArrayList<>();
        for (Devisautre devisautre : devisautres) {
            nDevis.add(devisautre.generate(devis));
        }
        this.devisautreService.create(nDevis,idUser);
    }

    public void loadFixe(List<Devisfixe> devisfixes,Devis devis,Integer idUser) throws Exception {
        List<Devisfixe> nDevis = new ArrayList<>();
        for (Devisfixe devisfixe : devisfixes) {
            nDevis.add(devisfixe.generate(devis));
        }
        this.devisfixeService.create(nDevis,idUser);
    }

    public void loadArtiste(List<Devisartiste> devisartistes,Devis devis,Integer idUser) throws Exception {
        List<Devisartiste> nDevis = new ArrayList<>();
        for (Devisartiste devisartiste : devisartistes) {
            nDevis.add(devisartiste.generate(devis));
        }
        this.devisartisteService.create(nDevis,idUser);
    }
}
