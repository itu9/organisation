package extract.devis;

import javax.persistence.Entity;

import extract.artiste.Artiste;
import extract.autre.Autredepense;
import extract.billet.Devisbillet;
import extract.depensefixe.Depensefixe;
import extract.devis.artiste.Devisartiste;
import extract.devis.autre.Devisautre;
import extract.devis.fixe.Devisfixe;
import extract.usualexception.NegativePriceException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

import extract.lieu.Lieu;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Devis extends DevisMere {
    public static int DEVIS = 0;
    public static int SPECTACLE = 25;

    public Devis generateBis(Dupliquer duplicParam) {
        Devis devis = new Devis();
        devis.setTitre(this.getTitre()+" Bis");
        devis.setLieuid(this.getLieuid());
        devis.setPrixlieu(this.getPrixlieu());
        devis.setDate(duplicParam.getDate());
        devis.setHour(duplicParam.getHeur());
        devis.setEtat(this.getEtat());
        return devis;
    }
}