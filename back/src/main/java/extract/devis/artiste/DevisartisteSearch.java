package extract.devis.artiste;

import extract.artiste.Artiste;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisartisteSearch extends Search<Devisartiste> {
    Artiste artisteid;Devis devisid;Double dureehmin;Double dureehmax;
    public DevisartisteSearch() {
        super(Devisartiste.class);
    }
};