package extract.devis.artiste;


import extract.devis.Devis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DevisartisteRepository extends JpaRepository<Devisartiste,Integer> {

    public List<Devisartiste> findByDevisid(Devis devis);
}
