package extract.devis.artiste;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/devis/artiste")
@RestController 
public class DevisartisteController extends CrudController<Devisartiste, Integer, DevisartisteRepository, DevisartisteService,DevisartisteSearch,DevisartisteUtils> {
    @Autowired 
    public DevisartisteController(DevisartisteService service) {
        super(service);
    }
}