package extract.devis.artiste;

import extract.artiste.Artiste;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisartisteUtils extends FormUtils<Devisartiste> {
    List<Artiste> artisteid = new ArrayList<Artiste>();List<Devis> devisid = new ArrayList<Devis>();
    public DevisartisteUtils() {
        super(Devisartiste.class);
    }
};