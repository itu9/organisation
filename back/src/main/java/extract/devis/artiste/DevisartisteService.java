package extract.devis.artiste;

import extract.controller.LoginCnt;
import extract.devis.Devis;
import extract.hmodel.SearchObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service
public class DevisartisteService extends HCrud<Devisartiste, Integer, DevisartisteRepository, DevisartisteSearch, DevisartisteUtils> {
    @Autowired
    @Override
    public void setRepos(DevisartisteRepository repos) {
        super.setRepos(repos);
    }

    public List<Devisartiste> getByDevis(Integer devisid, Integer idUser){
        Devis devis = new Devis();
        devis.setId(devisid);
        return this.getRepos().findByDevisid(devis);
    }

    @Override
    public byte[] toPdf(DevisartisteSearch search, Class<Devisartiste> clazz, Integer idUser) throws IllegalAccessException, Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.toPdf(search, clazz, idUser);
    }

    @Override
    public SearchObj search(DevisartisteSearch search, Class<Devisartiste> classes, Integer idUser) throws Exception, IllegalAccessException {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.search(search, classes, idUser);
    }

    @Override
    public List<Devisartiste> create(List<Devisartiste> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.create(obj, idUser);
    }

    @Override
    public Devisartiste create(Devisartiste obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        super.create(obj, idUser);
        return null;
    }

    @Override
    public Devisartiste update(Devisartiste obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        super.delete(obj, idUser);
    }
}
