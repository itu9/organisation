package extract.devis.artiste;

import javax.persistence.*;

import extract.spectacle.Depense;
import extract.usualexception.NegativePriceException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import extract.artiste.Artiste;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.devis.Devis;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Devisartiste extends HElement<Integer> implements Depense {
    @OneToOne
    @JoinColumn(name = "artisteid", nullable = false)
    Artiste artisteid;
    @OneToOne
    @JoinColumn(name = "devisid", nullable = false)
    Devis devisid;
    Double duree;
    @Transient
    Double val;

    public void eval() {
        double prix = this.getArtisteid().getTarif()*this.getDuree() / this.getArtisteid().getDuree();
        this.setVal(prix);
    }

    @Override
    public void verif() throws Exception {
        if (this.getDuree() == null || this.getDuree() < 0.)
            throw new NegativePriceException();
    }

    @Override
    public String getLabel() {
        if (this.getArtisteid() == null || this.getSum() >= 25) return "";
        return this.getArtisteid().getNom();
    }

    @Override
    public Double getValue() {
        if (this.getArtisteid() == null || this.getSum() >= 25) return 0.;
//        this.eval();
        return this.getVal();
    }

    public Devisartiste generate(Devis devis) {
        Devisartiste devisartiste = new Devisartiste();
        devisartiste.setDuree(this.getDuree());
        devisartiste.setArtisteid(this.getArtisteid());
        devisartiste.setDevisid(devis);
        return devisartiste;
    }
}