package extract.devis;

import extract.artiste.Artiste;
import extract.autre.Autredepense;
import extract.billet.Devisbillet;
import extract.depensefixe.Depensefixe;
import extract.devis.artiste.Devisartiste;
import extract.devis.autre.Devisautre;
import extract.devis.fixe.Devisfixe;
import extract.hmodel.HElement;
import extract.lieu.Lieu;
import extract.percent.Percent;
import extract.usualexception.NegativePriceException;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public class DevisMere  extends HElement<Integer> {
    String titre;
    @OneToOne
    @JoinColumn(name = "lieuid", nullable = false)
    Lieu lieuid;
    Double prixlieu = 0.;
    @Transient
    Double total = 0.;
    @Transient
    Double recette = 0.;
    @Transient
    Double benefit = 0.;
    @Transient
    Double realrecette = 0.;
    @Transient
    Double benefbrut = 0.;
    @Transient
    Double taxe = 0.;
    @Transient
    Double benefnet= 0.;
    @Transient
    Double taxeprov = 0.;
    @Transient
    Double benefprovnet= 0.;

    Date date;
    Time hour;
    Integer etat;

    @Transient
    List<Devisartiste> devisartistes = new ArrayList<>();

    @Transient
    List<Devisautre> devisautres = new ArrayList<>();

    @Transient
    List<Devisfixe> devisfixes = new ArrayList<>();

    @Transient
    List<Devisbillet> devisbillets = new ArrayList<>();



    @Override
    public void verif() throws Exception {
        if (this.getPrixlieu() == null || this.getPrixlieu() < 0.)
            throw new NegativePriceException();
    }

    public void eval(Percent taxe) {
        Devisautre autre = this.devisautresTotal();
        Devisartiste devisartiste = this.devisartisteTotal();
        Devisfixe devisfixe = this.devisfixeTotal();
        double cout = this.getPrixlieu() + autre.getPrix() + devisartiste.getVal() + devisfixe.getVal();
        this.setTotal(cout);
        this.setRecette(this.recette());
        this.setBenefit(this.getRecette()-cout);
        this.setBenefbrut(this.getRealrecette()-cout);
        this.setTaxe(taxe.eval(this.getBenefbrut()));
        this.setTaxeprov(taxe.eval(this.getBenefit()));
        this.setBenefnet(this.getBenefbrut()-this.getTaxe());
        this.setBenefprovnet(this.getBenefit()-this.getTaxeprov());
    }

    public void setDevisbillets(List<Devisbillet> devisbillets) {
        this.devisbillets = devisbillets;
        if (this.getDevisbillets() == null || this.getDevisbillets().size() == 0) return;
        for (Devisbillet dBillet : this.getDevisbillets())
            dBillet.setDevisid(null);
    }

    public Double recette() {
        if (this.getDevisbillets() == null || this.getDevisbillets().size() == 0) return 0.;
        Devisbillet devisbillet = new Devisbillet();
        devisbillet.setSum(25);
        devisbillet.loadTotal();
        double recette = 0.;
        double real = 0.;
        for (Devisbillet dBillet : this.getDevisbillets()) {
            recette += dBillet.evalPrix();
            real += dBillet.evalPrixReel();
        }
        devisbillet.setVal(recette);
        devisbillet.setRealRecette(real);
        this.setRealrecette(real);
        this.getDevisbillets().add(devisbillet);
        return recette;
    }

    public Devisfixe devisfixeTotal() {
        if (this.getDevisfixes().size() == 0) {
            Devisfixe devisfixe = new Devisfixe();
            devisfixe.setVal(0.);
            return devisfixe;
        }
        return this.getDevisfixes().get(this.getDevisfixes().size() - 1);
    }

    public Devisartiste devisartisteTotal() {
        if (this.getDevisartistes().size() == 0) {
            Devisartiste devisartiste = new Devisartiste();
            devisartiste.setVal(0.);
            return devisartiste;
        }
        return this.getDevisartistes().get(this.getDevisartistes().size() - 1);
    }

    public Devisautre devisautresTotal() {
        if (this.getDevisautres().size() == 0) {
            Devisautre devisautres = new Devisautre();
            devisautres.setPrix(0.);
            return devisautres;
        }
        return this.getDevisautres().get(this.getDevisautres().size() - 1);
    }

    public void setDevisautres(List<Devisautre> autres) {
        this.devisautres = autres;
        if (this.getDevisautres() == null || this.getDevisautres().size() == 0) return;
        for (Devisautre dev : this.getDevisautres()) {
            dev.setDevisid(null);
        }
        this.addSumOthers();
    }

    public void addSumOthers() {
        Devisautre dev = new Devisautre();
        Autredepense depense = new Autredepense();
        depense.setTitre("Total");
        dev.setSum(25);
        double sum = 0.;
        for (Devisautre d : this.getDevisautres())
            sum += d.getPrix();
        dev.setPrix(sum);
        dev.setAutreid(depense);
        this.getDevisautres().add(dev);
    }

    public void setDevisartistes(List<Devisartiste> devis) {
        this.devisartistes = devis;
        if (devis == null || devis.size() == 0) return;
        for (Devisartiste dev : this.getDevisartistes()) {
            dev.setDevisid(null);
            dev.eval();
        }
        this.addSumArtist();
    }

    public void addSumArtist() {
        Devisartiste newDevis = new Devisartiste();
        Artiste artiste = new Artiste();
        artiste.setNom("Total");
        double total = 0.;
        double duree = 0.;
        for (Devisartiste dev : this.getDevisartistes()) {
            total += dev.getVal();
            duree += dev.getDuree();
        }
        newDevis.setArtisteid(artiste);
        newDevis.setVal(total);
        newDevis.setDuree(duree);
        newDevis.setSum(25);
        this.getDevisartistes().add(newDevis);
    }

    public void setDevisfixes(List<Devisfixe> devis) {
        this.devisfixes = devis;
        if (devis == null || devis.size() == 0) return;
        for (Devisfixe dev : this.getDevisfixes()) {
            dev.setDevisid(null);
            dev.eval();
        }
        this.addSumFixes();
    }

    private void addSumFixes() {
        Devisfixe newDevis = new Devisfixe();
        Depensefixe artiste = new Depensefixe();
        artiste.setTitre("Total");
        double total = 0.;
        double duree = 0.;
        for (Devisfixe dev : this.getDevisfixes()) {
            total += dev.getVal();
            duree += dev.getDuree();
        }
        newDevis.setDepensefixeid(artiste);
        newDevis.setVal(total);
        newDevis.setDuree(duree);
        newDevis.setSum(25);
        this.getDevisfixes().add(newDevis);
    }
}
