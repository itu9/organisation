package extract.devis;


import org.springframework.data.jpa.repository.JpaRepository;
public interface DevisRepository extends JpaRepository<Devis,Integer> {

}
