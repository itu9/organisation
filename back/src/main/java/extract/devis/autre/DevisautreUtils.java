package extract.devis.autre;

import extract.autre.Autredepense;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisautreUtils extends FormUtils<Devisautre> {
    List<Autredepense> autreid = new ArrayList<Autredepense>();List<Devis> devisid = new ArrayList<Devis>();
    public DevisautreUtils() {
        super(Devisautre.class);
    }
};