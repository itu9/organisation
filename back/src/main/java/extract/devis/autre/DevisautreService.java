package extract.devis.autre;

import extract.auth.NoAuth;
import extract.controller.LoginCnt;
import extract.devis.Devis;
import extract.hmodel.SearchObj;
import extract.spectacle.Spectacle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class DevisautreService extends HCrud<Devisautre, Integer, DevisautreRepository,DevisautreSearch,DevisautreUtils> {
    @Autowired 
    @Override 
    public void setRepos(DevisautreRepository repos) {
        super.setRepos(repos);
    }

    public List<Devisautre> getByDevis(Devis devis,Integer idUser) throws NoAuth {
//        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return this.getRepos().findByDevisid(devis);
    }

    public List<Devisautre> getByDevis(Spectacle devis, Integer idUser) {
        Devis nDevis = new Devis();
        nDevis.setId(devis.getId());
        return this.getRepos().findByDevisid(nDevis);
    }

    @Override
    public byte[] toPdf(DevisautreSearch search, Class<Devisautre> clazz, Integer idUser) throws IllegalAccessException, Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.toPdf(search, clazz, idUser);
    }

    @Override
    public SearchObj search(DevisautreSearch search, Class<Devisautre> classes, Integer idUser) throws Exception, IllegalAccessException {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.search(search, classes, idUser);
    }

    @Override
    public List<Devisautre> create(List<Devisautre> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.create(obj, idUser);
    }

    @Override
    public Devisautre create(Devisautre obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.create(obj, idUser);
    }

    @Override
    public Devisautre update(Devisautre obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        super.delete(obj, idUser);
    }
}
