package extract.devis.autre;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/devis/autre")
@RestController 
public class DevisautreController extends CrudController<Devisautre, Integer, DevisautreRepository, DevisautreService,DevisautreSearch,DevisautreUtils> {
    @Autowired 
    public DevisautreController(DevisautreService service) {
        super(service);
    }
}