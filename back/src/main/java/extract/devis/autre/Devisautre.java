package extract.devis.autre;

import javax.persistence.*;

import extract.spectacle.Depense;
import extract.usualexception.NegativePriceException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import extract.autre.Autredepense;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.devis.Devis;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Devisautre extends HElement<Integer> implements Depense {
    @OneToOne
    @JoinColumn(name = "autreid", nullable = false)
    Autredepense autreid;
    @OneToOne
    @JoinColumn(name = "devisid", nullable = false)
    Devis devisid;
    Double prix;
    @Transient
    Double val;

    @Override
    public void verif() throws Exception {
        if (this.getPrix() == null || this.getPrix() < 0.)
            throw new NegativePriceException();
    }

    @Override
    public String getLabel() {
        if (this.getAutreid() == null || this.getSum() >= 25) return "";
        return this.getAutreid().getTitre();
    }

    @Override
    public Double getValue() {
        if (this.getAutreid() == null || this.getSum() >= 25) return 0.;
        return this.getPrix();
    }

    public Devisautre generate(Devis devis) {
        Devisautre devisautre = new Devisautre();
        devisautre.setPrix(this.getPrix());
        devisautre.setAutreid(this.getAutreid());
        devisautre.setDevisid(devis);
        return devisautre;
    }
}