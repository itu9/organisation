package extract.devis.autre;


import extract.devis.Devis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DevisautreRepository extends JpaRepository<Devisautre,Integer> {
    public List<Devisautre> findByDevisid(Devis devis);
}
