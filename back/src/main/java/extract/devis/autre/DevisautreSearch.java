package extract.devis.autre;

import extract.autre.Autredepense;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisautreSearch extends Search<Devisautre> {
    Autredepense autreid;Devis devisid;Double prixhmin;Double prixhmax;
    public DevisautreSearch() {
        super(Devisautre.class);
    }
};