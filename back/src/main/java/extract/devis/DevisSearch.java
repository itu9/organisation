package extract.devis;

import extract.lieu.Lieu;

import java.lang.Double;

import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data
@EqualsAndHashCode(callSuper = false)
public class DevisSearch extends Search<Devis> {
    Lieu lieuid;
    Double prixlieuhmin;
    Double prixlieuhmax;
    Double etathmin;
    Double etathmax;

    public DevisSearch() {
        super(Devis.class);
    }
};