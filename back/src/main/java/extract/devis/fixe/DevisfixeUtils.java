package extract.devis.fixe;

import extract.depensefixe.Depensefixe;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisfixeUtils extends FormUtils<Devisfixe> {
    List<Depensefixe> depensefixeid = new ArrayList<Depensefixe>();List<Devis> devisid = new ArrayList<Devis>();
    public DevisfixeUtils() {
        super(Devisfixe.class);
    }
};