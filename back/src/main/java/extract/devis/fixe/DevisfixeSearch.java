package extract.devis.fixe;

import extract.depensefixe.Depensefixe;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisfixeSearch extends Search<Devisfixe> {
    Depensefixe depensefixeid;Devis devisid;Double dureehmin;Double dureehmax;
    public DevisfixeSearch() {
        super(Devisfixe.class);
    }
};