package extract.devis.fixe;


import extract.devis.Devis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DevisfixeRepository extends JpaRepository<Devisfixe,Integer> {
    public List<Devisfixe> findByDevisid(Devis devis);
}
