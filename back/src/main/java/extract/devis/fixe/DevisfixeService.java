package extract.devis.fixe;

import extract.controller.LoginCnt;
import extract.devis.Devis;
import extract.devis.DevisRepository;
import extract.hmodel.SearchObj;
import extract.spectacle.Spectacle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class DevisfixeService extends HCrud<Devisfixe, Integer, DevisfixeRepository,DevisfixeSearch,DevisfixeUtils> {
    @Autowired
    DevisRepository devisRepository;

    @Autowired
    @Override
    public void setRepos(DevisfixeRepository repos) {
        super.setRepos(repos);
    }

    public List<Devisfixe> getByDevis(Devis devis,Integer idUser) {
        return this.getRepos().findByDevisid(devis);
    }

    public List<Devisfixe> getByDevis(Spectacle devis, Integer idUser) {
        Devis nDevis = new Devis();
        nDevis.setId(devis.getId());
        return this.getRepos().findByDevisid(nDevis);
    }

    @Override
    public SearchObj search(DevisfixeSearch search, Class<Devisfixe> classes, Integer idUser) throws Exception, IllegalAccessException {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.search(search, classes, idUser);
    }

    @Override
    public List<Devisfixe> create(List<Devisfixe> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.create(obj, idUser);
    }

    @Override
    public Devisfixe create(Devisfixe obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.create(obj, idUser);
    }

    @Override
    public Devisfixe update(Devisfixe obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.EMPLOYE);
        super.delete(obj, idUser);
    }
}
