package extract.devis.fixe;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/devis/fixe")
@RestController 
public class DevisfixeController extends CrudController<Devisfixe, Integer, DevisfixeRepository, DevisfixeService,DevisfixeSearch,DevisfixeUtils> {
    @Autowired 
    public DevisfixeController(DevisfixeService service) {
        super(service);
    }
}