package extract.devis.fixe;

import javax.persistence.*;

import extract.spectacle.Depense;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import extract.depensefixe.Depensefixe;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.devis.Devis;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Devisfixe extends HElement<Integer> implements Depense {
    @OneToOne
    @JoinColumn(name = "depensefixeid", nullable = false)
    Depensefixe depensefixeid;
    @OneToOne
    @JoinColumn(name = "devisid", nullable = false)
    Devis devisid;
    Double duree;
    @Transient
    Double val;
    public void eval() {
        double prix = this.getDepensefixeid().getTarif()*this.getDuree() / this.getDepensefixeid().getDuree();
        this.setVal(prix);
    }

    @Override
    public String getLabel() {
        if (this.getDepensefixeid() == null || this.getSum() >= 25) return "";
        return this.getDepensefixeid().getTitre();
    }

    @Override
    public Double getValue() {
        if (this.getDepensefixeid() == null || this.getSum() >= 25) return 0.;
//        this.eval();
        return this.getVal();
    }

    public Devisfixe generate(Devis devis) {
        Devisfixe devisfixe = new Devisfixe();
        devisfixe.setDevisid(devis);
        devisfixe.setDepensefixeid(this.getDepensefixeid());
        devisfixe.setDuree(this.getDuree());
        return devisfixe;
    }
}