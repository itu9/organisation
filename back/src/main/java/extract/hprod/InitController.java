package extract.hprod;

import extract.controller.Cnt;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/init")
@CrossOrigin("*")
@Data
@EqualsAndHashCode(callSuper = false)
public class InitController extends Cnt {
    @Autowired
    Initialization initialization;

    @GetMapping("")
    public ResponseEntity<?> initData() {
        try {
            this.getInitialization().initializeData();
            return this.returnSuccess("Les données sont réinitialisées.");
        } catch (Exception e) {
            return this.returnError(e);
        }
    }
}
