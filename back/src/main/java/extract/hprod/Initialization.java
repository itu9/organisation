package extract.hprod;

import extract.auth.Login;
import extract.auth.LoginRepository;
import extract.controller.LoginCnt;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Data
public class Initialization  {
    /**
     * reset data from database
     *
     * @throws Exception java.lang. exception
     */

    @Autowired
    LoginRepository loginSer;

    @Transactional(rollbackOn = {Exception.class})
    public void initializeData() throws Exception {
        this.getLoginSer().deleteAll();
        this.initLogin();
    }

    public void initLogin() {
        Login[] log =new Login[2];
        log[0] = new Login("admin@gmail.com","admin","Admin","Admin", LoginCnt.ADMIN,null);
        log[0].setId(0);
        log[1] = new Login("employe@gmail.com","employe","Employe","Employe", LoginCnt.EMPLOYE,null);
        log[1].setId(1);
        for (Login l:log) {
            System.out.println(l.getId());
            this.loginSer.save(l);
        }
    }

}
