package extract.autre;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class AutredepenseSearch extends Search<Autredepense> {
    
    public AutredepenseSearch() {
        super(Autredepense.class);
    }
};