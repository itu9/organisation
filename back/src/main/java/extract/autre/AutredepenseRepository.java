package extract.autre;


import org.springframework.data.jpa.repository.JpaRepository;
public interface AutredepenseRepository extends JpaRepository<Autredepense,Integer> {

}
