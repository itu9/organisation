package extract.autre;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/autre/depense")
@RestController
public class AutredepenseController extends CrudController<Autredepense, Integer, AutredepenseRepository, AutredepenseService, AutredepenseSearch, AutredepenseUtils> {
    @Autowired
    public AutredepenseController(AutredepenseService service) {
        super(service);
    }

}