package extract.autre;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class AutredepenseUtils extends FormUtils<Autredepense> {
    
    public AutredepenseUtils() {
        super(Autredepense.class);
    }
};