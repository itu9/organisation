package extract.autre;

import extract.controller.LoginCnt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service 
public class AutredepenseService extends HCrud<Autredepense, Integer, AutredepenseRepository,AutredepenseSearch,AutredepenseUtils> {
    @Autowired 
    @Override 
    public void setRepos(AutredepenseRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public List<Autredepense> create(List<Autredepense> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Autredepense create(Autredepense obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Autredepense update(Autredepense obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.update(obj, idUser);
    }

    @Override
    public void delete(Integer obj, Integer idUser) throws Exception {

        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        super.delete(obj, idUser);
    }
}
