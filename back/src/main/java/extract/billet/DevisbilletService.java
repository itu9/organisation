package extract.billet;

import extract.categories.lieu.Categorieslieu;
import extract.categories.lieu.CategorieslieuService;
import extract.devis.Devis;
import extract.spectacle.Spectacle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.hmodel.HCrud;

import java.util.List;


@Service
public class DevisbilletService extends HCrud<Devisbillet, Integer, DevisbilletRepository, DevisbilletSearch, DevisbilletUtils> {
    @Autowired
    CategorieslieuService categorieslieuService;
    @Autowired
    @Override
    public void setRepos(DevisbilletRepository repos) {
        super.setRepos(repos);
    }

    public List<Devisbillet> getByDevis(Devis devis,Integer idUser) throws Exception{
        return this.getRepos().findByDevisid(devis);
    }
    public List<Devisbillet>getByDevis(Spectacle devis, Integer idUser) {
        Devis nDevis = new Devis();
        nDevis.setId(devis.getId());
        return this.getRepos().findByDevisid(nDevis);
    }

    @Override
    public Devisbillet update(Devisbillet obj, Integer idUser) throws Exception {
        obj.setCategorieslieuid(this.categorieslieuService.findById(obj.getCategorieslieuid().getId(),idUser));
        obj.verif();
        return super.update(obj, idUser);
    }

    @Override
    public Devisbillet create(Devisbillet obj, Integer idUser) throws Exception {

        return super.create(obj, idUser);
    }
}
