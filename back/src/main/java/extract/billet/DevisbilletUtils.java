package extract.billet;

import extract.categories.lieu.Categorieslieu;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;
import java.util.List;
import java.util.ArrayList;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisbilletUtils extends FormUtils<Devisbillet> {
    List<Categorieslieu> categorieslieuid = new ArrayList<Categorieslieu>();List<Devis> devisid = new ArrayList<Devis>();
    public DevisbilletUtils() {
        super(Devisbillet.class);
    }
};