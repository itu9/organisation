package extract.billet;


import extract.devis.Devis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DevisbilletRepository extends JpaRepository<Devisbillet,Integer> {
    public List<Devisbillet> findByDevisid(Devis devis);
}
