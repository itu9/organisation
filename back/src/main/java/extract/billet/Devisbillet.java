package extract.billet;

import javax.persistence.*;

import extract.categories.Categories;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.hmodel.HElement;

import extract.categories.lieu.Categorieslieu;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import extract.devis.Devis;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Devisbillet extends HElement<Integer> {
    @OneToOne
    @JoinColumn(name = "categorieslieuid", nullable = false)
    Categorieslieu categorieslieuid;
    @OneToOne
    @JoinColumn(name = "devisid", nullable = false)
    Devis devisid;
    Double prix;

    Double place;

    @Transient
    Double val = 0.;

    @Transient
    Double realRecette;

    @Override
    public void verif() throws Exception {
        if (this.getPlace() > this.getCategorieslieuid().getNbplace())
            throw new Exception("PLace en exces");
    }

    public double evalPrix() {
        this.setVal(this.getPrix()*this.getCategorieslieuid().getNbplace());
        return this.getVal();
    }

    public double evalPrixReel() {
        this.setRealRecette(this.getPrix()*this.getPlace());
        return this.getRealRecette();
    }

    public void loadTotal() {
        Categorieslieu cat = new Categorieslieu();
        Categories categories = new Categories();
        categories.setTitre("Total");
        cat.setCategoriesid(categories);
        this.setCategorieslieuid(cat);
    }

    public Devisbillet generate(Devis devis) {
        Devisbillet devisbillet = new Devisbillet();
        devisbillet.setCategorieslieuid(this.getCategorieslieuid());
        devisbillet.setDevisid(devis);
        devisbillet.setPrix(this.getPrix());
        devisbillet.setPlace(0.);
        return devisbillet;
    }
}