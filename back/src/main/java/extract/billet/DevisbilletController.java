package extract.billet;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/devis/billet")
@RestController 
public class DevisbilletController extends CrudController<Devisbillet, Integer, DevisbilletRepository, DevisbilletService,DevisbilletSearch,DevisbilletUtils> {
    @Autowired 
    public DevisbilletController(DevisbilletService service) {
        super(service);
    }
}