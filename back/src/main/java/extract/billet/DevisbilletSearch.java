package extract.billet;

import extract.categories.lieu.Categorieslieu;import extract.devis.Devis;import java.lang.Double;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class DevisbilletSearch extends Search<Devisbillet> {
    Categorieslieu categorieslieuid;Devis devisid;Double prixhmin;Double prixhmax;
    public DevisbilletSearch() {
        super(Devisbillet.class);
    }
};