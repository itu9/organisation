package extract.auth;

import java.sql.Time;

import lombok.Data;

@Data
public class LoginParam {
    Login login;
    int etat;
    Time time;
}
