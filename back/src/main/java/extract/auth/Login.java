package extract.auth;

import extract.hmodel.SkypeInit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;


@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "client")
@SkypeInit
public class Login extends HPerson {

    public Login() {
    }

    public Login(String email, String pwd, String nom, String prenom, int acreditation, Date naissance) {
        super(email, pwd, nom, prenom, acreditation, naissance);
    }

    public Login(Integer id) {
        this.setId(id);
    }

}
