package extract.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import extract.hmodel.HElement;

import java.sql.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class HPerson extends HElement<Integer> {
    String email;

    @Column(name = "mdp")
    String pwd;

    String nom;

    String prenom;

    int acreditation;

    Date naissance;

    public void loadBeforeSend() {
        this.setPwd(null);
    }

}
