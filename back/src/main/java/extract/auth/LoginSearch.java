package extract.auth;

import java.sql.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data
@EqualsAndHashCode(callSuper = false)
public class LoginSearch extends Search<Login> {
    Integer acreditationhmin;
    Integer acreditationhmax;
    Date naissancehmin;
    Date naissancehmax;

    public LoginSearch() {
        super(Login.class);
    }
};