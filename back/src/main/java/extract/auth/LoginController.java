package extract.auth;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import extract.controller.CrudController;

@CrossOrigin("*")
@RequestMapping("/role")
@RestController 
public class LoginController extends CrudController<Login, Integer, LoginRepository, LoginService,LoginSearch,LoginUtils> {
    @Autowired 
    public LoginController(LoginService service) {
        super(service);
    }
}