package extract.auth;

import extract.controller.FormUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class LoginUtils extends FormUtils<Login> {
    public LoginUtils() {
        super(Login.class);
    }
};