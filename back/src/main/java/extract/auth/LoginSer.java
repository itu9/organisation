package extract.auth;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.controller.LoginCnt;
import lombok.Data;

@Data
@Service
public class LoginSer {
    @Autowired
    LoginRepos loginRepos;

    @Autowired
    TokenRepos tokenRepos;

    public Token login(LoginParam param) throws NoLogin, NoSuchAlgorithmException {
        List<Login> ans = this.getLoginRepos().getBy(param.getLogin().getEmail(), param.getLogin().getPwd());
        if (ans.size() > 0) {
            Login answer = ans.get(0);
            Token token = new Token(answer);
            this.getTokenRepos().save(token);
            token.loadBeforeSend();
            return token;
        }
        throw new NoLogin();
    }

    public Token login(LoginParam login, int etat) throws NoSuchAlgorithmException, NoLogin {
        Token token = this.login(login);
        if (token.getLogin().getAcreditation() != etat) {
            throw new NoLogin();
        }
        return token;
    }

    public Token loginClient(LoginParam login) throws NoSuchAlgorithmException, NoLogin {
        return this.login(login, LoginCnt.EMPLOYE);
    }

    public Token loginAdmin(LoginParam login) throws NoSuchAlgorithmException, NoLogin {
        return this.login(login, LoginCnt.ADMIN);
    }

    public void signUp(Login login) {
        this.getLoginRepos().save(login);
    }
}
