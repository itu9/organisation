package extract.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.controller.LoginCnt;
import extract.hmodel.HCrud;

@Service
public class LoginService extends HCrud<Login, Integer, LoginRepository, LoginSearch, LoginUtils> {
    @Autowired
    @Override
    public void setRepos(LoginRepository repos) {
        super.setRepos(repos);
    }

    @Override
    public Login update(Login obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.update(obj, idUser);
    }

    @Override
    public List<Login> create(List<Login> obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

    @Override
    public Login create(Login obj, Integer idUser) throws Exception {
        this.getTokenSer().askAccess(idUser, LoginCnt.ADMIN);
        return super.create(obj, idUser);
    }

}
