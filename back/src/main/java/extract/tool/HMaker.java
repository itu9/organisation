package extract.tool;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class HMaker {
    String model;
    Map<String, String> data = new HashMap<String, String>();
    static Configuration configuration;

    public HMaker(String model) {
        this.setModel(model);
    }

    public HMaker() {
    }

    static {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_31);
        configuration.setClassForTemplateLoading(HMaker.class, "/templates");
        HMaker.configuration = configuration;
    };

    public static Template getTemplate(String model)
            throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException {
        return HMaker.configuration.getTemplate(model);
    }

    public void clear() {
        this.setData(new HashMap<String, String>());
    }

    public void put(String key, String data) {
        this.getData().put(key, data);
    }

    public String changeContent(String model, Map<String, String> dataModel)
            throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException,
            TemplateException {
        this.setModel(model);
        this.setData(dataModel);
        return this.getContent();
    }

    public String changeContent(Map<String, String> dataModel)
            throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException,
            TemplateException {
        return this.changeContent(this.getModel(), dataModel);
    }

    public String changeContent(Template template, Map<String, String> dataModel)
            throws TemplateException, IOException {
        StringWriter stringWriter = new StringWriter();
        template.process(dataModel, stringWriter);
        return stringWriter.toString();
    }

    public String getContent() throws TemplateException, IOException {
        Template template = HMaker.getTemplate(this.getModel());
        return this.changeContent(template, this.getData());
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public static Configuration getConfiguration() {
        return configuration;
    }

    public static void setConfiguration(Configuration configuration) {
        HMaker.configuration = configuration;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

}
