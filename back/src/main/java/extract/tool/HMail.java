package extract.tool;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class HMail extends MimeMessageHelper {
    JavaMailSender sender;
    MimeMessage message;

    public HMail(JavaMailSender sender, String to, String subject, String text, MimeMessage message)
            throws MessagingException {
        super((message = sender.createMimeMessage()), true);
        this.setTo(to);
        this.setSubject(subject);
        this.setText(text, true);
        this.setSender(sender);
        this.setMessage(message);
    }

    public void send() {
        this.getSender().send(this.getMessage());
    }

}
