package extract.tool;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import extract.hmodel.HCrud;
import extract.scafold.Loader;

public class OwnTools {
    public static int min(double[] values) {
        int min = 0;
        for (int i = 1; i < values.length; i++) {
            min = (values[i] < values[min]) ? i : min;
        }
        return min;
    }

    public static String capitalize(String str) {
        String first = String.valueOf(str.charAt(0));
        return str.replaceFirst(first, first.toUpperCase());
    }

    public static String getSep(String packageName) {
        return packageName.equals("") ? "" : ".";
    }

    public static void find(ArrayList<Class<?>> result, File folder, String packageName, Loader loader,
            BiConsumer<Class<?>, ArrayList<Class<?>>> filter) {
        File[] file = folder.listFiles();
        for (File f : file) {
            if (f.isDirectory()) {
                OwnTools.find(result, f, packageName + OwnTools.getSep(packageName) + folder.getName(), loader,filter);
            } else {
                String path = packageName + OwnTools.getSep(packageName) + folder.getName()
                        + OwnTools.getSep(".") + f.getName().split("\\.")[0];
                try {
                    filter.accept(loader.findClass(path), result);
                } catch (ClassNotFoundException | LinkageError e) {
                    
                }
            }
        }
    }

    public static void extractClass(ArrayList<Class<?>> result, File folder, String packageName, Loader loader) {
        find(result, folder, packageName, loader, (clazz, res) -> {
            res.add(clazz);
        });
    }

    public static double reduce(double value, double percent) {
        return value - (value * percent / 100.);
    }

    public static int calcAge(Date date) {
        return Period.between(LocalDate.now(), date.toLocalDate()).getYears();
    }

    public static void onField(Object object, Consumer<Field> doField) {
        onField(object.getClass(), doField);
    }

    public static void onField(Class<?> object, Consumer<Field> doField) {
        List<Field> fields = new ArrayList<>();
        HCrud.getFields(object, fields);
        for (Field field : fields) {
            doField.accept(field);
        }
    }

    public static String toMonth(int mois) {
        String[] liste = {"Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre"};
        return liste[(mois-1) >= 0?mois-1 : 0];
    }

    public static void onFieldWithList(Class<?> object, Consumer<Field> doField) {
        OwnTools.onField(object, (field) -> {
            field.setAccessible(true);
            if (field.getType().equals(List.class)) {
                doField.accept(field);
            }
        });
    }

    public static Class<?> getClass(Field field) {
        field.setAccessible(true);
        ParameterizedType type = (ParameterizedType) field.getGenericType();
        Class<?> clazz = (Class<?>) type.getActualTypeArguments()[0];
        return clazz;
    }

    public static void onFieldWithList(Object object, Consumer<Field> doField) {
        onFieldWithList(object.getClass(), doField);
    }
}
