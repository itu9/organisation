package extract.tool;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class HWriter {
    public void write_data(String fic, Boolean fafaina, boolean newline, String... line) throws IOException {
        File file = new File(fic);
        this.existsFile(file);
        try {
            FileWriter writer = new FileWriter(file, !fafaina);
            BufferedWriter bw = new BufferedWriter(writer);
            boolean verif = this.intoFile(fic).length != 0;
            if (verif && !fafaina) {
                bw.newLine();
            }
            for (int i = 0; i < line.length; i++) {
                bw.write(line[i]);
                if (newline && line.length - 1 != i) {
                    bw.newLine();
                }
            }
            bw.close();
            writer.close();
        } catch (IOException e) {

        }
    }

    public void existsFile(File file) throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    public String[] intoFile(String fic) throws IOException {
        File file = new File(fic);
        this.existsFile(file);
        String[] answer = this.toString(intoFile(file));
        return answer;
    }

    public String[] toString(ArrayList<String> strings) {
        String[] answer = new String[strings.size()];
        for (int i = 0; i < answer.length; i++) {
            answer[i] = strings.get(i);
        }
        return answer;
    }

    public ArrayList<String> intoFile(File file) {
        ArrayList<String> str = new ArrayList<String>();
        try {
            BufferedReader read = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String line = read.readLine();
            while (line != null) {
                str.add(line);
                line = read.readLine();
            }
            read.close();
        } catch (IOException t) {
            t.printStackTrace();
        }
        return str;
    }

}
