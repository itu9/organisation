package extract.tool;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class HCsv<T> {
    CsvMapper mapper;
    InputStream stream;
    ObjectReader reader;

    public HCsv(CsvMapper mapper, Class<T> clazz) {
        this.setMapper(mapper);
        CsvSchema schema = this.getMapper().schemaFor(clazz).withHeader().withColumnReordering(true);
        this.setReader(this.getMapper().readerFor(clazz).with(schema));
    }

    public List<T> read(InputStream stream) throws IOException {
        MappingIterator<T> map = this.getReader().readValues(stream);
        return map.readAll();
    }

    public CsvMapper getMapper() {
        return mapper;
    }

    public void setMapper(CsvMapper mapper) {
        this.mapper = mapper;
    }

    public InputStream getStream() {
        return stream;
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }

    public ObjectReader getReader() {
        return reader;
    }

    public void setReader(ObjectReader reader) {
        this.reader = reader;
    }

}
