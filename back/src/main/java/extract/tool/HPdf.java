package extract.tool;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.itextpdf.html2pdf.HtmlConverter;

public class HPdf {
    public void convert(String htmlPath, String pdfPath) throws IOException {
        HtmlConverter.convertToPdf(new File(htmlPath), new File(pdfPath));
    }

    public byte[] toPdf(String html, String pdfPath) throws IOException {
        FileOutputStream pdf = new FileOutputStream(pdfPath);
        HtmlConverter.convertToPdf(html, pdf);
        pdf.close();
        return Files.readAllBytes(Paths.get(pdfPath));
    }

    // ByteArrayOutputStream baos = new ByteArrayOutputStream();
    // baos.writeTo(pdf);
    // pdf.close();
    // byte[] answer = baos.toByteArray();
    // baos.close();
    public static void main(String[] args) throws IOException {
        HPdf hPdf = new HPdf();
        hPdf.convert("./exemple.html", "exemple.pdf");
    }
}
