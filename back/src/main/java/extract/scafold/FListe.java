package extract.scafold;

import java.lang.reflect.Field;

import extract.tool.OwnTools;

public class FListe {
    String prefix;
    Field field;

    public FListe() {
    }

    public FListe(String prefix, Field field) {
        this.setPrefix(prefix);
        this.setField(field);
    }

    public String getName() {
        return this.getField().getName();
    }

    public String getTitle() {
        return OwnTools.capitalize(this.getName());
    }

    public String getSign() {
        return this.getPrefix() + "." + field.getName();
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

}
