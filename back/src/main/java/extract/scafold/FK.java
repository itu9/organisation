package extract.scafold;

public class FK {
    String name;
    String tableRef;

    public FK(String name, String tableRef) {
        this.setName(name);
        this.setTableRef(tableRef);
    }

    public FK() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTableRef() {
        return tableRef;
    }

    public void setTableRef(String tableRef) {
        this.tableRef = tableRef;
    }

    @Override
    public String toString() {
        return "FK [name=" + name + ", tableRef=" + tableRef + "]";
    }

}
