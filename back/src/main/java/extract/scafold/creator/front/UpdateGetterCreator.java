package extract.scafold.creator.front;

import java.util.HashMap;

import extract.scafold.creator.Creator;

public class UpdateGetterCreator extends FrontCreator {

    public UpdateGetterCreator(Creator creator) {
        super("updategetter.ftl", "UpdateGetter", creator);
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        map.put("className", this.getName());
    }

}
