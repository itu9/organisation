package extract.scafold.creator.front;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;

import extract.hmodel.HCrud;
import extract.scafold.creator.Creator;
import extract.scafold.creator.front.param.InheritParam;
import extract.scafold.creator.front.param.NotMatched;
import extract.tool.HMaker;
import extract.tool.OwnTools;
import freemarker.template.TemplateException;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class FicheCreator extends UpdateCreator {

    FicheGetterCreator fGetter;

    public FicheCreator(Creator creator) {
        super(creator);
        this.setFileName("fiche.ftl");
        this.setPrefixe("Info");
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        map.put("className", this.getName());
        map.put("urlSend", this.getBaseUrl()[0]);
        map.put("urlUpdate", this.getBaseUrl()[0] + "/update/");
        map.put("urlForm", this.getBaseUrl()[0] + "/form/");
        map.put("infoTitle", this.getParam().getInfoTitle());
        map.put("content", this.getFicheContent(new String[] {"id", "sum"}));
    }

    @Override
    public void create() throws Exception {
        String content = this.getInside();
        Creator.write_data(this.getPath(), true, false, content);
        FicheGetterCreator uGetterCreator = new FicheGetterCreator(this.getCreator());
        this.setFGetter(uGetterCreator);
        uGetterCreator.put("importClass", this.getName());
        uGetterCreator.create();
    }

    public String getFicheContent(String[] skype) {
        HMaker maker = new HMaker("desc.ftl");
        String[] content = { "" };
        OwnTools.onField(this.getCls(), (field) -> {
            if (HCrud.inHerit(field.getType())) {
                try {
                    this.onInHerit(content, maker, field);
                } catch (TemplateException | IOException e) {
                }
            } else if (!this.shouldSkip(field,skype)) {
                try {
                    this.onSimpleField(content, maker, field);
                } catch (TemplateException | IOException e) {
                }
            }
        });
        return content[0];
    }

    public void onInHerit(String[] content, HMaker desc, Field field) throws TemplateException, IOException {
        desc.clear();
        try {
            InheritParam param = this.getParam().match(field.getName());
            String title = param.getTitle();
            desc.put("label", title);
            desc.put("value", field.getName()+"." + param.getPureLabel());
            content[0] += desc.getContent();
        } catch (NotMatched e) {

        }

    }

    public void onSimpleField(String[] content, HMaker desc, Field field) throws TemplateException, IOException {
        desc.clear();
        try {
            String title = this.getParam().matchSimple(field.getName()).getTitle();
            desc.put("label", OwnTools.capitalize(title));
            desc.put("value", field.getName());
            content[0] += desc.getContent();
        } catch (NotMatched e) {
        }
    }

}
