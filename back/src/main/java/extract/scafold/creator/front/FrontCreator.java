package extract.scafold.creator.front;

import java.io.File;
import java.lang.reflect.Field;

import org.springframework.web.bind.annotation.RequestMapping;

import extract.controller.CrudController;
import extract.scafold.Loader;
import extract.scafold.creator.Creator;
import extract.scafold.creator.CreatorModel;
import extract.scafold.creator.back.ControllerCreator;

public abstract class FrontCreator extends CreatorModel {

    String construct = "";

    public FrontCreator(String fileName, String prefixe, Creator creator) {
        super(fileName, prefixe, creator);
    }

    @Override
    public String extJava(String file) {
        return file + ".jsx";
    }

    @Override
    public String getRealPath() {
        return super.getRealPath();
    }

    @Override
    public String getPath() {
        System.out.println(super.getPath());
        File file = new File(this.getRealPath());
        if (!file.exists()) {
            file.mkdirs();
        }
        return super.getPath();
    }

    public void createRef(String ref) {
        String refPattern = "this.%s = React.createRef(null);";
        this.setConstruct(this.getConstruct() + String.format(refPattern, ref));
    }
    public boolean shouldSkip(Field field, String[] content) {
        for (String c : content) {
            if (field.getName().equals(c)) {
                return true;
            }
        }
        return false;
    }


    public String getReference(Field field, String prefixe, boolean prepare) {
        String refmin = field.getName() + prefixe;
        this.createRef(refmin);
        String value = "this." + refmin;
        if (prepare) {
            this.prepare(refmin, value);
        }
        return value;
    }

    public void prepare(String refmin, String value) {
    }

    public String getUtilsUrl() throws ClassNotFoundException, ClassFormatError {
        String url = this.getBaseUrl()[0];
        return url + CrudController.UTILS;
    }

    public String[] getBaseUrl() throws ClassNotFoundException, ClassFormatError {
        String name = (new ControllerCreator(this.getCreator())).getName();
        String cls = this.getCreator().getPackageName() + "." + name;
        Class<?> clazz = (new Loader()).findClass(cls);
        RequestMapping mapping = clazz.getAnnotation(RequestMapping.class);
        return mapping != null ? mapping.value() : null;
    }

    public String getConstruct() {
        return construct;
    }

    public void setConstruct(String construct) {
        this.construct = construct;
    }
}
