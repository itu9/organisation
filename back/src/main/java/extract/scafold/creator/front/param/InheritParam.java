package extract.scafold.creator.front.param;

import extract.tool.HMaker;
import extract.tool.OwnTools;

public class InheritParam {
    /**
     * A definir avant l appel de {@link InheritParam#getSign()}
     */
    String prefixe = "this.state.utils";
    String field;
    String value;
    String label;
    String title;
    public static String dataName = "data";

    public InheritParam(String field, String value, String label) {
        this.setField(field);
        this.setValue(value);
        this.setLabel(label);
    }

    public InheritParam() {
    }

    public void config(HMaker inHerit) {
        String variable = this.getVariable();
        inHerit.put("variable", this.getPrefixe());
        inHerit.put("variableMap", variable);
        inHerit.put("value", this.getValue());
        inHerit.put("optLabel", this.getLabel());
        inHerit.put("data", InheritParam.dataName);
    }

    public String getVariable() {
        return (this.getField() == null) ? this.getPrefixe()
                : this.getPrefixe() + this.getSeparateur() + this.getField();
    }

    public String getSeparateur() {
        return this.getSeparateur(this.getField());
    }

    public String getSeparateur(String str) {
        return (str.compareTo("") == 0) ? "" : ".";
    }

    public String getTitleLable() {
        return OwnTools.capitalize(this.getTitle() != null ? this.getTitle() : this.getField());
    }

    /**
     * Definit d abord la valeur de {@link InheritParam#prefixe}
     * 
     * @return
     */
    public String getSign() {
        return String.format("<option value=\"{%s}\">{%s}</option>", this.getValue(), this.getLabel());
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getPureValue() {
        return value;
    }

    public String getListedValue() {
        return  InheritParam.dataName + "." + this.getField()+"."+this.getPureLabel();
    }

    public String getValue() {
        return InheritParam.dataName + "." + value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPureLabel() {
        return label;
    }

    public String getLabel() {
        return InheritParam.dataName + "." + label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPrefixe() {
        return prefixe;
    }

    public void setPrefixe(String prefixe) {
        this.prefixe = prefixe;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
