package extract.scafold.creator.front.param;

import java.util.ArrayList;
import java.util.List;

import extract.scafold.CntParam;

public class ListeParam extends CntParam {

    String listeTitle = "Titre de la liste";
    String infoTitle = "Fiche de details";
    String updateTitle = "Formulaire de modification";

    String formTitle = "Formulaire";
    List<InheritParam> inheritParams = new ArrayList<InheritParam>();
    List<InheritParam> simpleParams = new ArrayList<InheritParam>();

    public ListeParam(String tableName) {
        super(tableName, "");
    }

    public ListeParam() {
    }

    public String inside(String key) throws NotMatched {
        InheritParam inHerit = this.match(key);
        return inHerit.getSign();
    }

    public InheritParam find(String key,List<InheritParam> params) throws NotMatched {
        for (InheritParam inheritParam : params) {
            if (inheritParam.getField().equals(key)) {
                return inheritParam;
            }
        }
        throw new NotMatched();
    }

    public InheritParam match(String key) throws NotMatched {
        return this.find(key,this.getInheritParams());
    }

    public InheritParam matchSimple(String key) throws NotMatched {
        return this.find(key,this.getSimpleParams());
    }

    public List<InheritParam> getSimpleParams() {
        return simpleParams;
    }

    public void setSimpleParams(List<InheritParam> simpleParams) {
        this.simpleParams = simpleParams;
    }

    public List<InheritParam> getInheritParams() {
        return inheritParams;
    }

    public void setInheritParams(List<InheritParam> inheritParams) {
        this.inheritParams = inheritParams;
    }

    public String getListeTitle() {
        return listeTitle;
    }

    public void setListeTitle(String listeTitle) {
        this.listeTitle = listeTitle;
    }

    public String getUpdateTitle() {
        return updateTitle;
    }

    public void setUpdateTitle(String updateTitle) {
        this.updateTitle = updateTitle;
    }

    public String getInfoTitle() {
        return infoTitle;
    }

    public void setInfoTitle(String infoTitle) {
        this.infoTitle = infoTitle;
    }

    public String getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }
}
