package extract.scafold.creator.front;

import java.lang.reflect.Field;
import java.util.HashMap;

import extract.scafold.creator.Creator;
import extract.scafold.creator.front.param.InheritParam;

public class UpdateCreator extends FormCreator {

    UpdateGetterCreator getter;
    String variable = "this.state.oneValue";
    String loading = "";
    String loadingPattern = "this.%s.current.value = %s";

    public UpdateCreator(Creator creator) {
        super(creator);
        this.setFileName("update.ftl");
        this.setPrefixe("Update");
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        super.getContent(map);
        map.put("loading", this.getLoading());
        map.put("after", this.getBaseUrl()[0]);
        map.put("updateTitle",this.getParam().getUpdateTitle());
    }

//    @Override
//    public String getForm() {
//        return this.getForm(new String[0]);
//    }

    @Override
    public String getDefaultInHerit(Field field, InheritParam inheritParam) {
        String param = this.getParam(field) + "." + inheritParam.getPureValue();
        String result = String.format(this.getLoadingPattern(), field.getName(), param) + ";";
        this.setLoading(this.getLoading() + result);
        return "";
    }

    @Override
    public String getDefaultSimple(Field field) {
        String result = String.format(this.getLoadingPattern(), field.getName(), this.getParam(field)) + ";";
        this.setLoading(this.getLoading() + result);
        return "";
    }

    public String getParam(Field field) {
        return this.getVariable() + "." + field.getName();
    }

    @Override
    public void create() throws Exception {
        super.create();
        UpdateGetterCreator uGetterCreator = new UpdateGetterCreator(this.getCreator());
        uGetterCreator.put("importClass", this.getName());
        this.setGetter(uGetterCreator);
        uGetterCreator.create();
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getLoading() {
        return loading;
    }

    public void setLoading(String loading) {
        this.loading = loading;
    }

    public String getLoadingPattern() {
        return loadingPattern;
    }

    public void setLoadingPattern(String loadingPattern) {
        this.loadingPattern = loadingPattern;
    }

    public UpdateGetterCreator getGetter() {
        return getter;
    }

    public void setGetter(UpdateGetterCreator getter) {
        this.getter = getter;
    }
}
