package extract.scafold.creator.front;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;

import extract.hmodel.HCrud;
import extract.scafold.creator.Creator;
import extract.scafold.creator.front.param.InheritParam;
import extract.scafold.creator.front.param.ListeParam;
import extract.scafold.creator.front.param.NotMatched;
import extract.tool.HMaker;
import extract.tool.OwnTools;
import freemarker.template.TemplateException;

public class FormCreator extends FrontCreator {
    ListeParam param;
    String searchData = "";

    public FormCreator(Creator creator) {
        super("form.ftl", "Form", creator);
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        map.put("className", this.getName());
        map.put("data", this.getForm());
        map.put("urlSend", this.getBaseUrl()[0]);
        map.put("urlUtils", this.getUtilsUrl());
        map.put("preparedData", this.getSearchData());
        map.put("formTitle", this.getParam().getFormTitle());
        map.put("constructor", this.getConstruct());
    }

    @Override
    public void prepare(String label, String reference) {
        String pattern = (this.getSearchData().compareTo("") == 0 ? "" : ",") + "%s : %s";
        try {
            InheritParam param = this.getParam().match(label);
            reference = String.format("this.checkRefNull(%s,{%s:this.prepare(%s)},null)", reference,
                    param.getPureValue(), reference);
        } catch (NotMatched e) {
            reference = String.format("this.prepare(%s)", reference);
        }
        pattern = String.format(pattern, label, reference);
        this.setSearchData(this.getSearchData() + pattern);
    }

    public String getForm(String[] skype) {
        HMaker inHerit = new HMaker("inherit.ftl");
        HMaker simple = new HMaker("simplefield.ftl");
        String[] content = {""};
        OwnTools.onField(this.getCls(), (field) -> {
            for (String skp : skype) {
                if (field.getName().equals(skp))
                    return;
            }
            if (HCrud.inHerit(field.getType())) {
                try {
                    content[0] += this.onInHerit(field, inHerit);
                } catch (NotMatched | TemplateException | IOException e) {
                }
            } else {
                try {
                    content[0] += this.onSimpleField(field, simple);
                } catch (TemplateException | IOException e) {
                }
            }
        });
        return content[0];
    }

    public String getForm() {
        return this.getForm(new String[]{"id", "sum"});
    }

    public String onSimpleField(Field field, HMaker simple) throws TemplateException, IOException {
        simple.clear();
        String title = OwnTools.capitalize(field.getName());
        try {
            title = this.getParam().matchSimple(field.getName()).getTitle();
        } catch (NotMatched e) {

        }
        String type = "";
        String reference = this.getReference(field, "", true);
        if (field.getType().equals(Date.class)) {
            type = "date";
        } else if (field.getType().equals(Timestamp.class)) {
            type = "datetime-local";
        } else {
            type = "text";
        }
        simple.put("label", title);
        simple.put("type", type);
        simple.put("reference", reference);
        simple.put("default", this.getDefaultSimple(field));
        return simple.getContent();
    }

    public String getDefaultSimple(Field field) {
        return "";
    }

    public String onInHerit(Field field, HMaker inHerit) throws NotMatched, TemplateException, IOException {
        inHerit.clear();
        InheritParam inheritParam = this.getParam().match(field.getName());
        String reference = this.getReference(field, "", true);
        inheritParam.config(inHerit);
        inHerit.put("reference", reference);
        inHerit.put("default", this.getDefaultInHerit(field, inheritParam));
        inHerit.put("label", OwnTools.capitalize(inheritParam.getTitleLable()));
        return inHerit.getContent();
    }

    public String getDefaultInHerit(Field field, InheritParam inheritParam) {
        return "";
    }

    public ListeParam getParam() {
        return param;
    }

    public void setParam(ListeParam param) {
        this.param = param;
    }

    public String getSearchData() {
        return searchData;
    }

    public void setSearchData(String searchData) {
        this.searchData = searchData;
    }

}
