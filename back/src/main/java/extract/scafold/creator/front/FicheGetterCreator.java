package extract.scafold.creator.front;

import java.util.HashMap;

import extract.scafold.creator.Creator;

public class FicheGetterCreator extends FrontCreator {

    public FicheGetterCreator(Creator creator) {
        super("fichegetter.ftl", "FicheGetter", creator);
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        map.put("className", this.getName());
    }

}
