package extract.scafold.creator.front;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import extract.controller.CrudController;
import extract.hmodel.HCrud;
import extract.scafold.FListe;
import extract.scafold.creator.Creator;
import extract.scafold.creator.back.SearchCreator;
import extract.scafold.creator.front.param.InheritParam;
import extract.scafold.creator.front.param.ListeParam;
import extract.scafold.creator.front.param.NotMatched;
import extract.tool.HMaker;
import extract.tool.OwnTools;
import freemarker.template.TemplateException;

public class ListeCreator extends FrontCreator {
    ListeParam param;
    String searhData = "";

    public ListeCreator(Creator creator) {
        super("liste.ftl", "Liste", creator);
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        String[] skype = {"id", "sum"};
        List<FListe> fields = this.getFields();
        map.put("className", this.getName());
        map.put("baseUrl", this.getBaseUrl()[0]);
        map.put("url", this.getListeUrl());
        map.put("urlUtils", this.getUtilsUrl());
        map.put("search", this.getSearch(fields));
        map.put("searchData", this.getSearhData());
        map.put("head", this.getHead(fields, skype));
        map.put("body", this.getBody(fields, skype));
        map.put("constructor", this.getConstruct());
        map.put("newUrl", this.getNewUrl());
        map.put("pdfContent", this.getPdfContent(fields,skype));
        map.put("listeTile", this.getParam().getListeTitle());
    }

    private String getPdfContent(List<FListe> fields, String[] skypes) {
        HMaker maker = new HMaker("pdfField.ftl");
        return this.workOnListe(fields,
                (field, res) -> {
                    maker.clear();
                    if (HCrud.inHerit(field.getField().getType())) {
                        try {
                            String title = this.getParam().match(field.getField().getName()).getTitle();
                            maker.put("field", field.getField().getName());
                            maker.put("title",title);
                            res[0] += maker.getContent()+",";
                        } catch (NotMatched | TemplateException | IOException e) {
                        }
                    } else if (!this.shouldSkip(field.getField(),skypes)){
                        try {
                            String title = this.getParam().matchSimple(field.getField().getName()).getTitle();
                            maker.put("field", field.getField().getName());
                            maker.put("title",title);
                            res[0] += maker.getContent()+",";
                        } catch (NotMatched | TemplateException | IOException e) {
                        }
                    }
                }, (res) -> {
                    res[0] += "";
                },"%s");
    }

    private String getNewUrl() throws ClassNotFoundException, ClassFormatError {
        return this.getBaseUrl()[0] + "/form";
    }

    @Override
    public void prepare(String label, String reference) {
        String pattern = ",%s : %s";
        try {
            InheritParam param = this.getParam().match(label);
            reference = String.format("this.checkRefNull(%s,{%s:this.prepare(%s)},null)", reference, param.getPureValue(), reference);
        } catch (NotMatched e) {
            reference = String.format("this.prepare(%s)", reference);
        }
        pattern = String.format(pattern, label, reference);
        this.setSearhData(this.getSearhData() + pattern);
    }

    public List<FListe> getFields() {
        List<FListe> ans = new ArrayList<FListe>();
        OwnTools.onField(this.getCls(),
                (field) -> {
                    JsonIgnore ignore = field.getAnnotation(JsonIgnore.class);
                    JsonAnyGetter getter = field.getAnnotation(JsonAnyGetter.class);
                    if (!(ignore != null || getter != null))
                        ans.add(new FListe("", field));
                });
        return ans;
    }

    private String getBody(List<FListe> fields, String[] skype) throws TemplateException, IOException, ClassNotFoundException, ClassFormatError {
        String iteration = "{this.state.data !== undefined && this.state.data !== null ?this.state.data.map((data,index) =>(%s)):<></>}";
        String btnPlus = this.getBtnPlus();
        String content = this.workOnListe(fields,
                (field, res) -> {
                    if (!this.shouldSkip(field.getField(),skype)) {
                        String suffixe = "data."+field.getName();
                        if (HCrud.inHerit(field.getField().getType())) {
                            try {
                                suffixe = this.getParam().match(field.getField().getName()).getListedValue();
                            } catch (NotMatched e) {
                                return;
                            }
                        } else {
                            try {
                                this.getParam().matchSimple(field.getField().getName());
                            } catch (NotMatched e) {
                                return;
                            }
                        }
                        res[0] += "<td>{" + suffixe + "}</td>";
                    }
                }, (res) -> {
                    res[0] += btnPlus;
                });
        return String.format(iteration, content);
    }



    public String getBtnPlus() throws TemplateException, IOException, ClassNotFoundException, ClassFormatError {
        HMaker maker = new HMaker("btnPlus.ftl");
        maker.put("urlUpdate", this.getBaseUrl()[0] + "/update/");
        maker.put("urlInfo", this.getBaseUrl()[0] + "/");
        return maker.getContent();
    }

    private String getHead(List<FListe> fields,String[]skypes) {
        return this.workOnListe(fields,
                (field, res) -> {
                    if (HCrud.inHerit(field.getField().getType())) {
                        try {
                            res[0] += "<td>" + this.getParam().match(field.getField().getName()).getTitle() + "</td>";
                        } catch (NotMatched e) {
                        }
                    } else if (!this.shouldSkip(field.getField(),skypes)){
                        try {
                            res[0] += "<td>" + this.getParam().matchSimple(field.getField().getName()).getTitle() + "</td>";
                        } catch (NotMatched e) {
                        }
                    }
                }, (res) -> {
                    res[0] += "<td></td>";
                });
    }

    private String workOnListe(List<FListe> fields, BiConsumer<FListe, String[]> between,
                               Consumer<String[]> lastAction,String content) {
        String[] res = new String[1];
        res[0] = "";
        for (FListe fListe : fields) {
            between.accept(fListe, res);
        }
        lastAction.accept(res);
        content = String.format(content, res[0]);
        return content;
    }

    private String workOnListe(List<FListe> fields, BiConsumer<FListe, String[]> between,
                               Consumer<String[]> lastAction) {
        return this.workOnListe(fields, between, lastAction,"<tr>%s</tr>");
    }

    public String getSearch(List<FListe> fields) {
        SearchCreator sCreator = new SearchCreator(this.getCreator());
        HMaker inHerit = new HMaker("inherit.ftl");
        HMaker simple = new HMaker("intervale.ftl");
        String content = sCreator.filter((field, str) -> {
            try {
                this.onInHerit(field, str, inHerit);
            } catch (NotMatched e) {
            }
        }, (field, str) -> {
            this.onSimpleField(field, str, simple);
        })[0];
        return content;
    }

    public void onSimpleField(Field field, String[] str, HMaker simple) {
        String refmin = this.getReference(field, Creator.hmin, true);
        String refmax = this.getReference(field, Creator.hmax, true);
        simple.clear();
        String title = OwnTools.capitalize(field.getName());
        try {
            title = this.getParam().matchSimple(field.getName()).getTitle();
        } catch (NotMatched e) {
        }
        simple.put("label", title);
        simple.put("refmin", refmin);
        simple.put("refmax", refmax);
        try {
            str[0] += simple.getContent();
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

    public void onInHerit(Field field, String[] str, HMaker inHerit) throws NotMatched {
        inHerit.clear();
        InheritParam inheritParam = this.getParam().match(field.getName());
        String reference = this.getReference(field, "", true);
        inheritParam.config(inHerit);
        inHerit.put("reference", reference);
        inHerit.put("default", "");
        inHerit.put("label", OwnTools.capitalize(inheritParam.getTitleLable()));
        try {
            str[0] += inHerit.getContent();
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

    public String getListeUrl() throws ClassNotFoundException, ClassFormatError {
        String url = this.getBaseUrl()[0];
        return url + CrudController.FILTER;
    }

    public ListeParam getParam() {
        return param;
    }

    public void setParam(ListeParam param) {
        this.param = param;
    }

    /**
     * Doit être executé apres getSearch
     */
    public String getSearhData() {
        return searhData;
    }

    public void setSearhData(String searhData) {
        this.searhData = searhData;
    }

}
