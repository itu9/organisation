package extract.scafold.creator.back;

import java.lang.reflect.Field;
import java.util.HashMap;

import extract.scafold.creator.Creator;
import extract.scafold.creator.CreatorModel;

public class UtilsCreator extends CreatorModel {

    public UtilsCreator() {
        this(null);
    }

    public UtilsCreator(Creator creator) {
        super("utils.ftl", "Utils", creator);
    }

    public String[] getUtilsAttribute() {
        return this.filter(
                (Field field, String[] result) -> {
                    result[0] += String.format("List<%s> %s = new ArrayList<%s>();", field.getType().getSimpleName(),
                            field.getName(), field.getType().getSimpleName());
                },
                (Field field, String[] result) -> {
                });
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        String[] attributes = this.getUtilsAttribute();
        map.put("import", attributes[1]);
        map.put("className", this.getName());
        map.put("classMapped", this.getClassName());
        map.put("attribute", attributes[0]);
    }

}
