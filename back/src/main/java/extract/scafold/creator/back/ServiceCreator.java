package extract.scafold.creator.back;

import java.util.HashMap;

import extract.scafold.creator.Creator;
import extract.scafold.creator.CreatorModel;

public class ServiceCreator extends CreatorModel {

    public ServiceCreator(Creator creator) {
        super("service.ftl", "Service", creator);
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        map.put("import", "");
        map.put("className", this.getName());
        map.put("classMapped", this.getClassName());
        map.put("id", this.getCreator().getId());
        map.put("repository", this.getCreator().getReposName());
        map.put("search", this.getCreator().getSearchName());
        map.put("utils", this.getCreator().getUtilsName());
    }

}
