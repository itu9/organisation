package extract.scafold.creator.back;

import java.util.HashMap;

import extract.scafold.creator.Creator;
import extract.scafold.creator.CreatorModel;

public class ControllerCreator extends CreatorModel {

    public ControllerCreator(Creator creator) {
        super("controller.ftl", "Controller", creator);
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        map.put("import", this.getImport());
        map.put("urlMapping", this.getUrl());
        map.put("className", this.getName());
        map.put("classMapped", this.getClassName());
        map.put("id", "Integer");
        map.put("repository", this.getCreator().getReposName());
        map.put("service", this.getCreator().getServiceName());
        map.put("search", this.getCreator().getSearchName());
        map.put("utils", this.getCreator().getUtilsName());
    }

    public String getImport() {
        return "";
    }

}
