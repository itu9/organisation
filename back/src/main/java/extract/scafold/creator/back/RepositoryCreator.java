package extract.scafold.creator.back;

import java.util.HashMap;

import extract.scafold.creator.Creator;
import extract.scafold.creator.CreatorModel;

public class RepositoryCreator extends CreatorModel {

    public RepositoryCreator(Creator creator) {
        super("repository.ftl", "Repository", creator);
    }

    @Override
    public void getContent(HashMap<String, String> map) throws Exception {
        map.put("import", "");
        map.put("className", this.getName());
        map.put("classMapped", this.getClassName());
        map.put("id", this.getCreator().getId());
    }

}
