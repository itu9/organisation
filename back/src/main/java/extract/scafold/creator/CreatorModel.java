package extract.scafold.creator;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;

import extract.hmodel.HCrud;
import extract.tool.HMaker;

public abstract class CreatorModel {
    private String fileName;
    private String prefixe;
    private Creator creator;
    private HashMap<String, String> map;

    public CreatorModel(String fileName, String prefixe, Creator creator) {
        this.setFileName(fileName);
        this.setPrefixe(prefixe);
        this.setCreator(creator);
    }

    public String[] filter(BiConsumer<Field, String[]> onInHerit, BiConsumer<Field, String[]> onSimpleField) {
        List<Field> fields = HCrud.fieldsNoTransient(this.getCls());
        String[] result = { "", "" };
        for (Field field : fields) {
            if (field.getType() == String.class || field.getName().compareTo("id") == 0)
                continue;
            if (HCrud.inHerit(field.getType())) {
                onInHerit.accept(field, result);
            } else {
                onSimpleField.accept(field, result);
            }
            result[1] += "import " + field.getType().getName() + ";";
        }
        return result;
    }

    public String getUrl() {
        return this.getCreator().getUrl();
    }

    public String getName() {
        return this.getClassName() + this.getPrefixe();
    }

    public String getClassName() {
        return this.getCls().getSimpleName();
    }

    public Class<?> getCls() {
        return this.getCreator().getCls();
    }

    public String getPath() {
        return this.getRealPath() + "\\" + this.extJava(this.getName());
    }

    public String getRealPath() {
        return this.getCreator().getRealPath();
    }

    public abstract void getContent(HashMap<String, String> map) throws Exception;

    public String extJava(String file) {
        return file + ".java";
    }

    public void create() throws Exception {
        String content = this.getInside();
        Creator.write_data(this.getPath(), true, false, content);
    }

    public String getInside() throws Exception {
        this.verifMap();
        this.put("packageName", this.getPackageImport());
        this.getContent(this.getMap());
        return (new HMaker()).changeContent(this.getFileName(), map);
    }

    public String getPackageImport() {
        return this.getCreator().getPackageImport();
    }

    public String getPrefixe() {
        return prefixe;
    }

    public void setPrefixe(String prefixe) {
        this.prefixe = prefixe;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }

    public void verifMap() {
        if (this.getMap() == null) {
            this.setMap(new HashMap<String, String>());
        }
    }

    public void put(String key, String value) {
        this.verifMap();
        this.getMap().put(key, value);
    }

}
