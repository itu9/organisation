package extract.scafold.creator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import extract.scafold.creator.back.ControllerCreator;
import extract.scafold.creator.back.RepositoryCreator;
import extract.scafold.creator.back.SearchCreator;
import extract.scafold.creator.back.ServiceCreator;
import extract.scafold.creator.back.UtilsCreator;
import extract.scafold.creator.front.FicheCreator;
import extract.scafold.creator.front.FormCreator;
import extract.scafold.creator.front.ListeCreator;
import extract.scafold.creator.front.UpdateCreator;
import extract.scafold.creator.front.param.ListeParam;
import extract.scafold.orm.FrontResult;
import extract.tool.HWriter;

public class Creator {
    private Class<?> cls;
    private String url;
    private String id;
    private String path;
    private Package pack;
    private SearchCreator searchCreator;
    private ControllerCreator controllerCreator;
    private ServiceCreator serviceCreator;
    private RepositoryCreator repositoryCreator;
    private ListeCreator listeCreator;
    private UtilsCreator utilsCreator;
    private FormCreator formCreator;
    private UpdateCreator updateCreator;
    private FicheCreator ficheCreator;
    public static final String hmin = "hmin";
    public static final String hmax = "hmax";
    public static final HWriter writer = new HWriter();

    public Creator(String path, Package pack) {
        this.setPath(path);
        this.setPack(pack);
    }

    public Creator(Class<?> cls) {
        this.setCls(cls);
    }

    public Creator(Class<?> cls, String url, String path) {
        this(cls, url, "Integer", path);
        this.setSearchCreator(new SearchCreator(this));
        this.setControllerCreator(new ControllerCreator(this));
        this.setServiceCreator(new ServiceCreator(this));
        this.setRepositoryCreator(new RepositoryCreator(this));
        this.setUtilsCreator(new UtilsCreator(this));
        this.setListeCreator(new ListeCreator(this));
        this.setFormCreator(new FormCreator(this));
        this.setUpdateCreator(new UpdateCreator(this));
        this.setFicheCreator(new FicheCreator(this));
    }

    public Creator(Class<?> cls, String url, String id, String path) {
        this.setCls(cls);
        this.setUrl(url);
        this.setId(id);
        this.setPath(path);
    }

    public void init() throws Exception {
        this.makeBack();
    }

    public void makeBack() throws Exception {
        this.createSearch();
        this.createUtils();
        this.createRepos();
        this.createService();
        this.createConstructeur();
    }

    public void makeFront(FrontResult front) throws Exception {
        this.createListe();
        this.createForm();
        this.createUpdate();
        this.createFiche();
        this.init(front);
    }

    public void init(FrontResult front) throws Exception {
        front.setCompListe(this.getListeName());
        front.setCompForm(this.getFormCreator().getName());
        front.setCompUpdate(this.getUpdateCreator().getGetter().getName());
        front.setCompFiche(this.getFicheCreator().getFGetter().getName());
        front.setPath(this.getListeCreator().getBaseUrl()[0]);
        front.configRoot(this.getPack());
    }

    private void createFiche() throws Exception {
        this.getFicheCreator().create();
    }

    private void createUpdate() throws Exception {
        this.getUpdateCreator().create();
    }

    public void setListeParam(ListeParam param) {
        this.getListeCreator().setParam(param);
        this.getFormCreator().setParam(param);
        this.getUpdateCreator().setParam(param);
        this.getFicheCreator().setParam(param);
    }

    /**
     * Liste React
     */

    public String getListeName() {
        return this.getListeCreator().getName();
    }

    public void createListe() throws Exception {
        this.getListeCreator().create();
    }

    private void createForm() throws Exception {
        this.getFormCreator().create();
    }

    /**
     * Utilitaire formulaire
     */
    public String getUtilsName() {
        return this.getUtilsCreator().getName();
    }

    public void createUtils() throws Exception {
        this.getUtilsCreator().create();
    }

    /**
     * Recherche
     * 
     * @throws Exception
     */

    public String getSearchName() {
        return this.getSearchCreator().getName();
    }

    public void createSearch() throws Exception {
        (this.getSearchCreator()).create();
    }
    /*---------------Fin recherche-------------- */

    public String toPath() {
        String pack = this.getPackageName();
        return this.toPath(pack);
    }

    public String toPath(String pack) {
        String[] splt = pack.split("\\.");
        pack = String.join("\\", splt);
        return pack;
    }

    /**
     * Controlleur
     * 
     * @return
     */
    public String getConstructorName() {
        return this.getControllerCreator().getName();
    }

    public void createConstructeur() throws Exception {
        this.getControllerCreator().create();
    }
    /*-------------Fin Constructeur------------- */

    /**
     * Service
     * 
     * @return
     */
    public String getServiceName() {
        String name = this.getName() + "Service";
        return name;
    }

    public void createService() throws Exception {
        this.getServiceCreator().create();
    }

    /*------Fin Service--------- */

    /**
     * Repository
     * 
     * @return
     */
    public String getReposName() {
        return this.getRepositoryCreator().getName();
    }

    public void createRepos() throws Exception {
        this.getRepositoryCreator().create();
    }

    /*------Fin Repository---- */
    public static void write_data(String fic, Boolean fafaina, boolean newline, String... line) throws IOException {
        Creator.writer.write_data(fic, fafaina, newline, line);
    }

    public static ArrayList<String> data(String fileName) throws IOException {
        File file = new File(fileName);
        Creator.writer.existsFile(file);
        return Creator.writer.intoFile(file);
    }

    public String getRealPath(String packageName) {
        String pack = this.toPath(packageName);
        return this.getPath() + pack;
    }

    public String getRealPath() {
        String pack = this.toPath();
        return this.getPath() + pack;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Class<?> getCls() {
        return cls;
    }

    public void setCls(Class<?> cls) {
        this.cls = cls;
        Package pack = this.getCls().getPackage();
        this.setPack(pack);
    }

    public Package getPack() {
        return pack;
    }

    public void setPack(Package pack) {
        this.pack = pack;
    }

    public String getName() {
        return this.getCls().getSimpleName();
    }

    public String extJava(String file) {
        return file + ".java";
    }

    public String getPackageName() {
        return this.getPack().getName();
    }

    public String getPackageImport() {
        return "package " + this.getPackageName() + ";";
    }

    public SearchCreator getSearchCreator() {
        return searchCreator;
    }

    public void setSearchCreator(SearchCreator searchCreator) {
        this.searchCreator = searchCreator;
    }

    public static String getHmin() {
        return hmin;
    }

    public static String getHmax() {
        return hmax;
    }

    public ControllerCreator getControllerCreator() {
        return controllerCreator;
    }

    public void setControllerCreator(ControllerCreator controllerCreator) {
        this.controllerCreator = controllerCreator;
    }

    public ServiceCreator getServiceCreator() {
        return serviceCreator;
    }

    public void setServiceCreator(ServiceCreator serviceCreator) {
        this.serviceCreator = serviceCreator;
    }

    public RepositoryCreator getRepositoryCreator() {
        return repositoryCreator;
    }

    public void setRepositoryCreator(RepositoryCreator repositoryCreator) {
        this.repositoryCreator = repositoryCreator;
    }

    public UtilsCreator getUtilsCreator() {
        return utilsCreator;
    }

    public void setUtilsCreator(UtilsCreator utilsCreator) {
        this.utilsCreator = utilsCreator;
    }

    public static HWriter getWriter() {
        return writer;
    }

    public ListeCreator getListeCreator() {
        return listeCreator;
    }

    public void setListeCreator(ListeCreator listeCreator) {
        this.listeCreator = listeCreator;
    }

    public FormCreator getFormCreator() {
        return formCreator;
    }

    public void setFormCreator(FormCreator formCreator) {
        this.formCreator = formCreator;
    }

    public UpdateCreator getUpdateCreator() {
        return updateCreator;
    }

    public void setUpdateCreator(UpdateCreator updateCreator) {
        this.updateCreator = updateCreator;
    }

    public FicheCreator getFicheCreator() {
        return ficheCreator;
    }

    public void setFicheCreator(FicheCreator ficheCreator) {
        this.ficheCreator = ficheCreator;
    }

}
