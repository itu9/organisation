package extract.scafold.affichage;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.Cnt;

@RestController
@CrossOrigin("*")
@RequestMapping("/affichage")
public class AffichageController extends Cnt {
    ClassFinder finder = (new ClassFinder());

    @GetMapping("")
    public ResponseEntity<?> getClasses() {
        try {
            return this.returnSuccess(this.finder.getAll());
        } catch (Exception e) {
            return this.returnError(e);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> getOne(@RequestBody ClassResult result) {
        try {
            this.finder.getOne(result);
            return this.returnSuccess(result);
        } catch (Exception e) {
            return this.returnError(e);
        }
    }
}
