package extract.scafold.affichage;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import extract.hmodel.HCrud;

public class FieldResult {
    String name;
    List<FieldResult> fields;

    public FieldResult() {
    }
    public FieldResult(Field field, HashMap<String,Integer> map) {
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        this.setName(field.getName());
        if (HCrud.inHerit(field.getType()))
            map.put(field.getType().getName(),1);
        List<Field> ans = new ArrayList<>();
        try {
            this.setFields(new ArrayList<>());
            HCrud.getFields(field.getType(), ans);
            for (Field field2 : ans) {
                if (map.get(field2.getType().getName()) == null) {
                    this.getFields().add(new FieldResult(field2,map));
                }
            }
        } catch (Exception e) {
        }
    }
    public FieldResult(Field field) {
        this(field,new HashMap<>());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FieldResult> getFields() {
        return fields;
    }

    public void setFields(List<FieldResult> fields) {
        this.fields = fields;
    }

}
