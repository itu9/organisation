package extract.scafold.affichage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import extract.scafold.Executor;
import extract.scafold.Loader;
import extract.tool.OwnTools;

public class ClassFinder {
    public void getOne(ClassResult classResult) {
        classResult.extractFields();
    }

    public List<ClassResult> getAll() {
        String mainPackage = "extract";
        ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
        List<ClassResult> result = new ArrayList<ClassResult>();
        File file = new File(Executor.path + "\\" + mainPackage);
        OwnTools.find(classes, file, "", new Loader(), (clazz, res) -> {
            Entity entity = clazz.getAnnotation(Entity.class);
            IsTable table = clazz.getAnnotation(IsTable.class);
            if (entity != null || table != null) {
                result.add(new ClassResult(clazz));
            }
        });
        return result;
    }
}
