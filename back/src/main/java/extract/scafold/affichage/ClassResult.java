package extract.scafold.affichage;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import extract.hmodel.HCrud;

public class ClassResult {
    String name;
    String packageName;
    Class<?> clazz;
    List<FieldResult> fields = new ArrayList<FieldResult>();

    public ClassResult() {
    }

    public ClassResult(Class<?> clazz) {
        this.setClazz(clazz);
    }

    public void extractFields() {
        List<Field> ans = new ArrayList<>();
        HCrud.getFields(this.getClazz(), ans);
        this.setFields(new ArrayList<>());
        for (Field field : ans) {
            this.getFields().add(new FieldResult(field));
        }
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
        this.setName(this.getClazz().getSimpleName());
        this.setPackageName(this.getClazz().getPackage().getName());
        // this.extractFields();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FieldResult> getFields() {
        return fields;
    }

    public void setFields(List<FieldResult> fields) {
        this.fields = fields;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

}
