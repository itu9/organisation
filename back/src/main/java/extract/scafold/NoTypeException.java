package extract.scafold;

public class NoTypeException extends Exception {

    public NoTypeException(String arg0) {
        super(arg0);
    }
    
}
