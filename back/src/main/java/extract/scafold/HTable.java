package extract.scafold;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import extract.scafold.orm.ORMParam;
import extract.tool.HMaker;
import extract.tool.OwnTools;
import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class HTable {
    String tableName;
    String packageName;
    boolean created;
    List<HCol> cols = new ArrayList<HCol>();
    List<FK> fks = new ArrayList<FK>();

    public HTable(String tableName) {
        this.setTableName(tableName);
    }

    public HTable(ORMParam param, DatabaseMetaData metaData) throws SQLException {
        this.setTableName(param.getTable());
        this.setPackageName(param.getPackageName());
        List<FK> foreigns = this.getForeigns(metaData);
        this.initCols(metaData, foreigns);
        this.initForeign(metaData);
    }

    // genere la contenue du class
    public String getSignature(ORMParam params)
            throws TemplateNotFoundException, MalformedTemplateNameException, ParseException,
            IOException, TemplateException, NoTypeException {
        Map<String, String> info = new HashMap<String, String>();
        Vector<Object> imports = new Vector<>();
        String attributes = this.getColSign(imports, params);
        String imps = this.organise(imports);
        info.put("packageName", this.getPackageName());
        info.put("import", imps);
        info.put("className", this.getClassName());
        info.put("attribute", attributes);
        HMaker maker = new HMaker();
        return maker.changeContent("class.ftl", info);
    }

    public String organise(Vector<Object> imports) {
        String answer = "";
        for (Object imp : imports) {
            answer += ((TypeTrad) imp).getPath();
        }
        return answer;
    }

    public String getColSign(Vector<Object> imports, ORMParam params) throws NoTypeException {
        String cols = "";
        for (HCol col : this.getCols()) {
            if (col.ignore())
                continue;
            cols += col.getSignature(imports, params);
        }
        return cols;
    }

    

    public String getClassName() {
        return OwnTools.capitalize(this.getTableName());
    }

    public void initForeign(DatabaseMetaData metaData) throws SQLException {
        ResultSet exp = metaData.getExportedKeys(null, null, this.getTableName());
        while (exp.next()) {
            // System.out.println(exp.getString("FKTABLE_NAME"));
            String colName = exp.getString("FKCOLUMN_NAME");
            String fkName = exp.getString("FKTABLE_NAME");
            this.addFk(new FK(colName, fkName));
        }
    }

    public void initCols(DatabaseMetaData metatData, List<FK> foreigns) throws SQLException {
        ResultSet columns = metatData.getColumns(null, null, tableName, null);
        while (columns.next()) {
            this.getCols().add(new HCol(columns, foreigns));
        }
    }

    public List<FK> getForeigns(DatabaseMetaData metaData) throws SQLException {
        ResultSet foreigns = metaData.getImportedKeys(null, null, tableName);
        List<FK> answer = new ArrayList<FK>();
        while (foreigns.next()) {
            String fkCol = foreigns.getString("FKCOLUMN_NAME");
            String pkTable = foreigns.getString("PKTABLE_NAME");
            answer.add(new FK(fkCol, pkTable));
        }
        return answer;
    }

    public List<HCol> getCols() {
        return cols;
    }

    public void setCols(List<HCol> cols) {
        this.cols = cols;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
        this.verifCreated();
    }

    public void verifCreated() {
        this.setCreated(TypeTrad.info.get(this.getTableName()) != null);
    }

    public List<FK> getFks() {
        return fks;
    }

    public void setFks(List<FK> fks) {
        this.fks = fks;
    }

    public void addFk(FK fk) {
        this.getFks().add(fk);
    }

    @Override
    public String toString() {
        return "HTable [tableName=" + tableName + ", cols=" + cols + ", fks=" + fks + "]";
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

}
