package extract.scafold.orm;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import extract.scafold.ClassExistException;
import extract.scafold.CntParam;
import extract.scafold.Executor;
import extract.scafold.HTable;
import extract.scafold.NoTypeException;
import extract.scafold.TypeTrad;
import extract.scafold.creator.Creator;
import extract.scafold.creator.front.param.ListeParam;
import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

@Service
public class ORMService {

    @Autowired
    private DataSource dataSource;

    public void makeListe(ListeParam param, FrontResult front) throws ClassFormatError, Exception {
        Creator creator = new Creator(param.toClass(), param.getUrl(), Executor.pathFront);
        creator.setListeParam(param);
        creator.makeFront(front);
    }

    public void makeController(CntParam param)
            throws ClassFormatError, Exception {
        Creator creator = new Creator(param.toClass(), param.getUrl(), Executor.path);
        creator.makeBack();
    }

    public static void main(String[] args) throws ClassFormatError, Exception {
        // CntParam param = new CntParam("marque","/marque");
        // Creator creator = new Creator(param.toClass(), param.getUrl(),
        // Executor.path);
        // creator.init();
    }

    public ORMResult create(ORMParam param) throws SQLException, TemplateNotFoundException,
            MalformedTemplateNameException, ParseException, IOException, TemplateException, NoTypeException,
            ClassExistException {
        ORMResult result = this.preview(param);
        result.create();
        TypeTrad.init();
        return result;
    }

    public ORMResult preview(ORMParam param) throws SQLException, TemplateNotFoundException,
            MalformedTemplateNameException, ParseException, IOException, TemplateException, NoTypeException {
        HTable table = this.toTable(param);
        return new ORMResult(table, param);
    }

    public ArrayList<ORMTable> getTables() throws SQLException {
        ArrayList<ORMTable> answer = new ArrayList<>();
        Connection connection = this.dataSource.getConnection();
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet tables = metaData.getTables(null, null, null, new String[] { "TABLE","VIEW" });
        while (tables.next()) {
            ORMTable oTable = new ORMTable(tables);
            oTable.verifBlock(metaData);
            answer.add(oTable);
        }
        tables.close();
        connection.close();
        return answer;
    }

    public HTable toTable(ORMParam param) throws SQLException {
        Connection connection = this.dataSource.getConnection();
        DatabaseMetaData metadata = connection.getMetaData();
        HTable table = new HTable(param, metadata);
        connection.close();
        return table;
    }

    public String getCol(String tableName) throws SQLException {
        // System.out.println(table);
        return "ok";
    }

}
