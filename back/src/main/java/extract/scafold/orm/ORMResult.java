package extract.scafold.orm;

import java.io.File;
import java.io.IOException;

import extract.scafold.ClassExistException;
import extract.scafold.Executor;
import extract.scafold.HTable;
import extract.scafold.NoTypeException;
import extract.scafold.creator.Creator;
import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class ORMResult {
    String content;
    String packageName;
    String name;
    HTable table;

    public ORMResult(HTable table, ORMParam param) throws TemplateNotFoundException, MalformedTemplateNameException,
            ParseException, IOException, TemplateException, NoTypeException {
        this.setTable(table);
        this.setContent(table.getSignature(param));
        this.setPackageName(table.getPackageName());
        this.setName(table.getClassName());
    }

    public ORMResult(String content, String packageName, String name) {
        this.setContent(content);
        this.setPackageName(packageName);
        this.setName(name);
    }

    public void create() throws ClassExistException {
        if ((new HTable(this.getName())).isCreated())
            throw new ClassExistException(this.getName());
        Creator creator = new Creator(Executor.path, null);
        String folder = creator.getRealPath(this.getPackageName());
        String path = folder + "\\" + creator.extJava(this.getName());
        try {
            Creator.write_data(path, true, false, this.getContent());
            System.out.println(String.format("Model %s created. Path : %s", this.getName(), path));
        } catch (IOException e) {
            File file = new File(folder);
            file.mkdir();
            this.create();
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HTable getTable() {
        return table;
    }

    public void setTable(HTable table) {
        this.table = table;
    }

}
