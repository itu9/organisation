package extract.scafold.orm;

import extract.scafold.Executor;
import extract.tool.HMaker;
import extract.tool.OwnTools;
import freemarker.template.TemplateException;
import lombok.Data;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Data
public class FrontResult {
    String path;
    String root;
    String compListe;
    String compFiche;
    String compForm;
    String compUpdate;

    public String getApp() {
        return this.inside(Executor.appPath);
    }

    public String getContainer() {
        return this.inside(Executor.containerPath);
    }

    public String inside(String path) {
        try {
            String content = new String(Files.readAllBytes(Paths.get(path)));
            return content;
        } catch (IOException e) {
            return "";
        }
    }

    public String getContent() {
        HMaker maker = new HMaker("routeComp.ftl");
        maker.put("path",this.getPath());
        this.put(maker);
        try {
            return maker.getContent();
        } catch (TemplateException | IOException e) {
            return "Error on creating";
        }
    }

    public String getImport() {
        HMaker maker = new HMaker("route.ftl");
        maker.put("root",this.getRoot());
        this.put(maker);
        try {
            return maker.getContent();
        } catch (TemplateException | IOException e) {
            return "Error on creating";
        }
    }

    public void put(HMaker maker) {
        maker.put("compListe",this.getCompListe());
        maker.put("compFiche",this.getCompFiche());
        maker.put("compForm",this.getCompForm());
        maker.put("compUpdate",this.getCompUpdate());
    }

    public void configRoot(Package pack) {
        String[] splt = pack.getName().split("\\.");
        String root = Executor.mainFrontPackage +"/"+String.join("/", splt);
        System.out.println(root);
        this.setRoot(root);
    }
}
