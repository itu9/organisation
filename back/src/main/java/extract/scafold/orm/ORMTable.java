package extract.scafold.orm;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import extract.scafold.FK;
import extract.scafold.HTable;
import extract.scafold.NoTypeException;
import extract.scafold.TypeTrad;

public class ORMTable {
    String tableName;
    boolean isBlock;
    boolean created;
    boolean isView;

    public ORMTable(String tableName) {
        this.setTableName(tableName);
    }

    public ORMTable(ResultSet tables) throws SQLException {
        this(tables.getString("TABLE_NAME"));
        this.setView(tables.getString("TABLE_TYPE").compareToIgnoreCase("view") == 0);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
        try {
            TypeTrad.trad(this.getTableName());
            this.setCreated(true);
        } catch (NoTypeException e) {
            this.setCreated(false);
        }
    }

    public void verifBlock(DatabaseMetaData metaData) throws SQLException {
        HTable table = new HTable(this.getTableName());
        List<FK> foreigns = table.getForeigns(metaData);
        table.initCols(metaData, foreigns);
        Vector<Object> imports = new Vector<>();
        ORMParam param = new ORMParam();
        try {
            table.getColSign(imports, param);
            this.setBlock(false);
        } catch (NoTypeException e) {
            this.setBlock(true);
        }
    }

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean isBlock) {
        this.isBlock = isBlock;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public boolean isView() {
        return isView;
    }

    public void setView(boolean isView) {
        this.isView = isView;
    }

}
