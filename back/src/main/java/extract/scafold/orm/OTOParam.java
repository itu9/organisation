package extract.scafold.orm;

import extract.scafold.HCol;

public class OTOParam {
    String column;
    String replace;

    public boolean compatible(HCol col) {
        return this.getColumn().equals(col.getName());
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
        if (this.getReplace() == null) {
            this.setReplace(this.getColumn());
        }
    }

    public String getReplace() {
        return replace;
    }

    public void setReplace(String replace) {
        this.replace = replace;
    }

}
