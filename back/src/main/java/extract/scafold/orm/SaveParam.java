package extract.scafold.orm;

import extract.hmodel.HCrud;
import extract.scafold.Executor;
import extract.scafold.creator.Creator;
import extract.tool.OwnTools;
import lombok.Data;

import java.io.IOException;

@Data
public class SaveParam {
    String app;
    String container;

    public void save() throws IOException {
        Creator.write_data(Executor.appPath,true,false,this.getApp());
        Creator.write_data(Executor.containerPath,true,false,this.getContainer());
    }
}
