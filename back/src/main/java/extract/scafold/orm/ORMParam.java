package extract.scafold.orm;

import java.util.Vector;

import extract.scafold.HCol;
import extract.scafold.NoTypeException;
import extract.scafold.TypeTrad;

public class ORMParam {
    String table;
    String packageName;
    OTOParam[] oneToOne = new OTOParam[0];
    boolean valid;

    public ORMParam() {
        
    }

    public ORMParam(String table, String packageName, OTOParam[] oneToOne) {
        this.setTable(table);
        this.setPackageName(packageName);
        this.setOneToOne(oneToOne);
    }

    public TypeTrad oneToOneTrad(Vector<Object> imports) throws NoTypeException {
        TypeTrad trad = TypeTrad.trad("onetoone");
        imports.add(trad);
        return trad;
    }
    public TypeTrad transientTrad(Vector<Object> imports) throws NoTypeException {
        TypeTrad trad = TypeTrad.trad("transient");
        imports.add(trad);
        return trad;
    }
    public TypeTrad joinColumnTrad(Vector<Object> imports) throws NoTypeException {
        TypeTrad trad = TypeTrad.trad("joincol");
        imports.add(trad);
        return trad;
    }

    public String oneToOneValue(OTOParam p121, Vector<Object> imports) throws NoTypeException {
        String result = "";
        TypeTrad trad = this.oneToOneTrad(imports);
        result += trad.getType();
        trad = this.joinColumnTrad(imports);
        result += trad.getType();
        result += String.format("(name=\"%s\", nullable = false)", p121.getColumn());
        return result;
    }

    public OTOParam isOneToOne(HCol col) {
        for (OTOParam param : this.getOneToOne()) {
            if (param.compatible(col)) {
                return param;
            }
        }
        return null;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public OTOParam[] getOneToOne() {
        return oneToOne;
    }

    public void setOneToOne(OTOParam[] oneToOne) {
        this.oneToOne = oneToOne;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

}
