package extract.scafold.orm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import extract.controller.Cnt;
import extract.scafold.CntParam;
import extract.scafold.creator.front.param.ListeParam;

@RestController
@RequestMapping("/tables")
@CrossOrigin("*")
public class ORM extends Cnt {
    @Autowired
    ORMService service;

    @PostMapping("/makefront")
    public ResponseEntity<?> makeFront(@RequestBody ListeParam param) {
        try {
            FrontResult front = new FrontResult();
            this.service.makeListe(param,front);
            return this.returnSuccess(front);
        } catch (Exception e) {
            return this.returnError(e);
        }
    }
    @PostMapping("/saveModel")
    public ResponseEntity<?> makeFront(@RequestBody SaveParam param) {
        try {
            param.save();
            return this.returnSuccess(param);
        } catch (Exception e) {
            return this.returnError(e);
        }
    }

    @PostMapping("/makecontroller")
    public ResponseEntity<?> makeController(@RequestBody CntParam param) {
        try {
            this.service.makeController(param);
            return this.returnSuccess(param);
        } catch (Exception e) {
            return this.returnError(e);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> getTables() {
        try {
            return this.returnSuccess(this.service.getTables());
        } catch (Exception e) {
            return this.returnError(e);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> getCol(@RequestBody ORMParam param) {
        try {
            ORMResult result = null;
            if (param.isValid()) {
                result = this.service.create(param);
            } else {
                result = this.service.preview(param);
            }
            return this.returnSuccess(result);
        } catch (Exception e) {
            e.printStackTrace();
            return this.returnError(e);
        }
    }

}
