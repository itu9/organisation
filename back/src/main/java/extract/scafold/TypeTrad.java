package extract.scafold;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.Gson;

import extract.scafold.creator.Creator;
import extract.tool.OwnTools;

public class TypeTrad {
    String type;
    String path;
    static Map<String, TypeTrad> info = new HashMap<String, TypeTrad>();

    static {
        TypeTrad.init();
    };

    public static void show() {
        System.out.println("ok");
    }

    public static void init() {
        try {
            String conf = Creator.data("typeinfo.hasina").get(0);
            Gson gson = new Gson();
            TypeTrad.info = (Map<String, TypeTrad>) gson.fromJson(conf, TypeTrad.info.getClass());
            TypeTrad.scan();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "{type=" + type + ", path=" + path + "}";
    }

    public static void scan() {
        ArrayList<Class<?>> classes = TypeTrad.extractClasses();
        for (Class<?> class1 : classes) {
            TypeTrad.add(class1);
        }
    }

    public static ArrayList<Class<?>> extractClasses() {
        String mainPackage = "extract";
        ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
        File file = new File(Executor.path + "\\" + mainPackage);
        OwnTools.extractClass(classes, file, "", new Loader());
        return classes;
    }

    public static void add(Class<?> clazz) {
        if (!TypeTrad.isModel(clazz))
            return;
        String type = clazz.getSimpleName();
        String path = "import "+clazz.getName()+";";
        String key = TypeTrad.getTable(clazz);
        TypeTrad typeTrad = new TypeTrad(type, path);
        TypeTrad.info.put(key, typeTrad);
        // System.out.println(TypeTrad.info);
    }

    public static String getTable(Class<?> clazz) {
        Table table = clazz.getAnnotation(Table.class);
        return (table != null) ? table.name() : clazz.getSimpleName().toLowerCase();
    }

    public static boolean isModel(Class<?> clazz) {
        return clazz.getAnnotation(Entity.class) != null;
    }

    public static TypeTrad trad(String type) throws NoTypeException {
        Gson gson = new Gson();
        String res = gson.toJson(TypeTrad.info.get(type));
        TypeTrad result = gson.fromJson(res, TypeTrad.class);
        if (result != null)
            return result;
        throw new NoTypeException(String.format("Le type %s n'est pas encore créé.", type));
    }

    public TypeTrad(String type, String path) {
        this.setType(type);
        this.setPath(path);
    }

    // public static void main(String[] args) {
    // Map<String, TypeTrad> info = new HashMap<String, TypeTrad>();
    // info.put("varchar", new TypeTrad("String", ""));
    // info.put("text", new TypeTrad("String", ""));
    // info.put("int4", new TypeTrad("Integer", ""));
    // info.put("date", new TypeTrad("Date", "import java.sql.Date;"));
    // info.put("float8", new TypeTrad("Double", ""));
    // Gson gson = new Gson();
    // String data = gson.toJson(info);
    // Creator.write_data("typeInfo.hasina", true, false, data);
    // }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
