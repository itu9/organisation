package extract.scafold;

public class ClassExistException extends Exception {

    public ClassExistException(String arg0) {
        super(String.format("exist, La classe %s existe deja", arg0));
    }

}
