package extract.scafold;

public class CntParam {
    String tableName;
    Class<?> clazz;
    String url;

    public CntParam(String tableName, String url) {
        this.setTableName(tableName);
        this.setUrl(url);
    }

    public CntParam() {
    }

    public String getTableName() {
        return tableName;
    }

    public Class<?> toClass() throws NoTypeException, ClassNotFoundException, ClassFormatError {
        if (this.getClazz() != null) 
            return this.getClazz();
        String path = TypeTrad.trad(this.getTableName()).getPath();
        return (new Loader()).findClass(this.toClass(path));
    }

    public String toClass(String path) {
        path = path.split(" ")[1];
        path = path.split(";")[0];
        return path;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

}
