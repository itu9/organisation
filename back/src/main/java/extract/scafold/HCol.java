package extract.scafold;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import extract.scafold.orm.ORMParam;
import extract.scafold.orm.OTOParam;

public class HCol {
    String name;
    String type;
    int size;
    boolean isNullable;
    boolean isForeign = false;
    FK foreignKey;
    public static final String colName = "COLUMN_NAME";
    public static final String dataType = "DATA_TYPE";
    public static final String dataTypeName = "TYPE_NAME";
    public static final String columnSize = "COLUMN_SIZE";
    public static final String decimalDigits = "DECIMAL_DIGITS";
    public static final String nullable = "IS_NULLABLE";

    public HCol(ResultSet columns, List<FK> foreigns) throws SQLException {
        this.extractName(columns);
        this.extractType(columns);
        this.extractSize(columns);
        this.verifNullable(columns);
        this.verifForeign(foreigns);
    }

    public boolean ignore() {
        return this.getName().compareTo("id") == 0;
    }

    public String getSignature(Vector<Object> imports, ORMParam params) throws NoTypeException {
        OTOParam p121 = params.isOneToOne(this);
        String name = "";
        if (p121 == null) {
            name = this.getName();
        } else {
            name = p121.getReplace();
        }

        return String.format("%s %s;", this.getSignType(imports, params, p121), name);
    }

    public String getSignType(Vector<Object> imports, ORMParam params, OTOParam p121) throws NoTypeException {
        String res = "";
        String key = this.getType();
        if (this.isForeign()) {
            if (p121 != null) {
                key = this.getForeignKey().getTableRef();
                res += params.oneToOneValue(p121, imports);
            }
            // } else {
            // TypeTrad trad = params.transientTrad(imports);
            // res += trad.getType() + " ";
            // }
        }
        TypeTrad trad = TypeTrad.trad(key);
        imports.add(trad);
        return res + trad.getType();
    }

    public void verifForeign(List<FK> foreigns) {
        for (FK fk : foreigns) {
            if (fk.getName().equals(this.getName())) {
                this.setForeign(true);
                this.setForeignKey(fk);
            }
        }
    }

    public void verifNullable(ResultSet columns) throws SQLException {
        this.setNullable(columns.getString(HCol.nullable).compareTo("NO") != 0);
    }

    public void extractSize(ResultSet columns) throws SQLException {
        this.setSize(columns.getInt(HCol.columnSize));
    }

    public void extractType(ResultSet columns) throws SQLException {
        this.setType(columns.getString(HCol.dataTypeName));
    }

    public void extractName(ResultSet columns) throws SQLException {
        this.setName(columns.getString(HCol.colName));
    }

    public HCol(String name, String type, int size, boolean isNullable) {
        this.setName(name);
        this.setType(type);
        this.setSize(size);
        this.setNullable(isNullable);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public void setNullable(boolean isNullable) {
        this.isNullable = isNullable;
    }

    public boolean isForeign() {
        return isForeign;
    }

    public void setForeign(boolean isForeign) {
        this.isForeign = isForeign;
    }

    @Override
    public String toString() {
        return "HCol [name=" + name + ", type=" + type + ", size=" + size + ", isNullable=" + isNullable
                + ", isForeign=" + isForeign + "]";
    }

    public FK getForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(FK foreignKey) {
        this.foreignKey = foreignKey;
    }

}
