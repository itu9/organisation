package extract.scafold;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import extract.scafold.orm.ORM;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

@SpringBootApplication
public class Executor {
    public static String ignorePath = "F:\\Compose\\back\\ingored.h";
    @Autowired
    public ORM orm;

    public static String path = "F:\\Compose\\back\\src\\main\\java\\";
    public static String pathFront = "F:\\Compose\\front\\src\\components\\organisation\\";

    public static String appPath = "F:\\Compose\\front\\src\\App.jsx\\";

    public static String containerPath = "F:\\Compose\\front\\src\\components\\client\\nav\\HContainer.jsx";

    public static String mainFrontPackage = "organisation";

    public static void main(String[] args) throws TemplateNotFoundException, MalformedTemplateNameException,
            ParseException, IOException, TemplateException, ClassNotFoundException {
        // TypeTrad.scan();
        TypeTrad.show();
        // Creator creator = new FrontCreateur(cls, "/mouvement", Executor.path);
        // creator.init();
        // String str = ((FrontCreateur) creator).getFileName();
        // System.out.println(str);
        // creator.createSearch();
        // Class<?> cls = Stock.class;
        // Creator creator = new FrontCreateur(cls, "/vente/details", Executor.path);
        // creator.init();
    }

    // Class<?> cls = Stock.class;
    // Creator creator = new FrontCreateur(cls, "/vente/details", Executor.path);
    // String str = ((FrontCreateur) creator).getFileName();
    // System.out.println(str);
    // creator.createSearch();

    public void freemaker() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException,
            IOException, TemplateException {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_31);
        configuration.setClassForTemplateLoading(Executor.class, "/templates");

        Map<String, Object> dataModel = new HashMap<>();
        dataModel.put("packageName", "com.example.model");
        dataModel.put("className", "MyModel");

        Template template = configuration.getTemplate("model.ftl");

        StringWriter stringWriter = new StringWriter();
        template.process(dataModel, stringWriter);
        String str = stringWriter.toString();
    }
}
