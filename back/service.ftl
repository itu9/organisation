package package extract.marque;;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import extract.model.HCrud;


@Service 
public class MarqueService extends HCrud<Marque, Integer, MarqueRepository,MarqueSearch,MarqueUtils> {
    @Autowired 
    @Override 
    public void setRepos(MarqueRepository repos) {
        super.setRepos(repos);
    }
}
