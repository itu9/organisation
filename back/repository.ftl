package package extract.marque;;


import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 

public interface MarqueRepository extends JpaRepository<Marque,Integer> {

}
