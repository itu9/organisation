package extract.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;

import com.google.gson.Gson;

import lombok.Data;

@Data
public class Cnt {

    @Autowired
    Gson gson;

    @Autowired
    JavaMailSender sender;

    public ResponseEntity<SuccessResponse> returnSuccess(Object obj, HttpStatus status) throws Exception {
        return new ResponseEntity<>(new SuccessResponse(obj), status);
    }

    public ResponseEntity<SuccessResponse> returnSuccess(Object obj) throws Exception {
        return new ResponseEntity<>(new SuccessResponse(obj), HttpStatus.OK);
    }

    public ResponseEntity<ErrorDisplay> returnError(Exception obj, HttpStatus status) {
        obj.printStackTrace();
        return new ResponseEntity<>(new ErrorDisplay(status, obj), status);
    }

    public ResponseEntity<ErrorDisplay> returnError(Exception obj) {
        return this.returnError(obj, HttpStatus.NOT_FOUND);
    }

}
