package extract.marque;

Object idhmin;Object idhmax;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.Search;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class Marque extends Search<MarqueSearch> {
     sAttr[1]
    public Marque() {
        super(MarqueSearch.class);
    }
};