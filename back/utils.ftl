package package extract.marque;;

import java.lang.Object;
import lombok.Data;
import lombok.EqualsAndHashCode;
import extract.controller.FormUtils;

@Data 
@EqualsAndHashCode(callSuper = false) 
public class MarqueUtils extends Search<Marque> {
    
    public MarqueUtils() {
        super(Marque.class);
    }
};