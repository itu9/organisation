-- create database organisation;
-- \c pres

-- Client
CREATE TABLE client
(
    id           serial primary key,
    nom          varchar(40),
    prenom       varchar(40),
    email        varchar(40),
    mdp          varchar(40),
    naissance    date,
    acreditation integer
);

CREATE TABLE token
(
    id      serial primary key,
    dateins timestamp without time zone,
    value   varchar(150),
    loginid int
);
alter table token
    add foreign key (loginid) references client (id);

CREATE TABLE tokenexp
(
    duree time without time zone NOT NULL
);
CREATE
    or replace PROCEDURE removetoken()
    LANGUAGE SQL
AS
$$
delete
from token
where dateins < current_timestamp - (select duree from tokenexp);
$$;

insert into tokenexp
values ('24:00:00');
-- Fin Client

-- Ajout lieu
create table Lieu
(
    id      serial primary key,
    titre   varchar(50),
    nbplace double precision,
    prix    double precision
);

create table Type
(
    id    serial primary key,
    titre varchar(50)
);

create table Typedepense
(
    id    serial primary key,
    titre varchar(50)
);

create table depensefixe
(
    id            serial primary key,
    titre         varchar(50),
    tarif         double precision,
    typeid        integer,
    typedepenseid integer,
    duree         double precision
);
alter table depensefixe
    add foreign key (typeid) references Type (id);
alter table depensefixe
    add foreign key (typedepenseid) references Typedepense (id);

create table autredepense
(
    id    serial primary key,
    titre varchar(50)
);

create table artiste
(
    id    serial primary key,
    nom   varchar(50),
    tarif double precision,
    duree double precision
);

create table devis
(
    id       serial primary key,
    titre    varchar(50),
    lieuid   integer,
    prixlieu double precision
);
alter table devis
    add foreign key (lieuid) references Lieu (id);

create table devisartiste
(
    id        serial primary key,
    artisteid integer,
    devisid   integer,
    duree     double precision
);
alter table devisartiste
    add foreign key (artisteid) references artiste (id);
alter table devisartiste
    add foreign key (devisid) references Devis (id);

create table devisfixe
(
    id            serial primary key,
    depensefixeid integer,
    devisid       integer,
    duree         double precision
);
alter table devisfixe
    add foreign key (depensefixeid) references depensefixe (id);
alter table devisfixe
    add foreign key (devisid) references Devis (id);

create table devisautre
(
    id      serial primary key,
    autreid integer,
    devisid integer,
    prix    double precision
);
alter table devisautre
    add foreign key (devisid) references devis (id);
alter table devisautre
    add foreign key (autreid) references autredepense (id);

alter table artiste
    add column photo text;
alter table lieu
    add column photo text;

create table Categories
(
    id    serial primary key,
    titre varchar(50)
);

create table CategoriesLieu
(
    id           serial primary key,
    lieuid       integer,
    categoriesid integer,
    nbplace      double precision
);
alter table CategoriesLieu
    add foreign key (lieuid) references Lieu (id);
alter table CategoriesLieu
    add foreign key (categoriesid) references Categories (id);

create table DevisBillet
(
    id               serial primary key,
    categorieslieuid integer,
    devisid          integer,
    prix             double precision
);
alter table DevisBillet
    add foreign key (devisid) references Devis (id);
alter table DevisBillet
    add foreign key (categorieslieuid) references CategoriesLieu (id);

alter table devis add column date date;
alter table devis add column  hour time;

create table percent (
    id serial primary key ,
    value double precision
);

alter table devisbillet add column place double precision;

alter table devis add column etat integer default 0;

create or replace view spectacle as
    select d.id,
           d.titre,
           d.lieuid,
           d.prixlieu,
           d.date,
           d.hour,
           d.etat
    from devis d
    where d.etat >=25;