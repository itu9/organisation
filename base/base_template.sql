-- create database organisation;
-- \c pres

-- Client
CREATE TABLE client (
    id serial primary key,
    nom varchar(40),
    prenom varchar(40),
    email varchar(40),
    mdp  varchar(40),
    naissance date,
    acreditation integer
);

CREATE TABLE token (
    id serial primary key,
    dateins timestamp without time zone,
    value varchar(150),
    loginid int
);
alter table token add foreign key(loginid) references client(id);

CREATE TABLE tokenexp (
    duree time without time zone NOT NULL
);
CREATE or replace PROCEDURE removetoken()
LANGUAGE SQL
AS $$
    delete from token where dateins < current_timestamp - (select duree from tokenexp);
$$;

insert into tokenexp values('24:00:00');
-- Fin Client