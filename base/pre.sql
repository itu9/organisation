-- create database pres;
-- \c pres

-- Client
CREATE TABLE client (
    id serial primary key,
    nom varchar(40),
    prenom varchar(40),
    email varchar(40),
    mdp  varchar(40),
    naissance date,
    acreditation integer
);

CREATE TABLE token (
    id serial primary key,
    dateins timestamp without time zone,
    value varchar(150),
    loginid int
);
alter table token add foreign key(loginid) references client(id);

CREATE TABLE tokenexp (
    duree time without time zone NOT NULL
);
CREATE or replace PROCEDURE removetoken()
LANGUAGE SQL
AS $$
    delete from token where dateins < current_timestamp - (select duree from tokenexp);
$$;

insert into tokenexp values('24:00:00');
-- Data client
insert into client(id,acreditation,email,nom,prenom,mdp) values
    (1,25,'client.hasina@gmail.com','Hasina','Juniolah','hasina'),
    (2,25,'client.hery@gmail.com','Hery','Haja','hery'),
    (3,50,'admin@gmail.com','Admin','Admin','admin');
-- Fin Client

-- Regle de gestion
create table regletype (
    id serial primary key,
    titre varchar(20)
);

create table regle (
    id serial primary key,
    valeur double precision not null default 0,
    regletypeid integer
);
alter table regle add foreign key(regletypeid) references regletype(id);
-- Fin Regle de gestion

-- Rechargement argent
create table recharge(
    id serial primary key,
    clientid int,
    montant double precision NOT NULL default 0,
    etat int not null--0:entree 50:sortie 
);
alter table recharge add foreign key(clientid) references client(id);

create table rnotaccept(
    id serial primary key,
    clientid int,
    montant double precision not null default 0,
    etat int not null default 0--0:entree 50:sortie 
);
alter table rnotaccept add foreign key(clientid) references client(id);

CREATE OR REPLACE PROCEDURE acceptrecharge(idR INTEGER)
as $$
Declare
    rechargerow rnotaccept%ROWTYPE;
BEGIN
    select * into rechargerow from rnotaccept where id = idR;
    insert into recharge(clientid,montant,etat) values(rechargerow.clientid,rechargerow.montant, 0);
    update rnotaccept set etat=50 where id = idR;
END;
$$ LANGUAGE plpgsql;

-- Etat d'argent
create or replace view v_argent as 
    select r.clientid,
        sum(coalesce(r.montant,0)) montant,
        r.etat
    from recharge r 
    group by r.clientid,r.etat;

create table etat (
    value integer primary key,
    signe varchar(20)
);
insert into etat values(0,'entré'),(50,'sortie');

create or replace view etatclient as 
    select c.id,
        e.value
    from etat e 
    cross join client c;

create or replace view v_etatargent as 
    select ec.id clientid,
        ec.value etat,
        coalesce(a.montant,0) montant,
        row_number() over() id
    from etatclient ec
    left join v_argent a 
    on ec.id = a.clientid
    and ec.value = a.etat;

create or replace view etatargent as 
    select en.clientid,
        en.montant entree,
        so.montant sortie,
        en.montant-so.montant solde,
        row_number() over() id
    from (select * from v_etatargent where etat =0 ) as en
    left join (select * from v_etatargent where etat =50 ) as so
    on en.clientid = so.clientid;

-- Pres
create table presclient (
    id serial primary key,
    clientid integer,
    date date not null default current_date,
    valeur double precision not null default 0,
    duration integer not null default 0,
    etat integer not null default 0
);
alter table presclient add foreign key(clientid) references client(id);

CREATE OR REPLACE PROCEDURE acceptpres(idR INTEGER)
as $$
Declare
    rechargerow presclient%ROWTYPE;
BEGIN
    select * into rechargerow from presclient where id = idR;
    insert into recharge(clientid,montant,etat) values(rechargerow.clientid,rechargerow.valeur, 0);
    insert into recharge(clientid,montant,etat) values(0,rechargerow.valeur, 50);
    update presclient set etat=50 where id = idR;
END;
$$ LANGUAGE plpgsql;

insert into presclient(clientid,date,valeur,duration,etat) values(1,current_date,17020,12,0);
insert into presclient(clientid,date,valeur,duration,etat) values(1,current_date,3000,5,0);
-- Fin pres

-- Remboursement
create table remboursement (
    id serial primary key,
    date date not null default current_date,
    valeur double precision not null default 0,
    mois integer not null default 0,
    annee integer not null default 0,
    presclientid integer
);
alter table remboursement add foreign key(presclientid) references presclient(id);

create or replace view rembmois as
    select re.mois,
        re.annee,
        sum(coalesce( re.valeur,0)) valeur,
        row_number() over() id,
        p.clientid,
        re.presclientid
    from remboursement re 
    left join presclient p 
    on re.presclientid = p.id
    group by p.clientid,re.annee,re.mois,re.presclientid;

create or replace view allrem as 
    select re.mois,
        re.annee,
        sum(coalesce( re.valeur,0)) valeur,
        re.clientid,
        row_number() over() id
    from rembmois re 
    group by re.clientid,
    re.annee,
    re.mois;