-- create database compose ;
-- \c compose

create table produit (
    id serial primary key,
    titre varchar(40),
    pu double precision not null default 0,
    quantite double precision not null default 0
);

-- mouvement
create table mouvement (
    id serial primary key,
    produitid integer,
    quantite double precision,
    date date not null default current_date,
    etat integer -- 0 entree, 50 sortie
);
alter table mouvement add foreign key(produitid) references produit(id);

create or replace view vmouvement as
    select m.produitid,
        sum(coalesce(m.quantite,0)) quantite,
        m.etat
    from mouvement m
    group by m.produitid,m.etat;

create or replace view stock as 
    select en.produitid,
        coalesce(en.quantite,0)-coalesce(so.quantite,0) quantite,
        ((coalesce(en.quantite,0)-coalesce(so.quantite,0))*p.pu)/p.quantite prix,
        row_number() over() id
    from (select e.* from vmouvement e where e.etat = 0) as en 
    left join (select s.* from vmouvement s where s.etat = 50) as so 
    on en.produitid = so.produitid
    left join produit p 
    on en.produitid = p.id; 
-- fin mouvement

-- vente
create table vente (
    id serial primary key,
    produitid integer,
    date date,
    quantite double precision
);
alter table vente add foreign key (produitid) references produit(id);

-- achat 
create table achat (
    id serial primary key,
    produitid integer,
    date date,
    quantite double precision
);
alter table achat add foreign key (produitid) references produit(id);
-- fin achat

-- compose
create table compose (
    id serial primary key,
    produitid integer,
    ingredientid integer,
    quantite double precision
);
alter table compose add foreign key(produitid) references produit(id);
alter table compose add foreign key(ingredientid) references produit(id);

-- Ajout prix de vente
alter table produit add column pvente double precision default 0 not null ;