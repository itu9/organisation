import HContainer from "../client/nav/HContainer";
import Border from "../utils/Border";
import Card from "../utils/Card";
import Generation from "../utils/Generation";
import ListePage from "../utils/ListePage";
import HSearch from "../utils/Search";

export default class Test extends Generation {
    state = {
        person : {
            liste : [],
            limit : 2,
            count : 1,
        },
        img : ''
    }
    componentDidMount() {
        this.getPers(1,this.state.person.limit)
    }

    getPers = (page,limit) => {
        this.getListe(this.temp+'/person?_page='+page+'&_limit='+limit,
            (data) => {
                let pers = this.state.person;
                pers.liste = data;
                this.setState({
                    person : pers
                })
            },
            (header) => {
                let pers = this.state.person;
                pers.count = header.get('x-total-count')/this.state.person.limit;
                this.setState({
                    person : pers
                })
            }
        )
    }

    loadPers = (event,value) => {
        this.getPers(value,this.state.person.limit)
    }



    search = () => {
        this.getPers(2,this.state.person.limit)
    }

    render() {
        return (
             <HContainer>
                <ListePage title="Liste des pages"
                    count={this.state.person.count}
                    search = {
                        <>
                            <HSearch btnSize="4"
                                onClick={this.search}
                            >
                                <div className="col-4 mt-3">
                                    <h4>Name</h4>
                                    <input className="form-control"></input>
                                </div>
                                <div className="col-12"></div>
                                <div className="col-4 mt-3">
                                    <h4>Name</h4>
                                    <input className="form-control"></input>
                                </div>
                                <div className="col-12"></div>
                            </HSearch>
                        </>
                    }
                    head = {
                        <>
                            <tr>
                                <th>Nom</th>
                                <th>Prenom</th>
                            </tr>
                        </>
                    }
                    onChange ={ this.loadPers}
                >
                    {
                        this.state.person.liste.map(person => (
                            <tr key={person.id}>
                                <td>{person.name}</td>
                                <td>{person.firstname}</td>
                            </tr>
                        ))
                    }
                </ListePage>
                <div className="row">
                    {
                        this.state.person.liste.map((person,index) => (
                            <Card key={index} link={'data:image/png;base64,'+this.state.img}>
                                <h4>{person.name} {person.firstname}</h4>
                            </Card>
                        ))
                    }
                </div>
                <div className="row">
                    <div className="col-6">
                        <Border>
                            <input className="input-group-prepend form-control" type="file" onChange={this.changeImg}></input>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}