import { Line } from "react-chartjs-2";
import HContainer from "../client/nav/HContainer";
import Border from "../utils/Border";
import General from "../utils/General";
import {  Chart, registerables  } from "chart.js";
import React from "react";
Chart.register(...registerables)

export default class ChiffreAffaire extends General {
    constructor(params) {
        super(params);
        this.data = React.createRef(null);
        this.date = React.createRef(null);
        this.data.current = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
            {
                label: 'Pomme de terre',
                data: [65, 59, 80, 81, 56, 55, 40],
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgba(255, 99, 132, 0.2)',
            }
            ],
          }
    }
    componentDidMount() {
        this.setState({
            date : []
        },this.getData);
    }

    getData = () => {
        this.getListe(this.url+"/years",
            data => {
                this.verifData(data,
                    response => {
                        this.setState({
                            date : response.data
                        },
                            ()=> {
                                let data = {
                                    anneehmin : this.state.date[0].annee,
                                    anneehmax : this.state.date[0].annee
                                }
                                this.search(data);
                            }
                        )
                    }
                )
            },
            header => {

            }
        )
    }

    search = (dataSend) => {
        this.sendData(this.url+'/chiffremois/filter',dataSend,
            (data) => {
                this.verifData(data,
                    response => {
                        this.toStat(response.data,dataSend)
                    }
                )
            }
        )
    }

    toStat = (response,dataSend) => {
        let data = response.map(res => res.total);
        let dataField = {
            labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet','Aout','Septembre','Octobre','Novembre','Décembre'],
            datasets: [
            {
                label: `Chiffre d'affaire `+dataSend.anneehmin,
                data: data,
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgba(255, 99, 132, 0.2)',
            }
            ],
          };
        this.data.current = dataField;
        this.setState({
            data : dataField
        })
    }

    load = () => {
        let data = {
            anneehmin : this.date.current.value,
            anneehmax : this.date.current.value
        }
        this.search(data);
    }
        
    render() {
        return (
             <HContainer>
                <Border>    
                    <h2>Chiffre d'affaire par mois</h2>
                    <div className="row">
                        <div className="col-4">
                            <h3>Annee</h3>
                            <select className="form-control" ref={this.date} onChange={this.load}>
                                {
                                    this.state.date !== undefined && this.state.date !== null ?
                                    this.state.date.map(date => (
                                        <option value={date.annee} >{date.annee}</option>
                                    ))
                                    :<></>
                                }
                            </select>
                        </div>
                    </div>
                    {
                        this.data !== undefined && this.data !== null ?
                        <Line data={this.data.current} /> : <></>
                    }
                    
                </Border>
             </HContainer>
        );
    }
}