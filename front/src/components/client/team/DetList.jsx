import { Component } from "react";

export default class DetList extends Component {
    getColor = () =>{
        if (this.props.color !== null && this.props.color !== undefined) {
            return this.props.color;
        }
        return "dark"
    }

    render() {
        return (
             <div className="mt-3">
                <h3 className={"text-"+this.getColor()}><strong>{this.props.title} </strong></h3>
                <ul>
                    <>{
                        this.props.children    
                    }</>
                </ul>
             </div>
        );
    }
}