import { Component } from "react";
import OwnService from "../../../service/OwnService";
import HMiddle from "../services/HMiddle";
import Developper from "./Developper";

export default class Team extends Component {
    constructor(params) {
        super(params);
        this.state = {
            developper : []
        }
    }

    componentDidMount() {
        OwnService.getData(OwnService.url+'api/developers',
            (data) => {
                this.setState({
                    developper : data
                })
            }
        )
    }
    render() {
        return (
             <>
                <HMiddle
                    title="Our team"
                >
                    <div className="row">
                        {
                            this.state.developper.map(dev => (
                                <Developper developper = {dev} key={dev.id}></Developper>
                            ))
                        }
                    </div>
                </HMiddle>
             </>
        );
    }
}