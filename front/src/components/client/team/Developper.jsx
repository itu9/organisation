import { Avatar, Card, CardContent, CardHeader } from "@mui/material";
import { Component } from "react";
import DetList from "./DetList";
import { red } from '@mui/material/colors';
import './Developper.css'

export default class Developper extends Component {

    getSize = () => {
        if (this.props.size !== null && this.props.size !== undefined) {
            return this.props.size;
        }
        return '4';
    }
    render() {
        return (
            <div className={"col-xl-"+this.getSize()+" col-md-4 mb-5"}>
             <Card sx={{maxWidth : 345, boxShadow:"2px 2px 10px"}}>
                    <CardHeader
                        avatar={
                            <Avatar sx={{ bgcolor: red[500]}} aria-label="recipe">
                              {this.props.developper.name[0]}
                            </Avatar>
                          }
                          title={<h4>{this.props.developper.name}</h4>}
                          style={{fontSize:'36px'}}
                    >
                    </CardHeader>
                    <CardContent>
                    <DetList title="Specialization">
                        {
                            this.props.developper.specializations.map((sp,index) => (
                                <li key={index}> {sp.title}</li>
                            ))
                        }
                    </DetList>
                    <DetList title="Technologies maitrisées">
                        {
                            this.props.developper.technologies.map((tc,index) => (
                                <li key={index}>{tc.name}</li>
                            ))
                        }
                    </DetList>
                    <DetList title="Email">
                        <li>{this.props.developper.contact}</li>
                    </DetList>
                    <DetList title="Description">
                        <li>{this.props.developper.description}</li>
                    </DetList>
                    {
                        (this.props.admin !== undefined) ?
                            <button className="btn w-100 hremove mt-3"
                                onClick={
                                    () => {
                                        this.props.update(this.props.developper)
                                    }
                                }
                            >Update</button> 
                            : 
                            <></>
                    }
                        
                    </CardContent>
             </Card>
                    
                    </div>
        );
    }
}