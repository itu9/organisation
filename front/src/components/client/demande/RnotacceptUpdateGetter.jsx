import { useParams } from "react-router-dom";
import RnotacceptUpdate from "./RnotacceptUpdate";

export default function RnotacceptUpdateGetter() {
    const {id} = useParams();
    return (
        <RnotacceptUpdate id={id}>
        </RnotacceptUpdate>
    );
}