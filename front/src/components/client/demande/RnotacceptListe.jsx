import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";
import Swal from "sweetalert2";

export default class RnotacceptListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.clientid = React.createRef(null);
    this.montanthmin = React.createRef(null);
    this.montanthmax = React.createRef(null);
    this.etathmin = React.createRef(null);
    this.etathmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/demande/recharge/filter";
    this.urlUtils = "/demande/recharge/utils";
    this.baseUrl = "/demande/recharge";
  }
  componentDidMount() {
    this.init();
    this.checkConnexion();
  }
  findData = () => {
    let data = {
      keys: this.prepare(this.keys),
      clientid: this.checkRefNull(
        this.clientid,
        { id: this.prepare(this.clientid) },
        null
      ),
      montanthmin: this.prepare(this.montanthmin),
      montanthmax: this.prepare(this.montanthmax),
      etathmin: this.prepare(this.etathmin),
      etathmax: this.prepare(this.etathmax),
    };
    this.search(data);
  };

  getSearchUrl = () => {
    let auth = this.getSession(this.auth,{
      login : {
        id : 0
      }
    })
    return this.url+this.urlData+"?idUser="+auth.login.id
  }

  accept = (id) => {
    let auth = this.getSession(this.auth,{
      login : {
        id : 0
      }
    })
    this.getListe(this.url+this.baseUrl+"/accept/"+id+"?idUser="+auth.login.id,
      (data) => {
        this.verifData(data,
          response => {
            Swal.fire({
              icon : 'success',
              title : 'Felication',
              text : 'Validation prise en compte pas le systeme.',
              timer : 1800
            })
          }
        )
      }
    );
  }

  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Client</h3>
                      <select className="form-control" ref={this.clientid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.clientid.map((data, index) => (
                            <option value={data.id}>{data.email}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Montant</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.montanthmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.montanthmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Etat</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.etathmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.etathmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace(
                                    "/demande/recharge/form"
                                  );
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Clientid</td>
                    <td>Montant</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.clientid.nom} {data.clientid.prenom}</td>
                      <td>{data.montant}</td>
                      <td>
                       
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.accept(data.id)
                          }}
                        >
                          <i className="fas fa-check text-succes"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
