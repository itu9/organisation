import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class RnotacceptForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.clientid = React.createRef(null);
    this.montant = React.createRef(null);
    this.etat = React.createRef(null);
    this.urlSend = "/demande/recharge";
    this.urlUtils = "/demande/recharge/utils";
    this.afterValidation = "";
  }
  componentDidMount() {
    this.init();
    this.checkConnexion();
  }



  validateData = () => {
    let auth = this.getSession(this.auth,null)
    let data = {
      clientid: {
        id : auth.login.id
      },
      montant: this.prepare(this.montant),
      etat: 0
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Recharger mon compte</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              
              <h3 className="text-info mt-3">Montant</h3>
              <input
                type="text"
                className="form-control"
                ref={this.montant}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
