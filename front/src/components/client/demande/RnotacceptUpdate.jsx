import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class RnotacceptUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.clientid = React.createRef(null);this.montant = React.createRef(null);this.etat = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/demande/recharge";
        this.urlUtils = "/demande/recharge/utils";
        this.afterValidation = "/demande/recharge";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.clientid.current.value = this.state.oneValue.clientid.id;this.montant.current.value = this.state.oneValue.montant;this.etat.current.value = this.state.oneValue.etat;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            clientid : this.checkRefNull(this.clientid,{id:this.prepare(this.clientid)},null),montant : this.prepare(this.montant),etat : this.prepare(this.etat),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Client</h3>
<select className="form-control" ref={this.clientid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.clientid.map((data,index) => (
            <option value={data.id} >{data.email}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Montant</h3>
<input type="text" className="form-control" ref={this.montant} ></input><h3 className="text-info mt-3">Etat</h3>
<input type="text" className="form-control" ref={this.etat} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}