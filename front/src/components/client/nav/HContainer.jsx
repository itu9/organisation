/* eslint-disable react/prop-types */
import  { Component } from "react";
import Footer from "./Footer";
import Header from "./Header";
import "./HContainer.css";

export default class HContainer extends Component {
  constructor(params) {
    super(params);
    this.state = {
      email: "itfreel@gmail.com",
      contact: "No contact",
      appear: true,
      lien: {
        global :[

        ],
        client : [
            {
            title: "Spectacle",
            link: "/spectacle",
          },
          {
            title: "Devis",
            link: "/devis",
          },
          {
            title: "Devis billet",
            link: "/devis/billet",
          },
          {
            title: "Devis artiste",
            link: "/devis/artiste",
          },
          {
            title: "Devis fixe",
            link: "/devis/fixe",
          },
          {
            title: "Autre devis",
            link: "/devis/autre",
          }
        ],
        admin : [
            {
            title: "Taxe",
            link: "/percent",
          },
          {
            title: "Lieu",
            link: "/lieu",
          },
          {
            title: "Categorie de lieu",
            link: "/categories",
          },
          {
            title: "Nombre de place",
            link: "/categories/lieu",
          },
          {
            title: "Depense fixe",
            link: "/depensefixe",
          },
          {
            title: "Autre depense",
            link: "/autre/depense",
          },
          {
            title: "Type",
            link: "/type",
          },
           {
            title: "Type de depense",
            link: "/typedepense",
          },
           {
            title: "Artiste",
            link: "/artiste",
          },
           {
            title: "Employe",
            link: "/role",
          }
        ]
      } ,
      multiple: [
        {
          title: "Sign in as",
          list: [
            {
              title: "Client",
              link: "/login/client",
            },
          ],
        },
      ],
    };
  }

  setVisible = () => {
    this.setState({
      appear: !this.state.appear,
    });
  };

  render() {
    return (
      <>
        <div
          id="main-wrapper"
          data-theme="light"
          data-layout="vertical"
          data-navbarbg="skin6"
          data-sidebartype="full"
          data-sidebar-position="fixed"
          data-header-position="fixed"
          data-boxed-layout="full"
        >
          <Header key={150} />
          <Footer key={300} link={this.state.lien}></Footer>

          <div className="page-wrapper" key={13}>
            <div className="container-fluid">{this.props.children}</div>
          </div>
        </div>
      </>
    );
  }
}
