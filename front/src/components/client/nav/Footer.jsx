/* eslint-disable react/prop-types */
import './Footer.css'
import General from "@/components/utils/General";
export default class Footer extends General {

    getPages = () => {
        return this.state.pages
    }

    getLink = () => {
        return this.props.link
    }

    getAccred = () => {
        let user = this.getSession(this.auth,{login:{acreditation : 0}})
        return user.login.acreditation;
    }
    render() {
        return (
             <>
             <aside className="left-sidebar hfooter d-print-none" data-sidebarbg="skin6" >
            
            <div className="scroll-sidebar" data-sidebarbg="skin6">
                
                <nav className="sidebar-nav">
                    <ul id="sidebarnav">
                        {
                            this.props.link.global.map((link,index) => (
                                <>
                                    <li className="sidebar-item" key={"side_"+index}> 
                                        <a key={'foot_a_'+index} className="sidebar-link sidebar-link" href={link.link}
                                            aria-expanded="false">
                                            <i key={'i_'+index} data-feather="home" className="feather-icon"></i>
                                            <span key={'sp_'+index} className="hlink">{link.title}</span>
                                        </a>
                                    </li>
                                    <li className="list-divider " key={"side1_"+index}>
                                        
                                    </li>
                                </>
                            ))
                        }
                        {
                            this.getAccred() === 50 ?
                            this.props.link.admin.map((link,index) => (
                                <>
                                    <li className="sidebar-item" key={"side_"+index}> 
                                        <a key={'foot_a_'+index} className="sidebar-link sidebar-link" href={link.link}
                                            aria-expanded="false">
                                            <i key={'i_'+index} data-feather="home" className="feather-icon"></i>
                                            <span key={'sp_'+index} className="hlink">{link.title}</span>
                                        </a>
                                    </li>
                                    <li className="list-divider " key={"side1_"+index}>
                                        
                                    </li>
                                </>
                            )):<></>
                        }
                        {
                            this.getAccred() === 25 ?
                            this.props.link.client.map((link,index) => (
                                <>
                                    <li className="sidebar-item" key={"side_"+index}> 
                                        <a key={'foot_a_'+index} className="sidebar-link sidebar-link" href={link.link}
                                            aria-expanded="false">
                                            <i key={'i_'+index} data-feather="home" className="feather-icon"></i>
                                            <span key={'sp_'+index} className="hlink">{link.title}</span>
                                        </a>
                                    </li>
                                    <li className="list-divider " key={"side1_"+index}>
                                        
                                    </li>
                                </>
                            )):<></>
                        }
                        
                    </ul>
                </nav>
                
            </div>
            
        </aside>
             </>
        );
    }
}