import { Component } from "react";
import "./Footer.css";
export default class Header extends Component {
    auth = "token";
  constructor(params) {
    super(params);
    this.state = {
      auth: null,
    };
  }
    getSession = (key, defaultValue) => {
        let data = sessionStorage.getItem(key);
        return data === null || data === undefined
        ? defaultValue
        : JSON.parse(data);
    };
    componentDidMount() {
        let user = this.getSession(this.auth,null);
        this.setState({
            auth : user
        })
    }

    deconnect = () => {
        sessionStorage.removeItem(this.auth)
        window.location.replace("/")
    }
  render = () => {
    return (
      <>
        <header className="topbar hfooter" data-navbarbg="skin6">
          <nav className="navbar top-navbar navbar-expand-md">
            <div className="navbar-header" data-logobg="skin6">
              <a
                className="nav-toggler waves-effect waves-light d-block d-md-none"
                href="#a"
              >
                <i className="ti-menu ti-close"></i>
              </a>
              <div className="navbar-brand">
                <a href="/assistant">
                  <b className="logo-icon">
                    <img
                      src="/assets/images/logo-icon.png"
                      alt="homepage"
                      className="dark-logo"
                    />
                    <img
                      src="/assets/images/logo-icon.png"
                      alt="homepage"
                      className="light-logo"
                    />
                  </b>
                  <span className="logo-text">
                    <img
                      src="/assets/images/logo-text.png"
                      alt="homepage"
                      className="dark-logo"
                    />
                    <img
                      src="/assets/images/logo-light-text.png"
                      className="light-logo"
                      alt="homepage"
                    />
                  </span>
                </a>
              </div>
            </div>
              <div
                className="navbar-collapse collapse float-right "
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav h_header_avatar">
                  <li className="nav-item dropdown">
            {this.state.auth !== null && this.state.auth !== undefined ? (
                <>
                    <a
                      className="nav-link dropdown-toggle "
                      href="#a"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <img
                        src="/assets/images/users/profile-pic.jpg"
                        alt="user"
                        className="rounded-circle"
                        width="40"
                      />
                      <span className="ml-2 d-none d-lg-inline-block">
                        <span>Hello,</span>
                        <span className="text-dark">{this.state.auth.login.prenom}</span>
                        <i data-feather="chevron-down" className="svg-icon"></i>
                      </span>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                      <button className="dropdown-item" onClick={this.deconnect}>
                        <i
                          data-feather="power"
                          className="svg-icon mr-2 ml-1"
                        ></i>
                        Se deconnecter
                      </button>
                    </div>
                </> ): <>    
                    <button className="btn btn-info" onClick={()=>{window.location.replace("/")}}>
                                Se connecter
                            </button>
                </>
            }
                  </li>
                </ul>
              </div>
            
          </nav>
        </header>
      </>
    );
  };
}
