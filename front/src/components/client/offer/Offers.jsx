import { Component } from "react";
import OwnService from "../../../service/OwnService";
import HMiddle from "../services/HMiddle";
import Offer from "./Offer";

export default class Offers extends Component {
    constructor(params) {
        super(params);
        this.state = {
            offers : []
        };
    }

    componentDidMount() {
        OwnService.getData(OwnService.url + 'api/services',
            (data) => {
                this.setState({
                    offers : data
                })
            }
        )
    }
    render() {
        return (
             <>
                <HMiddle title="Services">
                    <div className="container">
                        <div className="row">
                            {
                                this.state.offers.map(offer => (
                                    <Offer key={offer.id} offer={offer}></Offer>
                                ))
                            }
                        </div>
                    </div>
                </HMiddle>
             </>
        );
    }
}