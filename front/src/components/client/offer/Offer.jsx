import { Component } from "react";
import HCard from "../../../service/HCard";
import './Offer.css'
export default class Offer extends Component {
    constructor(params) {
        super(params);
        this.state = {
            cls : 'truncate'
        }
    }

    flip = () => {
        if (this.state.cls === '') {
            this.setState({
                cls : 'truncate'
            })
        } else {
            this.setState({
                cls : ''
            })
        }
    }

    render() {
        return (
            <>
                <div className="col-lg-4 mb-3 hoffer">
                    <HCard>
                        <div className="content">
                            <strong>{this.props.offer.servicename}</strong>
                            <p className={this.state.cls} onClick={() => {this.flip()}}>{this.props.offer.description}</p>
                            <button className="btn w-100 contact "><strong> Contact Us</strong></button>
                        </div>
                    </HCard>
                </div>
            </>
        );
    }
}