import { Pagination } from "@mui/material";
import { Component } from "react";
import OwnService from "../../../service/OwnService";
import HMiddle from "./HMiddle";
import Service from "./Service";

export default class Services extends Component {

    constructor(params) {
        super(params);
        this.state = {
            projets : [],
            page : 10
        }
    }

    onChange = (event,value) => {
        this.init(value)
    }

    componentDidMount() {
        this.init(1);
    }
    init = (page) => {
        OwnService.getData(OwnService.url+'api/projects?page='+page,
            (data) => {
                this.setState({
                    projets : data.data,
                    page : data.last_page
                })
            }
        )
    }
    render() {
        return (
             <>
                <HMiddle
                    title = "Implementation of our skills"
                >
                    <div className="row">
                            {
                                this.state.projets.map(pr => (

                                    <Service key={pr.id} project={pr}></Service>
                                ))
                            }
                        </div>
                            <div className="row">
                                <div className="col-lg-6"></div>
                                <div className="col-lg-6">
                                    <Pagination count={this.state.page} size="large" color="primary" onChange={this.onChange} />
                                </div>
                            </div>
                </HMiddle>
             </>
        );
    }
}