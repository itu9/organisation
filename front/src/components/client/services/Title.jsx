import { Component } from "react";

export default class Title extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className="section_title text-center mb-50">
                        <h3>{this.props.title}</h3>
                    </div>
                </div>
            </div>
        );
    }
}