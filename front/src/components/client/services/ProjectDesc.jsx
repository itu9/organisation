import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import HCard from "../../../service/HCard";
import OwnService from "../../../service/OwnService";
import Comments from "../feedback/Comments";
import HContainer from "../nav/HContainer";
import DetList from "../team/DetList";
import HMiddle from "./HMiddle";
import './Service.css'

export default function ProjectDesc(props) {
    const [project,setProject] = useState(null);
    const {id} = useParams();
    useEffect(() => {
        OwnService.getData(OwnService.url+'api/projects/'+id, 
            (data) => {
                // console.log(data);
                setProject(data)
            }
        )
    }, [id])
    return (
        <HContainer>
            <div className="service_area">
                {
                    project !== null?
                    <HMiddle title="Describe a project">
                        <div className="row">
                            <div className="col-lg-6 mb-5">
                            <h1>{project.title}</h1>
                                <DetList title="">
                                    <li>
                                        <p>{project.description}</p>
                                    </li>
                                </DetList>
                                <DetList title="Link">
                                    <li>
                                        <button className="hpreview btn w-100" onClick={
                                            () => {
                                                window.location.replace(project.project_link);
                                            }
                                        }>Preview</button>
                                    </li>
                                </DetList>
                                <br/>
                                <DetList title="Technology">
                                    {
                                        project.technologies.map((tech,index) => (
                                            <li key={index}>{tech.name}</li>
                                        ))
                                    }
                                </DetList>
                                <br/>
                            </div>
                            <div className="col-lg-6 mb-5">
                                <HCard>
                                <div className="about_image">
                                    <img src="/assets/img/about/1.png" alt="" width={'100%'}/>
                                </div>
                                </HCard>
                            </div>
                            <div className="col-lg-12 mt-5 ">
                            <HCard>
                                <h2>Feed back</h2>
                                    <br/>
                                    <Comments projectid={id} feedback={project.feedbacks}></Comments>
                            </HCard>
                            </div>
                        </div>
                    </HMiddle>
                    : <></>
                }
            </div>
        </HContainer>
    )
}