import { Component } from "react";
import HCard from "../../../service/HCard";
import './Service.css'
export default class Service extends Component {
    render() {
        return (
            <div className="col-xl-4 col-md-4 mb-5 ">
             <>
                    <div className="single_service service_bg_2 hserv">
                        <div className="service_hover">
                            <img src="img/svg_icon/legal-paper.svg" alt=""/>
                            <h2 className="text-white">{this.props.project.title}</h2>
                            <div className="hover_content">
                                <div className="hover_content_inner">
                                    <h4>{this.props.project.title}</h4>
                                    <a href={this.props.project.project_link} className="btn btn-info hpreview"><strong>Preview <i className="fa fa-preview"></i></strong></a><br/>
                                    <a href={"/project/"+this.props.project.id} className="btn "><strong className="hseemore">See more <i className="fa fa-plus"></i></strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
             </>
                </div>
        );
    }
}