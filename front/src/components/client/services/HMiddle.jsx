import { Component } from "react";
import Title from "./Title";

export default class HMiddle extends Component {
    render() {
        return (
             
                <div className="container">
                    <Title title={this.props.title} />
                    {this.props.children} 
                </div>
            
        );
    }
}