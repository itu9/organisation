import React from "react";
import General from "../../utils/General";
import HModal from "../../utils/HModal";
import Swal from "sweetalert2";

export default class Charger extends General {
    constructor(params) {
        super(params);
        this.value = React.createRef(null);
    }

    valider = () => {
        let client = this.getConnected().login;
        let data = {
            client : client,
            montant : this.value.current.value,
            date : new Date()
        }
        this.sendData(this.json_server+"/depot",data,
            (data) => {
                Swal.fire({
                    title :'Félicitation',
                    text : 'Votre demande est prise en charge',
                    timer : 3000,
                    icon : 'success'
                }).then(data => {
                    this.props.afterValid()
                })
            }
        )
    }

    render() {
        return (
            <>
                <HModal open={this.props.open} onClose={this.props.onClose} size="50" decalage="45" >
                    <div class="text-center mt-5 mb-5 hcredit">
                        <i className="fas fa-credit-card "></i><span className="text-dark"> Ajouter de l'argent</span>
                    </div>
                    <div class="form-group mt-5">
                        <label for="username" >Montant</label>
                        <input class="form-control" type="text" id="username" required="" ref={this.value} defaultValue={1000} />
                    </div>
                    <div class="form-group text-center mt-5">
                        <button class="btn btn-success btn-block" onClick={this.valider}>Valider</button>
                    </div>                        
                </HModal>
            </>
        );
    }
}