import { Component } from "react";
import DetList from "../team/DetList";
import './Comment.css'
export default class Comment extends Component {
    render() {
        return (

            <div className="mb-3 hcomment">
             <>
                <div className="col-lg-12 mb-3 ">
                    <DetList title={this.props.feedback.pseudonym}>
                        <li>
                            <span className="hdate"> {this.props.feedback.date}</span>
                        </li>
                    </DetList>
                    
                </div>
                <div className="col-lg-12">
                    <p>{this.props.feedback.text}</p>
                </div>
                <div className="col-lg-12">
                    <hr/>
                </div>
             </>
             </div>
        );
    }
}