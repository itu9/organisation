import { List } from "@mui/material";
import { Component } from "react";
import Comment from "./Comment";
import CommentForm from "./CommentForm";

export default class Comments extends Component {
    render() {
        return (
             <>
                <div className="row">
                    <div className="col-lg-4">
                        <CommentForm projectid={this.props.projectid}/>
                    </div>
                    <div className="col-lg-8">
                        <List
                            sx={{
                                maxHeight:350,
                                overflow: 'auto'
                            }}
                        >
                    {
                        this.props.feedback.map((fe) => (
                            <Comment key={fe.id} feedback={fe}></Comment>

                        ))
                    }
                    </List>
                    </div>
                </div>
             </>
        );
    }
}