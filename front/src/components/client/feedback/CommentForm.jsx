import React, { Component } from "react";
import Swal from "sweetalert2";
import OwnService from "../../../service/OwnService";

export default class CommentForm extends Component {
    constructor(params) {
        super(params);
        this.email = React.createRef(null)
        this.pseudo = React.createRef(null)
        this.comment = React.createRef(null)
    }

    sendData = () => {
        let data = {
            email : this.email.current.value,
            text : this.comment.current.value,
            pseudonym : this.pseudo.current.value,
            projectid : this.props.projectid,
            star : 5
        }
        console.log(data);
        OwnService.sendData(OwnService.url + "api/feedbacks",data,
            (data) => {
                Swal.fire({
                    title : 'Bravo',
                    icon:'success',
                    text : 'You comment was added'
                }).then(() => {
                    window.location.reload();
                })
            }
        )
    }

    render() {
        return (
            <div className="">
                <h3>Make a feedback</h3>
                <strong>Pseudonym</strong>
                <br/>
                <input ref={this.pseudo} className="form-control w-100 mb-3" placeholder="Mark 102" type="text"/>
                <strong>Email</strong>
                <br/>
                <input ref={this.email} className="form-control w-100 mb-3" placeholder="marck@gmail.com" type="email"/>
                <strong>Your comment</strong>
                <br/>
                <textarea ref={this.comment} className="form-control w-100 mb-3" placeholder="Thank you" type="text"></textarea>
                <button className="btn w-100 hpreview" onClick={
                    () => {
                        this.sendData();
                    }
                }>Commit</button>
            </div>
        );
    }
}