import { Component } from "react";
import HMiddle from "../services/HMiddle";
import Comments from "./Comments";
import HCard from "../../../service/HCard";
import OwnService from "../../../service/OwnService";

export default class FeedBack extends Component {
    constructor(params) {
        super(params);
        this.state = {
            feedback : [
                {
                    id : 1,
                    pseudonym : 'Mathieu 102',
                    date : '24, june 2002',
                    text : `ITFreel put my project back 2 days before the time in term. I'm so satisfied.`
                },
                {
                    id : 2,
                    pseudonym : 'Jean Baptiste',
                    date : '14, may 2005',
                    text : `ITFreel make my website so optimized. I own more clients now.`
                },
                {
                    id : 3,
                    pseudonym : 'Mark Lion',
                    date : '24, june 2016',
                    text : `Sometimes, I'm afraid because they cost less but dit a great job.`
                }
            ]
        }
    }

    componentDidMount( ) {
        OwnService.getData(OwnService.url+'api/projects/0', 
            (data) => {
                // console.log(data);
                this.setState({
                    feedback : data.feedbacks
                })
            }
        )
    }
    render() {
        return (
             <>
                <HMiddle title="Feed Back">
                    <div className="container">
                        <HCard size="12">
                        <Comments projectid="0" feedback = {this.state.feedback} />
                        </HCard>
                    </div>
                </HMiddle>
             </>
        );
    }
}