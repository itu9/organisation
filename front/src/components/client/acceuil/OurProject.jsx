import { Component } from "react";
import HContainer from "../nav/HContainer";
import Services from "../services/Services";

export default class OurProject extends Component {
    render() {
        return (
            <HContainer>
                <div className="service_area">
                    <Services/>
                </div>
            </HContainer>
        );
    }
}