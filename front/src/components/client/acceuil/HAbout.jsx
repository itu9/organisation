import { Component } from "react";
import About from "../about/About";
import HContainer from "../nav/HContainer";

export default class HAbout extends Component {
    render() {
        return (
             <HContainer>
                <div className="service_area">
                    <About/>
                </div>
             </HContainer>
        );
    }
}