import { Component } from "react";
import HContainer from "../nav/HContainer";
import Team from "../team/Team";

export default class OurTeam extends Component {
    render() {
        return (
             <HContainer>
                <div className="service_area">
                    <Team></Team>
                </div>
             </HContainer>
        );
    }
}