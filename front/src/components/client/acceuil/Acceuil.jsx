import { Component } from "react";
import About from "../about/About";
import FeedBack from "../feedback/FeedBack";
import HContainer from "../nav/HContainer";
import Offers from "../offer/Offers";
import Services from "../services/Services";
import Team from "../team/Team";

export default class Acceuil extends Component {
    render() {
        return (
             <HContainer>
                <div className="service_area">
                    <Offers/>
                    <Services/>
                    <Team/>
                    <About/>
                    <FeedBack/>
                </div>
             </HContainer>
        );
    }
}