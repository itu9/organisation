import { Component } from "react";
import FeedBack from "../feedback/FeedBack";
import HContainer from "../nav/HContainer";

export default class HFeedBack extends Component {
    render() {
        return (
            <HContainer>
                <div className="service_area">
                    <FeedBack></FeedBack>
                </div>
            </HContainer>
        );
    }
}