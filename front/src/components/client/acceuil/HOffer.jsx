import { Component } from "react";
import HContainer from "../nav/HContainer";
import Offers from "../offer/Offers";

export default class HOffer extends Component {
    render() {
        return (
             <HContainer>
                <div className="service_area">
                    <Offers></Offers>
                </div>
             </HContainer>
        );
    }
}