import { useParams } from "react-router-dom";
import ObjectifDet from "./ObjectifDet";

export default function FObjectifDet () {
    const {id} = useParams();
    return (
            <>
                <ObjectifDet id={id}></ObjectifDet>
            </>
    );
    
}