import Border from "../utils/Border";
import Card from "../utils/Card";
import Generation from "../utils/Generation";
import HSearch from "../utils/Search";
import Ticket from "../utils/Ticket";

export default class Objectif extends Generation {
    state = {

    }
    render() {
        return (
             <>
                <div className="mt-5">
                    <h1 className="text-dark">Mes objectifs</h1>
                    <div className="row mt-5">
                        <div className="col-4">
                            <Ticket title={<><strong className="font-weight-medium text-success font-24">80%</strong><span className=""><br/>Suivez votre progression en temps réel.</span></>}>
                                <span className="text-info">Avancement</span>
                            </Ticket>
                        </div>
                        <div className="col-4">
                            <Ticket title={<span>Suggestion de date pour atteindre votre objectifs selon votre disponibilité.</span> }>
                                <span className="text-orange">Organiser vos temps</span>
                            </Ticket>
                        </div>
                        <div className="col-4">
                            <Ticket title={<span>On se chargera de vous rappeler ce que vous devez faire.</span> }>
                                <span className="text-success">Oublier votre agenda</span>
                            </Ticket>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-4"></div>
                            <div className="col-4">
                                <Border>
                                    <HSearch size="11">
                                    <h2 className="text-orange ">A voir aussi</h2>
                                        <button className="btn-success btn-block mb-3"
                                            onClick={
                                                () => {
                                                    this.setState({
                                                        addProject : true
                                                    })
                                                }
                                            }
                                        >
                                            <h4 className="pt-1 pb-1">+ Ajouter une épreuve</h4>
                                        </button>
                                        <h2 className="text-info ">Zone de recherche</h2>
                                        <input className="form-control text-dark mt-5" placeholder="Ecrit la recherche" />
                                    </HSearch>
                                </Border>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                            <Card size="6"
                                link="/assets/images/landingpage/db.png"
                            >
                                <h2 className="text-dark">Apprendre Ajax</h2>
                                <h4 >Mini objectif</h4>
                                <h4 className="text-info mt-5">Date limite</h4>
                                <h4 className="text-dark">24, juin 2023</h4>
                                <h4 className="text-info ">Durée</h4>
                                <h4 className="text-dark">0:0:0</h4>
                                <h4 className="text-info ">Temps restant</h4>
                                <h4 className="text-dark">0:0:0</h4>
                                <h4 className="text-info ">Avancement</h4>
                                <h4 className="text-dark">80%</h4>
                                <button className="mt-3 btn-outline-warning btn text-dark btn-block">
                                    <h4 className="pt-2">Pause</h4>
                                </button>
                                <a href={"/objectif/1"}>
                                    <button className="mt-3 btn-info btn-block">
                                        <h4 className="pt-2">Voir plus</h4>
                                    </button>
                                </a>
                            </Card>
                    </div>
                </>
        );
    }
}