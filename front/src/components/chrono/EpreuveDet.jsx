import HContainer from "../client/nav/HContainer";
import Generation from "../utils/Generation";
import HSearch from "../utils/Search";
import Liste from "../utils/Liste";
import './EpDet.css'
import QuestResponse from "./QuestReponse";
import React from "react";
import Ticket from "../utils/Ticket";
import Chat from "../etache/Chat";
import HModal from "../utils/HModal";

export default class EpreuveDet extends Generation {
    state = {
        project : null,
        chat : [],
        addProject :false
    }


    chat = React.createRef();
    reponse = React.createRef('');

    componentDidMount(){
        this.getEpreuve();
        this.getChat();
    }
    getChat = () => {
        this.getListe(this.url+"/chat-epreuve/epreuve/"+this.props.id,
            (data) => {
                this.setState({
                    chat : data.data
                })
            },
            (data) => {}
        )
    }
    getEpreuve = () => {
        this.getListe(this.url+"/epreuves/"+this.props.id,
            (data) => {
                console.log(data);
                this.setState({
                    project : data.data
                })
            },
            (data) => {}
        )
        
    }

    ask = async () => {
        let msg = this.chat.current.value;
        this.askChat(msg,(response) => {
            let data = {
                epreuveid : this.props.id,
                chat : {
                    question : msg,
                    response : response,
                    etudiant : {
                        id : this.getStudent().id
                    }
                }
            }
            console.log(data);
            this.sendData(this.url+'/chat-epreuve',data,(res) => {
                this.getChat();
            })
        }, (err) => {})
    }

    render() {
        return (
             <>
                <HContainer>
                    {
                        (this.state.project !== null) ? <>
                            <h1 className="text-info">{this.state.project.titre}</h1>
                            <h4>{this.state.project.type.titre}</h4>
                                <div className="row mt-5">
                                    <div className="col-4">
                                        <Ticket
                                            title={<span className="text-success">Durée de l'épreuve</span>}
                                        >
                                            {this.state.project.h}:
                                            {this.state.project.m}:
                                            {this.state.project.s} 
                                        </Ticket>
                                    </div>
                                    <div className="col-4">
                                        <Ticket
                                        title={<span className="text-orange">Vitesse actuelle</span>}
                                        >
                                            <span className={(this.state.project.depass?" text-danger" : "text-dark ")}>
                                                {this.state.project.vitesse.h}:
                                                {this.state.project.vitesse.m}:
                                                {this.state.project.vitesse.s}
                                            </span>
                                        </Ticket>
                                    </div>
                                    <div className="col-4">
                                        <Ticket
                                        title={<a className="text-info" href={"#a"} onClick={() => {this.setState({addProject : true})}}>Fonctionnalités terminés à temps</a>}
                                        >
                                            15
                                            
                                        </Ticket>
                                    </div>
                                </div>
                            <Liste title={<span className="text-info">Tâches rattachées à l'épreuve</span>}
                            size="12 mt-5"
                            tablesize="12"
                            tcolor="white"
                                search = {
                                    <div className="ml-3">
                                        <HSearch btnSize="4"
                                            onClick={this.search}
                                            size="12"
                                        >
                                            
                                        <h3 className="text-dark">Zone de recherche</h3>
                                        <input className="form-control w-100" placeholder="Text for search" />
                                    
                                        </HSearch>
                                    </div>
                                }
                                head = {
                                    <>
                                        <tr>
                                            <th>Tache</th>
                                            <th>Effectif</th>
                                            <th>Niveau</th>
                                            <th>Duree</th>
                                            <th>Perfection</th>
                                            <th></th>
                                        </tr>
                                    </>
                                }
                            >
                                {
                                    this.state.project.taches.map(
                                        (tache,index) => (
                                            <tr key={'tr_'+index} className={(tache.perfection< 3)?"tacheNotPerfect":""}>
                                                <td key={'td0_'+index}>{tache.tache}</td>
                                                <td key={'td1_'+index}>{tache.effectif}</td>
                                                <td key={'td2_'+index}>{tache.niveau}/5</td>
                                                <td key={'td3_'+index}>{tache.h}:{tache.m}:{tache.s}</td>
                                                <td key={'td4_'+index}>{tache.perfection}/5</td>
                                                <td key={'td5_'+index}><a  key={'a_td_'+index} href={"/epreuve/tache/"+tache.id}>Voir détails</a></td>
                                            </tr>
                                        )
                                    )
                                }
                            </Liste>
                            <div>{this.reponse.current}</div>
                            <Chat
                                chat= {this.chat}
                                ask={this.ask}
                            >
                            {
                                this.state.chat.map(chat => (
                                    <QuestResponse key={chat.id} chat={chat.chat}></QuestResponse>
                                ))
                            }
                            </Chat>
                            <HModal open={this.state.addProject} size="50" decalage="50"
                onClose={
                    () => {
                        this.setState({
                            addProject : false
                        })
                    }
                }
                >
                    <Liste title={<span className="text-info">Fonctionnalités terminés à temps</span>}
                            size="12 mt-5"
                            tablesize="12"
                            tcolor="white"
                                search = {
                                    <div className="ml-3">
                                    </div>
                                }
                                head = {
                                    <>
                                        <tr>
                                            <th>Tache</th>
                                            <th>Effectif</th>
                                            <th>Niveau</th>
                                            <th>Duree</th>
                                            <th>Perfection</th>
                                            <th></th>
                                        </tr>
                                    </>
                                }
                            >
                                {
                                    this.state.project.taches.map(
                                        (tache,index) => (
                                            <tr key={'tr_'+index} className={(tache.perfection< 3)?"tacheNotPerfect":""}>
                                                <td key={'td0_'+index}>{tache.tache}</td>
                                                <td key={'td1_'+index}>{tache.effectif}</td>
                                                <td key={'td2_'+index}>{tache.niveau}/5</td>
                                                <td key={'td3_'+index}>{tache.h}:{tache.m}:{tache.s}</td>
                                                <td key={'td4_'+index}>{tache.perfection}/5</td>
                                                <td key={'td5_'+index}><a  key={'a_td_'+index} href={"/epreuve/tache/"+tache.id}>Voir détails</a></td>
                                            </tr>
                                        )
                                    )
                                }
                            </Liste>
                </HModal>
                        </> : <></>
                    }
                </HContainer>
                
             </>
        );
    }
}