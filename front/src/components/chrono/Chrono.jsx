import HContainer from "../client/nav/HContainer";
import Generation from "../utils/Generation";
import Epreuve from "./Epreuve";
import Objectif from "./Objectif";

export default class Chrono extends Generation {
    state = {
        project : null
    }
    render() {
        return (
             <HContainer>
                <Epreuve/>
                <Objectif></Objectif>
             </HContainer>
        );
    }
}