import { Avatar } from "@mui/material";
import { green } from "@mui/material/colors";
import Generation from "../utils/Generation";
import './QuestReponse.css'

export default class QuestResponse extends Generation {
    render() {
        return (
            <>
            <li className="chat-item list-style-none mt-3">
                <div className="chat-img d-inline-block">
                        <Avatar sx={{ bgcolor: green[500]}} aria-label="recipe">
                              {this.props.chat.etudiant.nom[0]}
                            </Avatar>
                </div>
                <div className="chat-content d-inline-block pl-3">
                    <h6 className="font-weight-medium">
                        <strong>
                        {this.props.chat.etudiant.prenom} {this.props.chat.etudiant.nom}
                        </strong>
                    </h6>
                    <div className="msg p-2 d-inline-block mb-1">{this.props.chat.question}</div>
                </div>
                <div className="chat-time d-block font-10 mt-1 mr-0 mb-3">{this.props.chat.date}
                </div>
            </li>
            <li className="chat-item odd list-style-none mt-3 ml-5">
                <div className="chat-content d-inline-block pl-3">
                    <pre className="box msg p-2 d-inline-block mb-1 resp">
                        {this.props.chat.response}
                    </pre>
                    <br/>
                </div>
            </li>
            </>
        );
    }
}