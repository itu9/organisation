import { useParams } from "react-router-dom";
import EpreuveDet from "./EpreuveDet";

export default function ChronoDet () {
    const {id} = useParams();
    return (
            <>
                <EpreuveDet id={id}></EpreuveDet>
            </>
    );
    
}