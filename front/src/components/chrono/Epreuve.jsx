import Generation from "../utils/Generation";
import Border from "../utils/Border";
import Card from "../utils/Card";
import HSearch from "../utils/Search";
import HModal from "../utils/HModal";
import React from "react";
import Swal from "sweetalert2";
import Ticket from "../utils/Ticket";

export default class Epreuve extends Generation {
    state = {
        project : [

        ],
        etype : [],
        addProject : false
    }

    project = {
        title : React.createRef(),
        type : React.createRef(),
        h : React.createRef(),
        m : React.createRef(),
        s : React.createRef(),
    }

    componentDidMount() {
        this.getEpreuve();
        this.getType();
    }

    getEpreuve = () => {
        this.getListe(this.url+'/epreuves',
            (epr) => {
                console.log(epr);
                this.setState({
                    project : epr.data
                })
            },
            (data) => {}
        )
    }

    getType = () => {
        this.getListe(this.url+'/etypes',
            (types) => {
                this.setState({
                    etype : types.data
                })
            },
            (head) => {}
        )
    }

    createEpreuve = () => {
        let pr = {
            titre    : this.project.title.current.value,
            type : {
                id : this.project.type.current.value
            } ,
            h : this.project.h.current.value,
            m : this.project.m.current.value,
            s : this.project.s.current.value,
            etudiant : {
                id : this.getStudent().id
            }
        }
        this.sendData(this.url+"/epreuves",pr,
            (data) => {
                if (data.code === undefined) {
                    Swal.fire({
                        title : 'Bravo',
                        text : `L'épreuve est enregistrer avec succés`,
                        icon : 'success'
                    }).then(() => {
                        this.setState({
                            addProject : false
                        },
                            () => {
                                this.getEpreuve();
                            }
                        )
                    })
                    
                } else {
                    Swal.fire ({
                        title : 'Oupss...',
                        text : `Une erreur s'est produite`,
                        icon : 'error'
                    }).then(() => {
                        this.setState({
                            addProject : false
                        })
                    })
                }
            }
        )
    }

    render() {
        return (
            <>
             <> 
                <h1 className="text-dark">Amélioration aux épreuves</h1>
                <div className="row mt-5">
                    <div className="col-4">
                        <Ticket title={<>Identifier les points faibles et points forts de votre performance.</>}>
                            <span className="text-info">Chronomètre</span>
                        </Ticket>
                    </div>
                    <div className="col-4">
                        <Ticket title={<span>En cas de difficulté, consulter l'IA pour vous aider gratuitement.</span> }>
                            <span className="text-orange">IA comme assistant</span>
                        </Ticket>
                    </div>
                    <div className="col-4"> 
                        <Ticket title={<span>Proposition des taches à finir par ordre de priorité selon votre performance.</span> }>
                            <span className="text-success">Minimiser les risques</span>
                        </Ticket>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-4"></div>
                    <div className="col-4">
                        <Border>
                                <HSearch size="11">
                                    <h2 className="text-orange ">A voir aussi</h2>
                                    <button className="btn-success btn-block mb-3"
                                        onClick={
                                            () => {
                                                this.setState({
                                                    addProject : true
                                                })
                                            }
                                        }
                                    >
                                        <h4 className="pt-1 pb-1">+ Ajouter une épreuve</h4>
                                    </button>
                                    <h2 className="text-info mt-5">Zone de recherche</h2>
                                    <input className="form-control text-dark " placeholder="Ecrit la recherche" />
                                </HSearch>
                        </Border>
                    </div>
                </div>
                <div className="row mt-3">
                    {
                        this.state.project.map((project,index) => (
                            <Card key={index} size="6"
                                link="/assets/images/landingpage/db.png"
                            >
                                <h2 className="text-dark">{project.titre}</h2>
                                <h4 >{project.type.titre}</h4>
                                <h4 className="text-info mt-5">Durée de l'épreuve :</h4>
                                <h4 className="text-dark">{project.h}:{project.m}:{project.s}</h4>
                                <a href={"/chrono/"+project.id}>
                                    <button className="mt-3 btn-info btn-block">
                                        <h4 className="pt-2">Voir plus</h4>
                                    </button>
                                </a>
                            </Card>
                        ))
                    }
                </div>
             </>
            <HModal open={this.state.addProject} size="50" decalage="50"
                onClose={
                    () => {
                        this.setState({
                            addProject : false
                        })
                    }
                }
            >
                <div className="row ml-3 mr-3 mt-4 ">
                    <div className="col-12">
                        <h1 className="text-info">Ajouter l'épreuve</h1>
                        <h4>Afin qu'on puisse vous aider</h4>
                    </div>
                    <div className="col-12 mt-5">
                        <h3 className="text-dark">Titre</h3>
                        <input ref={this.project.title} className="form-control" placeholder="Une brieve titre pour la mémorisation" />
                    </div>
                    
                    <div className="col-12 mt-3">
                        <h3 className="text-dark">Type</h3>
                        <select ref={this.project.type} className="custom-select" >
                            {
                                this.state.etype.map((type,index) => (
                                    <option value={type.id} key={index}>{type.titre}</option>

                                ))
                            }
                        </select>
                    </div>

                    <div className="col-12 mt-3">
                        <h3 className="text-dark">Durée</h3>
                        <div className="row">
                            <div className="col-4">
                                <input ref={this.project.h} className="form-control" placeholder="hh" />
                            </div>
                            <div className="col-4">
                                <input ref={this.project.m} className="form-control" placeholder="mm" />
                            </div>
                            <div className="col-4">
                                <input ref={this.project.s} className="form-control" placeholder="ss" />
                            </div>
                        </div>
                        
                    </div>
                    <div className="col-12 mt-5">
                        <button className="btn-success btn-block" onClick={this.createEpreuve}>
                            <h4 className="pt-1">Ajouter</h4>
                        </button>
                    </div>
                </div>
            </HModal>
            </>
        );
    }
}