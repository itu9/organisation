import HContainer from "../client/nav/HContainer";
import Chat from "../etache/Chat";
import Generation from "../utils/Generation";
import Liste from "../utils/Liste";
import HSearch from "../utils/Search";
import Ticket from "../utils/Ticket";

export default class ObjectifDet extends Generation {
    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Perfectionner login</h1>
                <div className="row mt-5">
                    <div className="col-4">
                        <Ticket title={<span>24, juin 2023</span>}>
                            <span className="text-info"> Date limite</span>
                        </Ticket>
                    </div>
                    <div className="col-4">
                        <Ticket title="30%">
                            <span className="text-success"> Avancement</span>
                        </Ticket>
                    </div>
                    <div className="col-4">
                        <Ticket title={<span>12:00:00</span>}>
                            <span className="text-orange"> Temps restant</span>
                        </Ticket>
                    </div>
                    <div className="col-4">

                    </div>
                    <div className="col-4">
                        <Ticket title={<>
                            <div className="row">
                                <div className="col-12">
                                    <button className="btn btn-info w-100">Demander l'avis des autres</button>
                                </div>
                                <div className="col-12 mt-3">
                                    <button className="btn btn-outline-success w-100">Rechercher le sujet</button>
                                </div>
                                <div className="col-12 mt-3">
                                    <button className="btn btn-outline-info w-100" >Chercher un chreno</button>
                                </div>
                            </div>
                            </>}>
                            <span className="text-orange">A voir aussi</span>
                        </Ticket>
                    </div>
                </div>
                <Liste title={<span className="text-info">Démache pour le suivre</span>}
                            size="12 mt-5"
                            tablesize="12"
                            tcolor="white"
                                search = {
                                    <div className="ml-3">
                                        <HSearch btnSize="3"
                                            onClick={this.search}
                                            size="10 "
                                        >
                                            
                                        <h3 className="text-dark">Zone de recherche</h3>
                                        <input className="form-control w-100" placeholder="Text for search" />
                                    
                                        </HSearch>
                                    </div>
                                }
                                head = {
                                    <>
                                        <tr>
                                            <th>Etape</th>
                                            <th>Durée</th>
                                            <th>Avancement</th>
                                            <th>Etat</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </>
                                }
                                plus = {
                                    <div className="col-4">
                                        <button className="btn btn-success btn-block">Ajouter</button>
                                    </div>
                                }
                            >
                                <tr>
                                    <td>Recherche sur le sujet</td>
                                    <td>0:0:0</td>
                                    <td>60%</td>
                                    <td className="text-success">Terminé</td>
                                    <td><button className="btn btn-info">Modifier</button></td>
                                    <td><button className="btn btn-success">Demander de l'aide</button></td>
                                    <td><button className="btn btn-outline-success">Avis des autres</button></td>
                                </tr>
                            </Liste>
                <div className="mt-5">
                    <Chat></Chat>
                </div>
             </HContainer>
        );
    }
}