import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class RegleForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.valeur = React.createRef(null);
    this.regletypeid = React.createRef(null);
    this.urlSend = "/regle";
    this.urlUtils = "/regle/utils";
    this.afterValidation = "/regle";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      valeur: this.prepare(this.valeur),
      regletypeid: this.checkRefNull(
        this.regletypeid,
        { id: this.prepare(this.regletypeid) },
        null
      ),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Ajout de regle de gestion</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Valeur</h3>
              <input
                type="text"
                className="form-control"
                ref={this.valeur}
              ></input>
              <h3 className="text-info mt-3">Type de regle</h3>
              <select className="form-control" ref={this.regletypeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.regletypeid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
              <button
                onClick={()=> {window.location.replace('/regle/type/form')}}
                className="mt-3 btn btn-outline-orange btn-block"
              >
                Ajouter un nouveau type
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
