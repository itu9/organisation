import { useParams } from "react-router-dom";
import RegleInfo from "./RegleInfo";
export default function RegleFicheGetter() {
    const {id} = useParams();
    return (
        <RegleInfo id={id}>
        </RegleInfo>
    );
}