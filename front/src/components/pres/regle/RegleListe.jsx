import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";

export default class RegleListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.valeurhmin = React.createRef(null);
    this.valeurhmax = React.createRef(null);
    this.regletypeid = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/regle/filter";
    this.urlUtils = "/regle/utils";
    this.baseUrl = "/regle";
  }
  componentDidMount() {
    this.init();
  }
  prepareDataListe = () => {
    let data = {
      keys: this.prepare(this.keys),
      valeurhmin: this.prepare(this.valeurhmin),
      valeurhmax: this.prepare(this.valeurhmax),
      regletypeid: this.checkRefNull(
        this.regletypeid,
        { id: this.prepare(this.regletypeid) },
        null
      ),
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="La liste des regle de gestion"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Valeur</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.valeurhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.valeurhmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Type de regle</h3>
                      <select className="form-control" ref={this.regletypeid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.regletypeid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/regle/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Type de regle</td>
                    <td>Valeur</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                        <td>{data.regletypeid.titre}</td>
                      <td>{data.valeur}</td>
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace("/regle/update/" + data.id);
                          }}
                        >
                          <i className="fas fa-pencil-alt text-warning"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            window.location.replace("/regle/" + data.id);
                          }}
                        >
                          <i className="fas fa-plus text-info"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
