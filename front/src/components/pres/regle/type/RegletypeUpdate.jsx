import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class RegletypeUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.titre = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/regle/type";
    this.urlUtils = "/regle/type/utils";
    this.afterValidation = "/regle/type";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.titre.current.value = this.state.oneValue.titre;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      titre: this.prepare(this.titre),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Titre</h3>
              <input
                type="text"
                className="form-control"
                ref={this.titre}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
