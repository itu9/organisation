import { useParams } from "react-router-dom";
import RegletypeInfo from "./RegletypeInfo";

export default function RegletypeFicheGetter() {
    const {id} = useParams();
    return (
        <RegletypeInfo id={id}>
        </RegletypeInfo>
    );
}