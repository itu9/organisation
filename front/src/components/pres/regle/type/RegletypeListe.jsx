import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";

export default class RegletypeListe extends GeneralListe {
  constructor(params) {
    super(params);

    this.keys = React.createRef(null);
    this.urlData = "/regle/type/filter";
    this.urlUtils = "/regle/type/utils";
    this.baseUrl = "/regle/type";
  }
  componentDidMount() {
    this.init();
  }
  prepareDataListe = () => {
    let data = {
      keys: this.prepare(this.keys),
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />

                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/regle/type/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Titre</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.titre}</td>
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace(
                              "/regle/type/update/" + data.id
                            );
                          }}
                        >
                          <i className="fas fa-pencil-alt text-warning"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            window.location.replace("/regle/type/" + data.id);
                          }}
                        >
                          <i className="fas fa-plus text-info"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
