import { useParams } from "react-router-dom";
import RegletypeUpdate from "./RegletypeUpdate";

export default function RegletypeUpdateGetter() {
    const {id} = useParams();
    return (
        <RegletypeUpdate id={id}>
        </RegletypeUpdate>
    );
}