import { useParams } from "react-router-dom";

import RegleUpdate from "./RegleUpdate";

export default function RegleUpdateGetter() {
    const {id} = useParams();
    return (
        <RegleUpdate id={id}>
        </RegleUpdate>
    );
}