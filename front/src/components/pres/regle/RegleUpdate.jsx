import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class RegleUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.valeur = React.createRef(null);
    this.regletypeid = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/regle";
    this.urlUtils = "/regle/utils";
    this.afterValidation = "/regle";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.valeur.current.value = this.state.oneValue.valeur;
    this.regletypeid = this.state.oneValue.regletypeid.id;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      valeur: this.prepare(this.valeur),
      regletypeid: { id: this.regletypeid },
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Modification de regle</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Valeur</h3>
              <input
                type="text"
                className="form-control"
                ref={this.valeur}
              ></input>
              <h3 className="text-info mt-3">Type de regle</h3>
              <p>
                {
                this.state.oneValue !== undefined && this.state.oneValue !== null ? 
                this.state.oneValue.regletypeid.titre:<></>}</p>
              {/* <select className="form-control" ref={this.regletypeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.regletypeid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select> */}
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
