import { useParams } from "react-router-dom";
import RnotacceptInfo from "./RnotacceptInfo";
export default function RnotacceptFicheGetter() {
    const {id} = useParams();
    return (
        <RnotacceptInfo id={id}>
        </RnotacceptInfo>
    );
}