/* eslint-disable no-unused-vars */
import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";
import OwnService from "../../../service/OwnService";

export default class RnotacceptListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.clientid = React.createRef(null);
    this.montanthmin = React.createRef(null);
    this.montanthmax = React.createRef(null);
    this.etathmin = React.createRef(null);
    this.etathmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/recharger/filter";
    this.urlUtils = "/recharger/utils";
    this.baseUrl = "/recharger";
  }
  componentDidMount() {
    this.init();
  }
  prepareDataListe = () => {
    let data = {
      keys: this.prepare(this.keys),
      clientid: this.checkRefNull(
        this.clientid,
        { id: this.prepare(this.clientid) },
        null
      ),
      montanthmin: this.prepare(this.montanthmin),
      montanthmax: this.prepare(this.montanthmax),
      etathmin: this.prepare(this.etathmin),
      etathmax: this.prepare(this.etathmax),
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  refuserRecharge = (id) => {
    let idUser = this.getSession(this.auth,{login:{id:-100}}).login.id
    let uri = this.url +this.baseUrl+"/reject/"+id+"?idUser="+idUser;
    this.getListe(uri,
        data => {
          console.log(data);
          this.verifData(data,
            // eslint-disable-next-line no-unused-vars
            (response) => {
              window.location.reload();
            }
        )
        },
        header => {

        }
      )
  }
  validerRecharge = (id) => {
    let idUser = this.getSession(this.auth,{login:{id:-100}}).login.id
    let uri = this.url +this.baseUrl+"/accept/"+id+"?idUser="+idUser;
    this.getListe(uri,
        data => {
          this.verifData(data,
            // eslint-disable-next-line no-unused-vars
            (response) => {
              window.location.reload();
            }
        )
        },
        header => {

        }
      )
  }

  restorerRecharge = (id) => {
    let idUser = this.getSession(this.auth,{login:{id:-100}}).login.id
    let uri = this.url +this.baseUrl+"/restore/"+id+"?idUser="+idUser;
    this.getListe(uri,
        data => {
          this.verifData(data,
            // eslint-disable-next-line no-unused-vars
            (response) => {
              window.location.reload();
            }
        )
        },
        header => {

        }
      )
  }
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Demandes de rechargement"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Client</h3>
                      <select className="form-control" ref={this.clientid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.clientid.map((data, index) => (
                            <option key={index} value={data.id}>{data.email}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Montant</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.montanthmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.montanthmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Etat</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.etathmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.etathmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div className="btn-list w-100">
                                <div className="btn-group">
                                  <button
                                    type="button"
                                    className="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div className="dropdown-menu w-100">
                                    <a className="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a className="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/recharger/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Client</td>
                    <td>Montant</td>
                    <td>Etat</td>
                    <td></td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr key={index}>
                      <td>{data.clientid.nom}</td>
                      <td>{OwnService.format( data.montant)}</td>
                      <td>{
                          data.etat === 25 ?
                          <p className="text-danger">Réfusé </p> : data.etat === 50 ?
                          <p className="text-success"> Validé </p>:<p className="text-success"> En attente </p>
                          }</td>
                      <td>
                        {
                          data.etat === -25 ?
                          <button
                            className="btn btn-outline-success"
                            onClick={() => {
                              this.restorerRecharge(data.id)
                            }}
                          >
                            Réstorer
                          </button>: data.etat === 50 ?
                          <>  </>:
                        <>
                        <button
                            className="btn btn-success"
                            onClick={() => {
                              this.validerRecharge(data.id)
                            }}
                          >
                            Valider
                          </button>
                          <button
                            className="btn btn-outline-danger ml-3"
                            onClick={() => {
                              this.refuserRecharge(data.id)
                            }}
                          >
                            Réfuser
                          </button>
                        </>
                        }
                        
                      </td>
                      
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace(
                              "/recharger/update/" + data.id
                            );
                          }}
                        >
                          <i className="fas fa-pencil-alt text-warning"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            window.location.replace("/recharger/" + data.id);
                          }}
                        >
                          <i className="fas fa-plus text-info"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
