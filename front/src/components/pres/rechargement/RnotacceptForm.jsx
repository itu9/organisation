import OwnService from "../../../service/OwnService";
import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class RnotacceptForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.clientid = React.createRef(null);
    this.montant = React.createRef(null);
    this.etat = React.createRef(null);
    this.solde = React.createRef(null);
    this.urlSend = "/recharger";
    this.urlUtils = "/recharger/utils";
    this.afterValidation = "/recharger/form";
  }
  componentDidMount() {
    this.init();
    this.getSolde();
  }

  getSolde = () =>{
    this.checkConnexion()
    let id = this.getSession(this.auth,{login:{id:-100}}).login.id
    this.sendData(this.url+"/etatargent/filter",{
      clientid : {
        id : id
      }
    },
            response => {
                this.verifData(response,
                    result => {
                      this.solde.current.firstChild.data = OwnService.format(result.data[0].solde)
                    }
                )
            } 
        )
  }

  getClient = () => {
    let user = this.getSession(this.auth,{login:{id:-100}})
    return user;
  }
  validateData = () => {
    let data = {
      clientid: {id : this.getClient().login.id},
      montant: this.prepare(this.montant),
      etat: 0,
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Etat d&apos;argent</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h2 className="text-orange">Solde</h2>
              <p ref={this.solde}>{OwnService.format(25000)}</p>
            </Border>
            <Border>
              <h2 className="mt-3 text-info">Recharger mon compte</h2>
              <h3 className="text-info mt-3">Montant</h3>
              <input
                type="text"
                className="form-control"
                ref={this.montant}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
