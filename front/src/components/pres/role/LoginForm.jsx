import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class LoginForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.email = React.createRef(null);
    this.pwd = React.createRef(null);
    this.nom = React.createRef(null);
    this.prenom = React.createRef(null);
    this.acreditation = React.createRef(null);
    this.naissance = React.createRef(null);
    this.urlSend = "/role";
    this.urlUtils = "/role/utils";
    this.afterValidation = "/role";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      email: this.prepare(this.email),
      pwd: this.prepare(this.pwd),
      nom: this.prepare(this.nom),
      prenom: this.prepare(this.prenom),
      acreditation: this.prepare(this.acreditation),
      naissance: this.prepare(this.naissance),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire d'ajout de client</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Email</h3>
              <input
                type="text"
                className="form-control"
                ref={this.email}
              ></input>
              <h3 className="text-info mt-3">Mot de passe </h3>
              <input
                type="password"
                className="form-control"
                ref={this.pwd}
              ></input>
              <h3 className="text-info mt-3">Nom</h3>
              <input
                type="text"
                className="form-control"
                ref={this.nom}
              ></input>
              <h3 className="text-info mt-3">Prenom</h3>
              <input
                type="text"
                className="form-control"
                ref={this.prenom}
              ></input>
              <h3 className="text-info mt-3">Acreditation</h3>
              <input
                type="text"
                className="form-control"
                ref={this.acreditation}
                defaultValue={25}
              ></input>
              <h3 className="text-info mt-3">Naissance</h3>
              <input
                type="date"
                className="form-control"
                ref={this.naissance}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
