import { useParams } from "react-router-dom";
import RemboursementInfo from "./RemboursementInfo";
export default function RemboursementFicheGetter() {
    const {id} = useParams();
    return (
        <RemboursementInfo id={id}>
        </RemboursementInfo>
    );
}