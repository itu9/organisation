import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class RemboursementForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.date = React.createRef(null);this.valeur = React.createRef(null);this.mois = React.createRef(null);this.presclientid = React.createRef(null);
        this.urlSend = "/remboursement";
        this.urlUtils = "/remboursement/utils";
        this.afterValidation = "/remboursement";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            date : this.prepare(this.date),valeur : this.prepare(this.valeur),mois : this.prepare(this.mois),presclientid : this.checkRefNull(this.presclientid,{id:this.prepare(this.presclientid)},null)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Valeur</h3>
<input type="text" className="form-control" ref={this.valeur} ></input><h3 className="text-info mt-3">Mois</h3>
<input type="text" className="form-control" ref={this.mois} ></input><h3 className="text-info mt-3" >Pret</h3>
<select className="form-control" ref={this.presclientid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.presclientid.map((data,index) => (
            <option value={data.id} >{data.valeur}</option>
        ))
        :<></>
    }
</select>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}