import { useParams } from "react-router-dom";

import RemboursementUpdate from "./RemboursementUpdate";

export default function RemboursementUpdateGetter() {
    const {id} = useParams();
    return (
        <RemboursementUpdate id={id}>
        </RemboursementUpdate>
    );
}