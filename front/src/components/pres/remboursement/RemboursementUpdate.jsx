import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class RemboursementUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.date = React.createRef(null);this.valeur = React.createRef(null);this.mois = React.createRef(null);this.presclientid = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/remboursement";
        this.urlUtils = "/remboursement/utils";
        this.afterValidation = "/remboursement";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.date.current.value = this.state.oneValue.date;this.valeur.current.value = this.state.oneValue.valeur;this.mois.current.value = this.state.oneValue.mois;this.presclientid.current.value = this.state.oneValue.presclientid.id;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            date : this.prepare(this.date),valeur : this.prepare(this.valeur),mois : this.prepare(this.mois),presclientid : this.checkRefNull(this.presclientid,{id:this.prepare(this.presclientid)},null),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Valeur</h3>
<input type="text" className="form-control" ref={this.valeur} ></input><h3 className="text-info mt-3">Mois</h3>
<input type="text" className="form-control" ref={this.mois} ></input><h3 className="text-info mt-3" >Pret</h3>
<select className="form-control" ref={this.presclientid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.presclientid.map((data,index) => (
            <option value={data.id} >{data.valeur}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}