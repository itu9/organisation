import { useParams } from "react-router-dom";

import AllremUpdate from "./AllremUpdate";

export default function AllremUpdateGetter() {
    const {id} = useParams();
    return (
        <AllremUpdate id={id}>
        </AllremUpdate>
    );
}