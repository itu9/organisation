import { useParams } from "react-router-dom";
import AllremInfo from "./AllremInfo";
export default function AllremFicheGetter() {
    const {id} = useParams();
    return (
        <AllremInfo id={id}>
        </AllremInfo>
    );
}