import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import ListePage from "@/components/utils/ListePage";
import GeneralListe from "@/components/utils/GeneralListe";
import React from "react";

export default class AllremListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.moishmin = React.createRef(null);
    this.moishmax = React.createRef(null);
    this.anneehmin = React.createRef(null);
    this.anneehmax = React.createRef(null);
    this.valeurhmin = React.createRef(null);
    this.valeurhmax = React.createRef(null);
    this.clientid = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/remboursement/stat/filter";
    this.urlUtils = "/remboursement/stat/utils";
    this.baseUrl = "/remboursement/stat";
  }
  componentDidMount() {
    this.init();
  }
  prepareDataListe = () => {
    let data = {
      keys: this.prepare(this.keys),
      moishmin: this.prepare(this.moishmin),
      moishmax: this.prepare(this.moishmax),
      anneehmin: this.prepare(this.anneehmin),
      anneehmax: this.prepare(this.anneehmax),
      valeurhmin: this.prepare(this.valeurhmin),
      valeurhmax: this.prepare(this.valeurhmax),
      clientid: this.checkRefNull(
        this.clientid,
        { id: this.prepare(this.clientid) },
        null
      ),
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Mois</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.moishmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.moishmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Annee</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.anneehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.anneehmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Valeur</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.valeurhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.valeurhmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Client</h3>
                      <select className="form-control" ref={this.clientid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.clientid.map((data, index) => (
                            <option value={data.id}>{data.email}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace(
                                    "/remboursement/stat/form"
                                  );
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Mois</td>
                    <td>Annee</td>
                    <td>Valeur</td>
                    <td>Clientid</td>
                    <td>Id</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.mois}</td>
                      <td>{data.annee}</td>
                      <td>{data.valeur}</td>
                      <td>{data.clientid.hvalue}</td>
                      <td>{data.id}</td>{" "}
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace(
                              "/remboursement/stat/update/" + data.id
                            );
                          }}
                        >
                          <i className="fas fa-pencil-alt text-warning"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            window.location.replace(
                              "/remboursement/stat/" + data.id
                            );
                          }}
                        >
                          <i className="fas fa-plus text-info"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
