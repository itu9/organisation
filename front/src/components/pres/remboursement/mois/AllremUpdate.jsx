import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class AllremUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.mois = React.createRef(null);this.annee = React.createRef(null);this.valeur = React.createRef(null);this.clientid = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/remboursement/stat";
        this.urlUtils = "/remboursement/stat/utils";
        this.afterValidation = "/remboursement/stat";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.mois.current.value = this.state.oneValue.mois;this.annee.current.value = this.state.oneValue.annee;this.valeur.current.value = this.state.oneValue.valeur;this.clientid.current.value = this.state.oneValue.clientid.id;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            mois : this.prepare(this.mois),annee : this.prepare(this.annee),valeur : this.prepare(this.valeur),clientid : this.checkRefNull(this.clientid,{id:this.prepare(this.clientid)},null),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Mois</h3>
<input type="text" className="form-control" ref={this.mois} ></input><h3 className="text-info mt-3">Annee</h3>
<input type="text" className="form-control" ref={this.annee} ></input><h3 className="text-info mt-3">Valeur</h3>
<input type="text" className="form-control" ref={this.valeur} ></input><h3 className="text-info mt-3" >Client</h3>
<select className="form-control" ref={this.clientid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.clientid.map((data,index) => (
            <option value={data.id} >{data.email}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}