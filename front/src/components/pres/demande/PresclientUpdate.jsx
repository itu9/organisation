import HContainer from "components/client/nav/HContainer";
import Border from "components/utils/Border";
import GeneralUpdate from "components/utils/GeneralUpdate";
import React from "react";

export default class PresclientUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.clientid = React.createRef(null);this.date = React.createRef(null);this.valeur = React.createRef(null);this.duration = React.createRef(null);this.etat = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/pres";
        this.urlUtils = "/pres/utils";
        this.afterValidation = "/pres";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.clientid.current.value = this.state.oneValue.clientid.id;this.date.current.value = this.state.oneValue.date;this.valeur.current.value = this.state.oneValue.valeur;this.duration.current.value = this.state.oneValue.duration;this.etat.current.value = this.state.oneValue.etat;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            clientid : this.checkRefNull(this.clientid,{id:this.prepare(this.clientid)},null),date : this.prepare(this.date),valeur : this.prepare(this.valeur),duration : this.prepare(this.duration),etat : this.prepare(this.etat),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Client</h3>
<select className="form-control" ref={this.clientid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.clientid.map((data,index) => (
            <option value={data.id} >{data.nom}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Valeur</h3>
<input type="text" className="form-control" ref={this.valeur} ></input><h3 className="text-info mt-3">Duration</h3>
<input type="text" className="form-control" ref={this.duration} ></input><h3 className="text-info mt-3">Etat</h3>
<input type="text" className="form-control" ref={this.etat} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}