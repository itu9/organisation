import { useParams } from "react-router-dom";
import PresclientInfo from "./PresclientInfo";
export default function PresclientFicheGetter() {
    const {id} = useParams();
    return (
        <PresclientInfo id={id}>
        </PresclientInfo>
    );
}