import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class PresclientForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.clientid = React.createRef(null);
    this.date = React.createRef(null);
    this.valeur = React.createRef(null);
    this.duration = React.createRef(null);
    this.etat = React.createRef(null);
    this.urlSend = "/pres";
    this.urlUtils = "/pres/utils";
    this.afterValidation = "/pres/form";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    this.checkConnexion();
    let id = this.getSession(this.auth,{login:{id:-100}}).login.id
    let data = {
      clientid: { id: id },
      date: this.prepare(this.date),
      valeur: this.prepare(this.valeur),
      duration: this.prepare(this.duration),
      etat: 0,
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Demander un prêt</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Date</h3>
              <input
                type="date"
                className="form-control"
                ref={this.date}
              ></input>
              <h3 className="text-info mt-3">Valeur</h3>
              <input
                type="text"
                className="form-control"
                ref={this.valeur}
              ></input>
              <h3 className="text-info mt-3">Duration</h3>
              <input
                type="text"
                className="form-control"
                ref={this.duration}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
