import { useParams } from "react-router-dom";

import PresclientUpdate from "./PresclientUpdate";

export default function PresclientUpdateGetter() {
    const {id} = useParams();
    return (
        <PresclientUpdate id={id}>
        </PresclientUpdate>
    );
}