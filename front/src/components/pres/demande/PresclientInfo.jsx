/* eslint-disable no-unused-vars */
import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../service/OwnService";
import React from "react";
import Swal from "sweetalert2";

export default class PresclientInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/pres";
    this.urlUtils = "/pres/utils";
    this.debut = React.createRef(null);
    this.rem = React.createRef(null);
    this.date = React.createRef(null);
    this.valeur = React.createRef(null);
  }

  componentDidMount() {
    this.initUpdate();
  }
    validerRecharge = () => {
        let date = this.debut.current.value;
        if (date === '') {
            Swal.fire({
                icon:'error',
                title:'Oups',
                text : 'Entrer une date'
            })
            return
        }
        let id =  this.state.oneValue.id
        let idUser = this.getSession(this.auth,{login:{id:-100}}).login.id
        let uri = this.url +this.urlSend+"/accept/"+id+"?idUser="+idUser+"&debut="+date;
        this.getListe(uri,
            data => {
            console.log(data);
            this.verifData(data,
                // eslint-disable-next-line no-unused-vars
                (response) => {
                window.location.reload();
                }
            )
            },
            // eslint-disable-next-line no-unused-vars
            header => {

            }
        )
    }
    refuserRecharge = () => {
        let id =  this.state.oneValue.id
        let idUser = this.getSession(this.auth,{login:{id:-100}}).login.id
        let uri = this.url +this.urlSend+"/reject/"+id+"?idUser="+idUser;
        this.getListe(uri,
            data => {
            console.log(data);
            this.verifData(data,
                // eslint-disable-next-line no-unused-vars
                (response) => {
                window.location.reload();
                }
            )
            },
            // eslint-disable-next-line no-unused-vars
            header => {

            }
        )
    }

    rembourser = () => {
        let mois = this.rem.current.value
        let valeur = this.valeur.current.value
        let date = this.date.current.value
        if (mois === '' || valeur == '' || date == '') {
            Swal.fire({
                icon : 'error',
                title : 'Oups',
                text : 'Remplissez les champs'
            })
            return;
        }
        let m = mois.split("-")
        let id =  this.state.oneValue.id
        let data = {
            mois : m[1],
            annee : m[0],
            presclientid : {
                id : id
            },
            valeur : valeur,
            date : date
        }
        let idUser = this.getSession(this.auth,{login:{id:-100}}).login.id
        let uri = this.url +"/remboursement"+"?idUser="+idUser;
        this.sendData(uri,data,
            response => {
                this.verifData(response,
                    resp => {
                        window.location.reload()
                    }
                    )
            },
            header => {
                
            })
    }

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Fiche de demande de prêt</h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-warning btn-block"
                      onClick={() => {
                        window.location.replace(
                          "/pres/update/" + this.props.id
                        );
                      }}
                    >
                      Modifier
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/pres");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-success btn-block"
                      onClick={() => {
                        window.location.replace("/pres/form/");
                      }}
                    >
                      Ajouter
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={() => {
                        this.delete();
                      }}
                    >
                      Suprimer
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-info mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Client</h3>
                    <p className="text-dark">
                      {this.state.oneValue.clientid.nom} {this.state.oneValue.clientid.prenom}
                    </p>
                    <h3 className="text-info">Date</h3>
                    <p className="text-dark">{OwnService.formatDate( this.state.oneValue.date)}</p>
                    <h3 className="text-info">Valeur</h3>
                    <p className="text-dark">{OwnService.format( this.state.oneValue.valeur)}</p>
                    <h3 className="text-info">Duration</h3>
                    <p className="text-dark">{this.state.oneValue.duration} mois</p>
                    <h3 className="text-info">Etat</h3>
                    <p className="text-dark">
                    {this.state.oneValue.etat === -25 ? 
                      <p className="text-danger">Réfusé</p>:
                      this.state.oneValue.etat === 0 ? 
                      <p className="text-info">En attente</p>:
                      <p className="text-success">Validé</p>
                    }</p>
                    
                   
                        {
                          this.state.oneValue.etat === -25 ?
                          <></>: 
                          this.state.oneValue.etat === 50 ?
                          <>  </>:
                            <>
                             <div>
                        
                        <h2 className="text-dark mt-5">Validation</h2>
                        <h3 className="text-info mt-5">Date de début de remboursement</h3>
                        <input type="date" ref={this.debut} className="form-control mb-3"/>
                            <button
                                className="btn btn-success"
                                onClick={() => {
                                this.validerRecharge()
                                }}
                            >
                                Valider
                            </button>
                            <button
                                className="btn btn-outline-danger ml-3"
                                onClick={() => {
                                this.refuserRecharge()
                                }}
                            >
                                Réfuser
                            </button>
                        
                        </div>
                            </>
                        }
                  </>
                ) : (
                  <></>
                )}
              </div>
                <div className="col-12">
                
                {
                    this.state.oneValue !== null &&
                    this.state.oneValue !== undefined ?
                    <> {
                    this.state.oneValue.debut !== null && this.state.oneValue.etat >= 50 ?
                    <>
                        <h2 className="text-dark mt-5">Tableau de remboursement </h2>
                        <h3 className="text-info mt-3">Taux</h3>
                        <p>{this.state.oneValue.taux.valeur*100}%</p>
                        <h3 className="text-info">Total</h3>
                        <p>{OwnService.format(this.state.oneValue.valeur) }</p>
                        <table className="table mt-3">
                            <thead className="bg-info text-white">
                                <tr>
                                    <th></th>   
                                    <th>Reste</th>
                                    <th>Montant</th>
                                    <th>Intérêt</th>
                                    <th>Total</th>
                                    <th>Remboursé</th>
                                    <th>Etat</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.oneValue.remboursement  !== null ?
                                    <> {
                                    this.state.oneValue.remboursement.map((rem,index)=> (
                                        <>
                                            {
                                                rem.tot ?
                                                <tr  key={index} className="text-info">
                                                    <td>Total</td>
                                                    <td></td>
                                                    <td>{OwnService.format(rem.montant) }</td>
                                                    <td>{OwnService.format(rem.interet) }</td>
                                                    <td></td>
                                                </tr> : 
                                                <tr key={index}>
                                                    <td>{rem.hValue} {rem.annee}</td>
                                                    <td>{OwnService.format(rem.reste) }</td>
                                                    <td>{OwnService.format(rem.montant) }</td>
                                                    <td>{OwnService.format(rem.interet) }</td>
                                                    <td>{OwnService.format(rem.total) }</td>
                                                    <td>{OwnService.format(rem.remboursement !== null? rem.remboursement.valeur: 0)}</td>
                                                    <td>{(rem.percent) }%</td>
                                                </tr>
                                            }
                                        </>
                                    ))}
                                    </>
                                    :<></>
                                }
                            </tbody>
                        </table>
                        <div className="row">
                            <div className="col-6">
                                <h2 className="text-dark mt-5">Remboursement</h2>
                                <h3 className="text-info mt-3">Date</h3>
                                <input ref={this.date} type="date" className="form-control"></input>
                                <h3 className="text-info mt-3">Mois</h3>
                                <input ref={this.rem} type="month" className="form-control"></input>
                                <h3 className="text-info mt-3">Montant</h3>
                                <input ref={this.valeur} type="text" className="form-control" defaultValue={0}></input>
                                <button onClick={() => {
                                    this.rembourser()
                                }} className="btn btn-block btn-success mt-3">Rembourser</button>
                            </div>
                        </div>
                    </>:<></>
                    }</>
                    :<></>
                }
                </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
