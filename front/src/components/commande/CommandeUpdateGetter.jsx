import { useParams } from "react-router-dom";
import CommandeUpdate from "./CommandeUpdate";

export default function CommandeUpdateGetter() {
    const {id} = useParams();
    return (
        <CommandeUpdate id={id}>
        </CommandeUpdate>
    );
}