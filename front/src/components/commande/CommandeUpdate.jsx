import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class CommandeUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.numero = React.createRef(null);
    this.date = React.createRef(null);
    this.employeid = React.createRef(null);
    this.societeid = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/commande";
    this.urlUtils = "/commande/utils";
    this.afterValidation = "";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.numero.current.value = this.state.oneValue.numero;
    this.date.current.value = this.state.oneValue.date;
    this.employeid.current.value = this.state.oneValue.employeid.id;
    this.societeid.current.value = this.state.oneValue.societeid.id;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      numero: this.prepare(this.numero),
      date: this.prepare(this.date),
      employeid: this.checkRefNull(
        this.employeid,
        { id: this.prepare(this.employeid) },
        null
      ),
      societeid: this.checkRefNull(
        this.societeid,
        { id: this.prepare(this.societeid) },
        null
      ),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Numero</h3>
              <input
                type="text"
                className="form-control"
                ref={this.numero}
              ></input>
              <h3 className="text-info mt-3">Date</h3>
              <input
                type="date"
                className="form-control"
                ref={this.date}
              ></input>
              <h3 className="text-info mt-3">Employe</h3>
              <select className="form-control" ref={this.employeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.employeid.map((data, index) => (
                    <option value={data.id}>{data.description}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Societe</h3>
              <select className="form-control" ref={this.societeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.societeid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Id</h3>
              <input type="text" className="form-control" ref={this.id}></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
