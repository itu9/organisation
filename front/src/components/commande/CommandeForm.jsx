import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class CommandeForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.numero = React.createRef(null);
    this.date = React.createRef(null);
    this.employeid = React.createRef(null);
    this.societeid = React.createRef(null);
    this.urlSend = "/commande";
    this.urlUtils = "/commande/utils";
    this.afterValidation = "";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      numero: this.prepare(this.numero),
      date: this.prepare(this.date),
      employeid: this.checkRefNull(
        this.employeid,
        { id: this.prepare(this.employeid) },
        null
      ),
      societeid: this.checkRefNull(
        this.societeid,
        { id: this.prepare(this.societeid) },
        null
      ),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Numero</h3>
              <input
                type="text"
                className="form-control"
                ref={this.numero}
              ></input>
              <h3 className="text-info mt-3">Date</h3>
              <input
                type="date"
                className="form-control"
                ref={this.date}
              ></input>
              <h3 className="text-info mt-3">Employe</h3>
              <select className="form-control" ref={this.employeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.employeid.map((data, index) => (
                    <option value={data.id}>{data.description}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Societe</h3>
              <select className="form-control" ref={this.societeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.societeid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
