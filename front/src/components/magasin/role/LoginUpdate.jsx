import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class LoginUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.email = React.createRef(null);
    this.pwd = React.createRef(null);
    this.nom = React.createRef(null);
    this.prenom = React.createRef(null);
    this.acreditation = React.createRef(null);
    this.naissance = React.createRef(null);
    this.place = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/role";
    this.urlUtils = "/role/utils";
    this.afterValidation = "/role";
  }
  componentDidMount() {
    this.initUpdate();
  }
  
  getUpdateUrl = () => {
    this.checkConnexion();
    let auth = this.getSession(this.auth,null)
    return this.url+this.urlSend+"?idUser="+auth.login.id
}
  actionUpdate = () => {
    this.email.current.value = this.state.oneValue.email;
    this.pwd.current.value = this.state.oneValue.pwd;
    this.nom.current.value = this.state.oneValue.nom;
    this.prenom.current.value = this.state.oneValue.prenom;
    this.acreditation.current.value = this.state.oneValue.acreditation;
    this.naissance.current.value = this.state.oneValue.naissance;
    this.place.current.value = this.state.oneValue.place.id;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      email: this.prepare(this.email),
      pwd: this.prepare(this.pwd),
      nom: this.prepare(this.nom),
      prenom: this.prepare(this.prenom),
      acreditation: this.prepare(this.acreditation),
      naissance: this.prepare(this.naissance),
      place: this.checkRefNull(
        this.place,
        { id: this.prepare(this.place) },
        null
      ),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Email</h3>
              <input
                type="text"
                className="form-control"
                ref={this.email}
              ></input>
              <h3 className="text-info mt-3">Pwd</h3>
              <input
                type="text"
                className="form-control"
                ref={this.pwd}
              ></input>
              <h3 className="text-info mt-3">Nom</h3>
              <input
                type="text"
                className="form-control"
                ref={this.nom}
              ></input>
              <h3 className="text-info mt-3">Prenom</h3>
              <input
                type="text"
                className="form-control"
                ref={this.prenom}
              ></input>
              <h3 className="text-info mt-3">Acreditation</h3>
              <input
                type="text"
                className="form-control"
                ref={this.acreditation}
              ></input>
              <h3 className="text-info mt-3">Naissance</h3>
              <input
                type="date"
                className="form-control"
                ref={this.naissance}
              ></input>
              <h3 className="text-info mt-3">Place</h3>
              <select className="form-control" ref={this.place}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.place.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
