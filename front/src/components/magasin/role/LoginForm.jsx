import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class LoginForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.email = React.createRef(null);
    this.pwd = React.createRef(null);
    this.nom = React.createRef(null);
    this.prenom = React.createRef(null);
    this.acreditation = React.createRef(null);
    this.naissance = React.createRef(null);
    this.place = React.createRef(null);
    this.urlSend = "/role";
    this.urlUtils = "/role/utils";
    this.afterValidation = "/role";
  }
  componentDidMount() {
    this.init();
  }
  getFormUrl = () => {
    this.checkConnexion();
    let auth = this.getSession(this.auth,null)
    return this.url+this.urlSend+"?idUser="+auth.login.id
}

  validateData = () => {
    let data = {
      email: this.prepare(this.email),
      pwd: this.prepare(this.pwd),
      nom: this.prepare(this.nom),
      prenom: this.prepare(this.prenom),
      acreditation: 25,
      naissance: this.prepare(this.naissance),
      place: this.checkRefNull(
        this.place,
        { id: this.prepare(this.place) },
        null
      ),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Email</h3>
              <input
                type="text"
                className="form-control"
                ref={this.email}
              ></input>
              <h3 className="text-info mt-3">Mot de passe</h3>
              <input
                type="password"
                className="form-control"
                ref={this.pwd}
              ></input>
              <h3 className="text-info mt-3">Nom</h3>
              <input
                type="text"
                className="form-control"
                ref={this.nom}
              ></input>
              <h3 className="text-info mt-3">Prenom</h3>
              <input
                type="text"
                className="form-control"
                ref={this.prenom}
              ></input>
              <h3 className="text-info mt-3">Date de naissance</h3>
              <input
                type="date"
                className="form-control"
                ref={this.naissance}
              ></input>
              <h3 className="text-info mt-3">Place</h3>
              <select className="form-control" ref={this.place}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.place.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
