import { useParams } from "react-router-dom";
import LoginUpdate from "./LoginUpdate";

export default function LoginUpdateGetter() {
    const {id} = useParams();
    return (
        <LoginUpdate id={id}>
        </LoginUpdate>
    );
}