import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class LaptopUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.referenceid = React.createRef(null);
    this.ramid = React.createRef(null);
    this.ram = React.createRef(null);
    this.processeurid = React.createRef(null);
    this.ecranid = React.createRef(null);
    this.pouce = React.createRef(null);
    this.titre = React.createRef(null);
    this.stockage = React.createRef(null);
    this.prixachat = React.createRef(null);
    this.prix = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/laptop";
    this.urlUtils = "/laptop/utils";
    this.afterValidation = "/laptop";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.referenceid.current.value = this.state.oneValue.referenceid.id;
    this.ramid.current.value = this.state.oneValue.ramid.id;
    this.ram.current.value = this.state.oneValue.ram;
    this.processeurid.current.value = this.state.oneValue.processeurid.id;
    this.ecranid.current.value = this.state.oneValue.ecranid.id;
    this.pouce.current.value = this.state.oneValue.pouce;
    this.titre.current.value = this.state.oneValue.titre;
    this.stockage.current.value = this.state.oneValue.stockage;
    this.prixachat.current.value = this.state.oneValue.prixachat;
    this.prix.current.value = this.state.oneValue.prix;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      referenceid: this.checkRefNull(
        this.referenceid,
        { id: this.prepare(this.referenceid) },
        null
      ),
      ramid: this.checkRefNull(
        this.ramid,
        { id: this.prepare(this.ramid) },
        null
      ),
      ram: this.prepare(this.ram),
      processeurid: this.checkRefNull(
        this.processeurid,
        { id: this.prepare(this.processeurid) },
        null
      ),
      ecranid: this.checkRefNull(
        this.ecranid,
        { id: this.prepare(this.ecranid) },
        null
      ),
      pouce: this.prepare(this.pouce),
      titre: this.prepare(this.titre),
      stockage: this.prepare(this.stockage),
      prixachat: this.prepare(this.prixachat),
      prix: this.prepare(this.prix),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Reference</h3>
              <select className="form-control" ref={this.referenceid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.referenceid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Type de ram</h3>
              <select className="form-control" ref={this.ramid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.ramid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Ram</h3>
              <input
                type="text"
                className="form-control"
                ref={this.ram}
              ></input>
              <h3 className="text-info mt-3">Processeur</h3>
              <select className="form-control" ref={this.processeurid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.processeurid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Ecran</h3>
              <select className="form-control" ref={this.ecranid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.ecranid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Pouce</h3>
              <input
                type="text"
                className="form-control"
                ref={this.pouce}
              ></input>
              <h3 className="text-info mt-3">Titre</h3>
              <input
                type="text"
                className="form-control"
                ref={this.titre}
              ></input>
              <h3 className="text-info mt-3">Stockage</h3>
              <input
                type="text"
                className="form-control"
                ref={this.stockage}
              ></input>
              <h3 className="text-info mt-3">Prix d'achat</h3>
              <input type="text" className="form-control" ref={this.prixachat}></input>
              <h3 className="text-info mt-3">Prix de vente</h3>
              <input type="text" className="form-control" ref={this.prix}></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
