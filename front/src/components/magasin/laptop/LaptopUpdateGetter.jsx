import { useParams } from "react-router-dom";
import LaptopUpdate from "./LaptopUpdate";

export default function LaptopUpdateGetter() {
    const {id} = useParams();
    return (
        <LaptopUpdate id={id}>
        </LaptopUpdate>
    );
}