import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";
import OwnService from "service/OwnService";

export default class LaptopListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.referenceid = React.createRef(null);
    this.ramid = React.createRef(null);
    this.ramhmin = React.createRef(null);
    this.ramhmax = React.createRef(null);
    this.processeurid = React.createRef(null);
    this.ecranid = React.createRef(null);
    this.prixhmin = React.createRef(null);
    this.prixhmax = React.createRef(null);
    this.prixachathmin = React.createRef(null);
    this.prixachathmax = React.createRef(null);
    this.poucehmin = React.createRef(null);
    this.poucehmax = React.createRef(null);
    this.stockagehmin = React.createRef(null);
    this.stockagehmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/laptop/filter";
    this.urlUtils = "/laptop/utils";
    this.baseUrl = "/laptop";
  }
  componentDidMount() {
    this.init();
  }
  prepareDataListe = () => {
    let data = {
      keys: this.prepare(this.keys),
      referenceid: this.checkRefNull(
        this.referenceid,
        { id: this.prepare(this.referenceid) },
        null
      ),
      ramid: this.checkRefNull(
        this.ramid,
        { id: this.prepare(this.ramid) },
        null
      ),
      ramhmin: this.prepare(this.ramhmin),
      ramhmax: this.prepare(this.ramhmax),
      processeurid: this.checkRefNull(
        this.processeurid,
        { id: this.prepare(this.processeurid) },
        null
      ),
      ecranid: this.checkRefNull(
        this.ecranid,
        { id: this.prepare(this.ecranid) },
        null
      ),
      prixhmin: this.prepare(this.prixhmin),
      prixhmax: this.prepare(this.prixhmax),
      prixachathmin: this.prepare(this.prixachathmin),
      prixachathmax: this.prepare(this.prixachathmax),
      poucehmin: this.prepare(this.poucehmin),
      poucehmax: this.prepare(this.poucehmax),
      stockagehmin: this.prepare(this.stockagehmin),
      stockagehmax: this.prepare(this.stockagehmax),
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Modeles de laptop existants"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Reference</h3>
                      <select className="form-control" ref={this.referenceid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.referenceid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Type de ram</h3>
                      <select className="form-control" ref={this.ramid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.ramid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Ram</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.ramhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.ramhmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Processeur</h3>
                      <select className="form-control" ref={this.processeurid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.processeurid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Ecran</h3>
                      <select className="form-control" ref={this.ecranid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.ecranid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Pouce</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.poucehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.poucehmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Prix de vente</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.prixhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.prixhmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Prix d'achat</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.prixachathmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.prixachathmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Stockage</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.stockagehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.stockagehmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/laptop/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Reference</td>
                    <td>Ram</td>
                    <td>Stockage</td>
                    <td>Prix de vente</td>
                    <td>Prix d'achat</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.referenceid.titre}</td>
                      <td>{data.ram}go </td>
                      {/* <td>{data.ecranid.titre} {data.pouce} pouces</td> */}
                      <td>{data.stockage}Go</td>
                      <td>{OwnService.format( data.prix)}</td>
                      <td>{OwnService.format( data.prixachat)}</td>
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace(
                              "/laptop/update/" + data.id
                            );
                          }}
                        >
                          <i className="fas fa-pencil-alt text-warning"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            window.location.replace("/laptop/" + data.id);
                          }}
                        >
                          <i className="fas fa-plus text-info"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
