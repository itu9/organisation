import { useParams } from "react-router-dom";
import LaptopInfo from "./LaptopInfo";

export default function LaptopFicheGetter() {
    const {id} = useParams();
    return (
        <LaptopInfo id={id}>
        </LaptopInfo>
    );
}