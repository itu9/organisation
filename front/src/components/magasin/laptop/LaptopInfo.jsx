import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";

export default class LaptopInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/laptop";
    this.urlUtils = "/laptop/utils";
  }

  componentDidMount() {
    this.initUpdate();
  }

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Fiche de details du laptop&nbsp;
        {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (<span className="text-info">{this.state.oneValue.referenceid.titre}</span>):<></>}
        </h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-warning btn-block"
                      onClick={() => {
                        window.location.replace(
                          "/laptop/update/" + this.props.id
                        );
                      }}
                    >
                      Modifier
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/laptop");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-success btn-block"
                      onClick={() => {
                        window.location.replace("/laptop/form/");
                      }}
                    >
                      Ajouter
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={() => {
                        this.delete();
                      }}
                    >
                      Suprimer
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-info mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Reference</h3>
                    <p className="text-dark">
                      {this.state.oneValue.referenceid.titre}
                    </p>
                    <h3 className="text-info">Type de ram</h3>
                    <p className="text-dark">
                      {this.state.oneValue.ramid.titre}
                    </p>
                    <h3 className="text-info">Ram</h3>
                    <p className="text-dark">{this.state.oneValue.ram}go</p>
                    <h3 className="text-info">Processeur</h3>
                    <p className="text-dark">
                      {this.state.oneValue.processeurid.titre} {this.state.oneValue.processeurid.frequence}GHz
                    </p>
                    <h3 className="text-info">Ecran</h3>
                    <p className="text-dark">
                      {this.state.oneValue.ecranid.titre} {this.state.oneValue.pouce} pouces
                    </p>
                    <h3 className="text-info">Nom du laptop</h3>
                    <p className="text-dark">{this.state.oneValue.titre}</p>
                    <h3 className="text-info">Stockage</h3>
                    <p className="text-dark">{this.state.oneValue.stockage}Go</p>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
