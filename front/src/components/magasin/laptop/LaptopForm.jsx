import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class LaptopForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.referenceid = React.createRef(null);
    this.ramid = React.createRef(null);
    this.ram = React.createRef(null);
    this.processeurid = React.createRef(null);
    this.ecranid = React.createRef(null);
    this.prix = React.createRef(null);
    this.pouce = React.createRef(null);
    this.titre = React.createRef(null);
    this.stockage = React.createRef(null);
    this.urlSend = "/laptop";
    this.urlUtils = "/laptop/utils";
    this.afterValidation = "";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      referenceid: this.checkRefNull(
        this.referenceid,
        { id: this.prepare(this.referenceid) },
        null
      ),
      ramid: this.checkRefNull(
        this.ramid,
        { id: this.prepare(this.ramid) },
        null
      ),
      ram: this.prepare(this.ram),
      processeurid: this.checkRefNull(
        this.processeurid,
        { id: this.prepare(this.processeurid) },
        null
      ),
      ecranid: this.checkRefNull(
        this.ecranid,
        { id: this.prepare(this.ecranid) },
        null
      ),
      prix: this.prepare(this.prix),
      pouce: this.prepare(this.pouce),
      titre: this.prepare(this.titre),
      stockage: this.prepare(this.stockage),
      prixachat : 0
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Reference</h3>
              <select className="form-control" ref={this.referenceid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.referenceid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Type de ram</h3>
              <select className="form-control" ref={this.ramid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.ramid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Ram</h3>
              <input
                type="text"
                className="form-control"
                ref={this.ram}
              ></input>
              <h3 className="text-info mt-3">Prix</h3>
              <input
                type="text"
                className="form-control"
                ref={ this.prix}
              ></input>
              <h3 className="text-info mt-3">Processeur</h3>
              <select className="form-control" ref={this.processeurid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.processeurid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Ecran</h3>
              <select className="form-control" ref={this.ecranid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.ecranid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Pouce</h3>
              <input
                type="text"
                className="form-control"
                ref={this.pouce}
              ></input>
              <h3 className="text-info mt-3">Titre</h3>
              <input
                type="text"
                className="form-control"
                ref={this.titre}
              ></input>
              <h3 className="text-info mt-3">Stockage</h3>
              <input
                type="text"
                className="form-control"
                ref={this.stockage}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
