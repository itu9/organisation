import { useParams } from "react-router-dom";
import EcranUpdate from "./EcranUpdate";

export default function EcranUpdateGetter() {
    const {id} = useParams();
    return (
        <EcranUpdate id={id}>
        </EcranUpdate>
    );
}