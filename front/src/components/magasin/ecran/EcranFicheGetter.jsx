import { useParams } from "react-router-dom";
import EcranInfo from "./EcranInfo";

export default function EcranFicheGetter() {
    const {id} = useParams();
    return (
        <EcranInfo id={id}>
        </EcranInfo>
    );
}