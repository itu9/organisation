import { useParams } from "react-router-dom";
import MarqueUpdate from "./MarqueUpdate";

export default function MarqueUpdateGetter() {
    const {id} = useParams();
    return (
        <MarqueUpdate id={id}>
        </MarqueUpdate>
    );
}