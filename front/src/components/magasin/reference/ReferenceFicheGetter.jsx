import { useParams } from "react-router-dom";
import ReferenceInfo from "./ReferenceInfo";

export default function ReferenceFicheGetter() {
    const {id} = useParams();
    return (
        <ReferenceInfo id={id}>
        </ReferenceInfo>
    );
}