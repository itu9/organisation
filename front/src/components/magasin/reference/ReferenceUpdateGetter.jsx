import { useParams } from "react-router-dom";
import ReferenceUpdate from "./ReferenceUpdate";

export default function ReferenceUpdateGetter() {
    const {id} = useParams();
    return (
        <ReferenceUpdate id={id}>
        </ReferenceUpdate>
    );
}