import { Line } from "react-chartjs-2";
import General from "/src/components/utils/General";
import {  Chart, registerables  } from "chart.js";
import React from "react";
import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import OwnService from "service/OwnService";
import Liste from "/src/components/utils/Liste";
Chart.register(...registerables)

export default class VenteParPoint extends General {
    constructor(params) {
        super(params);
        this.data = React.createRef(null);
        this.date = React.createRef(null);
        this.id = React.createRef(null);
        this.data.current = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
            {
                label: 'Pomme de terre',
                data: [65, 59, 80, 81, 56, 55, 40],
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgba(255, 99, 132, 0.2)',
            }
            ],
          }
    }
    componentDidMount() {
        this.setState({
            data : [],
            annee : [],
            benef : []
        },this.getData);
    }

    getData = () => {
        this.getListe(this.url+"/place",
            data => {
                this.verifData(data,
                    response => {
                        this.setState({
                            produit : response.data
                        },
                            ()=> {
                                let data = this.state.produit[0]
                                this.gatherData(data);
                            }
                        )
                    }
                )
            },
            header => {

            }
        )
    }
    gatherData = (produit) => {
        this.getListe(this.url+"/years",
        data => {
            this.verifData(data,
                response => {
                    console.log(response.data);
                    this.setState({
                        annee : response.data
                    },
                    ()=>{
                        if (this.state.annee.length > 0) {
                            this.search(produit,this.state.annee[0].annee)
                        }
                    })
                }
            )
        },
        header => {})
    }

    search = (dataSend,annee) => {
        let param = {
            page : -1,
            placeid : {id : dataSend.id},
            anneehmin : annee,
            anneehmax : annee
        }
        this.sendData(this.url+'/stat/vente/filter',param,
            (data) => {
                this.verifData(data,
                    response => {
                        this.setState({
                            benef : response.data
                        })
                        this.toStat(response.data,dataSend,annee)
                    }
                )
            }
        )
    }

    toStat = (response,dataSend,annee) => {
        let data = response.map(res => res.prix);
        let dataField = {
            labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet','Aout','Septembre','Octobre','Novembre','Décembre'],
            datasets: [
            {
                label: `Le total des ventes par mois du point `+dataSend.titre+` de l'anné `+annee,
                data: data,
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgba(255, 99, 132, 0.2)',
            }
            ],
          };
        this.data.current = dataField;
        this.setState({
            data : dataField
        })
    }

    getProdBy = (id) => {
        let data = this.state.produit.filter(pr => pr.id === id)
        return data[0];
    }

    load = () => {
        let id = parseInt( this.id.current.value,10);
        let data = this.getProdBy(id)
        this.search(data,this.date.current.value);
    }
    getPdfUrl = () => {
        return this.url+'/stat/vente/filter'
    }
    toPdf = ()=> {
        let id = parseInt( this.id.current.value,10);
        let data = this.getProdBy(id)
        let param = {
            page : -1,
            placeid : {id : data.id},
            anneehmin : this.date.current.value,
            anneehmax : this.date.current.value
        }
        let ans = {
          ...param,
          pdf : {
            titre : `Le total des ventes par mois du `+data.titre+` de l'année `+this.date.current.value,
            fields : [
                {
                    field : "moislabel",
                    titre : "Mois"
                },
                {
                    field : "prix",
                    titre : "Montant de vente"
                }
            ]
          }
        }
        // console.log('ans',ans);
        this.pdf(ans);
        
      }
    render() {
        return (
             <HContainer>
                <Border>    
                    <h2 className="text-info">Statistique de vente global par point</h2>
                    <div className="row">
                        <div className="col-4">
                            <h3 className="text-dark mt-3">Point de vente</h3>
                            <select className="form-control" ref={this.id} onChange={this.load}>
                                {
                                    this.state.produit !== undefined && this.state.produit !== null ?
                                    this.state.produit.map(data => (
                                        <option value={data.id} >{data.titre}</option>
                                    ))
                                    :<></>
                                }
                            </select>
                            <h3 className="text-dark mt-3">Annee</h3>
                            <select className="form-control" ref={this.date} onChange={this.load}>
                                {
                                    this.state.annee !== undefined && this.state.annee !== null ?
                                    this.state.annee.map(data => (
                                        <option value={data.annee} >{data.annee}</option>
                                    ))
                                    :<></>
                                }
                            </select>
                            <button className="w-100 btn btn-success mt-3  d-print-none"
                                onClick={this.toPdf}
                            >
                                <i className="fas fa-file-pdf"></i>&nbsp;Extraire en pdf
                            </button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-8">
                            <Liste
                                head={
                                    <tr>
                                        <th>Mois</th>
                                        <th>Annee</th>
                                        <th>Montant</th>
                                    </tr>
                                }
                            >
                                {
                                    this.state.benef !== undefined && this.state.benef !== null ?
                                    this.state.benef.map(data => (
                                        <tr>
                                            <td>{data.hValue}</td>
                                            <td>{data.annee}</td>
                                            <td>{OwnService.format( data.prix)}</td>
                                        </tr>
                                    ))
                                    :<></>
                                }
                            </Liste>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4"> 
                            <button className="w-100 btn btn-success mt-3  mb-5 d-print-none"
                                onClick={()=>{window.print()}}
                            >
                                <i className="fas fa-file-pdf"></i>&nbsp;Extraire graph
                            </button>
                        </div>
                    </div>
                    {
                        this.data !== undefined && this.data !== null ?
                        <Line data={this.data.current} /> : <></>
                    }
                    
                </Border>
             </HContainer>
        );
    }
}