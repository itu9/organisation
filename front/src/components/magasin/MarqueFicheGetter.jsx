import { useParams } from "react-router-dom";
import MarqueInfo from "./MarqueInfo";

export default function MarqueFicheGetter() {
    const {id} = useParams();
    return (
        <MarqueInfo id={id}>
        </MarqueInfo>
    );
}