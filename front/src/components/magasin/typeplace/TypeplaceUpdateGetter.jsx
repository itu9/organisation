import { useParams } from "react-router-dom";
import TypeplaceUpdate from "./TypeplaceUpdate";

export default function TypeplaceUpdateGetter() {
    const {id} = useParams();
    return (
        <TypeplaceUpdate id={id}>
        </TypeplaceUpdate>
    );
}