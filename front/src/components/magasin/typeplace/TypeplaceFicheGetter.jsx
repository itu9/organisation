import { useParams } from "react-router-dom";
import TypeplaceInfo from "./TypeplaceInfo";

export default function TypeplaceFicheGetter() {
    const {id} = useParams();
    return (
        <TypeplaceInfo id={id}>
        </TypeplaceInfo>
    );
}