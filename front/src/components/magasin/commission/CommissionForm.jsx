import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class CommissionForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.min = React.createRef(null);this.max = React.createRef(null);this.valeur = React.createRef(null);
        this.urlSend = "/commission";
        this.urlUtils = "/commission/utils";
        this.afterValidation = "/commission";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            min : this.prepare(this.min),max : this.prepare(this.max),valeur : this.prepare(this.valeur)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Min</h3>
<input type="text" className="form-control" ref={this.min} ></input><h3 className="text-info mt-3">Max</h3>
<input type="text" className="form-control" ref={this.max} ></input><h3 className="text-info mt-3">Valeur</h3>
<input type="text" className="form-control" ref={this.valeur} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}