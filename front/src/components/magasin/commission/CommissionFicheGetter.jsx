import { useParams } from "react-router-dom";
import CommissionInfo from "./CommissionInfo";

export default function CommissionFicheGetter() {
    const {id} = useParams();
    return (
        <CommissionInfo id={id}>
        </CommissionInfo>
    );
}