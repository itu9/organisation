import { useParams } from "react-router-dom";
import CommissionUpdate from "./CommissionUpdate";

export default function CommissionUpdateGetter() {
    const {id} = useParams();
    return (
        <CommissionUpdate id={id}>
        </CommissionUpdate>
    );
}