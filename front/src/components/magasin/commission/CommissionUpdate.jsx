import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class CommissionUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.min = React.createRef(null);
    this.max = React.createRef(null);
    this.valeur = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/commission";
    this.urlUtils = "/commission/utils";
    this.afterValidation = "/commission";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.min.current.value = this.state.oneValue.min;
    this.max.current.value = this.state.oneValue.max;
    this.valeur.current.value = this.state.oneValue.valeur;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      min: this.prepare(this.min),
      max: this.prepare(this.max),
      valeur: this.prepare(this.valeur),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Min</h3>
              <input
                type="text"
                className="form-control"
                ref={this.min}
              ></input>
              <h3 className="text-info mt-3">Max</h3>
              <input
                type="text"
                className="form-control"
                ref={this.max}
              ></input>
              <h3 className="text-info mt-3">Valeur</h3>
              <input
                type="text"
                className="form-control"
                ref={this.valeur}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
