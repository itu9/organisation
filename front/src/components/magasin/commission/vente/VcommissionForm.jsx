import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class VcommissionForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.placeid = React.createRef(null);this.mois = React.createRef(null);this.prix = React.createRef(null);this.annee = React.createRef(null);
        this.urlSend = "/commission/vente";
        this.urlUtils = "/commission/vente/utils";
        this.afterValidation = "";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            placeid : this.checkRefNull(this.placeid,{id:this.prepare(this.placeid)},null),mois : this.prepare(this.mois),prix : this.prepare(this.prix),annee : this.prepare(this.annee)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3" >Point de vente</h3>
<select className="form-control" ref={this.placeid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.placeid.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Mois</h3>
<input type="text" className="form-control" ref={this.mois} ></input><h3 className="text-info mt-3">Prix</h3>
<input type="text" className="form-control" ref={this.prix} ></input><h3 className="text-info mt-3">Annee</h3>
<input type="text" className="form-control" ref={this.annee} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}