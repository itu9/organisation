import { useParams } from "react-router-dom";
import VcommissionInfo from "./VcommissionInfo";

export default function VcommissionFicheGetter() {
    const {id} = useParams();
    return (
        <VcommissionInfo id={id}>
        </VcommissionInfo>
    );
}