import { useParams } from "react-router-dom";

export default function VcommissionUpdateGetter() {
    const {id} = useParams();
    return (
        <VcommissionUpdate id={id}>
        </VcommissionUpdate>
    );
}