import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";

export default class VcommissionListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.placeid = React.createRef(null);
    this.moishmin = React.createRef(null);
    this.moishmax = React.createRef(null);
    this.prixhmin = React.createRef(null);
    this.prixhmax = React.createRef(null);
    this.anneehmin = React.createRef(null);
    this.anneehmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/commission/vente/filter";
    this.urlUtils = "/commission/vente/utils";
    this.baseUrl = "/commission/vente";
  }
  componentDidMount() {
    this.init();
  }
  prepareDataListe = () => {
    let inp = this.moishmin.current.value;
    let annee = null
    let mois = null
    if (inp !== '') {
        let spl = inp.split("-")
        annee = spl[0]
        mois = spl[1]
    }
    let data = {
      keys: this.prepare(this.keys),
      placeid: this.checkRefNull(
        this.placeid,
        { id: this.prepare(this.placeid) },
        null
      ),
      moishmin: mois,
      moishmax: mois,
      prixhmin: this.prepare(this.prixhmin),
      prixhmax: this.prepare(this.prixhmax),
      anneehmin: annee,
      anneehmax: annee
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Point de vente</h3>
                      <select className="form-control" ref={this.placeid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.placeid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Mois</h3>
                      <div className="row">
                        <div className="col-12">
                          <input
                            type="month"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.moishmin}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Prix</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.prixhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.prixhmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace(
                                    "/commission/vente/form"
                                  );
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Place</td>
                    <td>Mois</td>
                    <td>Annee</td>
                    <td>Benefice</td>
                    <td>Vente</td>
                    <td>Commission</td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.placeid.titre}</td>
                      <td>{data.hValue}</td>
                      <td>{data.annee}</td>
                      <td>{data.prix}</td>
                      <td>{data.vente}</td>
                      <td>{data.commission}</td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
