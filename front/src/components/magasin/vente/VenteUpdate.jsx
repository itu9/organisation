import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class VenteUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.prix = React.createRef(null);this.place = React.createRef(null);this.details = React.createRef(null);this.date = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/vente";
        this.urlUtils = "/vente/utils";
        this.afterValidation = "/vente";
    }
    componentDidMount() {
        this.initUpdate();
    }
    getUpdateUrl = () => {
        this.checkConnexion();
        let auth = this.getSession(this.auth,null)
        return this.url+this.urlSend+"?idUser="+auth.login.id
    }
    actionUpdate = ()=> {
        this.prix.current.value = this.state.oneValue.prix;this.place.current.value = this.state.oneValue.place.id;this.details.current.value = this.state.oneValue.details;this.date.current.value = this.state.oneValue.date;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            prix : this.prepare(this.prix),place : this.checkRefNull(this.place,{id:this.prepare(this.place)},null),details : this.prepare(this.details),date : this.prepare(this.date),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Prix</h3>
<input type="text" className="form-control" ref={this.prix} ></input><h3 className="text-info mt-3" >Place</h3>
<select className="form-control" ref={this.place}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.place.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Details</h3>
<input type="text" className="form-control" ref={this.details} ></input><h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}