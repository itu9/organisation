import { useParams } from "react-router-dom";

export default function VenteDetailsUpdateGetter() {
    const {id} = useParams();
    return (
        <VenteDetailsUpdate id={id}>
        </VenteDetailsUpdate>
    );
}