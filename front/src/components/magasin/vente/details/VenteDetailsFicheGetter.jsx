import { useParams } from "react-router-dom";
import VenteDetailsInfo from "./VenteDetailsInfo";

export default function VenteDetailsFicheGetter() {
    const {id} = useParams();
    return (
        <VenteDetailsInfo id={id}>
        </VenteDetailsInfo>
    );
}