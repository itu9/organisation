import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";
import OwnService from "service/OwnService";

export default class VenteDetailsListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.vente = React.createRef(null);
    this.laptop = React.createRef(null);
    this.nbhmin = React.createRef(null);
    this.nbhmax = React.createRef(null);
    this.prixhmin = React.createRef(null);
    this.prixhmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/ventedetails/recherche";
    this.urlUtils = "/ventedetails/utils";
    this.baseUrl = "/ventedetails";
  }
  componentDidMount() {
    this.init();
    this.setState({
      references : []
    })
  }
  getData = () =>{
    this.checkConnexion();
    let auth = this.getSession(this.auth,{})
    let dataSend = {
        place : {id : auth.login.place.id} 
    };
    this.search(dataSend);
}
  prepareDataListe = () => {
    this.checkConnexion();
    let auth = this.getSession(this.auth,{})
    let data = {
      prixhmin: this.prepare(this.prixhmin),
      prixhmax: this.prepare(this.prixhmax),
      place : {id : auth.login.place.id},
      references : this.state.references
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };

  addReferences = (val) => {
    let data = this.state.references.filter(ref => ref !== val)
    if (data.length === this.state.references.length) {
      data = [
        ...data,
        val
      ]
    }
    console.log(data);
    this.setState({
      references : data
    })
  }
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Historique de vente"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Reference</h3>
                      {
                        this.state.utils !== null && this.state.utils !== undefined ?
                        this.state.utils.references.map(ref => (
                            <label className="ml-3"><input type="checkbox" onClick={()=>{this.addReferences(ref.id)}}></input> {ref.titre}</label>

                        )) : <></>
                      }
                      <h3 className="text-info mt-3">Prix</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.prixhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.prixhmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/ventedetails/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                  <td>Reference</td>
                    <td>Date de Vente</td>
                    <td>P.U.</td>
                    <td>Quantité</td>
                    <td>Prix</td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                    <td>{data.laptop.referenceid.titre}</td>
                      <td>{OwnService.formatDate( data.vente.date)}</td>
                      <td>{OwnService.format( data.laptop.prix)}</td>
                      <td>{data.nb}</td>
                      <td>{OwnService.format( data.prix)}</td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
