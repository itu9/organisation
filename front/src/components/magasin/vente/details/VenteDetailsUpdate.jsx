import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class VenteDetailsUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.vente = React.createRef(null);this.laptop = React.createRef(null);this.nb = React.createRef(null);this.prix = React.createRef(null);this.nb = React.createRef(null);this.laptopid = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/ventedetails";
        this.urlUtils = "/ventedetails/utils";
        this.afterValidation = "/ventedetails";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.vente.current.value = this.state.oneValue.vente.id;this.laptop.current.value = this.state.oneValue.laptop.titre;this.nb.current.value = this.state.oneValue.nb;this.prix.current.value = this.state.oneValue.prix;this.nb.current.value = this.state.oneValue.nb;this.laptopid.current.value = this.state.oneValue.laptopid;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            vente : this.checkRefNull(this.vente,{id:this.prepare(this.vente)},null),laptop : this.checkRefNull(this.laptop,{titre:this.prepare(this.laptop)},null),nb : this.prepare(this.nb),prix : this.prepare(this.prix),nb : this.prepare(this.nb),laptopid : this.prepare(this.laptopid),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Date de vente</h3>
<select className="form-control" ref={this.vente}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.vente.map((data,index) => (
            <option value={data.id} >{data.date}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Reference</h3>
<select className="form-control" ref={this.laptop}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.laptop.map((data,index) => (
            <option value={data.titre} >{data.id}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Nb</h3>
<input type="text" className="form-control" ref={this.nb} ></input><h3 className="text-info mt-3">Prix</h3>
<input type="text" className="form-control" ref={this.prix} ></input><h3 className="text-info mt-3">Nb</h3>
<input type="text" className="form-control" ref={this.nb} ></input><h3 className="text-info mt-3">Laptopid</h3>
<input type="text" className="form-control" ref={this.laptopid} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}