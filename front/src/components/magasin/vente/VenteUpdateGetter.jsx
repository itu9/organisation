import { useParams } from "react-router-dom";
import VenteUpdate from "./VenteUpdate";

export default function VenteUpdateGetter() {
    const {id} = useParams();
    return (
        <VenteUpdate id={id}>
        </VenteUpdate>
    );
}