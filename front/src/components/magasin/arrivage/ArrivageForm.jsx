import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class ArrivageForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.laptopid = React.createRef(null);this.placeid = React.createRef(null);this.quantite = React.createRef(null);this.transfert_detailid = React.createRef(null);
        this.urlSend = "/arrivage";
        this.urlUtils = "/arrivage/utils";
        this.afterValidation = "";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            laptopid : this.checkRefNull(this.laptopid,{id:this.prepare(this.laptopid)},null),placeid : this.checkRefNull(this.placeid,{id:this.prepare(this.placeid)},null),quantite : this.prepare(this.quantite),transfert_detailid : this.prepare(this.transfert_detailid)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3" >Laptop</h3>
<select className="form-control" ref={this.laptopid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.laptopid.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Place</h3>
<select className="form-control" ref={this.placeid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.placeid.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Quantite</h3>
<input type="text" className="form-control" ref={this.quantite} ></input><h3 className="text-info mt-3">Transfert_detailid</h3>
<input type="text" className="form-control" ref={this.transfert_detailid} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}