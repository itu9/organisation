import { useParams } from "react-router-dom";
import ArrivageUpdate from "./ArrivageUpdate";

export default function ArrivageUpdateGetter() {
    const {id} = useParams();
    return (
        <ArrivageUpdate id={id}>
        </ArrivageUpdate>
    );
}