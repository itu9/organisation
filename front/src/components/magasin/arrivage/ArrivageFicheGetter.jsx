import { useParams } from "react-router-dom";
import ArrivageInfo from "./ArrivageInfo";

export default function ArrivageFicheGetter() {
    const {id} = useParams();
    return (
        <ArrivageInfo id={id}>
        </ArrivageInfo>
    );
}