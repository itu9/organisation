import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class ArrivageInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/arrivage";
    this.urlUtils = "/arrivage/utils";
    this.quantite = React.createRef(null);
  }

  componentDidMount() {
    this.initUpdate();
  }

  recevoir = () => {
    this.checkConnexion();
    let auth = this.getSession(this.auth,null)
    let data = {
      quantite : this.quantite.current.value,
      placeid : this.state.oneValue.placeid.id,
      transfert_detailid : this.state.oneValue.transfert_detailid
    };
    this.sendData(this.url+"/reception?idUser="+auth.login.id,data,
      response => {
        this.verifData(response,
          (respVerif) => {
            window.location.replace('/arrivage');
          }
          )
      }
    )
  }

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Détail de l'arrivage</h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/arrivage");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-info mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Laptop</h3>
                    <p className="text-dark">
                      {this.state.oneValue.laptopid.titre}
                    </p>
                    <h3 className="text-info">Place</h3>
                    <p className="text-dark">
                      {this.state.oneValue.placeid.titre}
                    </p>
                    <h3 className="text-info">Quantite envoyée</h3>
                    <p className="text-dark">{this.state.oneValue.quantite}</p>
                    <h2 className="text-info mt-5">Quantite reçue</h2>
                    <div className="row">
                      <div className="col-8">
                        <input className="form-control" placeholder="le nombre d'ordinateur arrivé au point de vente" ref={this.quantite}/>
                        <button className="mt-3 btn btn-success w-100" onClick={this.recevoir}>Valider</button>
                      </div>
                    </div>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
