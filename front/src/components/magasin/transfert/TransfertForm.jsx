import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";
import TransfertDetForm from "./TransfertDetForm";

export default class TransfertForm extends GeneralForm {
    constructor(params) {
        super(params);
        this.placeid = React.createRef(null);
        this.details = React.createRef(null);
        this.date = React.createRef(null);
        this.urlSend = "/transfert";
        this.urlUtils = "/transfert/utils";
        this.afterValidation = "/stock/magasin";
    }
    getFormUrl = () => {
        this.checkConnexion();
        let auth = this.getSession(this.auth,null)
        return this.url+this.urlSend+"?idUser="+auth.login.id
    }
    componentDidMount() {
        this.init();
        this.setState({
            liste: [],
            stock : []
            },this.getStock);
    }

    getStock = () => {
        
        let auths = this.getSession(this.auth,{
            login : {
                place : {
                    id : -1
                }
            }
        });
        let data = {
            page : -1,
            placeidhmin : auths.login.place.id,
            placeidhmax : auths.login.place.id
        }
        this.sendData(this.url+"/stock/filter",data,
            response => {
                this.verifData(response, 
                    res => {
                        console.log(res.data);
                        this.setState({
                            stock : res.data
                        })
                    }
                )
            }
        )
    }

    validateData = () => {
        let auths = this.getSession(this.auth,{
            login : {
                place : {
                    id : -1
                }
            }
        });
        let data = {
            placeid: this.checkRefNull(
                this.placeid,
                { id: this.prepare(this.placeid) },
                null
            ),
            details: this.state.liste,
            date: this.prepare(this.date),
            senderid : {
                id : auths.login.place.id
            }
        };
        this.validate(data);
    };
    change = (id, value) => {
        let data = this.state.liste.filter((li) => {
        console.log(value,li.value);
        if (li.id === id) {
            li.laptopid = value.laptopid;
            li.nb = value.nb;
        }
        return li;
        });
        this.setState({
            liste: data,
        },
            () => {
                console.log(this.state.liste);
            }
        );
    };
    addValue = (data) => {
        this.setState({
        liste: [...this.state.liste, data],
        });
    };
    addDet = () => {
        let id = this.getNextId();
        let data = {
            id: id,
            laptopid: 0,
            nb: 0,
        };
        this.addValue(data);
        this.addTemplate(
        <TransfertDetForm
            id={id}
            change={this.change}
            product={this.state.stock}
            remove={() => {
            this.removeTemplate(id, () => {
                let liste = this.state.liste.filter((l) => l.id !== id);
                this.setState({
                liste: liste,
                });
            });
            }}
        />,
        id
        );
    };
    render() {
        return (
        <HContainer>
            <h1 className="text-dark">Transférer des laptops vers d'autre point de vente</h1>
            <div className="row mt-3">
            <div className="col-8">
                <Border>
                <h3 className="text-info mt-3">Place</h3>
                <select className="form-control" ref={this.placeid}>
                    <option value=""></option>
                    {this.state.utils !== undefined && this.state.utils !== null ? (
                    this.state.utils.placeid.map((data, index) => (
                        <option value={data.id}>{data.titre}</option>
                    ))
                    ) : (
                    <></>
                    )}
                </select>
                <h3 className="text-info mt-3">Date</h3>
                <input
                    type="date"
                    className="form-control"
                    ref={this.date}
                ></input>
                <div className="col-12 mt-3">
                    <div className="row mt-3 text-info">
                    <div className="col-5">
                        <strong>Laptop</strong>
                    </div>
                    <div className="col-3">
                        <strong>Quantite</strong>
                    </div>
                    <div className="col-1">
                        <strong></strong>
                    </div>
                    </div>
                    {this.state.template.map((temp) => temp.data)}
                    <div className="row mt-3 text-info">
                    <div
                        className="col-12"
                        onClick={() => {
                        this.addDet();
                        }}
                    >
                        <h3 className="text-success">
                        <i className="fas fa-plus"></i>
                        </h3>
                    </div>
                    </div>
                </div>

                <button
                    onClick={this.validateData}
                    className="mt-3 btn btn-success btn-block"
                >
                    Valider
                </button>
                </Border>
            </div>
            </div>
        </HContainer>
        );
    }
}
