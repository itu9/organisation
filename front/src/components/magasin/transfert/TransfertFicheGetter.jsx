import { useParams } from "react-router-dom";
import TransfertInfo from "./TransfertInfo";

export default function TransfertFicheGetter() {
    const {id} = useParams();
    return (
        <TransfertInfo id={id}>
        </TransfertInfo>
    );
}