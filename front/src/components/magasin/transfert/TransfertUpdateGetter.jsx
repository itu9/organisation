import { useParams } from "react-router-dom";
import TransfertUpdate from "./TransfertUpdate";

export default function TransfertUpdateGetter() {
    const {id} = useParams();
    return (
        <TransfertUpdate id={id}>
        </TransfertUpdate>
    );
}