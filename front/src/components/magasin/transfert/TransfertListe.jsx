import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";

export default class TransfertListe extends GeneralListe {
    constructor(params) {
        super(params);
        this.placeid = React.createRef(null);this.datehmin = React.createRef(null);this.datehmax = React.createRef(null);
        this.keys = React.createRef(null);
        this.urlData = "/transfert/filter";
        this.urlUtils = "/transfert/utils";
        this.baseUrl = "/transfert"
    }
    componentDidMount() {
        this.init();
    }
    prepareDataListe = () => {
        let data = {
            keys : this.prepare(this.keys)
            ,placeid : this.checkRefNull(this.placeid,{id:this.prepare(this.placeid)},null),datehmin : this.prepare(this.datehmin),datehmax : this.prepare(this.datehmax)
        }
        return data;
    }
    findData = () => {
        this.search(this.prepareDataListe());
    }
    onChange = (event,page) => {
        let data = this.prepareDataListe();
        data = {
        ...data,
        page : page
        }
        this.search(data);
    }
    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                    <div className="col-12">
                        <Border>
                            <ListePage
                                title="Titre de la liste"
                                count={this.state.count}
                                onChange={this.onChange}
                                search = {
                                    <>
                                        <div className="col-6">
                                            <h3 className="text-info mt-3">Recherche</h3>
                                            <input className="form-control" type="text" placeholder="recherche par mot clé" ref={this.keys}/>
                                            <h3 className="text-info mt-3" >Place</h3>
<select className="form-control" ref={this.placeid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.placeid.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Date</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.datehmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.datehmax} />
    </div>
</div>
                                            <button className="btn btn-block btn-info mt-3" onClick={this.findData}>Rechercher</button>
                                        </div>
                                        <div className="col-12 mt-5">
                                            <div className="row">
                                                <div className="col-6"></div>
                                                <div className="col-6">
                                                    <div className="row">
                                                        <div className="col-2"></div>
                                                        <div className="col-4">
                                                        <div class="btn-list w-100">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-info dropdown-toggle w-100"
                                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Exporter
                                                                </button>
                                                                <div class="dropdown-menu w-100">
                                                                    <a class="dropdown-item " href="#a"><i className="fas fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;&nbsp;PDF </a>
                                                                    <a class="dropdown-item " href="#a"><i className="fas fa-file-excel"></i>&nbsp;&nbsp;&nbsp;&nbsp;CSV</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <div className="col-6">
                                                            <button className="btn btn-block btn-success" 
                                                                onClick={() => {
                                                                    window.location.replace("/transfert/form");
                                                                }}
                                                            >Ajouter un nouveau</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                                head = {
                                    <tr><td>Placeid</td><td>Details</td><td>Date</td><td>Mouvementid</td><td>Id</td><td></td></tr>
                                }
                            >
                            {this.state.data !== undefined && this.state.data !== null ?this.state.data.map((data,index) =>(<tr><td>{data.placeid.hvalue}</td><td>{data.details}</td><td>{data.date}</td><td>{data.mouvementid.hvalue}</td><td>{data.id}</td> <td>
    <button className="btn" onClick={()=> {
        window.location.replace("/transfert/update/"+data.id)
    }}>
        <i className="fas fa-pencil-alt text-warning"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        this.delete(data)
    }}>
        <i className="fas fa-trash-alt text-danger"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        window.location.replace("/transfert/"+data.id)
    }}>
        <i className="fas fa-plus text-info"></i>
    </button>
</td></tr>)):<></>}
                            </ListePage>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}