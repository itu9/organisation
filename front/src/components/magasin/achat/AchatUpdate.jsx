import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class AchatUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.date = React.createRef(null);this.prix = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/achat";
        this.urlUtils = "/achat/utils";
        this.afterValidation = "/achat";
    }
    
    getUpdateUrl = () => {
        this.checkConnexion();
        let auth = this.getSession(this.auth,null)
        return this.url+this.urlSend+"?idUser="+auth.login.id
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.date.current.value = this.state.oneValue.date;this.prix.current.value = this.state.oneValue.prix;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            date : this.prepare(this.date),prix : this.prepare(this.prix),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Prix</h3>
<input type="text" className="form-control" ref={this.prix} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}