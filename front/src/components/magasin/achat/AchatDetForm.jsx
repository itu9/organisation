import General from "/src/components/utils/General";
import React from "react";

export default class AchatDetForm extends General {
    constructor(params) {
        super(params);
        this.produit = React.createRef(null);
        this.pu = React.createRef(null);
        this.quantite = React.createRef(null);
    }

    recolte = () => {
        let data = {
            laptopid : this.produit.current.value,
            nb : this.quantite.current.value,
            prix : 0
        }
        this.props.change(this.props.id,data)
    }
    render() {
        return (
            <>
            <div className="row mt-3">
                <div className="col-5">
                    <select className="form-control" ref={this.produit} onChange={this.recolte}>
                        {
                            this.props.product.map(prod => (
                                <option value={prod.id}>{prod.titre}</option>
                            ))
                        }
                    </select>
                </div>
                <div className="col-4">
                    <input className="form-control" type="number" ref={this.quantite} onChange={this.recolte} defaultValue={1} />
                </div>
                <div className="col-1" onClick={
                    () => {
                        this.props.remove();
                    }
                }>
                    <h3 className="text-danger">
                    <i className="fas fa-times"></i>
                    </h3>
                </div>
            </div>
         </>
        );
    }
}