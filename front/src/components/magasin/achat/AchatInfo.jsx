import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import Liste from "/src/components/utils/Liste";
import OwnService from "service/OwnService";

export default class AchatInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/achat";
    this.urlUtils = "/achat/utils";
  }

  componentDidMount() {
    this.initUpdate();
  }

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Fiche de details</h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-warning btn-block"
                      onClick={() => {
                        window.location.replace(
                          "/achat/update/" + this.props.id
                        );
                      }}
                    >
                      Modifier
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/achat");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-success btn-block"
                      onClick={() => {
                        window.location.replace("/achat/form/");
                      }}
                    >
                      Ajouter
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={() => {
                        this.delete();
                      }}
                    >
                      Suprimer
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-info mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Date</h3>
                    <p className="text-dark">{this.state.oneValue.date}</p>
                    <h3 className="text-info">Prix</h3>
                    <p className="text-dark">{OwnService.format( this.state.oneValue.prix)}</p>
                    <Liste
                        head={
                            <tr>
                                <th>Laptop</th>
                                <th>P.U.</th>
                                <th>Quantite</th>
                            </tr>
                        }
                    >
                        {this.state.oneValue.details !== null ? 
                            this.state.oneValue.details.map(det => (
                                <tr>
                                    <td>{det.detailsmouvementid.laptopid.titre}</td>
                                    <td>{OwnService.format( det.detailsmouvementid.laptopid.prixachat)}</td>
                                    <td>{det.detailsmouvementid.quantite}</td>
                                </tr>
                            ))
                        : <></>}
                    </Liste>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
