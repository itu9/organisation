import { useParams } from "react-router-dom";
import AchatUpdate from "./AchatUpdate";

export default function AchatUpdateGetter() {
    const {id} = useParams();
    return (
        <AchatUpdate id={id}>
        </AchatUpdate>
    );
}