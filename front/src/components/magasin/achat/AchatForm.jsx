import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";
import AchatDetForm from "./AchatDetForm";

export default class AchatForm extends GeneralForm {
    constructor(params) {
        super(params);
        this.date = React.createRef(null);
        this.prix = React.createRef(null);
        this.urlSend = "/achat";
        this.urlUtils = "/achat/utils";
        this.afterValidation = "/achat";
    }
    getFormUrl = () => {
        this.checkConnexion();
        let auth = this.getSession(this.auth,null)
        return this.url+this.urlSend+"?idUser="+auth.login.id
    }
    componentDidMount() {
        this.init();
        this.setState({
        liste: [],
        });
    }

    validateData = () => {
        let data = {
            date: this.prepare(this.date),
            details : this.state.liste
        };
        this.validate(data);
    };

    change = (id, value) => {
        let data = this.state.liste.filter((li) => {
        // console.log(value,li.value);
        if (li.id === id) {
            li.laptopid = value.laptopid;
            li.nb = value.nb;
            li.prix = value.prix;
        }
        return li;
        });
        this.setState({
            liste: data,
        },
            () => {
                console.log(this.state.liste);
            }
        );
    };

    addValue = (data) => {
        this.setState({
        liste: [...this.state.liste, data],
        });
    };
    addDet = () => {
        let id = this.getNextId();
        let data = {
            id: id,
            laptopid: 0,
            nb: 0,
            prix: 0,
        };
        this.addValue(data);
        this.addTemplate(
        <AchatDetForm
            id={id}
            change={this.change}
            product={this.state.utils.laptops}
            remove={() => {
            this.removeTemplate(id, () => {
                let liste = this.state.liste.filter((l) => l.id !== id);
                this.setState({
                liste: liste,
                });
            });
            }}
        />,
        id
        );
    };
    render() {
        return (
        <HContainer>
            <h1 className="text-dark">Formulaire d'achat de laptop</h1>
            <div className="row mt-3">
            <div className="col-8">
                <Border>
                <h3 className="text-info mt-3">Date</h3>
                <input
                    type="date"
                    className="form-control"
                    ref={this.date}
                ></input>
                <div className="col-12 mt-3">
                    <div className="row mt-3 text-info">
                    <div className="col-5">
                        <strong>Laptop</strong>
                    </div>
                    <div className="col-4">
                        <strong>Prix</strong>
                    </div>
                    <div className="col-1">
                        <strong></strong>
                    </div>
                    </div>
                    {this.state.template.map((temp) => temp.data)}
                    <div className="row mt-3 text-info">
                    <div
                        className="col-12"
                        onClick={() => {
                        this.addDet();
                        }}
                    >
                        <h3 className="text-success">
                        <i className="fas fa-plus"></i>
                        </h3>
                    </div>
                    </div>
                </div>
                <button
                    onClick={this.validateData}
                    className="mt-3 btn btn-success btn-block"
                >
                    Valider
                </button>
                </Border>
            </div>
            </div>
        </HContainer>
        );
    }
}
