import { useParams } from "react-router-dom";
import RamInfo from "./RamInfo";

export default function RamFicheGetter() {
    const {id} = useParams();
    return (
        <RamInfo id={id}>
        </RamInfo>
    );
}