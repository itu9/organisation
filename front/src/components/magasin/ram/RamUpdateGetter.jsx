import { useParams } from "react-router-dom";
import RamUpdate from "./RamUpdate";

export default function RamUpdateGetter() {
    const {id} = useParams();
    return (
        <RamUpdate id={id}>
        </RamUpdate>
    );
}