import { useParams } from "react-router-dom";
import PlaceInfo from "./PlaceInfo";

export default function PlaceFicheGetter() {
    const {id} = useParams();
    return (
        <PlaceInfo id={id}>
        </PlaceInfo>
    );
}