import { useParams } from "react-router-dom";
import PlaceUpdate from "./PlaceUpdate";

export default function PlaceUpdateGetter() {
    const {id} = useParams();
    return (
        <PlaceUpdate id={id}>
        </PlaceUpdate>
    );
}