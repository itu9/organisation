import { useParams } from "react-router-dom";

export default function PlaceUpdateGetter() {
    const {id} = useParams();
    return (
        <PlaceUpdate id={id}>
        </PlaceUpdate>
    );
}