import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class ProcesseurForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.titre = React.createRef(null);
    this.frequence = React.createRef(null);
    this.urlSend = "/processeur";
    this.urlUtils = "/processeur/utils";
    this.afterValidation = "/processeur";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      titre: this.prepare(this.titre),
      frequence: this.prepare(this.frequence),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire d'ajout de processeur</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Titre</h3>
              <input
                type="text"
                className="form-control"
                ref={this.titre}
              ></input>
              <h3 className="text-info mt-3">Frequence</h3>
              <input
                type="text"
                className="form-control"
                ref={this.frequence}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
