import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class ProcesseurUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.titre = React.createRef(null);
    this.frequence = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/processeur";
    this.urlUtils = "/processeur/utils";
    this.afterValidation = "/processeur";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.titre.current.value = this.state.oneValue.titre;
    this.frequence.current.value = this.state.oneValue.frequence;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      titre: this.prepare(this.titre),
      frequence: this.prepare(this.frequence),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire de modificaton de processeur</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Titre</h3>
              <input
                type="text"
                className="form-control"
                ref={this.titre}
              ></input>
              <h3 className="text-info mt-3">Frequence en GHz</h3>
              <input
                type="text"
                className="form-control"
                ref={this.frequence}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
