import { useParams } from "react-router-dom";
import ProcesseurUpdate from "./ProcesseurUpdate";

export default function ProcesseurUpdateGetter() {
    const {id} = useParams();
    return (
        <ProcesseurUpdate id={id}>
        </ProcesseurUpdate>
    );
}