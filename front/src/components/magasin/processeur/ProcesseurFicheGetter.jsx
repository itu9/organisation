import { useParams } from "react-router-dom";
import ProcesseurInfo from "./ProcesseurInfo";

export default function ProcesseurFicheGetter() {
    const {id} = useParams();
    return (
        <ProcesseurInfo id={id}>
        </ProcesseurInfo>
    );
}