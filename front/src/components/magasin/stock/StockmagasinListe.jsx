import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";
import OwnService from "service/OwnService";

export default class StockmagasinListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.laptopid = React.createRef(null);
    this.quantitehmin = React.createRef(null);
    this.quantitehmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/stock/filter";
    this.urlUtils = "/stock/utils";
    this.baseUrl = "/stock/magasin";
  }
  componentDidMount() {
    this.init();
  }
  getData = () =>{
        
    let auths = this.getSession(this.auth,{
        login : {
            place : {
                id : -1
            }
        }
    });
    let dataSend = {
      placeidhmin : auths.login.place.id,
      placeidhmax : auths.login.place.id
    };
    this.search(dataSend);
}
  prepareDataListe = () => {
        
    let auths = this.getSession(this.auth,{
        login : {
            place : {
                id : -1
            }
        }
    });
    let data = {
      keys: this.prepare(this.keys),
      laptopid: this.checkRefNull(
        this.laptopid,
        { id: this.prepare(this.laptopid) },
        null
      ),
      quantitehmin: this.prepare(this.quantitehmin),
      quantitehmax: this.prepare(this.quantitehmax),
      placeidhmin : auths.login.place.id,
      placeidhmax : auths.login.place.id
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Stock magasin"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Laptop</h3>
                      <select className="form-control" ref={this.laptopid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.laptopid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Quantite</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.quantitehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.quantitehmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace(
                                    "/stock/magasin/form"
                                  );
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Laptop</td>
                    <td>Quantite</td>
                    <td>Valeur</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.laptopid.titre}</td>
                      <td>{data.quantite}</td>
                      <td>{OwnService.format( data.quantite*data.laptopid.prixachat)}</td>
                      <td>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            window.location.replace(
                              "/stock/magasin/" + data.id
                            );
                          }}
                        >
                          <i className="fas fa-plus text-info"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
