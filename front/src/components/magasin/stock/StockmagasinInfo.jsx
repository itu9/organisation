import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import OwnService from "service/OwnService";

export default class StockmagasinInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/stock";
    this.urlUtils = "/stock/utils";
  }

  componentDidMount() {
    this.initUpdate();
  }

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Details du stock</h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/stock/magasin");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-success btn-block"
                      onClick={() => {
                        window.location.replace("/achat");
                      }}
                    >
                      Ajouter
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-info mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Laptop</h3>
                    <p className="text-dark">
                      <a href={"/laptop/"+this.state.oneValue.laptopid.id}>{this.state.oneValue.laptopid.titre}</a>
                    </p>
                    <h3 className="text-info">Quantite</h3>
                    <p className="text-dark">{this.state.oneValue.quantite}</p>
                    <h3 className="text-info">Valeur</h3>
                    <p className="text-dark">{OwnService.format( this.state.oneValue.quantite*this.state.oneValue.laptopid.prixachat)}</p>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
