import { useParams } from "react-router-dom";
import StockmagasinInfo from "./StockmagasinInfo";

export default function StockmagasinFicheGetter() {
    const {id} = useParams();
    return (
        <StockmagasinInfo id={id}>
        </StockmagasinInfo>
    );
}