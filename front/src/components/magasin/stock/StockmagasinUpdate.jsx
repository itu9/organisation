import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class StockmagasinUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.laptopid = React.createRef(null);this.quantite = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/stock/magasin";
        this.urlUtils = "/stock/magasin/utils";
        this.afterValidation = "/stock/magasin";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.laptopid.current.value = this.state.oneValue.laptopid.id;this.quantite.current.value = this.state.oneValue.quantite;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            laptopid : this.checkRefNull(this.laptopid,{id:this.prepare(this.laptopid)},null),quantite : this.prepare(this.quantite),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Laptop</h3>
<select className="form-control" ref={this.laptopid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.laptopid.map((data,index) => (
            <option value={data.id} >{data.referenceid}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Quantite</h3>
<input type="text" className="form-control" ref={this.quantite} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}