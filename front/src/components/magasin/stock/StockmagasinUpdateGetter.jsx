import { useParams } from "react-router-dom";
import StockmagasinUpdate from "./StockmagasinUpdate";

export default function StockmagasinUpdateGetter() {
    const {id} = useParams();
    return (
        <StockmagasinUpdate id={id}>
        </StockmagasinUpdate>
    );
}