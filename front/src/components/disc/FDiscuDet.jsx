import { useParams } from "react-router-dom";
import DiscuDet from "./DiscuDet";

export default function FDiscuDet() {
    const {id} = useParams();
    return (
        <DiscuDet id={id}></DiscuDet>
    )
}