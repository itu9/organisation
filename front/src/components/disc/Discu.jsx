import Generation from "../utils/Generation";
import { Pagination } from "@mui/material";
import HContainer from "../client/nav/HContainer";
import Card from "../utils/Card";
export default class Discu extends Generation {
    render() {
        return (
             
            <>
            <HContainer>
                <h1 className="text-dark">Espace de discussion</h1>
                <div className="row">
                        <div className="col-3">
                            <input class="form-control custom-shadow custom-radius border-0 bg-white" type="search" placeholder="Search" aria-label="Search"/>
                        </div>
                        <div className="pt-1">

                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className=" feather feather-search form-control-icon"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                        </div>
                </div>
                <h2 className="mt-5 text-info"> Les discussions disponibles</h2> 
                <div className="row" >
                    <Card size="6" link="/assets/images/landingpage/db.png">
                        <h3 className="text-info">J'ai un bug sur l'implementation de l'algorithme hamiltonien</h3>
                        <p className="truncate">Une description du probleme pour la mamamam fsfsdkfj sdjfskj slkdjflsdj sdlkfjsdjf</p>
                        <p className="text-success">15 réponses</p>
                        <p className="text-orange">1 réponse de l'IA</p>
                        <a href={"/discu/1"}>
                            <button className="btn btn-block btn-info">Voir plus</button>
                        </a>
                    </Card>
                    <Card size="6" link="/assets/images/landingpage/db.png">
                        <h3 className="text-info">J'ai un bug sur l'implementation de l'algorithme hamiltonien</h3>
                        <p className="truncate">Une description du probleme pour la mamamam fsfsdkfj sdjfskj slkdjflsdj sdlkfjsdjf</p>
                        <p className="text-success">15 réponses</p>
                        <a href={"/discu/1"}>
                            <button className="btn btn-block btn-info">Voir plus</button>
                        </a>
                    </Card> 
                    <Pagination count={4} size="large" color="primary" onChange={() => {}} />
                </div>                               
            </HContainer>
        </>
        );
    }
}