import HContainer from "../client/nav/HContainer";
import Generation from "../utils/Generation";
import Border from '../utils/Border'
import { Avatar } from "@mui/material";
import { green } from "@mui/material/colors";

export default class DiscuDet extends Generation {
    render() {
        return (
            <HContainer>
                <h1 className="text-info">J'ai un bug sur l'implementation de l'algorithme hamiltonien</h1>
                <div className="mt-5">
                    <Border>
                        <div className="row">
                            <div className="col-6">
                                <img className="card-img-top img-fluid " src="/assets/images/big/algo.png" alt=""/>
                            </div>
                            <div className="col-6">
                                <h3 className="text-dark">Déscription du problème</h3>
                                <p>Ad leggings keytar, brunch id art party dolor labore. Pitchfork yr enim lo-fi before they sold out qui. Tumblr farm-to-table bicycle rights whatever. Anim keffiyeh carles cardigan. Velit seitan mcsweeney's photo booth 3 wolf moon irure. Cosby sweater lomo jean shorts, williamsburg hoodie minim qui you probably haven't heard of them et cardigan trust fund culpa biodiesel wes anderson aesthetic. Nihil tattooed accusamus, cred irony biodiesel keffiyeh artisan ullamco consequat.</p>
                                <h3 className="text-dark mt-3">Demander à l'IA</h3>
                                <div className="row">
                                    <div className="col-12">
                                        <textarea className="form-control"/>
                                    </div>
                                    <div className="col-12 mt-3">
                                        <button className="btn btn-info btn-block"> Valider</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Border>
                </div>
                <div>
                    <Border>
                        <div className="chat-img d-inline-block">
                            <Avatar sx={{ bgcolor: green[500]}} aria-label="recipe">
                                R
                            </Avatar>
                        </div>
                        <div className="chat-content d-inline-block pl-3">
                            <h6 className="font-weight-medium">
                                <strong>
                                Hasina Rivonandrasana
                                </strong>
                            </h6>
                        </div>
                        
                        <div className="msg p-2 d-inline-block mb-1 col-12">
                            Keytar twee blog, culpa messenger bag marfa whatever delectus food truck. Sapiente synth id assumenda. Locavore sed helvetica cliche irony, thundercats you probably haven't heard of them consequat hoodie gluten-free lo-fi fap aliquip.
                        </div>
                    </Border>
                </div>
                <div>
                    <Border>
                        <div className="chat-img d-inline-block">
                            <Avatar sx={{ bgcolor: green[500]}} aria-label="recipe">
                                N
                            </Avatar>
                        </div>
                        <div className="chat-content d-inline-block pl-3">
                            <h6 className="font-weight-medium">
                                <strong>
                                Hery Narindra
                                </strong>
                            </h6>
                        </div>
                        
                        <div className="msg p-2 d-inline-block mb-1 col-12">
                            Keytar twee blog, culpa messenger bag marfa whatever delectus food truck. Sapiente synth id assumenda. Locavore sed helvetica cliche irony, thundercats you probably haven't heard of them consequat hoodie gluten-free lo-fi fap aliquip.
                            <div className="col-6 mt-3">
                                <img className="card-img-top img-fluid " src="/assets/images/big/algo.png" alt=""/>
                            </div>
                        </div>
                    </Border>
                    <div>
                        <Border>
                            <h2 className="text-info">Déposez votre réponse</h2>
                            <div className="row">
                                    <div className="col-12">
                                        <h3 className="text-dark">Déscription</h3>
                                        <textarea className="form-control"/>
                                    </div>
                                    <div className="col-12 mt-3">
                                        <h3 className="text-dark">Image</h3>
                                        <input className="input-group-prepend form-control" type="file" onChange={this.changeImg}></input>

                                    </div>
                                    <div className="col-12 mt-3">
                                        <button className="btn btn-info btn-block"> Valider</button>
                                    </div>
                                </div>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}