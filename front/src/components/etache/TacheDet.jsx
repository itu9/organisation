import { useParams } from "react-router-dom";
import Tache from "./Tache";

export default function TacheDet () {
    const {id} = useParams();
    return (
        <>
            <Tache id={id}>

            </Tache>
        </>
    )
}