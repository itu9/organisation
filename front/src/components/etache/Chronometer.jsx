import Generation from "../utils/Generation";
import Ticket from "../utils/Ticket";

export default class Chronometer extends Generation {
    state = {
        h : 0,
        m : 0,
        s : 0
    }

    componentDidMount() {
        this.startTache();
    }

    startTache = () => {
        this.setH(this.props.tache.eval[0])
        this.setM(this.props.tache.eval[1])
        this.setS(this.props.tache.eval[2])
        if (this.props.tache.chrono.length === 0)
            return;
        let tache = this.props.tache.chrono[this.props.tache.chrono.length-1];
        if (tache.state !== 150 && tache.state !== 50)
            this.start();
    }

    start = () => {
        let date = this.props.tache.chrono.length > 0 ? this.props.tache.chrono[this.props.tache.chrono.length-1].date : new Date()
        this.startDate(new Date(date),this.props.tache.eval[0],this.props.tache.eval[1],this.props.tache.eval[2],
            this.setH,this.setM,this.setS, () => {}
        )
    }

    detState = () => {
        if (this.props.tache.chrono.length === 0)
            return -1;
        let tache = this.props.tache.chrono[this.props.tache.chrono.length-1];
        console.log(tache)
        return tache.state;
    }

    setH = (h) => {
        this.setState({
            h : h
        })
    }

    setM = (m) => {
        this.setState({
            m : m
        })
    }

    setS = (s) => {
        this.setState({
            s : s
        })
    }

    action = (state) => {
        let st = 0;
        if (state < 0  || state === 150) {
            st = 0;
        } else if (state === 200) {
            st = 150;
        } else if (state === 0 || state === 100) {
            st = 50;
        } else {
            st = 100
        }
        let data = {
            state : st,
            tache : {
                id : this.props.tache.id
            }
        }
        this.sendData(this.url+"/chrono",data,
            (data) => {
                window.location.reload();
            }
        )
    }

    render() {
        return (
             <>
                <Ticket>
                    <div className="row">
                        <div className="col-12">
                            <h2 className="text-orange">Chronomètre</h2>
                            <span>
                                {this.state.h }:
                                {this.state.m }:
                                {this.state.s }
                            </span>
                        </div>
                        <div className="col-12 mt-3">
                            <button className="btn btn-block btn-info w-100" onClick={() => {this.action(this.detState())}}><h3>{
                                (this.detState() < 0)? "Demarer" : 
                                (this.detState() === 0 || this.detState() === 100) ? "Pause" :
                                (this.detState() === 50) ? "Reprendre" :
                                (this.detState() === 150)? "Redemarer" : ""
                            }</h3></button>
                            {
                                ( 0 <= this.detState() && this.detState() <= 149) ? <button className="btn btn-block btn-info w-100" onClick={() =>{this.action(200)}}>Arreter</button> : <></>
                            }
                        </div>
                        <div className="col-12 mt-5">
                            <button className="btn btn-block btn-outline-info w-100"><h3>Ajouter au calendrier</h3></button>
                        </div>
                        <div className="col-12 mt-3">
                            <button className="btn btn-block btn-outline-success w-100"><h3>Publier</h3></button>
                        </div>
                        <div className="col-12 mt-3">
                            <button className="btn btn-block btn-outline-orange  w-100"><h3>Chercher sur le site</h3></button>
                        </div>
                    </div>
                </Ticket>
             </>
        );
    }
}