import { Component } from "react";
import Border from "../utils/Border";

export default class Chat extends Component {
    render() {
        return (
            <Border>
            <div className="col-12 mt-3">
                <h2 className="text-orange">Demander conseils à l'IA</h2>
                <h4>Votre assistant personel</h4>
                <div className="chat-box scrollable position-relative ps-container ps-theme-default"  data-ps-id="b86f390a-50cd-ea21-acf4-c3e0d1b8c282">
                        <ul className="chat-list list-style-none px-3 pt-3">
                            {
                                this.props.children
                            }
                        </ul>
                        <div className="ps-scrollbar-x-rail" ><div className="ps-scrollbar-x" tabIndex="0"></div></div>
                        <div className="ps-scrollbar-y-rail"><div className="ps-scrollbar-y" tabIndex="0" >
                            </div></div></div>
                        <div className="card-body border-top">
                            <div className="row">
                                <div className="col-9">
                                    <div className="input-field mt-0 mb-0">
                                        <input ref={this.props.chat} id="textarea1" placeholder="Type and enter" className="form-control border-0" type="text"/>
                                    </div>
                                </div>
                                <div className="col-3">
                                    <button className="btn-circle btn-lg btn-cyan float-right text-white" onClick={this.props.ask}> <i className="fas fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
            </Border>
        );
    }
}