import { Component } from "react";
import Ticket from "../utils/Ticket";

export default class Board extends Component {
    render() {
        return (
            <>
                <div className="row mt-5">
                    <div className="col-4">
                        <Ticket title="Vitesse normale">
                            <span className="text-info"> 1:15:00</span>
                        </Ticket>
                    </div>
                    <div className="col-4">
                        <Ticket title="Vitesse actuelle">
                            <span className="text-success"> 
                                {this.props.tache.eval[0]}:
                                {this.props.tache.eval[1]}:
                                {this.props.tache.eval[2]}
                            </span>
                        </Ticket>
                    </div>
                    <div className="col-4">
                        <Ticket title="Généralisation">
                            <span className="text-dark"> Conseillé</span>
                        </Ticket>
                    </div>
                </div>
            </>
        );
    }
}