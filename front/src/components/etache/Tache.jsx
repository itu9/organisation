import React from "react";
import QuestResponse from "../chrono/QuestReponse";
import HContainer from "../client/nav/HContainer";
import Generation from "../utils/Generation";
import Board from "./Board";
import Chat from "./Chat";
import Chronometer from "./Chronometer";

export default class Tache extends Generation {
    state = {
        chat : [],
        tache : null
    }

    chat = React.createRef();

    componentDidMount() {
        this.getTache();
    }

    getChat = () => {
        this.getListe(this.url+'/chat-tache/tache/'+this.props.id,
            (data) => {
                this.setState({
                    chat : data.data
                })
            },
            (header) => {}
        )
    }

    getTache = () => {
        this.getListe(this.url+"/etaches/"+this.props.id,
            (data) => {
                this.setState   ({
                    tache : data.data
                })
            },
            (header) => {

            }
        )
    }
    ask = async () => {
        let msg = this.chat.current.value;
        this.askChat(msg,(response) => {
            let data = {
                tacheid : this.props.id,
                chat : {
                    question : msg,
                    response : response,
                    etudiant : {
                        id : this.getStudent().id
                    }
                }
            }
            console.log(data);
            this.sendData(this.url+'/chat-tache',data,(res) => {
                this.getChat();
            })
        }, (err) => {})
    }
    render() {
        return (
             <HContainer>
                {
                    this.state.tache !== null ? 
                    <>
                    <h1 className="text-info">{this.state.tache.tache}</h1>
                        <h4>{this.state.tache.type.titre}</h4>
                        <Board tache={this.state.tache}></Board>
                        <div className="row">
                            <div className="col-4"></div>
                            <div className="col-4">
                                <Chronometer tache={this.state.tache} ></Chronometer>
                            </div>
                        </div>
                        <div className="mt-5">
                            <Chat
                                chat = {this.chat}
                                ask = {this.ask}
                            >
                            {   
                                this.state.chat.map(chat => (
                                    <QuestResponse key={chat.id} chat={chat.chat}></QuestResponse>
                                ))
                            }
                            </Chat>
                        </div>
                    </> : <></>
                }
                
             </HContainer>
        );
    }
}