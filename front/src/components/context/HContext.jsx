import React, { useState } from "react";

export const ContainerContext = React.createContext();

export default function HContext (props) {
    const [appear,setAppear] = useState(true);

    const setVisible = ()=> {
        setAppear(!appear)
    }
    return (
        <ContainerContext.Provider 
            value={{
                setVisible : setVisible,
                visible : appear
            }}
        >
            {this.props.children}
        </ContainerContext.Provider>
    )
}