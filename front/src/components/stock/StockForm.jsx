import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class StockForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.modeleid = React.createRef(null);
    this.date = React.createRef(null);
    this.nb = React.createRef(null);
    this.urlSend = "/stock";
    this.urlUtils = "/stock/utils";
    this.afterValidation = "/stock";
  }
  componentDidMount() {
    this.checkConnexion();
    this.init();
  }

  getFormUrl = () => {
    let auth = this.getSession(this.auth,{
      login : {
          id : -1
      }
    })
    return this.url+this.urlSend+"?idUser="+auth.login.id
  }

  validateData = () => {
    let data = {
      modeleid: this.checkRefNull(
        this.modeleid,
        { id: this.prepare(this.modeleid) },
        null
      ),
      date: this.prepare(this.date),
      nb: this.prepare(this.nb),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Produit</h3>
              <select className="form-control" ref={this.modeleid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.modeleid.map((data, index) => (
                    <option value={data.id}>{data.produitid.titre} {data.quantite}g</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Date</h3>
              <input
                type="date"
                className="form-control"
                ref={this.date}
              ></input>
              <h3 className="text-info mt-3">Nb</h3>
              <input type="text" className="form-control" ref={this.nb}></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
