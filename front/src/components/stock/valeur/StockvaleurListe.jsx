import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";
import OwnService from "service/OwnService";

export default class StockvaleurListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.modeleid = React.createRef(null);
    this.nbhmin = React.createRef(null);
    this.nbhmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/stockvaleur/filter";
    this.urlUtils = "/stockvaleur/utils";
    this.baseUrl = "/stockvaleur";
  }
  componentDidMount() {
    this.init();
  }
  findData = () => {
    let data = {
      keys: this.prepare(this.keys),
      modeleid: this.checkRefNull(
        this.modeleid,
        { id: this.prepare(this.modeleid) },
        null
      ),
      nbhmin: this.prepare(this.nbhmin),
      nbhmax: this.prepare(this.nbhmax),
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Produit</h3>
                      <select className="form-control" ref={this.modeleid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.modeleid.map((data, index) => (
                            <option value={data.id}>{data.produitid.titre} {data.quantite}g\{OwnService.format(data.prix)}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Nb</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.nbhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.nbhmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              {/* <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/stockvaleur/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Produit</td>
                    <td>Prix Unitaire</td>
                    <td>Quantite en stock</td>
                    <td>Total</td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.modeleid.produitid.titre} {data.modeleid.quantite}g</td>
                      <td>{OwnService.format( data.modeleid.prix)}</td>
                      <td>{data.nb}</td>
                      <td className="text-info">
                        {OwnService.format(data.nb*data.modeleid.prix)}
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
