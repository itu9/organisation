import { useParams } from "react-router-dom";
import StockvaleurInfo from "./StockvaleurInfo";

export default function StockvaleurFicheGetter() {
    const {id} = useParams();
    return (
        <StockvaleurInfo id={id}>
        </StockvaleurInfo>
    );
}