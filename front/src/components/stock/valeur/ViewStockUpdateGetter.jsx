import { useParams } from "react-router-dom";
import ViewStockUpdate from "./ViewStockUpdate";

export default function ViewStockUpdateGetter() {
    const {id} = useParams();
    return (
        <ViewStockUpdate id={id}>
        </ViewStockUpdate>
    );
}