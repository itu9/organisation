import { useParams } from "react-router-dom";
import StockvaleurUpdate from "./StockvaleurUpdate";

export default function StockvaleurUpdateGetter() {
    const {id} = useParams();
    return (
        <StockvaleurUpdate id={id}>
        </StockvaleurUpdate>
    );
}