import { useParams } from "react-router-dom";
import ViewStockInfo from "./ViewStockInfo";

export default function ViewStockFicheGetter() {
    const {id} = useParams();
    return (
        <ViewStockInfo id={id}>
        </ViewStockInfo>
    );
}