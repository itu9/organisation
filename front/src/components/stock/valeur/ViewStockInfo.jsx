import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";

export default class ViewStockInfo extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.urlSend = "/statistique/stock";
        this.urlUtils = "/statistique/stock/utils";
    }

    componentDidMount() {
        this.initUpdate();
    }

    render() {
        return (
             <HContainer>
             
                <h1 className="text-dark">Fiche de details</h1>
                <div className="mt-5">
                    <Border>
                    <div className="row">
                        <div className="col-3">
                            <h2 className="text-info">Alternatives</h2>
                            <div className="row mt-3 mb-3">
                                <div className="col-12 mb-3">
                                    <button className="btn btn-warning btn-block"
                                        onClick={
                                            () => {
                                                window.location.replace("/statistique/stock/update/"+this.props.id)
                                            }
                                        }
                                    >Modifier</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-info btn-block"
                                        onClick={
                                            () => {
                                                window.location.replace("/statistique/stock")
                                            }
                                        }
                                    >Liste</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-success btn-block" 
                                        onClick={
                                            () => {
                                                window.location.replace("/statistique/stock/form/")
                                            }
                                        }
                                    >Ajouter</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-danger btn-block" 
                                        onClick={
                                            () => {
                                                this.delete();
                                            }
                                        }
                                    >Suprimer</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-8">
                            <h2 className="text-info mb-3">Information</h2>
                            {
                                this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                <>
                                    <h3 className="text-info">Produitid</h3>
<p className="text-dark">{this.state.oneValue.produitid.hvalue}</p><h3 className="text-info">Quantite</h3>
<p className="text-dark">{this.state.oneValue.quantite}</p><h3 className="text-info">Nb</h3>
<p className="text-dark">{this.state.oneValue.nb}</p><h3 className="text-info">Id</h3>
<p className="text-dark">{this.state.oneValue.id}</p>
                                </>
                                :<></>
                            }
                        </div>
                    </div>
                    </Border>
                </div>
             </HContainer>
        );
    }
}
