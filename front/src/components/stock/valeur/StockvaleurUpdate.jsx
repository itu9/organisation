import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class StockvaleurUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.modeleid = React.createRef(null);this.nb = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/stockvaleur";
        this.urlUtils = "/stockvaleur/utils";
        this.afterValidation = "/stockvaleur";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.modeleid.current.value = this.state.oneValue.modeleid.id;this.nb.current.value = this.state.oneValue.nb;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            modeleid : this.checkRefNull(this.modeleid,{id:this.prepare(this.modeleid)},null),nb : this.prepare(this.nb),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Produit</h3>
<select className="form-control" ref={this.modeleid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.modeleid.map((data,index) => (
            <option value={data.id} >{data.quantite}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Nb</h3>
<input type="text" className="form-control" ref={this.nb} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}