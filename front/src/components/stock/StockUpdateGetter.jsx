import { useParams } from "react-router-dom";
import StockUpdate from "./StockUpdate";

export default function StockUpdateGetter() {
    const {id} = useParams();
    return (
        <StockUpdate id={id}>
        </StockUpdate>
    );
}