import { useParams } from "react-router-dom";
import StockInfo from "./StockInfo";

export default function StockInfoGetter() {
    const {id} = useParams();
    return (
        <StockInfo id={id}>
        </StockInfo>
    );
}