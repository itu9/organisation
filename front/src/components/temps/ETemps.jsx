
import FullCalendar from "@fullcalendar/react";
import HContainer from "../client/nav/HContainer";
import Generation from "../utils/Generation";
import './ETemps.css'
import timeGridPlugin from '@fullcalendar/timegrid';
import Liste from "../utils/Liste";

export default class ETemps extends Generation {
    render() {
        return (
            <>
                <HContainer>
                    <h1 className="text-dark">Emploie du temps </h1>
                    <div className="row">
                        <div className="col-4 mt-3">
                            <h2 className="text-info">Liste des tâches</h2>
                            <div className="mt-5">
                                <Liste
                                    title={
                                        <div className="col-12">
                                            <button className=" btn btn-success btn-block mr-5">Proposer</button>
                                        </div>
                                    }
                                    head={
                                        <tr>
                                            <th>Tâche</th>
                                            <th></th>
                                        </tr>
                                    }
                                >
                                    <tr>
                                        <td>Apprendre javascript</td>
                                        <td><button className="btn btn-outline-success">Assigner</button></td>
                                    </tr>
                                    <tr>
                                        <td>Perfectioner login</td>
                                        <td><button className="btn btn-outline-success">Assigner</button></td>
                                    </tr>
                                    <tr>
                                        <td>Creer un site avec AJAX</td>
                                        <td><button className="btn btn-outline-success">Assigner</button></td>
                                    </tr>
                                    <tr>
                                        <td>Gestion de panier</td>
                                        <td><button className="btn btn-outline-success">Assigner</button></td>
                                    </tr>
                                </Liste>
                            </div>
                        </div>
                        <div className="col-8">
                            <FullCalendar
                                plugins={[timeGridPlugin ]}
                                initialView="timeGridWeek"
                                weekends={true}
                                businessHours={
                                    [
                                        {
                                            startTime : '08:00',
                                            endTime : '17:00'
                                        }
                                    ]
                                }
                                contentHeight={500}
                                slotEventOverlap ={false}
                                events={[
                                    { title: 'Event 1', start: '2023-03-07T10:00:00', end: '2023-03-07T12:00:00' },
                                    { title: 'Event 2', start: '2023-03-08T14:00:00', end: '2023-03-08T16:00:00' },
                                    { title: 'Event 3', start: '2023-03-09T09:00:00', end: '2023-03-09T11:00:00' },
                                    { title: 'Event 4', start: '2023-03-10T15:00:00', end: '2023-03-10T17:00:00' },
                                    { title: 'Event 5', start: '2023-03-11T11:00:00', end: '2023-03-11T13:00:00',eventColor:'orange' }
                                ]}
                            ></FullCalendar>
                        </div>
                    </div>
                </HContainer>
            </>
        );
    }
}