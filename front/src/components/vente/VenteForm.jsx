import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class VenteForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.stockid = React.createRef(null);
    this.date = React.createRef(null);
    this.nb = React.createRef(null);
    this.urlSend = "/vente";
    this.urlUtils = "/vente/utils";
    this.afterValidation = "/vente";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      stockid: this.checkRefNull(
        this.stockid,
        { id: this.prepare(this.stockid) },
        null
      ),
      date: this.prepare(this.date),
      nb: this.prepare(this.nb),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Stock</h3>
              <select className="form-control" ref={this.stockid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.stockid.map((data, index) => (
                    <option value={data.id}>{data.produitid.titre} {data.quantite}g</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Date</h3>
              <input
                type="date"
                className="form-control"
                ref={this.date}
              ></input>
              <h3 className="text-info mt-3">Nb</h3>
              <input type="text" className="form-control" ref={this.nb}></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
