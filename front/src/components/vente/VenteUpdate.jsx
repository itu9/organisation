import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class VenteUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.stockid = React.createRef(null);this.date = React.createRef(null);this.nb = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/vente";
        this.urlUtils = "/vente/utils";
        this.afterValidation = "/vente";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.stockid.current.value = this.state.oneValue.stockid.id;this.date.current.value = this.state.oneValue.date;this.nb.current.value = this.state.oneValue.nb;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            stockid : this.checkRefNull(this.stockid,{id:this.prepare(this.stockid)},null),date : this.prepare(this.date),nb : this.prepare(this.nb),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Stock</h3>
<select className="form-control" ref={this.stockid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.stockid.map((data,index) => (
            <option value={data.id} >{data.produitid}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Nb</h3>
<input type="text" className="form-control" ref={this.nb} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}