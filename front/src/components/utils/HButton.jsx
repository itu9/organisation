import Swal from "sweetalert2";
import General from "./General";

export default class HButton extends General {
    getText = () => {
        return this.props.title === undefined ? "Valider" : this.props.title
    }

    onClick = () => {
        if (this.props.action === undefined) {
            Swal.fire({
                icon : 'error',
                title : 'Sorry',
                text : 'A problem was occured'
            })
        } else 
            this.props.action()
    }

    render() {
        return (
            <>
                <button className={"btn btn-"+this.getColor()+" btn-block w-100"} onClick={this.onClick}>
                    <h4 className={"pt-1 text-"+this.getTColor()}>
                        {this.getText()}
                    </h4>
                </button>
            </>
        );
    }
}