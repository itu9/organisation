import Generation from "./Generation";

export default class Liste extends Generation {

    render() {
        return (
             <>
                <div className="row">
                    <div className={"col-lg-"+this.getSize()}>
                            <h1 className="card-title">{this.props.title}</h1>
                            <h6 className="card-subtitle">{this.props.desc}</h6>
                            <div className="row">
                                {this.props.search}
                            </div>
                            <div className="row">
                                <div className={"table-responsive mt-5 col-"+this.getTableSize()}>
                                    <table className="table">
                                        <thead className={"bg-"+this.getColor()+" text-"+this.getTextColor()}>
                                            {
                                                this.props.head
                                            }
                                        </thead>
                                        <tbody>
                                            {
                                                this.props.children
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        {this.props.plus}
                    </div>
                </div>
             </>
        );
    }
}