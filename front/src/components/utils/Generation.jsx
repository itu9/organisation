/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
    import { Configuration, OpenAIApi } from "openai";
    import { Component } from "react";
    import Swal from "sweetalert2";
    import OwnService from "../../service/OwnService";

    export default class Generation extends Component {
    panierKey = "produits";
    url = "http://localhost:8080";
    temp = "http://localhost:3001";
    json_server = "http://localhost:3000";
    apiKey = "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";
    auth = "token";
    afterAuth = "/admin";
    allCode = -10;
    clientCode = 50;
    adminCode = 150;
    assistant = "Histant";

    /**
     * Capitalise la premiere lettre
     */
    firstTop = (text) => {
        return text.replace(text.charAt(0), text.charAt(0).toUpperCase());
    };
    /**
     *  -inf <-> 0 tous le monde
     *  1 <-> 100 client
     *  101 <-> +inf admin
     *  niveau : accreditation requis pour le site
     */
    accreditation = (niveau) => {
        if (niveau <= 0) return true;
        // let user = this.
    };

    addAuth = (data) => {
        this.addSession(this.auth, data);
    };

    getSession = (key, defaultValue) => {
        let data = sessionStorage.getItem(key);
        return data === null || data === undefined
        ? defaultValue
        : JSON.parse(data);
    };

    addSession = (key, value) => {
        sessionStorage.setItem(key, JSON.stringify(value));
    };

    removeSession = (key, id) => {
        let data = this.getSession(key, []);
        data = data.filter((d) => d.id !== id);
        this.addSession(key, data);
    };

    oneStorage = (key,id) => {
        let data = this.getStorage(key, []);
        data = data.filter((d) => d.id === id);
        return (data.length === 0)? null : data[0];
    }

    removeStorage = (key, id) => {
        let data = this.getStorage(key, []);
        data = data.filter((d) => d.id !== id);
        this.addStorage(key, data);
    };

    getStorage = (key, defaultValue) => {
        let data = localStorage.getItem(key);
        return data === null || data === undefined
        ? defaultValue
        : JSON.parse(data);
    };

    addStorage = (key, value) => {
        localStorage.setItem(key, JSON.stringify(value));
    };

    replace = (text, spec, repl) => {
        for (let i = 0; i < spec.length; i++) {
        text = text.replaceAll(spec[i], repl);
        }
        return text;
    };

    toRef = (title) => {
        title = title.toLowerCase();
        return this.replace(title, [" ", "_", `'`, "/", "."], "-");
    };

    startDate = (start, h, m, s, setH, setM, setS, action) => {
        OwnService.startTime(start, setH, setM, setS, action, h, m, s);
    };

    askChat = async (msg, action, onError = (err) => {}) => {
        const config = new Configuration({
        apiKey: this.apiKey,
        });
        const openai = new OpenAIApi(config);
        try {
        const completion = await openai.createCompletion({
            model: "text-davinci-003",
            prompt: msg,
            temperature: 0.6,
            presence_penalty: 0,
            stop: ["asdfgh"],
            max_tokens: 1024,
        });
        action(completion.data.choices[0].text);
        } catch (error) {
        Swal.fire({
            title: "Oups...",
            text: "Une erreur inatendue",
            icon: "error",
        });
        onError(error);
        }
    };

    getConnected = () => {
        return {
        id: 1,
        login: {
            id: 1,
            nom: "Rivonandrasana",
            prenom: "Hasina Juniolah",
            matriculation: "cli_001689",
        },
        };
    };

    getStudent = () => {
        return {
        id: 1,
        };
    };

    update = (url, data, action) => {
        OwnService.update(url,data,action)
    };

    sendData = (url, data, action,header=()=>{}) => {
        OwnService.sendData(url, data, action,header);
    };

    deleteData = (url, data, action) => {
        OwnService.delete(url, data, action);
    };

    getPdfs = (url,data,action) => {
        OwnService.getPdf(url,data,action);
    }

    getListe = (url, action, readHeader) => {
        OwnService.getData(
        url,
        (data) => {
            action(data);
        },
        (header) => {
            readHeader(header);
        }
        );
    };

    upload = (event, change) => {
        let reader = new FileReader();
        reader.onloadend = () => {
        const base64String = reader.result
            .replace("data:", "")
            .replace(/^.+,/, "");
        change(base64String);
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    /**
     * Front
     */

    getSize = () => {
        let size = "12";
        if (this.props.size !== undefined && this.props.size !== null) {
        return this.props.size;
        }
        return size;
    };

    getTableSize = () => {
        let size = "12";
        if (this.props.tablesize !== undefined && this.props.tablesize !== null) {
        return this.props.tablesize;
        }
        return size;
    };

    getColor = () => {
        let color = "primary";
        if (this.props.color !== undefined) {
        return this.props.color;
        }
        return color;
    };

    getTextColor() {
        let color = "white";
        if (this.props.tcolor !== undefined) {
        return this.props.tcolor;
        }
        return color;
    }

    getBtnSize = () => {
        let size = "12";
        if (this.props.btnSize !== undefined) {
        return this.props.btnSize;
        }
        return size;
    };

    getDecale = () => {
        let dec = "20";
        if (this.props.decalage !== undefined) {
        return this.props.decalage;
        }
        return dec;
    };
}
