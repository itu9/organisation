import Border from "./Border";
import Generation from "./Generation";
import './Card.css'
///assets/images/big/img1.jpg
//'data:image/png;base64,'

export default class Card extends Generation {
    render() {
        return (
            
            <div className={"col-"+this.getSize()}>
                <Border>
                    <div className="row ">
                        <div className="himg">
                            {
                                (this.props.link !== undefined) ? <img className="card-img-top img-fluid " src={this.props.link}
                                alt=""/> : <></>
                            }
                        </div>
                        <div className="card-body">
                            {this.props.children}
                        </div>
                    </div>
                </Border>
            </div>
        );
    }
}