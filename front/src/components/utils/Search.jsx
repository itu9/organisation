import Generation from "./Generation";

export default class HSearch extends Generation {

    search = () => {
        this.props.onClick();
    }

    render() {
        return (
             <div className={"row"}>
                    <div className={"ml-3 mt-3 col-"+this.getSize()}>
                    {
                        this.props.children
                    }
                    </div>
                    <div className="col-12"></div>
                    <div className={"mt-4 col-"+this.getSize()}>
                        <button className="ml-3 mr-3 btn btn-info btn-block w-100" onClick={this.search}>
                            <h4 className="pt-1">
                            <i className="form-control-icon" data-feather="search"></i>&nbsp;
                            Rechercher 
                            </h4>
                        </button>
                    </div>
             </div>
        );
    }
}