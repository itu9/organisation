import Modal from "react-modal";
import Generation from "./Generation";

export default class HModal extends Generation {
    render() {
        return (
             <Modal isOpen={this.props.open}
             onRequestClose={this.props.onClose}
             style={
                {
                    overlay : {
                        zIndex : 600,
                        backgroundColor: 'rgba(0, 0, 0, 0.5)'
                    },
                    content : {
                        width:this.getSize()+'%',
                        transform : 'translate('+this.getDecale()+'%,0)'
                    }
                }
            }
             >
                {this.props.children}
             </Modal>
        );
    }
}