import General from "./General";

export default class FrontGenerator extends General {
    initialize() {
        this.setState({
            utils : null
        },this.getUtils)
    }

    getUtils = () =>{
        this.sendData(this.url+this.urlUtils,{},
            data => {
                this.verifData(data,(response)=>{
                    console.log("utils",response.data);
                    this.setState({
                        utils : response.data
                    })
                })
            }
        );
    }
}