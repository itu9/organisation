import { Pagination } from "@mui/material";
import Liste from "./Liste";
import Generation from "./Generation";

export default class ListePage extends Generation {
    onChange = (event,value) => {
        this.props.onChange(event,value);
    }

    render() {
        return (
             <>
                <Liste title={this.props.title} color={this.props.color} tcolor={this.props.tcolor} 
                    head={this.props.head}
                    size = {this.getSize()}
                    search={this.props.search}
                    plus={
                        <>
                        <Pagination count={this.props.count} size="large" color="primary" onChange={this.onChange} />
                        </>
                    }
                > 
                    {
                        this.props.children
                    }
                </Liste>
             </>
        );
    }
}