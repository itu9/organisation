import { Component } from "react";
import '../client/nav/Footer.css'

export default class Border extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-body">
                    {this.props.children}
                </div>
            </div>
        );
    }
}