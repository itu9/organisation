import Swal from "sweetalert2";
import FrontGenerator from "./FrontGenerator";

export default class GeneralForm extends FrontGenerator {
    init () {
        this.initialize()
    }

    getFormUrl = () => {
        let auth = this.getSession(this.auth,{
            login : {
                id : -100
            }
        })
        return this.url+this.urlSend+"?idUser="+auth.login.id;
    }

    validate = (data) => {
        this.sendData(this.getFormUrl(),data,
            response => {
                this.verifData(response,
                    // eslint-disable-next-line no-unused-vars
                    result => {
                        Swal.fire({
                            icon : 'success',
                            title : 'Bravo',
                            text : 'Votre demande est prise en charge',
                            timer : 1500
                        }).then(() => {
                            window.location.replace(this.afterValidation)
                        })
                    }
                )
            } 
        )
    }
}