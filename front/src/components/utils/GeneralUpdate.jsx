import Swal from "sweetalert2";
import GeneralForm from "./GeneralForm";

export default class GeneralUpdate extends GeneralForm {
    initUpdate = () => {
        this.init();
        this.setState({
            oneValue : null
        },this.getOneUpdate)
    };

    actionUpdate = ()=> {

    }

    delete = () => {
        this.deleteData(this.getOneUrl(),{},
            (response) => {
                this.verifResponse(response,
                    // eslint-disable-next-line no-unused-vars
                    (data) => {
                        Swal.fire({
                            icon : 'success',
                            title : 'Bravo',
                            text : `L'objet a été suprimé`,
                            timer : 1000
                        }).then(()=> {
                            window.location.replace(this.urlSend);
                        })
                    }
                )
            }
        )
    }

    getUpdateUrl = () => {
        let auth = this.getSession(this.auth,{
            login : {
                id : -100
            }
        })
        return this.url+this.urlSend+"?idUser="+auth.login.id;
    }

    validate = (data) => {
        this.update(this.getUpdateUrl(),data,
            response => {
                this.verifData(response,
                    // eslint-disable-next-line no-unused-vars
                    result => {
                        window.location.replace(this.afterValidation)
                    }
                )
            } 
        )
    }

    getOneUrl = () => {
        let auth = this.getSession(this.auth,{
            login : {
                id : -100
            }
        })
        return this.url+this.urlSend+"/"+this.props.id+"?idUser="+auth.login.id;
    }

    getOneUpdate = () => {
        console.log("update");
        this.getListe(this.getOneUrl(),
            data => {
                this.verifData(data,
                    response => {
                        console.log(response);
                        this.setState({
                            oneValue : response.data
                        }, this.actionUpdate
                        )
                    }
                )
            },
            // eslint-disable-next-line no-unused-vars
            header => {

            }
        )
    }
}
