import { Component } from "react";
import Border from "./Border";

export default class Ticket extends Component {
    render() {
        return (
             <Border>
                <div className="d-inline-flex align-items-center">
                    <h2 className="text-dark mb-1 font-weight-medium">
                        {this.props.children}
                    </h2>
                </div>
                <h4 className=" font-weight-normal mb-0 w-100 text-dark">{this.props.title}</h4>
             </Border>
        );
    }
}