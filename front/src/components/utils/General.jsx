/* eslint-disable no-unused-vars */
import Swal from 'sweetalert2';
import Generation from './Generation';

class Template {
    constructor(data, id) {
        this.data = data
        this.id = id
    }
}

export default class General extends Generation {
    constructor(params) {
        super(params);
        this.state = {
            validation : [],
            template : [],
            lastId : 1
        }
    }

    getPdfUrl = () => {
        return this.url
    }
    pdf = (dataSend) => {
        this.getPdfs(this.getPdfUrl(),dataSend,
            () => {
                Swal.fire({
                    icon : 'success',
                    title : 'Bravo',
                    text : 'Votre pdf a été téléchargé'
                })
            }
        )
    }

    checkConnexion = () => {
        let auth = this.getSession(this.auth,null)
        if (auth === null) {
            Swal.fire({
                icon : 'error',
                title : 'Authorisation',
                text : `Vous n'avez pas acces à cette page`,
                timer : 3000
            }).then(() => {
                window.location.replace("/");
            });
        }
    }
    

    checkRefNull = (ref,onVoid,doElse) => {
        if (ref.current.value !== "") {
            return onVoid;
        } else {
            return doElse;
        }
    }

    prepare = (key) => {
        return (key.current.value === "")? null : key.current.value
    }

    verifResponse = (data,action= (data)=> {},message = 'Le systeme refuse de faire cette action') => {
        if (!(200 <= data.status && data.status < 300)) {
            Swal.fire({
                icon : 'error',
                title : 'Oupss',
                text : message,
                timer : 3000
            })
        } else {
            action(data);
        }
    }

    verifData = (data,action = (data)=> {},message = 'Le systeme refuse de faire cette action') => {
        if (data.code !== undefined && data.code !== null) {
            Swal.fire({
                icon : 'error',
                title : 'Oupss',
                text : message,
                timer : 3000
            })
        } else {
            action(data)
        }
    }

    addValidation = (prod,action = ()=>{}) => {
        let data = this.state.validation.filter(val => val.id !== prod.id);
        if (data.length === this.state.validation.length) {
            data.push(prod);
        }
        this.setState({
            validation : data
        },
            action
        )
    }

    getNextId = () => {
        let id = this.state.lastId +1;
        this.setState({
            lastId : id
        })
        return id;
    }

    removeTemplate = (id,action) => {
        let data = this.state.template.filter(d => d.id !== id)
        this.setState({
            template : data
        })
        action();
    }

    addTemplate = (data,id) => {
        let d = new Template(data,id)
        this.setState({
            template : [
                ...this.state.template,
                d
            ]
        })
    }

    getColor = () => this.props.color === undefined ? "info" : this.props.color;

    getTColor = () => this.props.tColor === undefined ? "dark" : this.props.tColor;
}