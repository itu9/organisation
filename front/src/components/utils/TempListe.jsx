import HContainer from "../client/nav/HContainer";
import Border from "./Border";
import GeneralListe from "./GeneralListe";
import ListePage from "./ListePage";

export default class TempListe extends GeneralListe {
    constructor(params) {
        super(params);
        this.urlData = "/produit/search";
        this.urlUtils = "/produit/utils";
    }
    componentDidMount() {
        this.init();
    }

    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                    <div className="col-12">
                        <Border>
                            <ListePage
                                title="Titre de la liste"
                                search = {
                                    <>
                                    </>
                                }
                                head = {
                                    <tr>
                                        <th>Colonne 1</th>
                                        <th>Colonne 2</th>
                                        <th>Colonne 3</th>
                                    </tr>
                                }
                            >
                            <tr>
                                <td>Valeur 1</td>
                                <td>Valeur 2</td>
                                <td>Valeur 3</td>
                            </tr>
                            </ListePage>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}