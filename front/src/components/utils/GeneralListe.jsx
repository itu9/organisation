import Swal from "sweetalert2";
import FrontGenerator from "./FrontGenerator";

export default class GeneralListe extends FrontGenerator {
    init() {
        this.initialize();
        this.setState({
            data : [],
            count : 0
        }, this.getData)
    }
    getData = () =>{
        let dataSend = {};
        this.search(dataSend);
    }

    getDeleteUrl = (data) => {
        this.checkConnexion();
        let auth = this.getSession(this.auth,{
            login : {
                id : -100
            }
        })
        return this.url+this.baseUrl+"/"+data.id+"?idUser="+auth.login.id;
    }

    delete = (data) => {
        this.deleteData(this.getDeleteUrl(data),{},
            (response) => {
                this.verifResponse(response,
                    // eslint-disable-next-line no-unused-vars
                    (data) => {
                        Swal.fire({
                            icon : 'success',
                            title : 'Bravo',
                            text : `L'objet a été suprimé`,
                            timer : 1500
                        }).then(()=> {
                            window.location.reload();
                        })
                    }
                )
            }
        )
    }

    getSearchUrl = () => {
        let auth = this.getSession(this.auth,{
            login : {
                id : -100
            }
        })
        return this.url+this.urlData+"?idUser="+auth.login.id;
    }

    search = (dataSend) => {
        this.sendData(this.getSearchUrl(),dataSend,
            data => {
                this.verifData(data,
                    response => {
                        console.log('data',data);
                        this.setState({
                            data : response.data,
                            count : response.nbPage
                        },() => {
                            console.log(this.state.data);
                        })
                    }
                )
            // eslint-disable-next-line no-unused-vars
            },header => {
            }
        );
    }

    getPdfUrl = () => {
        return this.url+this.urlData
    }

    
}