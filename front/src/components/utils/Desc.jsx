import General from "./General";

export default class Desc extends General {
    render() {
        return (
            <>
                <h4 className="card-title">{this.props.title}</h4>
                <p className="card-text">{this.props.children}</p>
            </>
        );
    }
}