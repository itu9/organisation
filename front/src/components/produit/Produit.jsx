import HContainer from "../client/nav/HContainer";
import Border from "../utils/Border";
import General from "../utils/General";
import CProduit from "./CProduit";

export default class Produit extends General {
    componentDidMount() {
        this.getListe(this.json_server+"/produits", (data) => {
            this.setState({
                produits : data.data
            })
        },
        data => {})
    }
    render() {
        return (
            <HContainer>
                <div className="col-12">
                    <h1 className="text-dark mb-5">Nos produits alimentaires de qualité</h1>
                    <div className="row">
                        <div className="col-6">
                        <Border>
                            <h4 className="card-title">Recherche</h4>
                            <input type="text" className="form-control" placeholder="Par mot clé"/>  
                            
                            <h4 className="card-title mt-3">Catégorie</h4>
                            <select className="form-control">
                                <option></option>
                                <option>Fruit</option>
                                <option>Legume</option>
                            </select>

                            {/* Fourchette */}
                            <h4 className="card-title mt-3">Prix</h4>
                            <div className="row">
                                <div className="col-6">
                                    <input type="text" className="form-control" placeholder="Prix min"/>  
                                </div>
                                <div className="col-6">
                                    <input type="text" className="form-control" placeholder="Prix max"/>  
                                </div>
                            </div>

                            
                            <h4 className="card-title mt-3">Quantité</h4>
                            <div className="row">
                                <div className="col-6">
                                    <input type="text" className="form-control" placeholder="Quantité min"/>  
                                </div>
                                <div className="col-6">
                                    <input type="text" className="form-control" placeholder="Quantité max"/>  
                                </div>
                            </div>
                            
                        </Border>
                        </div>
                    </div>
                    <div className="row">
                        {
                            this.state.produits !== undefined ? 
                            this.state.produits.map(produit => (
                                <CProduit key={produit.id} produit={produit}></CProduit>
                            )) : <></>
                        }
                    </div>
                </div>
            </HContainer>
        );
    }
}