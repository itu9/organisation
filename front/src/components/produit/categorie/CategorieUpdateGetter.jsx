import { useParams } from "react-router-dom";
import CategorieUpdate from "./CategorieUpdate";

export default function CategorieUpdateGetter() {
    const {id} = useParams();
    return (
        <CategorieUpdate id={id}>
        </CategorieUpdate>
    );
}