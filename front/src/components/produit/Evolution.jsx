import { Line } from "react-chartjs-2";
import HContainer from "../client/nav/HContainer";
import Border from "../utils/Border";
import General from "../utils/General";
import {  Chart, registerables  } from "chart.js";
import React from "react";
Chart.register(...registerables)

export default class Evolution extends General {
    constructor(params) {
        super(params);
        this.data = React.createRef(null);
        this.date = React.createRef(null);
        this.data.current = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
            {
                label: 'Pomme de terre',
                data: [65, 59, 80, 81, 56, 55, 40],
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgba(255, 99, 132, 0.2)',
            }
            ],
          }
    }
    componentDidMount() {
        this.setState({
            data : []
        },this.getData);
    }

    getData = () => {
        this.getListe(this.url+"/produit",
            data => {
                this.verifData(data,
                    response => {
                        this.setState({
                            produit : response.data
                        },
                            ()=> {
                                let data = this.state.produit[0]
                                this.search(data);
                            }
                        )
                    }
                )
            },
            header => {

            }
        )
    }

    search = (dataSend) => {
        this.getListe(this.url+'/variation/'+dataSend.id,
            (data) => {
                this.verifData(data,
                    response => {
                        this.toStat(response.data,dataSend)
                    }
                )
            },
            header => {

            }
        )
    }

    toStat = (response,dataSend) => {
        let data = response.prevision.map(res => res.total);
        let dataField = {
            labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet','Aout','Septembre','Octobre','Novembre','Décembre'],
            datasets: [
            {
                label: `Chiffre d'affaire `+dataSend.titre,
                data: data,
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgba(255, 99, 132, 0.2)',
            }
            ],
          };
        this.data.current = dataField;
        this.setState({
            data : dataField
        })
    }

    load = () => {
        let id = parseInt( this.date.current.value,10);
        console.log(this.state.produit);
        console.log(id);
        let data = this.state.produit.filter(pr => pr.id === id)
        console.log(data);
        this.search(data[0]);
    }
        
    render() {
        return (
             <HContainer>
                <Border>    
                    <h2>Prévision</h2>
                    <div className="row">
                        <div className="col-4">
                            <h3>Annee</h3>
                            <select className="form-control" ref={this.date} onChange={this.load}>
                                {
                                    this.state.produit !== undefined && this.state.produit !== null ?
                                    this.state.produit.map(data => (
                                        <option value={data.id} >{data.titre}</option>
                                    ))
                                    :<></>
                                }
                            </select>
                        </div>
                    </div>
                    {
                        this.data !== undefined && this.data !== null ?
                        <Line data={this.data.current} /> : <></>
                    }
                    
                </Border>
             </HContainer>
        );
    }
}