import Swal from "sweetalert2";
import General from "../utils/General";
import React  from "react";

export default class BPanier extends General {
    constructor(params) {
        super(params);
        this.nb = React.createRef(null)
    }

    componentDidMount = () => {
        this.nb.current.value = 1;
    }

    addToList = (data,newItem) => {
        for (let i = 0; i < data.length; i++) {
            if (data[i].id === newItem.id) {
                data[i].nb += newItem.nb;
                return data;
            }
        }
        data = [
            ...data,
            newItem
        ]
        return data;
    }

    getMessage = (prod) => {
        let verbe = prod.nb <=1 ? "sera" : "seront";
        let product = prod.info.titre.toLowerCase();
        return prod.nb+' '+product+' '+verbe+' mis dans votre panier. Vous confirmez?';
    }

    addPanier = (prod) => {
        Swal.fire({
            title : 'Question',
            text : this.getMessage(prod),
            icon : 'question'
        }).then(
            resp => {
                if (resp.isConfirmed) {
                    let key = this.panierKey
                    let data = this.getStorage(key,[]);
                    data = this.addToList(data,prod);
                    this.addStorage(key,data);
                }
            }
        )
    }

    add = () => {
        let data = {
            id : this.props.id,
            info : this.props.info,
            nb : parseInt( this.nb.current.value)
        }
        this.addPanier(data);
    }

    render() {
        return (
            <>
                <input type="text" className="form-control" placeholder="Nb" ref={this.nb} />
                <button className="mt-3 btn-success btn-block" onClick={this.add}>
                    <h4 className="pt-1">Panier</h4>
                </button>
            </>
        );
    }
}