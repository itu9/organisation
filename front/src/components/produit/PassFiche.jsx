import { useParams } from "react-router-dom"
import FicheProduit from "./FicheProduit";

export default function PassFiche () {
    const {id} = useParams();
    return (
        <>
            <FicheProduit id={id}></FicheProduit>
        </>
    )
}