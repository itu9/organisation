import Swal from "sweetalert2";
import General from "../utils/General";
import './PanierDet.css'
import PLink from "./PLink";
import OwnService from "../../service/OwnService";
import React from "react";

export default class PanierDet extends General {
    componentDidMount = () => {
        this.checked = React.createRef(null)
    }
    removeIt = () => {
        this.removeStorage(this.panierKey,this.props.panier.id)
        window.location.reload()
    }

    remove = () => {
        let product = this.props.panier.info.titre.toLowerCase();
        let nb = this.props.panier.nb
        Swal.fire({
            title : 'Vous confirmer?',
            html: '<p style="color:#ff4f70 ;">Cette action va enlever '+nb+' '+product+' dans votre panier.</p>',
            icon : 'question'
        }).then((data) => {
            if (data.isConfirmed) {
                this.removeIt();
            }
        })
    }
    render() {
        return (
            <tr>
                <td className="text-info hvRemove">
                    <input type="checkbox" ref={this.checked} onClick={() => {
                        this.props.add(this.props.panier)
                    }} />
                </td>
                <td>
                    <PLink produit={this.props.panier.info}> {this.props.panier.info.titre}</PLink>
                </td>
                <td>{this.props.panier.info.categorie.titre}</td>
                <td>{this.props.panier.nb}</td>
                <td>{OwnService.format( this.props.panier.info.prix)}</td>
                <td className="text-info">{OwnService.format( this.props.panier.info.prix*this.props.panier.nb)}</td>
                <td>
                    <h3 className="text-danger hvRemove" onClick={
                        this.remove
                    }>
                        <i className="fas fa-times"></i>
                    </h3>
                </td>
            </tr>
        );
    }
}