import General from "../utils/General";
import HContainer from "../client/nav/HContainer";
import Border from "../utils/Border";
import PanierDet from "./PanierDet";
import OwnService from "../../service/OwnService";
import React from 'react';
import './Panier.css'
import Charger from "../client/recharge/Charger";


export default class Panier extends General {
    componentDidMount = () => {
        let data = this.getStorage(this.panierKey,[])
        this.setState({
            panier : data,
            totSel : 0.,
            addMoney : false
        })
        let refs = []
        for (let i = 0; i < data.length; i++) {
            refs.push(React.createRef(null))
        }
        this.references = refs;
    }

    closeForm = () => {
        this.setState({
            addMoney : false
        })
    }

    openForm = () => {
        this.setState({
            addMoney : true
        })
    }

    valider = () => {
        console.log(this.references);
    }

    getTotal = () => {
        return this.somme(this.state.panier)
    }

    value = (prod) => prod.info.prix*prod.nb

    somme = (panier) => {
        // console.log(panier);
        let res = 0.
        for (let i = 0; i < panier.length; i++) {
            res += this.value(panier[i]);
        }
        return res;
    }

    confirmer = (prod) => {
        this.addValidation(prod,
            () => {
                let montantSel = this.somme(this.state.validation)
                this.setState({
                    totSel : montantSel
                })
            }
        );
    }

    render() {
        return (
            <>
            <HContainer>
                <h1 className="text-dark mb-5">Panier</h1>
                <div className="row">
                    <div className="col-10">
                    {
                                        this.state.panier !== null && this.state.panier !== undefined? <>
                    <div className="card-group">
                        <div class="card border-right">
                            <div class="card-body">
                                <div class="d-flex d-lg-flex d-md-block align-items-center">
                                    <div>
                                        <div class="d-inline-flex align-items-center">
                                            <h2 class="text-dark mb-1 font-weight-medium">{OwnService.format( this.getTotal())}</h2>
                                        </div>
                                        <h6 class="  font-weight-normal mb-0 w-100 text-info">Valeur du panier</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card border-right ml-3">
                            <div class="card-body">
                                <div class="d-flex d-lg-flex d-md-block align-items-center">
                                    <div>
                                        <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">80000</h2>
                                        <h6 class="text-success font-weight-normal mb-0 w-100 text-truncate">Etat d'argent
                                        </h6>
                                    </div>
                                    <div class="ml-auto mt-md-3 mt-lg-0 hvRemove">
                                        <span class="">
                                            <i className="fas fa-credit-card hcredit" data-toggle="tooltip" title="Ajouter de l'argent" onClick={this.openForm}></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> </> : <></> }
                
                        <Border>
                            <h2 className="text-info">Les produits dans le panier</h2>
                            <table className="table"> 
                                <thead className="bg-info text-white">
                                    <tr>
                                        <th><input type="checkbox"/></th>
                                        <th>Produit</th>
                                        <th>Categorie</th>
                                        <th>Nombre</th>
                                        <th>Sous total</th>
                                        <th>Prix</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.panier !== null && this.state.panier !== undefined? this.state.panier.map((pan,index) => (
                                            <PanierDet add={this.confirmer} key={pan.id} panier={pan} ref={this.references[index]}></PanierDet>
                                        )) : <></>
                                    }
                                </tbody>
                            </table>
                                <div className="row mt-5">{
                                        this.state.panier !== null && this.state.panier !== undefined? 
                                        <>
                                            <div className="col-4">
                                                <p className="text-info"><span className="text-danger">Sélectionnés :</span> {OwnService.format( this.state.totSel)}</p>
                                            </div>
                                            <div className="col-4">
                                            </div> 
                                        </>: <></>
                                    }
                                    <div className="col-4">
                                        <button className="btn btn-success btn-block" onClick={this.valider}>Payer</button>
                                    </div>
                                </div>
                        </Border>
                    </div>
                </div>
                <Charger open={this.state.addMoney} onClose={this.closeForm} afterValid={this.closeForm}></Charger>
            </HContainer>
            </>
        );
    }
}