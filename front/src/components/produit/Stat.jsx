import { Line } from "react-chartjs-2";
import General from "../utils/General";
import {  Chart, registerables  } from "chart.js";
import HContainer from "../client/nav/HContainer";
import Border from "../utils/Border";
Chart.register(...registerables)

export default class Stat extends General {
    constructor(params) {
        super(params);
        this.data = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
            {
                label: 'Pomme de terre',
                data: [65, 59, 80, 81, 56, 55, 40],
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgba(255, 99, 132, 0.2)',
            }
            ],
          };
    }
    render() {
        return (
             <HContainer>
                <Border>
                    <div className="row">
                        <div className="col-12">
                            <h2>Line Example</h2>
                        </div>
                        <div className="col-8">
                            <Line data={this.data} />
                        </div>
                    </div>
                </Border>
             </HContainer>
        );
    }
}