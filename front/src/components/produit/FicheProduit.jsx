import General from "../utils/General";
import HContainer from "../client/nav/HContainer";
import Border from "../utils/Border";
import BPanier from "./BPanier";

export default class FicheProduit extends General {
    componentDidMount = () => {
        this.getListe(this.json_server+"/produits-fiche/"+this.props.id,
            data => {
                this.setState({
                    fiche : data.data
                })
            },
            header => {

            }
        )
    }
    render() {
        return (
            <HContainer>
                {
                    this.state.fiche !== undefined ?
                    <div className="col-12">
                        <h1 className="text-dark">{this.state.fiche.produit.titre}</h1>
                        <h4>{this.state.fiche.produit.categorie.titre}</h4>
                        <div className="row">
                            <div className="col-12  mt-5">
                                <Border>
                                    <div className="row pb-3">
                                        <div className="col-12">
                                            <div className="row">
                                                <div className="col-4">
                                                </div>
                                                <div className="col-8">
                                                    <h2 className="card-title">Caractéristique</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 mt-3">
                                            <div className="row">
                                                <div className="col-4 ">
                                                    <img className="card-img-top img-fluid " src="/assets/images/landingpage/db.png"
                                                alt=""/>
                                                </div>
                                                <div className="col-4 ">
                                                    <h4 className="text-info ">Quantité</h4>
                                                    <h4 className="">{this.state.fiche.produit.quantite}g</h4>
                                                    <h4 className="text-info mt-3">Prix</h4>
                                                    <h4 className="">{this.state.fiche.produit.prix}ar</h4>
                                                    <h4 className="text-info mt-3">Disponible</h4>
                                                    <h4 className="">{this.state.fiche.disponible}</h4>
                                                    <p className="text-dark">Nombre :</p>
                                                    <BPanier id={this.state.fiche.produit.id} info={this.state.fiche.produit}></BPanier>
                                                </div>
                                                <div className="col-4 ">
                                                    <h4 className="card-title ">Description</h4>
                                                    <p className="">
                                                    {this.state.fiche.produit.description}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </Border>
                            </div>
                        </div>
                    </div> :<></>
                }
            </HContainer>
        );
    }
}