import { useParams } from "react-router-dom";
import ProduitUpdate from "./ProduitUpdate";

export default function ProduitUpdateGetter() {
    const {id} = useParams();
    return (
        <ProduitUpdate id={id}>
        </ProduitUpdate>
    );
}