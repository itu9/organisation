import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class ProduitForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);this.categorieid = React.createRef(null);
        this.urlSend = "/produit";
        this.urlUtils = "/produit/utils";
        this.afterValidation = "";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            titre : this.prepare(this.titre),categorieid : this.checkRefNull(this.categorieid,{id:this.prepare(this.categorieid)},null)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Titre</h3>
<input type="text" className="form-control" ref={this.titre} ></input><h3 className="text-info mt-3" >Categorie</h3>
<select className="form-control" ref={this.categorieid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.categorieid.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}