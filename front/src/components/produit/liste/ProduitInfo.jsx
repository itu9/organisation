import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class ProduitInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/produit";
    this.urlUtils = "/produit/utils";
    this.nb = React.createRef(null);
    this.quantite = React.createRef(null);
  }

  componentDidMount() {
    this.initUpdate();
  }

  oneProduct = () => {
    let quantite = this.quantite.current.value;
    let data = this.getStorage(this.panierKey, []);
    data = data.filter((d) => d.id === parseInt(quantite,10));
    return (data.length === 0)? null : data[0];
  }

  getModele = () => {
    let quantite = this.quantite.current.value;
    let data = this.state.oneValue.stock;
    data = data.filter((d) => d.modeleid.id === parseInt(quantite,10) );
    return (data.length === 0)? null : data[0].modeleid;

  }

  addProduitInfo = () => {
    let prod = this.oneProduct();
    let nb = parseFloat( this.nb.current.value);
    let produit = null;
    if (prod !== null) {
      this.removeStorage(this.panierKey,prod.id);
      nb += parseFloat(prod.nb);
      produit = prod.data
    } else {
      produit = this.getModele()
      let val = this.state.oneValue
      val.stock = []
      produit.produitid = val
    }
    let data = {
      id : produit.id,
      data : produit,
      nb : nb,
    }
    let all = this.getStorage(this.panierKey,[]);
    all = [
      ...all,
      data
    ]
    this.addStorage(this.panierKey,all);
  }

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Fiche de details</h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-warning btn-block"
                      onClick={() => {
                        window.location.replace(
                          "/produit/update/" + this.props.id
                        );
                      }}
                    >
                      Modifier
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/produit");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-success btn-block"
                      onClick={() => {
                        window.location.replace("/produit/form/");
                      }}
                    >
                      Ajouter
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={() => {
                        this.delete();
                      }}
                    >
                      Suprimer
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-info mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Titre</h3>
                    <p className="text-dark">{this.state.oneValue.titre}</p>
                    <h3 className="text-info">Categorieid</h3>
                    <p className="text-dark">
                      {this.state.oneValue.categorieid.titre}
                    </p>
                    <h3 className="text-info">La valeur en stock</h3>
                    <table className="table">
                      <thead className="bg-info text-white">
                      <tr>
                        <th>Quantite\Prix</th>
                        <th>Reste</th>
                      </tr>
                      </thead>
                      {
                        this.state.oneValue.stock.map((stk,index) =>  (
                          <tr>
                            <td>{stk.modeleid.quantite}g\{stk.modeleid.prix}ar</td>
                            <td>{stk.nb}</td>
                          </tr>
                        ))
                      }
                    </table>
                {
                  this.state.oneValue.stock.length > 0 ?
                    <>
                    <h2 className="text-info">Ajouter au panier</h2>
                    <div className="row">
                      <div className="col-6">
                        <h3 className="text-info">Quantite\Prix</h3>
                        <select className="form-control" ref={this.quantite}>
                          {
                            this.state.oneValue.stock.map((stk,index) => (
                              <option value={stk.modeleid.id}>{stk.modeleid.quantite}g\{stk.modeleid.prix}ar</option>
                            ))
                          }
                        </select>
                        <h3 className="text-info">Nombre</h3>
                        <input type="text" className="form-control" defaultValue="1" ref={this.nb}/>
                        <button className="btn btn-block btn-success mt-3" onClick={this.addProduitInfo}>Valider</button>
                      </div>
                    </div>
                    
                    </>
                  : <></>
                }
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
