import General from "../utils/General";

export default class PLink extends General {
    render() {
        return (
            <a href={"/produit/"+this.toRef(this.props.produit.titre)+"/"+this.props.produit.id} data-placement="top"  data-toggle="tooltip" title="Voir le fiche produit">
                {this.props.children}
            </a>
        );
    }
}