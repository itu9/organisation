import General from "../utils/General";
import Card from "../utils/Card";
import './CProduit.css'
import BPanier from "./BPanier";
import PLink from "./PLink";
import OwnService from "../../service/OwnService";

export default class CProduit extends General {
    render() {
        return (
            <Card size="6" link="/assets/images/landingpage/db.png">
                <PLink produit={this.props.produit}><h2 className="text-dark hvTitle ">{this.props.produit.titre}</h2></PLink>
                <h4 >{this.props.produit.categorie.titre}</h4>
                <h4 className="text-dark mt-3">Quantite :</h4>
                <h4 className="">{this.props.produit.quantite}g</h4>
                <h4 className="text-dark mt-3">Prix :</h4>
                <h4 className="text-info">{OwnService.format( this.props.produit.prix)}</h4>
                <BPanier id={this.props.produit.id} info={this.props.produit}></BPanier>
            </Card>
        );
    }
}