import { Component } from "react";
import './Authentification.css'

export default class Authentication extends Component {

    render() {
        return (
            <>
                <div className="about_area mt-5 auth">
                    <div className="container">
                        <div class="row ">
                            <div className="col-lg-6">
                                <div className="about_image">
                                    <img src="/assets/img/about/about.png" alt=""/>
                                </div>
                            </div>
                            <div className="col-lg-4 authform">
                                <h1>Login {this.props.title}</h1>
                                <div className="mt-3">
                                    <strong >Email</strong>
                                    <br></br>
                                    <input ref={this.props.email} className="form-control w-50" type="email" placeholder="mark@gmail.com" ></input>
                                </div>
                                <div className="mt-3">
                                    <strong >Password</strong>
                                    <br></br>
                                    <input ref={this.props.pwd} className="form-control w-50" type="password" placeholder="********"></input>
                                </div>
                                <button className="btn w-100 mt-3"
                                    onClick={
                                        () => {
                                            this.props.action();
                                        }
                                    }
                                >
                                    Login
                                </button>
                                <a href={this.props.conlink} className="mt-3 hconnect">{this.props.conas}</a>
                                <div>Don't have a count? <a href="/login/admin" className="mt-3 hconnect">Sing up</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}