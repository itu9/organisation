import React from "react";
import General from "../utils/General";
import './Login.css';

export default class Login extends General {
    constructor(params) {
        super(params);
        this.email = React.createRef();
        this.pwd = React.createRef();
        this.afterAuth = this.props.after;
        this.state = {
            fail : false
        }
    }

    componentDidMount = () => {
        this.email.current.value = this.props.defaultemail;
        this.pwd.current.value =this.props.defaultpwd;
    }

    sendData = () => {
        let data = {
            login :{
                email : this.email.current.value,
                pwd : this.pwd.current.value
            }
        };
        fetch('http://localhost:8080'+this.props.url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if (data.code !== undefined) {
                this.setState({
                    fail : true
                });
            }else {
                console.log(data);
                this.addAuth(data.data);
                window.location.replace(this.afterAuth);
            }
        });

    }

    render() {
        return (
             <React.Fragment>
                <div className="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative loutside"
                    >
                    <div className="auth-box row">
                        <div className="col-lg-7 col-md-5 modal-bg-img next">
                        </div>
                        <div className="col-lg-5 col-md-7 bg-white">
                            <div className="p-3">
                                <div className="text-center">
                                    <img src="/assets/images/big/icon.png" alt="wrapkit"/>
                                </div>
                                <h2 className="mt-3 text-center">Authentification</h2>
                                <p className="text-center">Connectez-vous avec votre email et mot de passe</p>
                                <div className="mt-4">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label className="text-dark" htmlFor="uname">Email</label>
                                                <input className="form-control" id="uname" type="email"
                                                    placeholder="jean@gmail.com" ref={this.email} />
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group">
                                                <label className="text-dark" htmlFor="pwd">Mot de passe</label>
                                                <input className="form-control" id="pwd" type="password"
                                                    placeholder="**********" ref={this.pwd} />
                                            </div>
                                        </div>
                                        {
                                            this.state.fail ? <p className="text-center ml-3 text-danger hfail">**Authentification échouée**</p>:<p></p>
                                        }
                                        

                                        <div className="col-lg-12 text-center"> 
                                            <button type="submit" className="btn btn-block btn-dark" onClick={this.sendData}>Se connecter</button>
                                        </div>
                                        <div className="col-lg-12 text-center mt-3">
                                            Se connecter en tant <a href={this.props.etat !== "25" ? "/":"/admin"} className="text-info">{this.props.etat !== "25" ? "que client": "qu'admin"}</a>
                                        </div>
                                        <div className="col-lg-12 text-center ">
                                            Pas de compte? <a href="/signup" className="text-danger">S&apos;inscrire</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </React.Fragment>
        );
    }
}