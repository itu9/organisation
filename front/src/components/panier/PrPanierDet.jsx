import General from "/src/components/utils/General";
import React from "react";
import OwnService from "service/OwnService";

export default class PrPanierDet extends General {
    constructor(params) {
        super(params);
        this.checked = React.createRef(null)
    }

    flipflap = (data) => {
        if (this.checked.current.checked) {
            this.props.add(data);
        } else {
            this.props.remove(data);
        }
    }
    render() {
        return (
            <tr>
                <td>{this.props.panier.data.produitid.titre} {this.props.panier.data.quantite}g</td>
                <td>{OwnService.format(this.props.panier.data.prix)}</td>
                <td>{this.props.panier.nb}</td>
                <td>{OwnService.format(this.props.panier.nb*this.props.panier.data.prix)}</td>
                {/* <td>
                    <button className="btn">
                        <i className="fas fa-plus text-success"></i>
                    </button>
                    <button className="btn ml-3">
                        <i className="fas fa-minus text-danger"></i>
                    </button>
                </td> */}
                <td>
                    <input type="checkbox" ref={this.checked} onClick={() => {
                        this.flipflap(this.props.panier)
                        }} />
                </td>
            </tr>
        );
    }
}