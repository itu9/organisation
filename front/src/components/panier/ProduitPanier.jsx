import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralListe from "/src/components/utils/GeneralListe";
import ListePage from "/src/components/utils/ListePage";
import PrPanierDet from "./PrPanierDet";
import Swal from "sweetalert2";
import OwnService from "service/OwnService";

export default class ProduitPanier extends GeneralListe {
    constructor(params) {
        super(params);
        this.state = {
            panier : [],
            validation : [],
            argent : null
        }
    }

    componentDidMount() {
        this.getPanier();
        this.getMoney();
    }

    getMoney = () => {
        let user = this.getSession(this.auth,null);
        if (user !== null) {
            let id = user.login.id;
            this.getListe(this.url+"/client/argent/client/"+id+"?idUser="+id,
                data => {
                    this.setState({
                        argent : data.data
                    })
                },
                header => {}
            )
        }
    }

    add = (data) => {
        this.setState({
            validation : [
                ...this.state.validation,
                data
            ]
        })
    }

    remove = (data) => {
        let liste = this.state.validation.filter(val => val.id !== data.id);
        this.setState({
            validation : liste
        })
    }

    getPanier = () => {
        let data = this.getStorage(this.panierKey,[]);
        this.setState({
            panier : data
        })
    }

    removePayed = () => {
        let data = this.getStorage(this.panierKey,[]);
        for (let i = 0; i < this.state.validation.length; i++) {
            data = data.filter(d => d.id !== this.state.validation[i].id)
        }
        this.addStorage(this.panierKey,data)    
    }

    payer = () => {
        // this.removePayed();
        this.checkConnexion();
        let user = this.getSession(this.auth,{login : {id : -1}})
        this.sendData(this.url+"/vente/payement?idUser="+user.login.id,this.state.validation,
            answer => {
                this.verifData(answer,
                    response => {
                        this.removePayed();
                        Swal.fire({
                            icon : 'success',
                            title : 'Panier payé',
                            text : 'Merci',
                            timer : 1800
                        },
                            () => {
                                window.location.reload();
                            }
                        )
                    }
                )
            }
        )

    }

    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                <div className="col-12">
                    <Border>
                        <h1 className="text-dark">Les produits dans le panier</h1>
                        {
                            this.state.argent !== null ? 
                            <h3>Porte feuille  : <span className="text-info">{OwnService.format(this.state.argent.montant)}</span></h3> : <></>
                        }
                        
                        <ListePage
                            head={
                                <tr>
                                    <th>Produit</th>
                                    <th>Prix Unitaire</th>
                                    <th>Nombre</th>
                                    <th>Total</th>
                                    {/* <th>Action</th> */}
                                    <th></th>
                                </tr>
                            }
                        >
                            {
                                this.state.panier.map((pan,index)=> (
                                    <PrPanierDet key={index} panier={pan} add={this.add} remove={this.remove}></PrPanierDet>
                                ))
                            }
                        </ListePage>
                        <div className="row mt-3">
                            <div className="col-6">
                                <button className="btn btn-block btn-success" onClick={this.payer}>Payer le panier</button>
                            </div>
                        </div>
                    </Border>
                </div>
                </div>
            </HContainer>
        );
    }
}
