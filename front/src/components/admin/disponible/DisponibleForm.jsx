import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class DisponibleForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.numero = React.createRef(null);
    this.modeleid = React.createRef(null);
    this.urlSend = "/disponible";
    this.urlUtils = "/disponible/utils";
    this.afterValidation = "/disponible";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      numero: this.prepare(this.numero),
      modeleid: this.checkRefNull(
        this.modeleid,
        { id: this.prepare(this.modeleid) },
        null
      ),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Numero</h3>
              <input
                type="text"
                className="form-control"
                ref={this.numero}
              ></input>
              <h3 className="text-info mt-3">Modele</h3>
              <select className="form-control" ref={this.modeleid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.modeleid.map((data, index) => (
                    <option value={data.id}>{data.marqueid.titre} {data.typeid.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
