import { useParams } from "react-router-dom";

export default function DisponibleUpdateGetter() {
    const {id} = useParams();
    return (
        <DisponibleUpdate id={id}>
        </DisponibleUpdate>
    );
}