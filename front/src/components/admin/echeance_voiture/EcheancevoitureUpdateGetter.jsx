import { useParams } from "react-router-dom";
import EcheancevoitureUpdate from "./EcheancevoitureUpdate";

export default function EcheancevoitureUpdateGetter() {
    const {id} = useParams();
    return (
        <EcheancevoitureUpdate id={id}>
        </EcheancevoitureUpdate>
    );
}