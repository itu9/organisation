import { Component } from "react";
import HContainer from "../../client/nav/HContainer";
import Developpers from "../developper/Developpers";
import AddProject from "../project/AddProject";
import './AcceuilAd.css'

export default class AcceuilAd extends Component {
    render() {
        return (
             <HContainer>
                <div className="container hcnt">
                    <AddProject></AddProject>
                    <Developpers></Developpers>
                </div>
             </HContainer>
        );
    }
}