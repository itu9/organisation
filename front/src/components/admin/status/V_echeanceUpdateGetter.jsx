import { useParams } from "react-router-dom";
import V_echeanceUpdate from "./V_echeanceUpdate";

export default function V_echeanceUpdateGetter() {
    const {id} = useParams();
    return (
        <V_echeanceUpdate id={id}>
        </V_echeanceUpdate>
    );
}