import General from '../../utils/General'
import HContainer from "../../client/nav/HContainer";
import Border from '../../utils/Border';
import Champ from './Champ';
import HButton from '../../utils/HButton';

export default class Model extends General {
    componentDidMount = () => {
        this.setState({
            label : [
                {
                    id : 1,
                    titre : "Ram"
                },
                {
                    id : 2,
                    titre : "Processeur"
                }
            ]
        },
            this.addDet
        )
    }

    addDet = () => {
        let id = this.getNextId();
        this.addTemplate(<Champ id={id} label={this.state.label} remove ={
            () => {
                this.removeTemplate(id, 
                    () => {}
                )
            }
        }/>,id)
    }

    render() {
        return (
             <HContainer>
                <div className="col-8">
                    <Border>
                        <h2 className="text-info">Ajout d'un nouveau model</h2>
                        <div className="row">
                            <div className="col-12 mt-3">
                                <h4 className="text-dark">Titre</h4>
                                <input className="form-control" type="text"></input>
                            </div>
                            <div className="col-12 mt-3">
                                <h4 className="text-dark">Marque</h4>
                                <select className="form-control">
                                    <option>LG</option>
                                </select>
                            </div>
                            <div className="col-12 mt-3">
                                <h4 className="text-dark">Type de materielle</h4>
                                <select className="form-control">
                                    <option>LG</option>
                                </select>
                            </div>
                            <div className="col-12 mt-3">
                                <div className="row mt-3 text-info">
                                        <div className="col-5"><strong>Nom</strong></div>
                                        <div className="col-6"><strong>Label</strong></div>
                                        <div className="col-1"><strong></strong></div>
                                </div>
                                {
                                    this.state.template.map(temp => (
                                        temp.data
                                    ))
                                }
                                <div className="row mt-3 text-info">
                                    <div className="col-12" onClick={
                                        () => {
                                            this.addDet();
                                        }
                                    }>
                                        <h3 className="text-success">
                                            <i className="fas fa-plus"></i>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 mt-3">
                                <HButton title={<>Ok</>}  tColor= "white" />
                            </div>
                        </div>
                    </Border>
                </div>
             </HContainer>
        );
    }
}