import General from "../../utils/General";
import React from 'react';

export default class Champ extends General {

    constructor(params) {
        super(params);
        this.title = React.createRef(null);
        this.label = React.createRef(null);
    }

    render() {
        return (
             <>
                <div className="row mt-3">
                    <div className="col-5">
                        <input className="form-control" type="number" ref={this.titre} onChange={this.recolte} />
                    </div>
                    <div className="col-6">
                        <select className="form-control" ref={this.label}>
                            {
                                this.props.label.map(prod => (
                                    <option value={prod.id}>{prod.titre}</option>
                                ))
                            }
                        </select>
                    </div>
                    <div className="col-1" onClick={
                        () => {
                            this.props.remove();
                        }
                    }>
                        <h3 className="text-danger">
                        <i className="fas fa-times"></i>
                        </h3>
                    </div>
                </div>
             </>
        );
    }
}