import HContainer from "../../client/nav/HContainer";
import General from "../../utils/General";
import ListePage from "../../utils/ListePage";

export default class LModel extends General {
    render() {
        return (
            <HContainer>
                <ListePage
                    title = "Liste des models existants"
                    size="8"
                    head = {
                        <tr>
                            <th>Titre</th>
                            <th>Marque</th>
                            <th>Type materiel</th>
                            <th></th>
                        </tr>
                    }
                >
                    <tr>
                        <td>LG+</td>
                        <td>LG</td>
                        <td>Telephone</td>
                        <td><a href="/lmodel/1"> Voir plus </a></td>
                    </tr>
                    <tr>
                        <td>LG+</td>
                        <td>LG</td>
                        <td>Telephone</td>
                        <td><a href="/lmodel/1"> Voir plus </a></td>
                    </tr>
                </ListePage>
            </HContainer>
        );
    }
}