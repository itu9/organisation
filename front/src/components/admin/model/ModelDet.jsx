import HContainer from "../../client/nav/HContainer";
import Border from "../../utils/Border";
import Desc from "../../utils/Desc";
import General from "../../utils/General";

export default class ModelDet extends General {
    componentDidMount = () => {
        this.setState({
            product : {
                titre : "LG+",
                marque : {
                    id : 1,
                    titre : "LG"
                },
                type : {
                    id : 1,
                    titre : "Telephone"
                },
                champs : [
                    {
                        id : 1,
                        nom : "Memoire vive : ",
                        label : {
                            id : 1,
                            titre : "ram"
                        }
                    }
                ]
            }
        })
    }

    getTitre = () => {
        return this.state.product !== undefined ? this.state.product.titre : ''
    }
    render() {
        return (
            <HContainer>
                <div className="col-8">
                    <Border>
                        <h2 className="text-info card-title">Fiche du model {this.getTitre()}</h2>
                        <div className="row">
                            <div className="col-12 mt-3">
                                <Desc title="Titre">LG+</Desc>
                            </div>
                            <div className="col-12 mt-3">
                                <Desc title="Marque">LG</Desc>
                            </div>
                            <div className="col-12 mt-3">
                                <Desc title="Type materiel">Téléphone</Desc>
                            </div>
                            <div className="col-12 mt-5">
                                <h3 className="card-title">Details</h3>
                            </div>
                            <div className="col-12 mt-3">
                                
                                <div className="row mt-3 text-info">
                                    <div className="col-6"><h4 className="card-title">Nom</h4></div>
                                    <div className="col-6"><h4 className="card-title">Label</h4></div>
                                </div>
                                {
                                    this.state.product !== undefined ? 
                                        this.state.product.champs.map(champ => (
                                            <div className="row mt-3 card-text">
                                                <div className="col-6"><strong>{champ.nom}</strong></div>
                                                <div className="col-6"><strong>{champ.label.titre}</strong></div>
                                            </div>
                                        ))
                                    : <></>
                                }
                            </div>
                        </div>
                    </Border>
                </div>
            </HContainer>
        );
    }
}