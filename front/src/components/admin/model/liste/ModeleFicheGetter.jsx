import { useParams } from "react-router-dom";
import ModeleInfo from "./ModeleInfo";

export default function ModeleFicheGetter() {
    const {id} = useParams();
    return (
        <ModeleInfo id={id}>
        </ModeleInfo>
    );
}