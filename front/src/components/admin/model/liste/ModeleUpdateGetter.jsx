import { useParams } from "react-router-dom";
import ModeleUpdate from "./ModeleUpdate";

export default function ModeleUpdateGetter() {
    const {id} = useParams();
    return (
        <ModeleUpdate id={id}>
        </ModeleUpdate>
    );
}