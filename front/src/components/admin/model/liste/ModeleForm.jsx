import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class ModeleForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.produitid = React.createRef(null);
    this.quantite = React.createRef(null);
    this.prix = React.createRef(null);
    this.urlSend = "/modele";
    this.urlUtils = "/modele/utils";
    this.afterValidation = "/modele";
  }
  componentDidMount() {
    this.init();
    this.checkConnexion();
  }
    getFormUrl = () => {
        this.checkConnexion();
        let auth = this.getSession(this.auth,{
            login : {
                id : -1
            }
        })
        return this.url+this.urlSend+"?idUser="+auth.login.id;
    }

  validateData = () => {
    let data = {
      produitid: this.checkRefNull(
        this.produitid,
        { id: this.prepare(this.produitid) },
        null
      ),
      quantite: this.prepare(this.quantite),
      prix: this.prepare(this.prix),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Produit</h3>
              <select className="form-control" ref={this.produitid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.produitid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Quantite</h3>
              <input
                type="text"
                className="form-control"
                ref={this.quantite}
              ></input>
              <h3 className="text-info mt-3">Prix</h3>
              <input
                type="text"
                className="form-control"
                ref={this.prix}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
