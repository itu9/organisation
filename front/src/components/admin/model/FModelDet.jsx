import { useParams } from "react-router-dom"
import ModelDet from "./ModelDet";

export default function FModelDet() {
    const {id} = useParams();
    return (
        <ModelDet id={id}></ModelDet>
    )
}