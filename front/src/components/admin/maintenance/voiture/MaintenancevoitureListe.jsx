import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";

export default class MaintenancevoitureListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.vehiculeid = React.createRef(null);
    this.datehmin = React.createRef(null);
    this.datehmax = React.createRef(null);
    this.maintenanceid = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/maintenance/voiture/filter";
    this.urlUtils = "/maintenance/voiture/utils";
    this.baseUrl = "/maintenance/voiture";
  }
  componentDidMount() {
    this.init();
  }
  findData = () => {
    let data = {
      keys: this.prepare(this.keys),
      vehiculeid: this.checkRefNull(
        this.vehiculeid,
        { id: this.prepare(this.vehiculeid) },
        null
      ),
      datehmin: this.prepare(this.datehmin),
      datehmax: this.prepare(this.datehmax),
      maintenanceid: this.checkRefNull(
        this.maintenanceid,
        { id: this.prepare(this.maintenanceid) },
        null
      ),
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Vehicule</h3>
                      <select className="form-control" ref={this.vehiculeid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.vehiculeid.map((data, index) => (
                            <option value={data.id}>{data.numero}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Date</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.datehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.datehmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Maintenance</h3>
                      <select className="form-control" ref={this.maintenanceid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.maintenanceid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace(
                                    "/maintenance/voiture/form"
                                  );
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Vehiculeid</td>
                    <td>Date</td>
                    <td>Maintenanceid</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr>
                      <td>{data.vehiculeid.numero}</td>
                      <td>{data.date}</td>
                      <td>{data.maintenanceid.titre}</td>
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace("/maintenance/voiture/update/"+data.id);
                          }}
                        >
                          <i className="fas fa-pencil-alt text-info"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
