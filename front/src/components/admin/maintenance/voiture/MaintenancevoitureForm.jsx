import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class MaintenancevoitureForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.vehiculeid = React.createRef(null);
    this.date = React.createRef(null);
    this.maintenanceid = React.createRef(null);
    this.urlSend = "/maintenance/voiture";
    this.urlUtils = "/maintenance/voiture/utils";
    this.afterValidation = "/maintenance/voiture";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      vehiculeid: this.checkRefNull(
        this.vehiculeid,
        { id: this.prepare(this.vehiculeid) },
        null
      ),
      date: this.prepare(this.date),
      maintenanceid: this.checkRefNull(
        this.maintenanceid,
        { id: this.prepare(this.maintenanceid) },
        null
      ),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Vehicule</h3>
              <select className="form-control" ref={this.vehiculeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.vehiculeid.map((data, index) => (
                    <option value={data.id}>{data.numero}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Date</h3>
              <input
                type="date"
                className="form-control"
                ref={this.date}
              ></input>
              <h3 className="text-info mt-3">Maintenance</h3>
              <select className="form-control" ref={this.maintenanceid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.maintenanceid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
