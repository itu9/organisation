import { useParams } from "react-router-dom";
import MaintenancevoitureUpdate from "./MaintenancevoitureUpdate";

export default function MaintenancevoitureUpdateGetter() {
    const {id} = useParams();
    return (
        <MaintenancevoitureUpdate id={id}>
        </MaintenancevoitureUpdate>
    );
}