import { useParams } from "react-router-dom";
import Maintenance_statusUpdate from "./Maintenance_statusUpdate";

export default function Maintenance_statusUpdateGetter() {
    const {id} = useParams();
    return (
        <Maintenance_statusUpdate id={id}>
        </Maintenance_statusUpdate>
    );
}