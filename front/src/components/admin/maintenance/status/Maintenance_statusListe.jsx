import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";

export default class Maintenance_statusListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.vehiculeid = React.createRef(null);
    this.maintenanceid = React.createRef(null);
    this.kilometragehmin = React.createRef(null);
    this.kilometragehmax = React.createRef(null);
    this.restehmin = React.createRef(null);
    this.restehmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/maintenance/status/filter";
    this.urlUtils = "/maintenance/status/utils";
    this.baseUrl = "/maintenance/status";
  }
  componentDidMount() {
    this.init();
  }
  findData = () => {
    let data = {
      keys: this.prepare(this.keys),
      vehiculeid: this.checkRefNull(
        this.vehiculeid,
        { id: this.prepare(this.vehiculeid) },
        null
      ),
      maintenanceid: this.checkRefNull(
        this.maintenanceid,
        { id: this.prepare(this.maintenanceid) },
        null
      ),
      kilometragehmin: this.prepare(this.kilometragehmin),
      kilometragehmax: this.prepare(this.kilometragehmax),
      restehmin: this.prepare(this.restehmin),
      restehmax: this.prepare(this.restehmax),
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Vehicule</h3>
                      <select className="form-control" ref={this.vehiculeid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.vehiculeid.map((data, index) => (
                            <option value={data.id}>{data.numero}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Maintenance</h3>
                      <select className="form-control" ref={this.maintenanceid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.maintenanceid.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Kilometrage</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.kilometragehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.kilometragehmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Reste</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.restehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.restehmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              {/* <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace(
                                    "/maintenance/status/form"
                                  );
                                }}
                              >
                                Ajouter un nouveau
                              </button> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Vehicule</td>
                    <td>Maintenance</td>
                    <td>Kilometrage</td>
                    <td>Reste</td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr className={data.reste <= 200 ? "bg-danger text-white" : data.reste <= 500 ? "bg-yellow" : ""}>
                      <td>{data.vehiculeid.numero}</td>
                      <td>{data.maintenanceid.titre}</td>
                      <td>{data.kilometrage}</td>
                      <td>{data.reste}</td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
