import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class Maintenance_statusUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.vehiculeid = React.createRef(null);this.maintenanceid = React.createRef(null);this.kilometrage = React.createRef(null);this.reste = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/maintenance/status";
        this.urlUtils = "/maintenance/status/utils";
        this.afterValidation = "";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.vehiculeid.current.value = this.state.oneValue.vehiculeid.id;this.maintenanceid.current.value = this.state.oneValue.maintenanceid.id;this.kilometrage.current.value = this.state.oneValue.kilometrage;this.reste.current.value = this.state.oneValue.reste;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            vehiculeid : this.checkRefNull(this.vehiculeid,{id:this.prepare(this.vehiculeid)},null),maintenanceid : this.checkRefNull(this.maintenanceid,{id:this.prepare(this.maintenanceid)},null),kilometrage : this.prepare(this.kilometrage),reste : this.prepare(this.reste),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Vehicule</h3>
<select className="form-control" ref={this.vehiculeid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.vehiculeid.map((data,index) => (
            <option value={data.id} >{data.numero}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Maintenance</h3>
<select className="form-control" ref={this.maintenanceid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.maintenanceid.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Kilometrage</h3>
<input type="text" className="form-control" ref={this.kilometrage} ></input><h3 className="text-info mt-3">Reste</h3>
<input type="text" className="form-control" ref={this.reste} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}