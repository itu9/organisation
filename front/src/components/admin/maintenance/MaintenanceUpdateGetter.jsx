import { useParams } from "react-router-dom";
import MaintenanceUpdate from "./MaintenanceUpdate";

export default function MaintenanceUpdateGetter() {
    const {id} = useParams();
    return (
        <MaintenanceUpdate id={id}>
        </MaintenanceUpdate>
    );
}