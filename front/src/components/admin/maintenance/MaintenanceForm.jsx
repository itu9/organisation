import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class MaintenanceForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.titre = React.createRef(null);
    this.valable = React.createRef(null);
    this.urlSend = "/maintenance";
    this.urlUtils = "/maintenance/utils";
    this.afterValidation = "/maintenance";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      titre: this.prepare(this.titre),
      valable: this.prepare(this.valable),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Titre</h3>
              <input
                type="text"
                className="form-control"
                ref={this.titre}
              ></input>
              <h3 className="text-info mt-3">Valable</h3>
              <input
                type="text"
                className="form-control"
                ref={this.valable}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
