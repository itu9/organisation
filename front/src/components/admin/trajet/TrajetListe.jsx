import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import ListePage from "/src/components/utils/ListePage";
import GeneralListe from "/src/components/utils/GeneralListe";
import React from "react";
import OwnService from "service/OwnService";

export default class TrajetListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.hdeparthmin = React.createRef(null);
    this.hdeparthmax = React.createRef(null);
    this.lieudep = React.createRef(null);
    this.kilodephmin = React.createRef(null);
    this.kilodephmax = React.createRef(null);
    this.harrivehmin = React.createRef(null);
    this.harrivehmax = React.createRef(null);
    this.lieuarr = React.createRef(null);
    this.kiloarrhmin = React.createRef(null);
    this.kiloarrhmax = React.createRef(null);
    this.vehiculeid = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/trajet/filter";
    this.urlUtils = "/trajet/utils";
    this.baseUrl = "/trajet";
  }
  componentDidMount() {
    this.init();
  }
  findData = () => {
    this.search(this.getDataExemple());
  };

  getDataExemple = () => {
    let data = {
      keys: this.prepare(this.keys),
      hdeparthmin: this.prepare(this.hdeparthmin),
      hdeparthmax: this.prepare(this.hdeparthmax),
      lieudep: this.checkRefNull(
        this.lieudep,
        { id: this.prepare(this.lieudep) },
        null
      ),
      kilodephmin: this.prepare(this.kilodephmin),
      kilodephmax: this.prepare(this.kilodephmax),
      harrivehmin: this.prepare(this.harrivehmin),
      harrivehmax: this.prepare(this.harrivehmax),
      lieuarr: this.checkRefNull(
        this.lieuarr,
        { id: this.prepare(this.lieuarr) },
        null
      ),
      kiloarrhmin: this.prepare(this.kiloarrhmin),
      kiloarrhmax: this.prepare(this.kiloarrhmax),
      vehiculeid: this.checkRefNull(
        this.vehiculeid,
        { id: this.prepare(this.vehiculeid) },
        null
      ),
    };
    return data;
  }

  toPdf = ()=> {
    let data = this.getDataExemple();
    let ans = {
      ...data,
      pdf : {
        titre : "Les commandes du mois d'avril",
        inheritParams : [
            {
                field:"lieudep",
                value:"id",
                label:"titre",
                title : "Lieu de depart"
            },
            {
                field:"lieuarr",
                value:"id",
                label:"titre",
                title : "Lieu d'arrivé"
            }
        ],
        fields : [
            {
                field : "kilodep",
                titre : "Depart"
            },
            {
                field : "kiloarr",
                titre : "Arrivé"
            },
            {
                field : "hdepart",
                titre : "Heure de depart"
            },
            {
                field : "harrive",
                titre : "Heure d'arrivé"
            }
        ]
      }
    }
    // console.log('ans',ans);
    this.pdf(ans);
    
  }
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Titre de la liste"
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Heure de départ</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.hdeparthmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.hdeparthmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Lieu de depart</h3>
                      <select className="form-control" ref={this.lieudep}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.lieudep.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Kilometrage au depart</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.kilodephmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.kilodephmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Heure d'arrivé</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.harrivehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.harrivehmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Lieu d'arrivé</h3>
                      <select className="form-control" ref={this.lieuarr}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.lieuarr.map((data, index) => (
                            <option value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Kilometrage à l'arrivé</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.kiloarrhmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.kiloarrhmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Vehicule</h3>
                      <select className="form-control" ref={this.vehiculeid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.vehiculeid.map((data, index) => (
                            <option value={data.id}>{data.numero}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div class="btn-list w-100">
                                <div class="btn-group">
                                  <button
                                    type="button"
                                    class="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div class="dropdown-menu w-100">
                                    <a class="dropdown-item " href="#a" onClick={()=> {
                                      this.toPdf();
                                    }}>
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a class="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/trajet/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Depart</td>
                    <td>Lieu</td>
                    <td>Kilometrage</td>
                    <td>Arrivé</td>
                    <td>Lieu</td>
                    <td>Kilometrage</td>
                    <td>Vehicule</td>
                    <td>Motif</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr key={index}>
                      <td>{OwnService.formatDate( data.hdepart)}</td>
                      <td>{data.lieudep.titre}</td>
                      <td>{data.kilodep}</td>
                      <td>{OwnService.formatDate(data.harrive)}</td>
                      <td>{
                        data.lieuarr !== null ?
                        data.lieuarr.titre :''
                      }</td>
                      <td>{data.kiloarr}</td>
                      <td>{data.vehiculeid.numero}</td>
                      <td>{data.motif}</td>
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace("/trajet/update/" + data.id);
                          }}
                        >
                          <i className="fas fa-pencil-alt text-info"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
