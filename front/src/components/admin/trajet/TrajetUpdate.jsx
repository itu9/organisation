import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class TrajetUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.hdepart = React.createRef(null);
    this.lieudep = React.createRef(null);
    this.kilodep = React.createRef(null);
    this.harrive = React.createRef(null);
    this.lieuarr = React.createRef(null);
    this.kiloarr = React.createRef(null);
    this.vehiculeid = React.createRef(null);
    this.motif = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/trajet";
    this.urlUtils = "/trajet/utils";
    this.afterValidation = "";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.hdepart.current.value = this.state.oneValue.hdepart;
    this.lieudep.current.value = this.state.oneValue.lieudep.id;
    this.kilodep.current.value = this.state.oneValue.kilodep;
    this.harrive.current.value = this.state.oneValue.harrive;
    this.lieuarr.current.value = this.state.oneValue.lieuarr.titre;
    this.kiloarr.current.value = this.state.oneValue.kiloarr;
    this.vehiculeid.current.value = this.state.oneValue.vehiculeid.id;
    this.motif.current.value = this.state.oneValue.motif;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      hdepart: this.prepare(this.hdepart),
      lieudep: this.checkRefNull(
        this.lieudep,
        { id: this.prepare(this.lieudep) },
        null
      ),
      kilodep: this.prepare(this.kilodep),
      harrive: this.prepare(this.harrive),
      lieuarr: this.checkRefNull(
        this.lieuarr,
        { id: this.prepare(this.lieuarr) },
        null
      ),
      kiloarr: this.prepare(this.kiloarr),
      vehiculeid: this.checkRefNull(
        this.vehiculeid,
        { id: this.prepare(this.vehiculeid) },
        null
      ),
      motif: this.prepare(this.motif),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Hdepart</h3>
              <input
                type="datetime-local"
                className="form-control"
                ref={this.hdepart}
              ></input>
              <h3 className="text-info mt-3">Lieu de depart</h3>
              <select className="form-control" ref={this.lieudep}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.lieudep.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Kilodep</h3>
              <input
                type="text"
                className="form-control"
                ref={this.kilodep}
              ></input>
              <h3 className="text-info mt-3">Harrive</h3>
              <input
                type="datetime-local"
                className="form-control"
                ref={this.harrive}
              ></input>
              <h3 className="text-info mt-3">Lieu d'arriv�</h3>
              <select className="form-control" ref={this.lieuarr}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.lieuarr.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Kiloarr</h3>
              <input
                type="text"
                className="form-control"
                ref={this.kiloarr}
              ></input>
              <h3 className="text-info mt-3">Vehicule</h3>
              <select className="form-control" ref={this.vehiculeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.vehiculeid.map((data, index) => (
                    <option value={data.id}>{data.numero}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Motif</h3>
              <input
                type="text"
                className="form-control"
                ref={this.motif}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
