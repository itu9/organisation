import { useParams } from "react-router-dom";
import TrajetUpdate from "./TrajetUpdate";

export default function TrajetUpdateGetter() {
    const {id} = useParams();
    return (
        <TrajetUpdate id={id}>
        </TrajetUpdate>
    );
}