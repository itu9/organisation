import General from "../../utils/General";
import HContainer from "../../client/nav/HContainer";
import Border from "../../utils/Border";
import OwnService from "../../../service/OwnService";
import PLink from "../../produit/PLink";

export default class Stock extends General {
    componentDidMount() {
        this.getListe(this.json_server+"/stock", (data) => {
            this.setState({
                produits : data.data
            })
        },
        data => {})
    }

    render() {
        return (
            <HContainer>
                <div className="col-12">
                    <h1 className="text-dark mb-5">Les produits dans le stock</h1>
                    <div className="row">
                        <div className="col-4">
                            <Border>
                                <h4 className="card-title">Recherche</h4>
                                <input type="text" className="form-control" placeholder="Par mot clé"/>  
                                <h4 className="card-title mt-3">Catégorie</h4>
                                <select className="form-control">
                                    <option></option>
                                    <option>Fruit</option>
                                    <option>Legume</option>
                                </select>
                                {/* Fourchette */}
                                <h4 className="card-title mt-3">Prix</h4>
                                <div className="row">
                                    <div className="col-6">
                                        <input type="text" className="form-control" placeholder="Prix min"/>  
                                    </div>
                                    <div className="col-6">
                                        <input type="text" className="form-control" placeholder="Prix max"/>  
                                    </div>
                                </div>
                                <h4 className="card-title mt-3">Quantité</h4>
                                <div className="row">
                                    <div className="col-6">
                                        <input type="text" className="form-control" placeholder="Quantité min"/>  
                                    </div>
                                    <div className="col-6">
                                        <input type="text" className="form-control" placeholder="Quantité max"/>  
                                    </div>
                                </div>
                            </Border>
                        </div>
                        <div className="col-8">
                            <Border>
                                <table className="table"> 
                                    <thead className="bg-orange text-white">
                                        <tr>
                                            <th>Produit</th>
                                            <th>Catégorie</th>
                                            <th>Qté</th>
                                            <th>Valeur</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.produits !== null && this.state.produits !== undefined? this.state.produits.map(produit => (
                                                <tr>
                                                    <td><PLink produit={produit.produit}>{produit.produit.titre}</PLink></td>
                                                    <td>{produit.produit.categorie.titre}</td>
                                                    <td>{produit.disponible}</td>
                                                    <td className="text-danger">{OwnService.format( produit.disponible*produit.produit.prix)}</td>
                                                </tr>
                                            )) : <></>
                                        }
                                    </tbody>
                                </table>
                            </Border>
                        </div>
                    </div>
                </div>
            </HContainer>
        );
    }
}