import React, { Component } from "react";
import HModal from "../../../service/HModal";
import OwnService from "../../../service/OwnService";
import HMiddle from "../../client/services/HMiddle";
import Developper from "../../client/team/Developper";
import DevelopperForm from './DevelopperForm'
export default class Developpers extends Component {
    constructor(params) {
        super(params);
        this.name = React.createRef();
        this.contact = React.createRef();
        this.desc = React.createRef();
        this.state = {
            developper : [],
            id : 0,
            addDev : false,
            count : 10,
            personName : []
        }
    }

    componentDidMount() {
        OwnService.getData(OwnService.url+"api/developers",
            (data) => {
                this.setState({
                    developper : data
                });
            }
        )
    }

    sendProject = () => {

    }

    update = (developper) => {
        this.name.current.value = developper.name
        this.contact.current.value = developper.contact
        this.desc.current.value = developper.description
        this.setState({
            id : developper.id
        })
    }

    onChange = (event) => {
        const {
            target: { value },
          } = event;
          this.setState({
            personName : typeof value === 'string' ? value.split(',') : value
          })
    }

    render() {
        
        return (
             <>
                <HMiddle title="Staff">
                    <div className="row" >
                        <div className="col-lg-12">
                            <h3>Staff list</h3>
                            <div className="row mb-5">
                                <div className="col-lg-4">
                                    <button className="btn w-100 contact"
                                        onClick={
                                            () => {
                                                this.setState({
                                                    addDev : true
                                                })
                                            }
                                        }
                                    >Add a member</button>
                
                                </div>
                            </div>
                            <div className="row">
                                {
                                    this.state.developper.map((developper) => (
                                        <Developper key={developper.id} size="4" developper={developper} admin={true} update={this.update}></Developper>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </HMiddle>
                <HModal 
                    isOpen={this.state.addDev}
                    onClose = {
                        () => {
                            this.setState({
                                addDev : false
                            })
                        }
                    }
                >
                    <DevelopperForm></DevelopperForm>
                </HModal>
             </>
        );
    }
}