import {  Step, StepContent, StepLabel, Stepper } from "@mui/material";
import { Box } from "@mui/system";
import { Component } from "react";
import './DevelopperForm.css'
import FirstForm from "./FirstForm";
import SecondForm from "./SecondForm";
export default class DevelopperForm extends Component {
    state = {
        page : 0,
        idDevelopper : -1
    }

    next = () => {
        this.setState({
            page : this.state.page+1
        })
    }

    configFirst = (data) =>  {
        this.setState({
            idDevelopper : data.id
        })
    }

    render() {
        return (
             <>
                <div className="col-lg-12 hdev">
                    <Box sx={{marginLeft: '5%',marginRight:'5%',marginTop : '-7%' }}>
                        <Stepper activeStep={this.state.page} orientation="vertical" >
                            <Step key={0}>
                                <StepLabel
                                >
                                <h3 className={this.state.page === 0 ? "hnow" : ""}>Add/Update developper</h3>
                                </StepLabel>
                                <StepContent>
                                    <FirstForm next={this.next} conf={this.configFirst}></FirstForm> 
                                </StepContent>
                            </Step>
                            <Step key={1}>
                            <StepLabel
                                >
                                <h3 className={this.state.page === 1 ? "hnow" : ""}>Additionnal information </h3>
                                </StepLabel>
                                <StepContent>
                                    <SecondForm id={this.state.idDevelopper}></SecondForm> 
                                </StepContent>
                            </Step>
                        </Stepper>
                    </Box>
                    {/* <FirstForm></FirstForm> */}
                </div>
             </>
        );
    }
}