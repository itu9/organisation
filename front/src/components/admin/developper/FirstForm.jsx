import React, { Component } from "react";
import Swal from "sweetalert2";
import OwnService from "../../../service/OwnService";

export default class FirstForm extends Component {
    constructor(params) {
        super(params);
        this.name = React.createRef(null);
        this.birthdate = React.createRef(null);
        this.description = React.createRef(null);
        this.contact = React.createRef(null);
    }

    componentDidMount() {
        
        this.name.current.value = 'Admin'
        this.birthdate.current.value = '2015-01-12'
        this.description.current.value = 'Here is my portfolio'
        this.contact.current.value = 'admin@gmail.com'
    }
    sendProject = () => {
        let data = {
            name : this.name.current.value,
            birthdate : this.birthdate.current.value,
            description : this.description.current.value,
            contact : this.contact.current.value
        }
        OwnService.sendData(OwnService.url+'api/developers',data,
            data => {
                Swal.fire({
                    title:'Bravo',
                    text : 'You added a staff',
                    icon : 'success'
                }).then(() => {
                    this.props.conf(data);
                    this.props.next();
                })
            }
        )
    }
    render() {
        return (
             <>
                    <strong className="">Name</strong>
                    <br/>
                    <input ref={this.name} className="form-control w-100 mb-3" placeholder="Title of your project" type="text"/>
                    
                    <strong className="">Birthday</strong>
                    <br/>
                    <input ref={this.birthdate} className="form-control w-100 mb-3" placeholder="Title of your project" type="date"/>
                    <strong>Contact</strong>
                    <br/>
                    <input ref={this.contact} className="form-control w-100 mb-3" placeholder="http://www.lien.com" type="tel"/>
                    <strong>Describe</strong>
                    <br/>
                    <textarea ref={this.description} className="form-control w-100 mb-3" placeholder="Talk about the developper" type="text"></textarea>
                    <button className="btn w-100 hpreview" 
                        onClick={
                            () => {
                                this.sendProject();
                            }
                        }
                    >Commit</button>
             </>
        );
    }
}