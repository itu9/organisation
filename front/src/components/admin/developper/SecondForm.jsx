import React, { Component } from "react";
import Swal from "sweetalert2";
import HMultSelect from "../../../service/HMultSelect";
import OwnService from "../../../service/OwnService";

export default class SecondForm extends Component {
    constructor(params) {
        super(params);
        this.state ={
            spec : [],
            tech : [],
            specAdded : [],
            techAdded : []
        }
    }

    componentDidMount () {
        OwnService.getData(OwnService.url+'api/specializations',
            (data) => {
                this.setState({
                    spec : data
                })
            }
        )
        OwnService.getData(OwnService.url+'api/technologies',
            (data) => {
                this.setState({
                    tech : data
                })
            }
        )
    }

    addTech = async (id) => {
        await OwnService.sendData(OwnService.url+'api/developers/'+this.props.id+'/tech/'+id,
            (data) => {
                console.log("ok");
            }
        )
    }

    sendTech = async () => {
        for (let i = 0; i < this.state.techAdded.length; i++) {
            await this.addTech(this.state.techAdded[i])
        }
    }

    addSpec = async (id) => {
        await OwnService.sendData(OwnService.url+'api/developers/'+this.props.id+'/spec/'+id,
            (data) => {
                console.log("ok");
            }
        )
    }

    sendSpec = async () => {
        for (let i = 0; i < this.state.specAdded.length; i++) {
            await this.addSpec(this.state.specAdded[i])
        }
    }

    send = async () => {
        await this.sendTech()
        await this.sendSpec();
    }

    sendProject = () => {
       
        this.send().then(() => {
            Swal.fire({
                icon : 'success',
                title : 'Bravo',
                text : 'You did it'
            }).then(() => {
                window.location.reload()
            })
        }).catch((err) => {
            console.log(err)
            Swal.fire({
                icon : 'error',
                title : 'Sorry',
                text : 'A problem was occured'
            })
        });
    }

    setSpec = (spec) => {
        this.setState({
            specAdded : spec
        })
    }
    setTech = (tech) => {
        this.setState({
            techAdded : tech
        })
    }
    render() {
        return (
             <>
                    <strong className="">Specialization</strong>
                    <br/>
                    <HMultSelect data={this.state.spec} keyVal="title" label="" result={this.state.specAdded} onChange={this.setSpec}></HMultSelect>
                    <strong className="">Technology</strong>
                    <br/>
                    <HMultSelect data={this.state.tech} keyVal="name" label="" result={this.state.techAdded} onChange={this.setTech}></HMultSelect>
                    <button className="btn w-100 hpreview" 
                        onClick={
                            () => {
                                this.sendProject();
                            }
                        }
                    >Commit</button>
             </>
        );
    }
}