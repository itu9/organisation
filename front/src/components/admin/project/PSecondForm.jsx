import { Component } from "react";
import Swal from "sweetalert2";
import HMultSelect from "../../../service/HMultSelect";
import OwnService from "../../../service/OwnService";

export default class PSecondForm extends Component {
    constructor(params) {
        super(params);
        this.state ={
            tech : [],
            techAdded : []
        }
    }
    componentDidMount () {
        OwnService.getData(OwnService.url+'api/technologies',
            (data) => {
                this.setState({
                    tech : data
                })
            }
        )
    }
    addTech = async (id) => {
        await OwnService.sendData(OwnService.url+'api/projects/'+this.props.id+'/tech/'+id,
            (data) => {
                console.log("ok");
            }
        )
    }

    sendTech = async () => {
        for (let i = 0; i < this.state.techAdded.length; i++) {
            await this.addTech(this.state.techAdded[i])
        }
    }
    send = async () => {
        await this.sendTech();
    }

    sendProject = () => {
       
        this.send().then(() => {
            Swal.fire({
                icon : 'success',
                title : 'Bravo',
                text : 'You did it'
            }).then(() => {
                window.location.reload()
            })
        }).catch((err) => {
            console.log(err)
            Swal.fire({
                icon : 'error',
                title : 'Sorry',
                text : 'A problem was occured'
            })
        });
    }
    setTech = (tech) => {
        this.setState({
            techAdded : tech
        })
    }

    render() {
        return (
             <>
             <strong className="">Technology</strong>
                    <br/>
                    <HMultSelect data={this.state.tech} keyVal="name" label="" result={this.state.techAdded} onChange={this.setTech}></HMultSelect>
                    
                    <button className="btn w-100 hpreview" 
                        onClick={
                            () => {
                                this.sendProject();
                            }
                        }
                    >Commit</button>
             </>
        );
    }
}