import React, { Component } from "react";
import {  Step, StepContent, StepLabel, Stepper } from "@mui/material";
import { Box } from "@mui/system";
import PFirstForm from "./PFirstForm";
import PSecondForm from "./PSecondForm";

export default class ProjectForm extends Component {
    state = {
        page : 0,
        id : -1
    }
    
    next = () => {
        this.setState({
            page : this.state.page+1
        })
    }

    config = (data) => {
        this.setState({
            id : data.id
        })
    }
    render() {
        return (
             
             <Box sx={{marginLeft: '5%',marginRight:'5%',marginTop : '-7%' }}>
                 <Stepper activeStep={this.state.page} orientation="vertical" >
                     <Step key={0}>
                         <StepLabel 
                         >
                         <h3 className={this.state.page === 0 ? "hnow" : ""}>Add/Update project</h3>
                         </StepLabel>
                         <StepContent>
                         <PFirstForm next={this.next} config={this.config}></PFirstForm>
                         </StepContent>
                     </Step>
                     <Step key={1}>
                     <StepLabel
                         >
                         <h3 className={this.state.page === 1 ? "hnow" : ""}>Additionnal information </h3>
                         </StepLabel>
                         <StepContent>
                            <PSecondForm id={this.state.id}></PSecondForm>
                         </StepContent>
                     </Step>
                 </Stepper>
             </Box>
        );
    }
}