import { Component } from "react";
import './OneProject.css'
import '../../client/offer/Offer.css'
import HCard from "../../../service/HCard";

export default class OneProject extends Component {
    state = {
        cls : 'truncate'
    }

    flip = () => {
        this.setState({
            cls : (this.state.cls === '') ? 'truncate' : ''
        })
    }

    render() {
        return (
             <>
                <div className="col-lg-4 mb-5">
                    <HCard>
                    <h2>{this.props.project.title}</h2>
                    <p className={this.state.cls} 
                        onClick={
                            () => {
                                this.flip()
                            }
                        }
                    >{this.props.project.description}</p>
                    <button className="btn w-100 mb-3"><strong> Preview</strong></button>
                    <button className="btn w-100 contact mb-3" onClick={() => {
                        this.props.update(this.props.project)
                    }}><strong> Update</strong></button>
                    <button className="btn w-100 mb-3 hremove"><strong> Remove</strong></button>
                    </HCard>
                </div>
             </>
        );
    }
}