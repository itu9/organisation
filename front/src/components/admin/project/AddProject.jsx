import React, { Component } from "react";
import OwnService from "../../../service/OwnService";
import HMiddle from "../../client/services/HMiddle";
import OneProject from "./OneProject";
import ProjectForm from './ProjectForm'
// import Modal from 'react-modal';
import './AddProject.css'
import HModal from "../../../service/HModal";
import { Pagination } from "@mui/material";

export default class AddProject extends Component {
    constructor(params) {
        super(params);
        this.state = {
            addProd : false,
            projects : [],
            id : 0,
            page : 1
        }
    }

    componentDidMount() {
        this.init(1)
    }

    init = (page) => {
        OwnService.getData(OwnService.url+'api/projects?page='+page,
            (data) => {
                this.setState({
                    projects : data.data,
                    page : data.last_page
                })
            }
        )
    }

    update = (project) => {
        this.title.current.value = project.title;
        this.link.current.value = project.project_link;
        this.desc.current.value = project.describe;
        this.setState({
            id : project.id
        })
    }

    onChange = (event,value) => {
        this.init(value)
    }

    render() {
        return (
            <>
                <HMiddle title="Project">
                    <div className="row">
                        
                        <div className="col-lg-12">
                            <h3>List project</h3>
                            <div className="row mb-5">
                                <div className="col-lg-4">
                                    <button className="btn w-100 contact"
                                        onClick={
                                            () => {
                                                this.setState({
                                                    addProd : true
                                                })
                                            }
                                        }
                                    >Add Project</button>
                                </div>
                                <div></div>
                            </div>
                            <div className="row">
                                {
                                    this.state.projects.map(pr => (
                                        <OneProject key={pr.id} project={pr} update={this.update} ></OneProject>
                                    ))
                                }
                            </div>
                            <div className="row">
                                <div className="col-lg-6"></div>
                                <div className="col-lg-6">
                                    <Pagination count={this.state.page} size="large" color="primary" onChange={this.onChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                    </HMiddle>
                        <HModal isOpen={this.state.addProd}
                            onClose={() => {
                                this.setState({
                                    addProd : false
                                })
                            }}
                        >
                            <ProjectForm></ProjectForm>
                        </HModal>
            </>
        );
    }
}