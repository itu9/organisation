import React,{Component } from 'react'

import Swal from "sweetalert2";
import OwnService from "../../../service/OwnService";


export default class PFirstForm extends Component {
    constructor(params) {
        super(params);
        this.title = React.createRef(null);
        this.link = React.createRef(null);
        this.tech = React.createRef(null);
        this.desc = React.createRef(null);
        this.state = {
            categories : [],
            id : 0
        }
    }

    componentDidMount() {
        this.title.current.value = 'Project';
        this.link.current.value = 'http://link.google.com';
        this.desc.current.value = 'My description of this project is just for the test.';
        OwnService.getData(OwnService.url+'api/categories',
            (data) => {
                this.setState({
                    categories : data
                })
            }
        )
    }

    sendProject = () => {
        let data = {
            title : this.title.current.value,
            project_link : this.link.current.value,
            categoryid : this.tech.current.value,
            description : this.desc.current.value,
            conception_link : 'a',
            source_link : 'a',
            picture : 'a'
        }
        console.log(JSON.stringify(data));
        OwnService.sendData(OwnService.url + 'api/projects',data,
            (data) => {
                Swal.fire({
                    title : 'Bravo',
                    text : 'Project added successfully',
                    icon : 'success',
                }).then(() => {
                    this.props.config(data);
                    this.props.next()
                })
            }
        )
    }
    render() {
        return (
            <div className="col-lg-12">
            <strong>Title</strong>
            <br/>
            <input ref={this.title} className="form-control w-100 mb-3" placeholder="Title of your project" type="text"/>
            <strong>Link</strong>
            <br/>
            <input ref={this.link} className="form-control w-100 mb-3" placeholder="http://www.lien.com" type="text"/>
            <strong>Category</strong>
            <br/>
            <select ref={this.tech} className="form-select" >
                {
                    this.state.categories.map(ca => (
                        <option key={ca.id} value={ca.id}>{ca.name}</option>
                    ))
                }
            </select>
            <br/>
            <strong>About</strong>
            <br/>
            <textarea ref={this.desc} className="form-control w-100 mb-3" placeholder="Talk about your project" type="text"></textarea>
            <button className="btn w-100 hpreview" 
                onClick={
                    () => {
                        this.sendProject();
                    }
                }
            >Commit</button>
            </div>
        );
    }

}