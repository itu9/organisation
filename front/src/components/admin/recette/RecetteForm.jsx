import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralForm from "/src/components/utils/GeneralForm";
import React from "react";

export default class RecetteForm extends GeneralForm {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.date = React.createRef(null);
        this.urlSend = "/recette";
        this.urlUtils = "/recette/utils";
        this.afterValidation = "";
        this.state = {
            ings : []
        }
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
        titre: this.prepare(this.titre),
        date: new Date(),
        };
        console.log(data);
        // this.validate(data);
    };

    render() {
        return (
        <HContainer>
            <h1 className="text-dark">Formulaire</h1>
            <div className="row mt-3">
            <div className="col-8">
                <Border>
                <h2 className="text-dark mt-3">Information de la recette</h2>
                <h3 className="text-info mt-3">Titre</h3>
                <input
                    type="text"
                    className="form-control"
                    ref={this.titre}
                ></input>
                <h2 className="text-dark mt-3">Ingredients</h2>
                <div className="row mt-3">
                    <div className="col-5">
                    <h3  className="text-info mt-3">Ingredient</h3>
                    </div>
                    <div className="col-5">
                    <h3  className="text-info mt-3">Quantité</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-5">
                    <select className="form-control" >
                    <option value=""></option>
                    {this.state.utils !== undefined && this.state.utils !== null ? (
                        this.state.utils.ingredients.map((ing, index) => (
                        <option value={ing.id}>{ing.titre}</option>
                        ))
                    ) : (
                        <></>
                    )}
                    </select>
                    </div>
                    <div className="col-5">
                        <input className="form-control"></input>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-danger">
                        <i className="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
                <button className="btn btn-success  mt-3"><i className="fas fa-plus"></i></button>
                <button
                    onClick={this.validateData}
                    className="mt-3 btn btn-success btn-block"
                >
                    Valider
                </button>
                </Border>
            </div>
            </div>
        </HContainer>
        );
    }
}
