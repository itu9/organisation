import { useParams } from "react-router-dom";
import RecetteUpdate from "./RecetteUpdate";

export default function RecetteUpdateGetter() {
    const {id} = useParams();
    return (
        <RecetteUpdate id={id}>
        </RecetteUpdate>
    );
}