import General from "/src/components/utils/General";
import React from "react";

export default class Ingredient extends General {
    constructor(params) {
        super(params);
        this.ingredient = React.createRef(null);
    }
}
