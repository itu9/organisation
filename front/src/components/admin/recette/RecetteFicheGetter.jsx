import { useParams } from "react-router-dom";
import RecetteInfo from "./RecetteInfo";

export default function RecetteFicheGetter() {
    const {id} = useParams();
    return (
        <RecetteInfo id={id}>
        </RecetteInfo>
    );
}