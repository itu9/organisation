import General from "../../utils/General";
import React from 'react';

export default class AchatDetForm extends General {

    constructor(params) {
        super(params);
        this.produit = React.createRef(null);
        this.pu = React.createRef(null);
        this.pvente = React.createRef(null);
        this.quantite = React.createRef(null);
        this.remise = React.createRef(null);
    }

    componentDidMount() {
        this.pu.current.value = 0;
        this.pvente.current.value = 0;
        this.quantite.current.value = 0;
        this.remise.current.value = 0;
    }

    getPu = () => this.pu.current.value

    getPvente = () => this.pvente.current.value

    getRemise = () => this.remise.current.value

    getQuatite = () => this.quantite.current.value

    reduce = (value,remise) => value - ((value * remise)/100.0)

    eval = () => this.reduce(this.getPu()*this.getQuatite(),this.getRemise())

    recolte = () => {
        let data = this.eval();
        console.log(parseInt(data,10));
        this.props.change(this.props.id,parseInt(data,10))
    }
    
    render() {
        return (
             <>
                <div className="row mt-3">
                    <div className="col-3">
                        <select className="form-control" ref={this.produit}>
                            {
                                this.props.product.map(prod => (
                                    <option value={prod.id}>{prod.titre}</option>
                                ))
                            }
                        </select>
                    </div>
                    <div className="col-2">
                        <input className="form-control" type="number" ref={this.pu} onChange={this.recolte} />
                    </div>
                    <div className="col-2">
                        <input className="form-control" type="number" ref={this.pvente} />
                    </div>
                    <div className="col-2">
                        <input className="form-control" type="number" ref={this.quantite} onChange={this.recolte}  />
                    </div>
                    <div className="col-2">
                        <input className="form-control" type="number" ref={this.remise} onChange={this.recolte}  />
                    </div>
                    <div className="col-1" onClick={
                        () => {
                            this.props.remove();
                        }
                    }>
                        <h3 className="text-danger">
                        <i className="fas fa-times"></i>
                        </h3>
                    </div>
                </div>
             </>
        );
    }
}