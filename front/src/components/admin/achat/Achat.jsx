import HContainer from "../../client/nav/HContainer";
import Border from "../../utils/Border";
import HButton from "../../utils/HButton";
import General from "../../utils/General";
import AchatDetForm from "./AchatDetForm";
import Swal from "sweetalert2";

export default class Achat extends General {
    
    componentDidMount() {
        this.setState({
            link : true,
            total : 0,
            liste : [],
            produit : [
                {
                    id : 1,
                    titre : "ASUS ROG"
                },
                {
                    id : 2,
                    titre : "HP"
                }
            ]
        },
            this.addDet
        )
    }

    addValue = (data) => {
        this.setState({
            liste : [
                ...this.state.liste,
                data
            ]
        })
    }

    change = (id,value) => {
        let data = this.state.liste.filter(li => {
            console.log(value,li.value);
            if (li.id === id) 
                li.value = value
            return li
        })
        console.log(data);
        this.setState({
            liste : data
        },
            this.evalTot
        )
    }

    evalTot = () => {
        if (this.state.link) {
            let res = 0.;
            for( let i = 0; i < this.state.liste.length; i++)
                res += this.state.liste[i].value
            this.setState({
                total : res
            })
        }
    }

    addDet = () => {
        let id = this.getNextId();
        let data = {
            id : id,
            value : 0
        }
        this.addValue(data)
        this.addTemplate(<AchatDetForm id={id} change={this.change} product={this.state.produit} remove ={
            () => {
                this.removeTemplate(id, 
                    () => {
                        let liste = this.state.liste.filter(l => l.id !== id)
                        this.setState({
                            liste : liste
                        },
                            this.evalTot
                        )
                    }
                )
            }
        }/>,id)
    }

    changeTot = (data) => {
        if (!this.state.link) {
            this.setState({total : data})
        } else {
            Swal.fire({
                title : 'Oups',
                text : `Enlever d'abord la liaison`,
                icon : 'error'
            })
        }
    }
    
    render() {
        return (
            <HContainer>
                <div className="col-12">
                    <Border>
                        <h2 className="text-info">Achat</h2>
                        <div className="row">
                            <div className="col-12 mt-3">
                                <h4 className="text-dark">Date d'achat</h4>
                                <input className="form-control" type="date"></input>
                            </div>
                            <div className="col-12 mt-3">
                                <h4 className="text-dark">Fournisseur</h4>
                                <select className="form-control">
                                    <option>Henri</option>
                                </select>
                            </div>
                            

                            <div className="col-12 mt-3">
                                <div className="row mt-3 text-info">
                                        <div className="col-3"><strong>Produit</strong></div>
                                        <div className="col-2"><strong>Prix</strong></div>
                                        <div className="col-2"><strong>PVente</strong></div>
                                        <div className="col-2"><strong>Quantite</strong></div>
                                        <div className="col-2"><strong>Remise</strong></div>
                                        <div className="col-1"><strong></strong></div>
                                </div>
                                {
                                    this.state.template.map(temp => (
                                        temp.data
                                    ))
                                }
                                <div className="row mt-3 text-info">
                                    <div className="col-12" onClick={
                                        () => {
                                            this.addDet();
                                        }
                                    }>
                                        <h3 className="text-success">
                                            <i className="fas fa-plus"></i>
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            
                            <div className="col-12 mt-3">
                                <div className="row">
                                    <div className="col-7"></div>
                                    <div className="col-2">
                                        <h4 className="text-dark">
                                            Total&nbsp;&nbsp;&nbsp;
                                            {
                                                this.state.link ? 
                                                    <i className="fas fa-link text-info" onClick={() => {
                                                        this.setState({
                                                            link : false
                                                        })
                                                    }}></i> : 
                                                    <i className="fas fa-unlink text-danger" onClick={() => {
                                                        this.setState({
                                                            link : true
                                                        },
                                                            this.evalTot
                                                        )
                                                    }}></i>
                                            }
                                            
                                        </h4>
                                    </div>
                                    <div className="col-3">
                                        <input className="form-control" type="number" value={this.state.total} onChange={e => this.changeTot( parseInt( e.target.value,10))}></input>
                                    </div>
                                </div>
                            </div>

                            <div className="col-12 mt-3">
                                <HButton title={<>Ok</>} color="orange" tColor= "info" />
                            </div>
                        </div>
                    </Border>
                </div>
            </HContainer>
        );
    }
}