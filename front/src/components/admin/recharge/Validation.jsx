import General from "../../utils/General";
import HContainer from "../../client/nav/HContainer";
import Border from "../../utils/Border";
import OwnService from "../../../service/OwnService";
import React from "react";

export default class Validation extends General {
    componentDidMount() {
        this.getListe(this.json_server+"/rechargement", (data) => {
            this.setState({
                recharges : data.data
            })
        },
        data => {})
        this.checked = React.createRef(null);
    }

    valider = () => {
        let data = this.state.validation;
        console.log(data);
        // TODO auto-generated
    }



    render() {
        return (
            <HContainer>
                <div className="col-12">
                    <h1 className="text-info mb-5">Les rechargements en attente de validation</h1>
                    <div className="row">
                        <div className="col-12">
                            <Border>
                                <table className="table"> 
                                    <thead className="bg-orange text-white">
                                        <tr>
                                            <th>Nom Client</th>
                                            <th>Prénom Client</th>
                                            <th>Matricule</th>
                                            <th>Montant</th>
                                            <th>Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.recharges !== null && this.state.recharges !== undefined? this.state.recharges.map(recharge => (
                                                <tr>
                                                    <td>{recharge.client.nom}</td>
                                                    <td>{recharge.client.prenom}</td>
                                                    <td>{recharge.client.matriculation}</td>
                                                    <td className="text-danger">{OwnService.format( recharge.montant)}</td>
                                                    <td className="text-info hvRemove">{recharge.date}</td>
                                                    <td className="text-info hvRemove">
                                                        <input type="checkbox" onClick={() => {
                                                            this.addValidation(recharge)
                                                        }} />
                                                    </td>
                                                </tr>
                                            )) : <></>
                                        }
                                    </tbody>
                                </table>
                                <div className="row mt-5">
                                    <div className="col-4">
                                        <button className="btn btn-success btn-block" onClick={this.valider}>Valider</button>
                                    </div>
                                </div>
                            </Border>
                        </div>
                    </div>
                </div>
            </HContainer>
        );
    }
}