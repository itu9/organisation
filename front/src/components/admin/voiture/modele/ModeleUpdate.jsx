import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import GeneralUpdate from "/src/components/utils/GeneralUpdate";
import React from "react";

export default class ModeleUpdate extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.typeid = React.createRef(null);
    this.marqueid = React.createRef(null);
    this.id = React.createRef(null);
    this.urlSend = "/modele";
    this.urlUtils = "/modele/utils";
    this.afterValidation = "/modele";
  }
  componentDidMount() {
    this.initUpdate();
  }
  actionUpdate = () => {
    this.typeid.current.value = this.state.oneValue.typeid.id;
    this.marqueid.current.value = this.state.oneValue.marqueid.id;
    this.id.current.value = this.state.oneValue.id;
  };
  validateData = () => {
    let data = {
      typeid: this.checkRefNull(
        this.typeid,
        { id: this.prepare(this.typeid) },
        null
      ),
      marqueid: this.checkRefNull(
        this.marqueid,
        { id: this.prepare(this.marqueid) },
        null
      ),
      id: this.prepare(this.id),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <input type="hidden" ref={this.id} value={this.props.id} />
              <h3 className="text-info mt-3">Type</h3>
              <select className="form-control" ref={this.typeid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.typeid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Marque</h3>
              <select className="form-control" ref={this.marqueid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.marqueid.map((data, index) => (
                    <option value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
