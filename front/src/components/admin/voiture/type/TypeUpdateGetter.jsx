import { useParams } from "react-router-dom";
import TypeUpdate from "./TypeUpdate";

export default function TypeUpdateGetter() {
    const {id} = useParams();
    return (
        <TypeUpdate id={id}>
        </TypeUpdate>
    );
}