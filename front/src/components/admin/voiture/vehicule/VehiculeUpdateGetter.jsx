import { useParams } from "react-router-dom";
import VehiculeUpdate from "./VehiculeUpdate";

export default function VehiculeUpdateGetter() {
    const {id} = useParams();
    return (
        <VehiculeUpdate id={id}>
        </VehiculeUpdate>
    );
}