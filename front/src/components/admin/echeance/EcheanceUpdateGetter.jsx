import { useParams } from "react-router-dom";
import EcheanceUpdate from "./EcheanceUpdate";

export default function EcheanceUpdateGetter() {
    const {id} = useParams();
    return (
        <EcheanceUpdate id={id}>
        </EcheanceUpdate>
    );
}