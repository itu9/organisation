import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import ListePage from "@/components/utils/ListePage";
import GeneralListe from "@/components/utils/GeneralListe";
import React from "react";

export default class SpectacleListe extends GeneralListe {
    constructor(params) {
        super(params);
        this.lieuid = React.createRef(null);this.prixlieuhmin = React.createRef(null);this.prixlieuhmax = React.createRef(null);this.datehmin = React.createRef(null);this.datehmax = React.createRef(null);this.hourhmin = React.createRef(null);this.hourhmax = React.createRef(null);this.etathmin = React.createRef(null);this.etathmax = React.createRef(null);
        this.keys = React.createRef(null);
        this.urlData = "/spectacle/filter";
        this.urlUtils = "/spectacle/utils";
        this.baseUrl = "/spectacle"
    }
    componentDidMount() {
        this.init();
    }
    prepareDataListe = () => {
        let data = {
            keys : this.prepare(this.keys)
            ,lieuid : this.checkRefNull(this.lieuid,{titre:this.prepare(this.lieuid)},null),prixlieuhmin : this.prepare(this.prixlieuhmin),prixlieuhmax : this.prepare(this.prixlieuhmax),datehmin : this.prepare(this.datehmin),datehmax : this.prepare(this.datehmax),hourhmin : this.prepare(this.hourhmin),hourhmax : this.prepare(this.hourhmax),etathmin : this.prepare(this.etathmin),etathmax : this.prepare(this.etathmax)
        }
        return data;
    }
    findData = () => {
        this.search(this.prepareDataListe());
    }
    onChange = (event,page) => {
        let data = this.prepareDataListe();
        data = {
        ...data,
        page : page
        }
        this.search(data);
    }

    toPdf = ()=> {
        let data = this.prepareDataListe();
        data = {
            ...data,
            page : -1
        }
        let ans = {
            ...data,
            pdf : {
                titre : "Les spectacle",
                fields : [
                    {
    field : "titre",
    titre : "Titre "
},{
    field : "lieuid",
    titre : "Lieu"
},{
    field : "date",
    titre : "Date"
},{
    field : "hour",
    titre : "Hour"
},
                ]
            }
        }
        // console.log('ans',ans);
        this.pdf(ans);

    }
    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                    <div className="col-12">
                        <Border>
                            <ListePage
                                title="Les spectacle"
                                count={this.state.count}
                                onChange={this.onChange}
                                search = {
                                    <>
                                        <div className="col-6">
                                            <h3 className="text-info mt-3">Recherche</h3>
                                            <input className="form-control" type="text" placeholder="recherche par mot clé" ref={this.keys}/>
                                            <h3 className="text-info mt-3" >Lieu</h3>
<select className="form-control" ref={this.lieuid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.lieuid.map((data,index) => (
            <option key={index} value={data.titre} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Prixlieu</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.prixlieuhmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.prixlieuhmax} />
    </div>
</div><h3 className="text-info mt-3">Date</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.datehmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.datehmax} />
    </div>
</div><h3 className="text-info mt-3">Hour</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.hourhmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.hourhmax} />
    </div>
</div><h3 className="text-info mt-3">Etat</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.etathmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.etathmax} />
    </div>
</div>
                                            <button className="btn btn-block btn-info mt-3" onClick={this.findData}>Rechercher</button>
                                        </div>
                                        <div className="col-12 mt-5">
                                            <div className="row">
                                                <div className="col-6"></div>
                                                <div className="col-6">
                                                    <div className="row">
                                                        <div className="col-2"></div>
                                                        <div className="col-4">
                                                        <div className="btn-list w-100">
                                                            <div className="btn-group">
                                                                <button type="button" className="btn btn-info dropdown-toggle w-100"
                                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Exporter
                                                                </button>
                                                                <div className="dropdown-menu w-100">
                                                                    <a className="dropdown-item " href="#a" onClick={this.toPdf}><i className="fas fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;&nbsp;PDF </a>
                                                                    <a className="dropdown-item " href="#a"><i className="fas fa-file-excel"></i>&nbsp;&nbsp;&nbsp;&nbsp;CSV</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <div className="col-6">
                                                            <button className="btn btn-block btn-success"
                                                                onClick={() => {
                                                                    window.location.replace("/spectacle/form");
                                                                }}
                                                            >Ajouter un nouveau</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                                head = {
                                    <tr><td>Titre </td><td>Lieu</td><td>Date</td><td>Hour</td><td></td></tr>
                                }
                            >
                            {this.state.data !== undefined && this.state.data !== null ?this.state.data.map((data,index) =>(<tr><td>{data.titre}</td><td>{data.lieuid.titre}</td><td>{data.date}</td><td>{data.hour}</td> <td>
    <button className="btn" onClick={()=> {
        window.location.replace("/spectacle/update/"+data.id)
    }}>
        <i className="fas fa-pencil-alt text-warning"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        this.delete(data)
    }}>
        <i className="fas fa-trash-alt text-danger"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        window.location.replace("/spectacle/"+data.id)
    }}>
        <i className="fas fa-plus text-info"></i>
    </button>
</td></tr>)):<></>}
                            </ListePage>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}