import HContainer from "@/components/client/nav/HContainer";
import {  Chart, registerables  } from "chart.js";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../../service/OwnService.jsx";
import React from "react";
import {Doughnut} from "react-chartjs-2";
Chart.register(...registerables)

export default class SpectacleInfo extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.data = React.createRef(null);
        this.urlSend = "/spectacle";
        this.urlUtils = "/spectacle/utils";
        this.data.current = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label: 'Pomme de terre',
                    data: [65, 59, 80, 81, 56, 55, 40],
                    fill: false,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgba(255, 99, 132, 0.2)',
                }
            ],
        }
    }
    getRandomColor = () => {
        var letters = "0123456789ABCDEF";
        var color = "#";
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    actionUpdate = () => {
        let data = this.state.oneValue.depenses.map(value => value.value)
        let labels = this.state.oneValue.depenses.map(value => value.label)
        let dataField = {
            labels: labels,
            datasets: [
                {
                    label:"Répartition des dépenses",
                    data: data,
                    fill: false,
                    borderWidth: 2,
                    backgroundColor:  [
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                        this.getRandomColor(),
                    ]
                }
            ],
        };
        console.log(dataField)
        this.data.current = dataField;
    }
    componentDidMount() {
        this.initUpdate();
    }

    dupliquer = () => {
        window.location.replace("/bis/"+this.props.id)
    }
    render() {
        return (
            <HContainer>

                <h1 className="text-dark">Fiche de details de spectacle</h1>
                <div className="mt-5">
                    <Border>
                        <div className="row">
                            <div className="col-3">
                                <h2 className="text-info">Alternatives</h2>
                                <div className="row mt-3 mb-3">
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-warning btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/spectacle/update/" + this.props.id)
                                                    }
                                                }
                                        >Modifier
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-info btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/spectacle")
                                                    }
                                                }
                                        >Liste
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-success btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/spectacle/form/")
                                                    }
                                                }
                                        >Ajouter
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-danger btn-block"
                                                onClick={
                                                    () => {
                                                        this.delete();
                                                    }
                                                }
                                        >Suprimer
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-success btn-block"
                                                onClick={
                                                    () => {
                                                        this.dupliquer();
                                                    }
                                                }
                                        >Dupliquer
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-8">
                                <h2 className="text-info mb-3">Information</h2>
                                {
                                    this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                        <>
                                            <h3 className="text-info">Titre du spectacle </h3>
                                            <p className="text-dark">{this.state.oneValue.titre}</p><h3
                                            className="text-info">Lieu</h3>
                                            <p className="text-dark">{this.state.oneValue.lieuid.titre}</p><h3
                                            className="text-info">Date</h3>
                                            <p className="text-dark">{this.state.oneValue.date}</p><h3
                                            className="text-info">Heur</h3>
                                            <p className="text-dark">{this.state.oneValue.hour}</p>
                                            <h3 className="text-info">Depense</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.total)}</p>
                                            <h3 className="text-info">Recette reel</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.realrecette)}</p>
                                            <h3 className="text-info">Benefice brut</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.benefbrut)}</p>
                                            <h3 className="text-info">Taxe</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.taxe)}</p>
                                            <h3 className="text-info">Benefice net</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.benefnet)}</p>
                                        </>
                                        : <></>
                                }
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-3">
                            </div>
                            <div className={"col-9"}>
                            {
                                this.data !== undefined && this.data !== null ?
                                    <Doughnut  data={this.data.current} /> : <></>
                            }
                            </div>
                        </div>
                    </Border>
                </div>
            </HContainer>
        );
    }
}
