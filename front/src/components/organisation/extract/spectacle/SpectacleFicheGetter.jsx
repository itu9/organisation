import { useParams } from "react-router-dom";
import SpectacleInfo from "./SpectacleInfo";
export default function SpectacleFicheGetter() {
    const {id} = useParams();
    return (
        <SpectacleInfo id={id}>
        </SpectacleInfo>
    );
}