import { useParams } from "react-router-dom";

import SpectacleUpdate from "./SpectacleUpdate";

export default function SpectacleUpdateGetter() {
    const {id} = useParams();
    return (
        <SpectacleUpdate id={id}>
        </SpectacleUpdate>
    );
}