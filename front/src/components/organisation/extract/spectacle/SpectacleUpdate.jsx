import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class SpectacleUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);this.lieuid = React.createRef(null);this.prixlieu = React.createRef(null);this.total = React.createRef(null);this.recette = React.createRef(null);this.benefit = React.createRef(null);this.realrecette = React.createRef(null);this.date = React.createRef(null);this.hour = React.createRef(null);this.etat = React.createRef(null);this.devisartistes = React.createRef(null);this.devisautres = React.createRef(null);this.devisfixes = React.createRef(null);this.devisbillets = React.createRef(null);
        this.urlSend = "/spectacle";
        this.urlUtils = "/spectacle/utils";
        this.afterValidation = "/spectacle";
        this.id = React.createRef(null);
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.titre.current.value = this.state.oneValue.titre;this.lieuid.current.value = this.state.oneValue.lieuid.titre;this.prixlieu.current.value = this.state.oneValue.prixlieu;this.total.current.value = this.state.oneValue.total;this.recette.current.value = this.state.oneValue.recette;this.benefit.current.value = this.state.oneValue.benefit;this.realrecette.current.value = this.state.oneValue.realrecette;this.date.current.value = this.state.oneValue.date;this.hour.current.value = this.state.oneValue.hour;this.etat.current.value = this.state.oneValue.etat;this.devisartistes.current.value = this.state.oneValue.devisartistes;this.devisautres.current.value = this.state.oneValue.devisautres;this.devisfixes.current.value = this.state.oneValue.devisfixes;this.devisbillets.current.value = this.state.oneValue.devisbillets;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            titre : this.prepare(this.titre),lieuid : this.checkRefNull(this.lieuid,{titre:this.prepare(this.lieuid)},null),prixlieu : this.prepare(this.prixlieu),total : this.prepare(this.total),recette : this.prepare(this.recette),benefit : this.prepare(this.benefit),realrecette : this.prepare(this.realrecette),date : this.prepare(this.date),hour : this.prepare(this.hour),etat : this.prepare(this.etat),devisartistes : this.prepare(this.devisartistes),devisautres : this.prepare(this.devisautres),devisfixes : this.prepare(this.devisfixes),devisbillets : this.prepare(this.devisbillets)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire de modification</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Titre </h3>
<input type="text" className="form-control" ref={this.titre} ></input><h3 className="text-info mt-3" >Lieu</h3>
<select className="form-control" ref={this.lieuid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.lieuid.map((data,index) => (
            <option key={index} value={data.titre} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Prixlieu</h3>
<input type="text" className="form-control" ref={this.prixlieu} ></input><h3 className="text-info mt-3">Total</h3>
<input type="text" className="form-control" ref={this.total} ></input><h3 className="text-info mt-3">Recette</h3>
<input type="text" className="form-control" ref={this.recette} ></input><h3 className="text-info mt-3">Benefit</h3>
<input type="text" className="form-control" ref={this.benefit} ></input><h3 className="text-info mt-3">Realrecette</h3>
<input type="text" className="form-control" ref={this.realrecette} ></input><h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Hour</h3>
<input type="text" className="form-control" ref={this.hour} ></input><h3 className="text-info mt-3">Etat</h3>
<input type="text" className="form-control" ref={this.etat} ></input><h3 className="text-info mt-3">Devisartistes</h3>
<input type="text" className="form-control" ref={this.devisartistes} ></input><h3 className="text-info mt-3">Devisautres</h3>
<input type="text" className="form-control" ref={this.devisautres} ></input><h3 className="text-info mt-3">Devisfixes</h3>
<input type="text" className="form-control" ref={this.devisfixes} ></input><h3 className="text-info mt-3">Devisbillets</h3>
<input type="text" className="form-control" ref={this.devisbillets} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}