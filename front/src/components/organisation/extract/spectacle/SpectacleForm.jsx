import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class SpectacleForm extends GeneralForm {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.lieuid = React.createRef(null);
        this.prixlieu = React.createRef(null);
        this.total = React.createRef(null);
        this.recette = React.createRef(null);
        this.benefit = React.createRef(null);
        this.realrecette = React.createRef(null);
        this.date = React.createRef(null);
        this.hour = React.createRef(null);
        this.etat = React.createRef(null);
        this.devisartistes = React.createRef(null);
        this.devisautres = React.createRef(null);
        this.devisfixes = React.createRef(null);
        this.devisbillets = React.createRef(null);
        this.urlSend = "/spectacle";
        this.urlUtils = "/spectacle/utils";
        this.afterValidation = "/spectacle";
    }

    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            titre: this.prepare(this.titre),
            lieuid: this.checkRefNull(this.lieuid, {titre: this.prepare(this.lieuid)}, null),
            prixlieu: this.prepare(this.prixlieu),
            total: this.prepare(this.total),
            recette: this.prepare(this.recette),
            benefit: this.prepare(this.benefit),
            realrecette: this.prepare(this.realrecette),
            date: this.prepare(this.date),
            hour: this.prepare(this.hour),
            etat: this.prepare(this.etat),
            devisartistes: this.prepare(this.devisartistes),
            devisautres: this.prepare(this.devisautres),
            devisfixes: this.prepare(this.devisfixes),
            devisbillets: this.prepare(this.devisbillets)
        }
        this.validate(data)
    }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Titre </h3>
                            <input type="text" className="form-control" ref={this.titre}></input><h3
                            className="text-info mt-3">Lieu</h3>
                            <select className="form-control" ref={this.lieuid}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.lieuid.map((data, index) => (
                                            <option key={index} value={data.titre}>{data.titre}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Prixlieu</h3>
                            <input type="text" className="form-control" ref={this.prixlieu}></input><h3
                            className="text-info mt-3">Total</h3>
                            <input type="text" className="form-control" ref={this.total}></input><h3
                            className="text-info mt-3">Recette</h3>
                            <input type="text" className="form-control" ref={this.recette}></input><h3
                            className="text-info mt-3">Benefit</h3>
                            <input type="text" className="form-control" ref={this.benefit}></input><h3
                            className="text-info mt-3">Realrecette</h3>
                            <input type="text" className="form-control" ref={this.realrecette}></input><h3
                            className="text-info mt-3">Date</h3>
                            <input type="date" className="form-control" ref={this.date}></input><h3
                            className="text-info mt-3">Hour</h3>
                            <input type="text" className="form-control" ref={this.hour}></input><h3
                            className="text-info mt-3">Etat</h3>
                            <input type="text" className="form-control" ref={this.etat}></input><h3
                            className="text-info mt-3">Devisartistes</h3>
                            <input type="text" className="form-control" ref={this.devisartistes}></input><h3
                            className="text-info mt-3">Devisautres</h3>
                            <input type="text" className="form-control" ref={this.devisautres}></input><h3
                            className="text-info mt-3">Devisfixes</h3>
                            <input type="text" className="form-control" ref={this.devisfixes}></input><h3
                            className="text-info mt-3">Devisbillets</h3>
                            <input type="text" className="form-control" ref={this.devisbillets}></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}