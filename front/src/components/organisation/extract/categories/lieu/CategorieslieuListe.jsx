import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import ListePage from "@/components/utils/ListePage";
import GeneralListe from "@/components/utils/GeneralListe";
import React from "react";

export default class CategorieslieuListe extends GeneralListe {
    constructor(params) {
        super(params);
        this.lieuid = React.createRef(null);this.categoriesid = React.createRef(null);this.nbplacehmin = React.createRef(null);this.nbplacehmax = React.createRef(null);
        this.keys = React.createRef(null);
        this.urlData = "/categories/lieu/filter";
        this.urlUtils = "/categories/lieu/utils";
        this.baseUrl = "/categories/lieu"
    }
    componentDidMount() {
        this.init();
    }
    prepareDataListe = () => {
        let data = {
            keys : this.prepare(this.keys)
            ,lieuid : this.checkRefNull(this.lieuid,{id:this.prepare(this.lieuid)},null),categoriesid : this.checkRefNull(this.categoriesid,{id:this.prepare(this.categoriesid)},null),nbplacehmin : this.prepare(this.nbplacehmin),nbplacehmax : this.prepare(this.nbplacehmax)
        }
        return data;
    }
    findData = () => {
        this.search(this.prepareDataListe());
    }
    onChange = (event,page) => {
        let data = this.prepareDataListe();
        data = {
        ...data,
        page : page
        }
        this.search(data);
    }

    toPdf = ()=> {
        let data = this.prepareDataListe();
        data = {
            ...data,
            page : -1
        }
        let ans = {
            ...data,
            pdf : {
                titre : "Nombres de place par categorie",
                fields : [
                    {
    field : "lieuid",
    titre : "Lieu"
},{
    field : "categoriesid",
    titre : "Categories"
},{
    field : "nbplace",
    titre : "Nombre de place"
},
                ]
            }
        }
        // console.log('ans',ans);
        this.pdf(ans);

    }
    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                    <div className="col-12">
                        <Border>
                            <ListePage
                                title="Nombres de place par categorie"
                                count={this.state.count}
                                onChange={this.onChange}
                                search = {
                                    <>
                                        <div className="col-6">
                                            <h3 className="text-info mt-3">Recherche</h3>
                                            <input className="form-control" type="text" placeholder="recherche par mot clé" ref={this.keys}/>
                                            <h3 className="text-info mt-3" >Lieu</h3>
<select className="form-control" ref={this.lieuid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.lieuid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Categories</h3>
<select className="form-control" ref={this.categoriesid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.categoriesid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Nombre de place</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.nbplacehmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.nbplacehmax} />
    </div>
</div>
                                            <button className="btn btn-block btn-info mt-3" onClick={this.findData}>Rechercher</button>
                                        </div>
                                        <div className="col-12 mt-5">
                                            <div className="row">
                                                <div className="col-6"></div>
                                                <div className="col-6">
                                                    <div className="row">
                                                        <div className="col-2"></div>
                                                        <div className="col-4">
                                                        <div className="btn-list w-100">
                                                            <div className="btn-group">
                                                                <button type="button" className="btn btn-info dropdown-toggle w-100"
                                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Exporter
                                                                </button>
                                                                <div className="dropdown-menu w-100">
                                                                    <a className="dropdown-item " href="#a" onClick={this.toPdf}><i className="fas fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;&nbsp;PDF </a>
                                                                    <a className="dropdown-item " href="#a"><i className="fas fa-file-excel"></i>&nbsp;&nbsp;&nbsp;&nbsp;CSV</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <div className="col-6">
                                                            <button className="btn btn-block btn-success"
                                                                onClick={() => {
                                                                    window.location.replace("/categories/lieu/form");
                                                                }}
                                                            >Ajouter un nouveau</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                                head = {
                                    <tr><td>Lieu</td><td>Categories</td><td>Nombre de place</td><td></td></tr>
                                }
                            >
                            {this.state.data !== undefined && this.state.data !== null ?this.state.data.map((data,index) =>(<tr><td>{data.lieuid.titre}</td><td>{data.categoriesid.titre}</td><td>{data.nbplace}</td> <td>
    <button className="btn" onClick={()=> {
        window.location.replace("/categories/lieu/update/"+data.id)
    }}>
        <i className="fas fa-pencil-alt text-warning"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        this.delete(data)
    }}>
        <i className="fas fa-trash-alt text-danger"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        window.location.replace("/categories/lieu/"+data.id)
    }}>
        <i className="fas fa-plus text-info"></i>
    </button>
</td></tr>)):<></>}
                            </ListePage>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}