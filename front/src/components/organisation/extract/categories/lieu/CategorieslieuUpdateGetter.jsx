import { useParams } from "react-router-dom";

import CategorieslieuUpdate from "./CategorieslieuUpdate";

export default function CategorieslieuUpdateGetter() {
    const {id} = useParams();
    return (
        <CategorieslieuUpdate id={id}>
        </CategorieslieuUpdate>
    );
}