import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class CategorieslieuForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.lieuid = React.createRef(null);this.categoriesid = React.createRef(null);this.nbplace = React.createRef(null);
        this.urlSend = "/categories/lieu";
        this.urlUtils = "/categories/lieu/utils";
        this.afterValidation = "/categories/lieu";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            lieuid : this.checkRefNull(this.lieuid,{id:this.prepare(this.lieuid)},null),categoriesid : this.checkRefNull(this.categoriesid,{id:this.prepare(this.categoriesid)},null),nbplace : this.prepare(this.nbplace)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire de details de nombre de place</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3" >Lieu</h3>
<select className="form-control" ref={this.lieuid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.lieuid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Categories</h3>
<select className="form-control" ref={this.categoriesid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.categoriesid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Nombre de place</h3>
<input type="text" className="form-control" ref={this.nbplace} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}