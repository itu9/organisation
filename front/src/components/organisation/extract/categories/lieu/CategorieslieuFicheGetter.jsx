import { useParams } from "react-router-dom";
import CategorieslieuInfo from "./CategorieslieuInfo";
export default function CategorieslieuFicheGetter() {
    const {id} = useParams();
    return (
        <CategorieslieuInfo id={id}>
        </CategorieslieuInfo>
    );
}