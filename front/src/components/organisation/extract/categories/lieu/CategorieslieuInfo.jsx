import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";

export default class CategorieslieuInfo extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.urlSend = "/categories/lieu";
        this.urlUtils = "/categories/lieu/utils";
    }

    componentDidMount() {
        this.initUpdate();
    }

    render() {
        return (
             <HContainer>
             
                <h1 className="text-dark">Fiche de details de nombre de place</h1>
                <div className="mt-5">
                    <Border>
                    <div className="row">
                        <div className="col-3">
                            <h2 className="text-info">Alternatives</h2>
                            <div className="row mt-3 mb-3">
                                <div className="col-12 mb-3">
                                    <button className="btn btn-warning btn-block"
                                        onClick={
                                            () => {
                                                window.location.replace("/categories/lieu/update/"+this.props.id)
                                            }
                                        }
                                    >Modifier</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-info btn-block"
                                        onClick={
                                            () => {
                                                window.location.replace("/categories/lieu")
                                            }
                                        }
                                    >Liste</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-success btn-block" 
                                        onClick={
                                            () => {
                                                window.location.replace("/categories/lieu/form/")
                                            }
                                        }
                                    >Ajouter</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-danger btn-block" 
                                        onClick={
                                            () => {
                                                this.delete();
                                            }
                                        }
                                    >Suprimer</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-8">
                            <h2 className="text-info mb-3">Information</h2>
                            {
                                this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                <>
                                    <h3 className="text-info">Lieu</h3>
<p className="text-dark">{this.state.oneValue.lieuid.titre}</p><h3 className="text-info">Categories</h3>
<p className="text-dark">{this.state.oneValue.categoriesid.titre}</p><h3 className="text-info">Nombre de place</h3>
<p className="text-dark">{this.state.oneValue.nbplace}</p>
                                </>
                                :<></>
                            }
                        </div>
                    </div>
                    </Border>
                </div>
             </HContainer>
        );
    }
}
