import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class CategoriesForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.urlSend = "/categories";
        this.urlUtils = "/categories/utils";
        this.afterValidation = "/categories";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            titre : this.prepare(this.titre)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire d'ajout de categorie de place</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Titre</h3>
<input type="text" className="form-control" ref={this.titre} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}