import { useParams } from "react-router-dom";
import CategoriesInfo from "./CategoriesInfo";
export default function CategoriesFicheGetter() {
    const {id} = useParams();
    return (
        <CategoriesInfo id={id}>
        </CategoriesInfo>
    );
}