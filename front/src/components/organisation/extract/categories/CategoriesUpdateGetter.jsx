import { useParams } from "react-router-dom";

import CategoriesUpdate from "./CategoriesUpdate";

export default function CategoriesUpdateGetter() {
    const {id} = useParams();
    return (
        <CategoriesUpdate id={id}>
        </CategoriesUpdate>
    );
}