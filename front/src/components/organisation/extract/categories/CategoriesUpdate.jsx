import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class CategoriesUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.urlSend = "/categories";
        this.urlUtils = "/categories/utils";
        this.afterValidation = "/categories";
        this.id = React.createRef(null);
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.titre.current.value = this.state.oneValue.titre;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            titre : this.prepare(this.titre)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire de modification de categorie de place</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Titre</h3>
<input type="text" className="form-control" ref={this.titre} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}