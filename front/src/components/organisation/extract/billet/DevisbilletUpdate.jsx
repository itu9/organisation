import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class DevisbilletUpdate extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.categorieslieuid = React.createRef(null);
        this.devisid = React.createRef(null);
        this.prix = React.createRef(null);
        this.place = React.createRef(null);
        this.urlSend = "/devis/billet";
        this.urlUtils = "/devis/billet/utils";
        this.afterValidation = "/devis/billet";
        this.id = React.createRef(null);
    }

    componentDidMount() {
        this.initUpdate();
    }

    actionUpdate = () => {
        this.devisid.current.value = this.state.oneValue.devisid.id;
        this.prix.current.value = this.state.oneValue.prix;
        this.place.current.value = this.state.oneValue.place;
        this.changeCate()
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            categorieslieuid: this.checkRefNull(this.categorieslieuid, {id: this.prepare(this.categorieslieuid)}, null),
            devisid: this.checkRefNull(this.devisid, {id: this.prepare(this.devisid)}, null),
            prix: this.prepare(this.prix),
            place: this.prepare(this.place),
        }
        this.validate(data)
    }

    changeCate = () => {
        let devic = (this.devisid.current.value)
        if (devic !== '') {
            devic = parseInt(devic, 10)
            let ans = this.state.utils.devisid.filter((dev) => dev.id === devic)
            if (ans.length > 0) {
                let param = {
                    lieuid: {
                        id: ans[0].lieuid.id
                    },
                    page: -1
                }
                this.sendData(this.url + "/categories/lieu/filter", param,
                    data => {
                        this.verifData(data,
                            response => {
                                let temp = this.state.utils;
                                temp.categorieslieuid = response.data
                                this.setState({
                                    utils: temp
                                },
                                () => {
                                    this.categorieslieuid.current.value = this.state.oneValue.categorieslieuid.id;
                                })
                            }
                        )
                    }
                )
            }
        }
    }
    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire de modification de details de billet</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Categorieslieu</h3>
                            <select className="form-control" ref={this.categorieslieuid}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.categorieslieuid.map((data, index) => (
                                            <option key={index}
                                                    value={data.id}>{data.lieuid.titre} {data.categoriesid.titre} {data.nbplace}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Devis</h3>
                            <select className="form-control" ref={this.devisid} onChange={this.changeCate}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.devisid.map((data, index) => (
                                            <option key={index} value={data.id}>{data.titre}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Prix Unitaire</h3>
                            <input type="text" className="form-control" ref={this.prix}></input>
                            <h3 className="text-info mt-3">Place vendue</h3>
                            <input type="text" className="form-control" ref={this.place}></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}