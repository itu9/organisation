import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class DevisbilletForm extends GeneralForm {
    constructor(params) {
        super(params);
        this.categorieslieuid = React.createRef(null);
        this.devisid = React.createRef(null);
        this.prix = React.createRef(null);
        this.place = React.createRef(null);
        this.urlSend = "/devis/billet";
        this.urlUtils = "/devis/billet/utils";
        this.afterValidation = "/devis/billet";
    }

    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            categorieslieuid: this.checkRefNull(this.categorieslieuid, {id: this.prepare(this.categorieslieuid)}, null),
            devisid: this.checkRefNull(this.devisid, {id: this.prepare(this.devisid)}, null),
            prix: this.prepare(this.prix),
            place: 0
        }
        this.validate(data)
    }

    changeCate = () => {
        let devic = (this.devisid.current.value)
        if (devic !== '') {
            devic = parseInt(devic, 10)
            let ans = this.state.utils.devisid.filter((dev) => dev.id === devic)
            if (ans.length > 0) {
                let param = {
                    lieuid: {
                        id: ans[0].lieuid.id
                    },
                    page: -1
                }
                this.sendData(this.url + "/categories/lieu/filter", param,
                    data => {
                        this.verifData(data,
                            response => {
                                let temp = this.state.utils;
                                temp.categorieslieuid = response.data
                                this.setState({
                                    utils: temp
                                })
                            }
                        )
                    }
                )
            }
        }
    }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire d&apos;ajout de details de billet</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Categorieslieu</h3>
                            <select className="form-control" ref={this.categorieslieuid}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.categorieslieuid.map((data, index) => (
                                            <option key={index}
                                                    value={data.id}>{data.lieuid.titre} {data.categoriesid.titre} {data.nbplace}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Devis</h3>
                            {/*<select className="form-control" ref={this.devisid} >*/}
                            <select className="form-control" ref={this.devisid} onChange={this.changeCate}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.devisid.map((data, index) => (
                                            <option key={index} value={data.id}>{data.titre}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Prix Unitaire</h3>
                            <input type="text" className="form-control" ref={this.prix}></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}