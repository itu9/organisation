import { useParams } from "react-router-dom";

import DevisbilletUpdate from "./DevisbilletUpdate";

export default function DevisbilletUpdateGetter() {
    const {id} = useParams();
    return (
        <DevisbilletUpdate id={id}>
        </DevisbilletUpdate>
    );
}