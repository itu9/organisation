import { useParams } from "react-router-dom";
import DevisbilletInfo from "./DevisbilletInfo";
export default function DevisbilletFicheGetter() {
    const {id} = useParams();
    return (
        <DevisbilletInfo id={id}>
        </DevisbilletInfo>
    );
}