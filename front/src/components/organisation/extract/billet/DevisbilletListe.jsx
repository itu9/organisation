import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import ListePage from "@/components/utils/ListePage";
import GeneralListe from "@/components/utils/GeneralListe";
import React from "react";

export default class DevisbilletListe extends GeneralListe {
    constructor(params) {
        super(params);
        this.categorieslieuid = React.createRef(null);
        this.devisid = React.createRef(null);
        this.prixhmin = React.createRef(null);
        this.prixhmax = React.createRef(null);
        this.keys = React.createRef(null);
        this.urlData = "/devis/billet/filter";
        this.urlUtils = "/devis/billet/utils";
        this.baseUrl = "/devis/billet"
    }

    componentDidMount() {
        this.init();
    }

    prepareDataListe = () => {
        let data = {
            keys: this.prepare(this.keys)
            ,
            categorieslieuid: this.checkRefNull(this.categorieslieuid, {id: this.prepare(this.categorieslieuid)}, null),
            devisid: this.checkRefNull(this.devisid, {id: this.prepare(this.devisid)}, null),
            prixhmin: this.prepare(this.prixhmin),
            prixhmax: this.prepare(this.prixhmax)
        }
        return data;
    }
    findData = () => {
        this.search(this.prepareDataListe());
    }
    onChange = (event, page) => {
        let data = this.prepareDataListe();
        data = {
            ...data,
            page: page
        }
        this.search(data);
    }

    toPdf = () => {
        let data = this.prepareDataListe();
        data = {
            ...data,
            page: -1
        }
        let ans = {
            ...data,
            pdf: {
                titre: "Devis des billets",
                fields: [
                    {
                        field: "categorieslieuid",
                        titre: "Categorieslieu"
                    }, {
                        field: "devisid",
                        titre: "Devis"
                    }, {
                        field: "prix",
                        titre: "Prix Unitaire"
                    },
                ]
            }
        }
        // console.log('ans',ans);
        this.pdf(ans);

    }

    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                    <div className="col-12">
                        <Border>
                            <ListePage
                                title="Devis des billets"
                                count={this.state.count}
                                onChange={this.onChange}
                                search={
                                    <>
                                        <div className="col-6">
                                            <h3 className="text-info mt-3">Recherche</h3>
                                            <input className="form-control" type="text"
                                                   placeholder="recherche par mot clé" ref={this.keys}/>
                                            <h3 className="text-info mt-3">Categorieslieu</h3>
                                            <select className="form-control" ref={this.categorieslieuid}>
                                                <option value=""></option>
                                                {
                                                    this.state.utils !== undefined && this.state.utils !== null ?
                                                        this.state.utils.categorieslieuid.map((data, index) => (
                                                            <option key={index} value={data.id}>{data.nbplace}</option>
                                                        ))
                                                        : <></>
                                                }
                                            </select><h3 className="text-info mt-3">Devis</h3>
                                            <select className="form-control" ref={this.devisid}>
                                                <option value=""></option>
                                                {
                                                    this.state.utils !== undefined && this.state.utils !== null ?
                                                        this.state.utils.devisid.map((data, index) => (
                                                            <option key={index} value={data.id}>{data.titre}</option>
                                                        ))
                                                        : <></>
                                                }
                                            </select><h3 className="text-info mt-3">Prix Unitaire</h3>
                                            <div className="row">
                                                <div className="col-6">
                                                    <input type="text" className="form-control" placeholder="Minimum"
                                                           ref={this.prixhmin}/>
                                                </div>
                                                <div className="col-6">
                                                    <input type="text" className="form-control" placeholder="Maximum"
                                                           ref={this.prixhmax}/>
                                                </div>
                                            </div>
                                            <button className="btn btn-block btn-info mt-3"
                                                    onClick={this.findData}>Rechercher
                                            </button>
                                        </div>
                                        <div className="col-12 mt-5">
                                            <div className="row">
                                                <div className="col-6"></div>
                                                <div className="col-6">
                                                    <div className="row">
                                                        <div className="col-2"></div>
                                                        <div className="col-4">
                                                            <div className="btn-list w-100">
                                                                <div className="btn-group">
                                                                    <button type="button"
                                                                            className="btn btn-info dropdown-toggle w-100"
                                                                            data-toggle="dropdown" aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                        Exporter
                                                                    </button>
                                                                    <div className="dropdown-menu w-100">
                                                                        <a className="dropdown-item " href="#a"
                                                                           onClick={this.toPdf}><i
                                                                            className="fas fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;&nbsp;PDF
                                                                        </a>
                                                                        <a className="dropdown-item " href="#a"><i
                                                                            className="fas fa-file-excel"></i>&nbsp;&nbsp;&nbsp;&nbsp;CSV</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-6">
                                                            <button className="btn btn-block btn-success"
                                                                    onClick={() => {
                                                                        window.location.replace("/devis/billet/form");
                                                                    }}
                                                            >Ajouter un nouveau
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                                head={
                                    <tr>
                                        <td>Categorieslieu</td>
                                        <td>Devis</td>
                                        <td>Prix Unitaire</td>
                                        <td>Place vendue</td>
                                        <td></td>
                                    </tr>
                                }
                            >
                                {this.state.data !== undefined && this.state.data !== null ? this.state.data.map((data, index) => (
                                    <tr>
                                        <td>{data.categorieslieuid.lieuid.titre} {data.categorieslieuid.categoriesid.titre} {data.categorieslieuid.nbplace}</td>
                                        <td>{data.devisid.titre}</td>
                                        <td>{data.prix}</td>
                                        <td>{data.place}</td>
                                        <td>
                                            <button className="btn" onClick={() => {
                                                window.location.replace("/devis/billet/update/" + data.id)
                                            }}>
                                                <i className="fas fa-pencil-alt text-warning"></i>
                                            </button>
                                            <button className="btn ml-3" onClick={() => {
                                                this.delete(data)
                                            }}>
                                                <i className="fas fa-trash-alt text-danger"></i>
                                            </button>
                                            <button className="btn ml-3" onClick={() => {
                                                window.location.replace("/devis/billet/" + data.id)
                                            }}>
                                                <i className="fas fa-plus text-info"></i>
                                            </button>
                                        </td>
                                    </tr>)) : <></>}
                            </ListePage>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}