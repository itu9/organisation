import { useParams } from "react-router-dom";
import AutredepenseInfo from "./AutredepenseInfo";
export default function AutredepenseFicheGetter() {
    const {id} = useParams();
    return (
        <AutredepenseInfo id={id}>
        </AutredepenseInfo>
    );
}