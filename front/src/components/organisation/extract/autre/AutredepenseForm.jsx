import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class AutredepenseForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.urlSend = "/autre/depense";
        this.urlUtils = "/autre/depense/utils";
        this.afterValidation = "/autre/depense";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            titre : this.prepare(this.titre)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire d&apos;ajout d'autre depense</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Titre</h3>
<input type="text" className="form-control" ref={this.titre} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}