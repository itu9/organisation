import { useParams } from "react-router-dom";

import AutredepenseUpdate from "./AutredepenseUpdate";

export default function AutredepenseUpdateGetter() {
    const {id} = useParams();
    return (
        <AutredepenseUpdate id={id}>
        </AutredepenseUpdate>
    );
}