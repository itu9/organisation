import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";
import '../artiste/Artiste.css';

export default class LieuUpdate extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.nbplace = React.createRef(null);
        this.urlSend = "/lieu";
        this.urlUtils = "/lieu/utils";
        this.afterValidation = "/lieu";
        this.id = React.createRef(null);
    }

    componentDidMount() {
        this.initUpdate();
        this.setState({
            img: ''
        })
    }

    actionUpdate = () => {
        this.titre.current.value = this.state.oneValue.titre;
        this.nbplace.current.value = this.state.oneValue.nbplace;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            titre: this.prepare(this.titre),
            nbplace: this.prepare(this.nbplace),
            photo: this.state.img
        }
        this.validate(data)
    }
    changeImg = (event) => {
        this.upload(event, (img) => {
            this.setState({
                img: img
            }, () => {
                console.log(img)
            })
        })
    }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire de modification de lieu</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-5">Titre</h3>
                            <input type="text" className="form-control" ref={this.titre}></input><h3
                            className="text-info mt-3">Nombre de place</h3>
                            <input type="text" className="form-control" ref={this.nbplace}></input>
                            <h3 className={"text-info mt-3"}>Photo</h3>
                            <input className="input-group-prepend form-control" type="file" onChange={this.changeImg}/>
                            <img className="card-img-top img-fluid himg"
                                 src={this.state.img !== undefined && this.state.img !== null ?
                                     'data:image/png;base64,' + this.state.img : ''}
                                 alt=""/>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}