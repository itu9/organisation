import { useParams } from "react-router-dom";
import LieuInfo from "./LieuInfo";
export default function LieuFicheGetter() {
    const {id} = useParams();
    return (
        <LieuInfo id={id}>
        </LieuInfo>
    );
}