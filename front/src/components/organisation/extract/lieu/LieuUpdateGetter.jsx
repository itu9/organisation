import { useParams } from "react-router-dom";

import LieuUpdate from "./LieuUpdate";

export default function LieuUpdateGetter() {
    const {id} = useParams();
    return (
        <LieuUpdate id={id}>
        </LieuUpdate>
    );
}