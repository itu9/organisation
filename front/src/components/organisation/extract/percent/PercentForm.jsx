import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class PercentForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.value = React.createRef(null);
        this.urlSend = "/percent";
        this.urlUtils = "/percent/utils";
        this.afterValidation = "/percent";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            value : this.prepare(this.value)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire d'ajout de taxe</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Pourcentage</h3>
<input type="text" className="form-control" ref={this.value} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}