import { useParams } from "react-router-dom";

import PercentUpdate from "./PercentUpdate";

export default function PercentUpdateGetter() {
    const {id} = useParams();
    return (
        <PercentUpdate id={id}>
        </PercentUpdate>
    );
}