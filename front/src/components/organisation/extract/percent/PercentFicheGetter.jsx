import { useParams } from "react-router-dom";
import PercentInfo from "./PercentInfo";
export default function PercentFicheGetter() {
    const {id} = useParams();
    return (
        <PercentInfo id={id}>
        </PercentInfo>
    );
}