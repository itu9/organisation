import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class PercentUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.value = React.createRef(null);
        this.urlSend = "/percent";
        this.urlUtils = "/percent/utils";
        this.afterValidation = "/percent";
        this.id = React.createRef(null);
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.value.current.value = this.state.oneValue.value;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            value : this.prepare(this.value)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire de modification de taxe</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Pourcentage</h3>
<input type="text" className="form-control" ref={this.value} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}