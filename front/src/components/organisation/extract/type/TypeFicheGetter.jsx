import { useParams } from "react-router-dom";
import TypeInfo from "./TypeInfo";
export default function TypeFicheGetter() {
    const {id} = useParams();
    return (
        <TypeInfo id={id}>
        </TypeInfo>
    );
}