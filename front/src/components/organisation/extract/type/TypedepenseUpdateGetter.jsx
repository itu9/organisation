import { useParams } from "react-router-dom";

import TypedepenseUpdate from "./TypedepenseUpdate";

export default function TypedepenseUpdateGetter() {
    const {id} = useParams();
    return (
        <TypedepenseUpdate id={id}>
        </TypedepenseUpdate>
    );
}