import { useParams } from "react-router-dom";
import TypedepenseInfo from "./TypedepenseInfo";
export default function TypedepenseFicheGetter() {
    const {id} = useParams();
    return (
        <TypedepenseInfo id={id}>
        </TypedepenseInfo>
    );
}