import { useParams } from "react-router-dom";
import LoginInfo from "./LoginInfo";
export default function LoginFicheGetter() {
    const {id} = useParams();
    return (
        <LoginInfo id={id}>
        </LoginInfo>
    );
}