import { useParams } from "react-router-dom";
import ArtisteInfo from "./ArtisteInfo";
export default function ArtisteFicheGetter() {
    const {id} = useParams();
    return (
        <ArtisteInfo id={id}>
        </ArtisteInfo>
    );
}