import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../../service/OwnService.jsx";
import React from "react";
import './Artiste.css'

export default class ArtisteInfo extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.urlSend = "/artiste";
        this.urlUtils = "/artiste/utils";
    }

    componentDidMount() {
        this.initUpdate();
    }

    render() {
        return (
            <HContainer>

                <h1 className="text-dark">Fiche de details d&apos;artiste</h1>
                <div className="mt-5">
                    <Border>
                        <div className="row">
                            <div className="col-3">
                                <h2 className="text-info">Alternatives</h2>
                                <div className="row mt-3 mb-3">
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-warning btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/artiste/update/" + this.props.id)
                                                    }
                                                }
                                        >Modifier
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-info btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/artiste")
                                                    }
                                                }
                                        >Liste
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-success btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/artiste/form/")
                                                    }
                                                }
                                        >Ajouter
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-danger btn-block"
                                                onClick={
                                                    () => {
                                                        this.delete();
                                                    }
                                                }
                                        >Suprimer
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-8">
                                <h2 className="text-info mb-3">Information</h2>
                                {
                                    this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                        <>
                                            <img className="card-img-top img-fluid himg"
                                                 src={this.state.oneValue.photo !== undefined && this.state.oneValue.photo !== null ?
                                                     'data:image/png;base64,' + this.state.oneValue.photo : ''}
                                                 alt=""/>
                                            <h3 className="text-info mt-5">Nom</h3>
                                            <p className="text-dark">{this.state.oneValue.nom}</p><h3
                                            className="text-info">Tarif</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.tarif) }</p><h3
                                            className="text-info">Duree</h3>
                                            <p className="text-dark">{this.state.oneValue.duree}h</p>
                                        </>
                                        : <></>
                                }
                            </div>
                        </div>
                    </Border>
                </div>
            </HContainer>
        );
    }
}
