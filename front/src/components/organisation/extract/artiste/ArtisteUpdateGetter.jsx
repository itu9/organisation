import { useParams } from "react-router-dom";

import ArtisteUpdate from "./ArtisteUpdate";

export default function ArtisteUpdateGetter() {
    const {id} = useParams();
    return (
        <ArtisteUpdate id={id}>
        </ArtisteUpdate>
    );
}