import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

import './Artiste.css'
export default class ArtisteForm extends GeneralForm {
    constructor(params) {
        super(params);
        this.nom = React.createRef(null);
        this.tarif = React.createRef(null);
        this.duree = React.createRef(null);
        this.urlSend = "/artiste";
        this.urlUtils = "/artiste/utils";
        this.afterValidation = "/artiste";
    }

    componentDidMount() {
        this.init();
        this.setState({
            img: ''
        })
    }

    validateData = () => {
        let data = {
            nom: this.prepare(this.nom),
            tarif: this.prepare(this.tarif),
            duree: this.prepare(this.duree),
            photo : this.state.img
        }
        this.validate(data)
    }

    changeImg = (event) => {
        this.upload(event, (img) => {
            this.setState({
                img: img
            })
        })
    }
    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire d&apos;ajout d&apos;artiste</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Nom</h3>
                            <input type="text" className="form-control" ref={this.nom}></input><h3
                            className="text-info mt-3">Tarif</h3>
                            <input type="text" className="form-control" ref={this.tarif}></input><h3
                            className="text-info mt-3">Duree</h3>
                            <input type="text" className="form-control" ref={this.duree}></input>
                            <h3 className={"text-info mt-3"}>Photo</h3>
                            <input className="input-group-prepend form-control" type="file" onChange={this.changeImg}/>
                            <img className="card-img-top img-fluid himg"
                                 src={this.state.img !== undefined && this.state.img !== null ?
                                     'data:image/png;base64,' + this.state.img : ''}
                                 alt=""/>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}