import General from "../../../utils/General.jsx";
import HContainer from "../../../client/nav/HContainer";
import Border from "../../../utils/Border";
import React from "react"

export default class Bis extends General {
    constructor(props) {
        super(props);
        this.date = React.createRef(null)
        this.heur = React.createRef(null)
    }

    dupliquer = () => {
        let data = {
            date: this.date.current.value,
            heur: this.heur.current.value,
            id : this.props.id
        }
        let auth = this.getSession(this.auth,{
            login : {
                id : -100
            }
        })
        let url = this.url+"/devis/bis?idUser="+auth.login.id
        this.sendData(url, data,
            answer => {
                // window.location.replace("/spectacle")
            },
            header => {

            })
    }

    render() {
        return (
            <HContainer>
                <h1 className={"text-dark"}>Formulaire de spectacle bis</h1>
                <Border>
                    <div className={"row"}>
                        <div className={"col-6"}>
                            <h3 className={"text-info"}>Date</h3>
                            <input type={"date"} className={"form-control"} ref={this.date}/>
                            <h3 className={"text-info mt-3"}>Heure</h3>
                            <input type={"text"} className={"form-control"} ref={this.heur}/>
                            <button className={"btn btn-success mt-3"} onClick={this.dupliquer}>Duplique</button>
                        </div>
                    </div>
                </Border>
            </HContainer>
        );
    }

}