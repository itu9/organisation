import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class DevisfixeUpdate extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.depensefixeid = React.createRef(null);
        this.devisid = React.createRef(null);
        this.duree = React.createRef(null);
        this.urlSend = "/devis/fixe";
        this.urlUtils = "/devis/fixe/utils";
        this.afterValidation = "/devis/fixe";
        this.id = React.createRef(null);
        this.jour = React.createRef(null);
    }

    componentDidMount() {
        this.initUpdate();
    }

    actionUpdate = () => {
        this.depensefixeid.current.value = this.state.oneValue.depensefixeid.id;
        this.devisid.current.value = this.state.oneValue.devisid.id;
        this.duree.current.value = this.state.oneValue.duree;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            depensefixeid: this.checkRefNull(this.depensefixeid, {id: this.prepare(this.depensefixeid)}, null),
            devisid: this.checkRefNull(this.devisid, {id: this.prepare(this.devisid)}, null),
            duree: this.prepare(this.duree)
        }
        this.validate(data)
    }

    onChangeDuree = ()=> {
        let duration = parseFloat(this.duree.current.value !== ''?this.duree.current.value : "0",10)
        let dJour = duration / 24.
        this.jour.current.value = dJour
    }

    onChangeJour = ()=> {
        let duration = parseFloat(this.jour.current.value !== ''?this.jour.current.value : "0",10)
        let dJour = duration * 24.
        this.duree.current.value = dJour
    }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire de modification de devis fixe</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Depense fixe</h3>
                            <select className="form-control" ref={this.depensefixeid}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.depensefixeid.map((data, index) => (
                                            <option key={index} value={data.id}>{data.titre}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Devis</h3>
                            <select className="form-control" ref={this.devisid}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.devisid.map((data, index) => (
                                            <option key={index} value={data.id}>{data.titre}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Duree</h3>
                            <input type="text" className="form-control" ref={this.duree} onChange={this.onChangeDuree}></input>
                            <h3 className="text-info mt-3">Duree en jour</h3>
                            <input type="text" className="form-control" ref={this.jour} onChange={this.onChangeJour}></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}