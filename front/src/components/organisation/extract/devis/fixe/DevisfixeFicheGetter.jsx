import { useParams } from "react-router-dom";
import DevisfixeInfo from "./DevisfixeInfo";
export default function DevisfixeFicheGetter() {
    const {id} = useParams();
    return (
        <DevisfixeInfo id={id}>
        </DevisfixeInfo>
    );
}