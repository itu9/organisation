import { useParams } from "react-router-dom";

import DevisfixeUpdate from "./DevisfixeUpdate";

export default function DevisfixeUpdateGetter() {
    const {id} = useParams();
    return (
        <DevisfixeUpdate id={id}>
        </DevisfixeUpdate>
    );
}