import { useParams } from "react-router-dom";
import DevisAffichage from "./DevisAffichage.jsx";
export default function DevisAffichageGetter() {
    const {id} = useParams();
    return (
        <DevisAffichage id={id}>
        </DevisAffichage>
    );
}