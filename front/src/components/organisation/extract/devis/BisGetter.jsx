import { useParams } from "react-router-dom";
import SpectacleInfo from "./Bis";
export default function BisGetter() {
    const {id} = useParams();
    return (
        <SpectacleInfo id={id}>
        </SpectacleInfo>
    );
}