import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class DevisForm extends GeneralForm {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.lieuid = React.createRef(null);
        this.prixlieu = React.createRef(null);
        this.date = React.createRef(null);
        this.hour = React.createRef(null);
        this.urlSend = "/devis";
        this.urlUtils = "/devis/utils";
        this.afterValidation = "/devis";
    }

    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            titre: this.prepare(this.titre),
            lieuid: this.checkRefNull(this.lieuid, {id: this.prepare(this.lieuid)}, null),
            prixlieu: this.prepare(this.prixlieu),
            date: this.prepare(this.date),
            hour: this.prepare(this.hour),
            etat : 0
        }
        this.validate(data)
    }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire d&apos;ajout de devis</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">Titre</h3>
                            <input type="text" className="form-control" ref={this.titre}></input>
                            <h3 className="text-info mt-3">Date</h3>
                            <input type="date" className="form-control" ref={this.date}></input>
                            <h3 className="text-info mt-3">Heur</h3>
                            <input type="text" className="form-control" ref={this.hour}></input>
                            <h3 className="text-info mt-3">Lieu</h3>
                            <select className="form-control" ref={this.lieuid}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.lieuid.map((data, index) => (
                                            <option key={index} value={data.id}>{data.titre}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Prix du lieu</h3>
                            <input type="text" className="form-control" ref={this.prixlieu}></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}