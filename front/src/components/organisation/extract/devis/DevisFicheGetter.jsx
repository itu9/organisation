import { useParams } from "react-router-dom";
import DevisInfo from "./DevisInfo";
export default function DevisFicheGetter() {
    const {id} = useParams();
    return (
        <DevisInfo id={id}>
        </DevisInfo>
    );
}