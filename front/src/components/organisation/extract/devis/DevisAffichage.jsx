import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../../service/OwnService.jsx";
import './Devis.css'
import React from "react";


export default class DevisAffichage extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.urlSend = "/devis";
        this.urlUtils = "/devis/utils";
    }

    actionUpdate = ()=> {
    }

    componentDidMount() {
        this.initUpdate();
    }

    exportPdf =()=>{
        window.print();
    }

    formatAriary = (prix)=>{
        return prix.split('MGA')[0]+"Ariary";
    }

    render() {
        return (
            <HContainer>
                <div className="mt-5">
                    <button className={"d-print-none btn btn-success mb-5"} onClick={this.exportPdf}>
                        <i className="fas fa-file-pdf"></i> &nbsp;&nbsp;Exporter pdf </button>
                    <Border>
                        <div className="row">
                            <div className="col-12">
                                {
                                    this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                        <>
                                            <h1 className="text-dark">{this.state.oneValue.titre}</h1>
                                            <h3
                                            className="text-info">{this.state.oneValue.lieuid.titre}&nbsp;&nbsp;
                                            {OwnService.formatDate(this.state.oneValue.date)}&nbsp;&nbsp;
                                            {this.state.oneValue.hour}
                                            </h3>
                                                <img className="card-img-top img-fluid hbackground"
                                                     src={this.state.oneValue.lieuid.photo !== undefined && this.state.oneValue.lieuid.photo !== null ?
                                                         'data:image/png;base64,' + this.state.oneValue.lieuid.photo : ''}
                                                     alt=""/>

                                            <div className={"row"}>
                                                <div className={"col-12"}>
                                                    <h1 className="text-dark mb-3 mt-5">Artistes</h1>

                                                    <div className="row">
                                                        {
                                                            this.state.oneValue.devisartistes !== null ?
                                                                this.state.oneValue.devisartistes.map((dev,index) => (
                                                                    <>
                                                                        {dev.sum >= 25 ? <></> :
                                                                            <div className="col-4">
                                                                                <div className={"row hcardartiste"}>
                                                                                    <div className={"col-12"}>
                                                                                        <img className="card-img-top img-fluid hartiste"
                                                                                             src={dev.artisteid.photo !== undefined && dev.artisteid.photo !== null ?
                                                                                                 'data:image/png;base64,' + dev.artisteid.photo : ''}
                                                                                             alt=""/>
                                                                                    </div>
                                                                                </div>
                                                                                <div className={"row mt-3"}>
                                                                                    <div className={"col-12"}>
                                                                                        <h2 className={"text-dark"}>{dev.artisteid.nom}</h2>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        }

                                                                    </>
                                                                )):<></>
                                                        }
                                                    </div>
                                                </div>
                                            </div>


                                            <h2 className="text-dark mb-3 mt-5">Tarif :</h2>
                                            <div className="row">
                                                <div className="col-6">
                                                    <table className={"table"}>
                                                        <thead className={"bg-info text-white"}>
                                                        <tr>
                                                            <th>Categorie</th>
                                                            <th>Prix unitaire</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            this.state.oneValue.devisbillets !== null ?
                                                                this.state.oneValue.devisbillets.map((dev,index) => (
                                                                    <>{
                                                                        dev.sum >= 25 ?
                                                                            <></> :
                                                                            <tr className={dev.sum >= 25 ? "bg-info text-white" : ""}>
                                                                                <td
                                                                                    onClick={() => {
                                                                                        window.location.replace("/devis/billet/"+dev.id)
                                                                                    }}
                                                                                    className={dev.sum >= 25 ? "":"text-info hlink"}>{dev.categorieslieuid.categoriesid.titre}</td>
                                                                                <td>{dev.sum >= 25 ? "":  this.formatAriary(OwnService.format(dev.prix)) }</td>
                                                                            </tr>
                                                                    }</>

                                                                ))
                                                                :<></>
                                                        }

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>

                                        </>
                                        : <></>
                                }
                            </div>
                        </div>
                    </Border>
                </div>
            </HContainer>
        );
    }
}
