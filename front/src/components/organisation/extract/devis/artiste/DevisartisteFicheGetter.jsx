import { useParams } from "react-router-dom";
import DevisartisteInfo from "./DevisartisteInfo";
export default function DevisartisteFicheGetter() {
    const {id} = useParams();
    return (
        <DevisartisteInfo id={id}>
        </DevisartisteInfo>
    );
}