import { useParams } from "react-router-dom";

import DevisartisteUpdate from "./DevisartisteUpdate";

export default function DevisartisteUpdateGetter() {
    const {id} = useParams();
    return (
        <DevisartisteUpdate id={id}>
        </DevisartisteUpdate>
    );
}