import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class DevisartisteUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.artisteid = React.createRef(null);this.devisid = React.createRef(null);this.duree = React.createRef(null);
        this.urlSend = "/devis/artiste";
        this.urlUtils = "/devis/artiste/utils";
        this.afterValidation = "/devis/artiste";
        this.id = React.createRef(null);
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.artisteid.current.value = this.state.oneValue.artisteid.id;this.devisid.current.value = this.state.oneValue.devisid.id;this.duree.current.value = this.state.oneValue.duree;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            artisteid : this.checkRefNull(this.artisteid,{id:this.prepare(this.artisteid)},null),devisid : this.checkRefNull(this.devisid,{id:this.prepare(this.devisid)},null),duree : this.prepare(this.duree)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire de modification de devis d&apos;artiste</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Artiste</h3>
<select className="form-control" ref={this.artisteid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.artisteid.map((data,index) => (
            <option key={index} value={data.id} >{data.nom}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Devis</h3>
<select className="form-control" ref={this.devisid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.devisid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Duree</h3>
<input type="text" className="form-control" ref={this.duree} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}