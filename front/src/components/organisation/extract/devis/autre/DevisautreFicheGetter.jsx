import { useParams } from "react-router-dom";
import DevisautreInfo from "./DevisautreInfo";
export default function DevisautreFicheGetter() {
    const {id} = useParams();
    return (
        <DevisautreInfo id={id}>
        </DevisautreInfo>
    );
}