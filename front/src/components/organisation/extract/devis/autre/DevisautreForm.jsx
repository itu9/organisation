import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class DevisautreForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.autreid = React.createRef(null);this.devisid = React.createRef(null);this.prix = React.createRef(null);
        this.urlSend = "/devis/autre";
        this.urlUtils = "/devis/autre/utils";
        this.afterValidation = "/devis/autre";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            autreid : this.checkRefNull(this.autreid,{id:this.prepare(this.autreid)},null),devisid : this.checkRefNull(this.devisid,{id:this.prepare(this.devisid)},null),prix : this.prepare(this.prix)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire d&apos;ajout d&apos;autre devis</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3" >Autre</h3>
<select className="form-control" ref={this.autreid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.autreid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Devis</h3>
<select className="form-control" ref={this.devisid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.devisid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Prix</h3>
<input type="text" className="form-control" ref={this.prix} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}