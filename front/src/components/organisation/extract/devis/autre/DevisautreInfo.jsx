import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../../../service/OwnService.jsx";

export default class DevisautreInfo extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.urlSend = "/devis/autre";
        this.urlUtils = "/devis/autre/utils";
    }

    componentDidMount() {
        this.initUpdate();
    }

    render() {
        return (
            <HContainer>

                <h1 className="text-dark">Fiche de details d&apos;autre devis</h1>
                <div className="mt-5">
                    <Border>
                        <div className="row">
                            <div className="col-3">
                                <h2 className="text-info">Alternatives</h2>
                                <div className="row mt-3 mb-3">
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-warning btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/devis/autre/update/" + this.props.id)
                                                    }
                                                }
                                        >Modifier
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-info btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/devis/autre")
                                                    }
                                                }
                                        >Liste
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-success btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/devis/autre/form/")
                                                    }
                                                }
                                        >Ajouter
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-danger btn-block"
                                                onClick={
                                                    () => {
                                                        this.delete();
                                                    }
                                                }
                                        >Suprimer
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-8">
                                <h2 className="text-info mb-3">Information</h2>
                                {
                                    this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                        <>
                                            <h3 className="text-info">Autre</h3>
                                            <p className="text-dark">{this.state.oneValue.autreid.titre}</p><h3
                                            className="text-info">Devis</h3>
                                            <p className="text-dark">{this.state.oneValue.devisid.titre}</p><h3
                                            className="text-info">Prix</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.prix) }</p>
                                        </>
                                        : <></>
                                }
                            </div>
                        </div>
                    </Border>
                </div>
            </HContainer>
        );
    }
}
