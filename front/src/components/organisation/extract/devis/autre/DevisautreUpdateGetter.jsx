import { useParams } from "react-router-dom";

import DevisautreUpdate from "./DevisautreUpdate";

export default function DevisautreUpdateGetter() {
    const {id} = useParams();
    return (
        <DevisautreUpdate id={id}>
        </DevisautreUpdate>
    );
}