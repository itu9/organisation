import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../../service/OwnService.jsx";
import './Devis.css'

export default class DevisInfo extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.urlSend = "/devis";
        this.urlUtils = "/devis/utils";
    }

    componentDidMount() {
        this.initUpdate();
    }
    genereSpec = () => {
        this.checkConnexion();
        let auth = this.getSession(this.auth,{
            login : {
                id : -100
            }
        })
        let url = this.url+"/devis/"+this.props.id+"/spectacle?idUser=+"+auth.login.id
        this.getListe(url,(data) => {
            this.verifData(data, response=> {
                window.location.replace("/spectacle")
            })
        },
            headers => {})
    }

    render() {
        return (
            <HContainer>

                <h1 className="text-dark">Fiche de details de devis</h1>
                <div className="mt-5">
                    <Border>
                        <div className="row">
                            <div className="col-3">
                                <h2 className="text-info">Alternatives</h2>
                                <div className="row mt-3 mb-3">
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-warning btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/devis/update/" + this.props.id)
                                                    }
                                                }
                                        >Modifier
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-info btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/devis")
                                                    }
                                                }
                                        >Liste
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-success btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/devis/form/")
                                                    }
                                                }
                                        >Ajouter
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-danger btn-block"
                                                onClick={
                                                    () => {
                                                        this.delete();
                                                    }
                                                }
                                        >Suprimer
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-success btn-block"
                                                onClick={
                                                    () => {
                                                        window.location.replace("/devis/affichage/"+this.props.id)
                                                    }
                                                }
                                        >Générer affichage
                                        </button>
                                    </div>
                                    <div className="col-12 mb-3">
                                        <button className="btn btn-success btn-block"
                                                onClick={
                                                    this.genereSpec
                                                }
                                        >Générer spectacle
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-8">
                                <h2 className="text-info mb-3">Information</h2>
                                {
                                    this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                        <>
                                            <h3 className="text-info">Titre</h3>
                                            <p className="text-dark">{this.state.oneValue.titre}</p><h3
                                            className="text-info">Lieu</h3>
                                            <p className="text-dark">{this.state.oneValue.lieuid.titre}</p>
                                            <h3 className="text-info">Date</h3>
                                            <p className="text-dark">{OwnService.formatDate(this.state.oneValue.date) }</p>
                                            <h3 className="text-info">Prix du lieu</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.prixlieu)}</p>
                                            <h3 className="text-info">Prix total</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.total)}</p>
                                            <h3 className="text-info">Recette</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.recette)}</p>
                                            <h3 className="text-info">Benefice provisoire brut</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.benefit)}</p>
                                            <h3 className="text-info">Taxe provisoire</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.taxeprov)}</p>
                                            <h3 className="text-info">Benefice provisoire net</h3>
                                            <p className="text-dark">{OwnService.format(this.state.oneValue.benefprovnet)}</p>
                                            <h2 className="text-dark mb-3 mt-5">Tarif billet</h2>

                                            <table className={"table"}>
                                                <thead className={"bg-info text-white"}>
                                                <tr>
                                                    <th>Categorie</th>
                                                    <th>Nombre de place</th>
                                                    <th>Vendue</th>
                                                    <th>Prix unitaire</th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.oneValue.devisbillets !== null ?
                                                        this.state.oneValue.devisbillets.map((dev,index) => (
                                                            <tr className={dev.sum >= 25 ? "bg-info text-white" : ""}>
                                                                <td
                                                                    onClick={() => {
                                                                        window.location.replace("/devis/billet/"+dev.id)
                                                                    }}
                                                                    className={dev.sum >= 25 ? "":"text-info hlink"}>{dev.categorieslieuid.categoriesid.titre}</td>
                                                                <td>{dev.categorieslieuid.nbplace}</td>
                                                                <td>{dev.place}</td>
                                                                <td>{dev.sum >= 25 ? "":  OwnService.format(dev.prix)}</td>
                                                                <td>{OwnService.format(dev.val) }</td>
                                                            </tr>
                                                        ))
                                                        :<></>
                                                }

                                                </tbody>
                                            </table>
                                            <button className={"btn btn-success"}
                                                    onClick={()=>{
                                                        window.location.replace("/devis/billet/form")
                                                    }
                                                    }
                                            >Ajouter billet</button>
                                            <h2 className="text-dark mb-3 mt-5">Les artistes</h2>

                                            <table className={"table"}>
                                                <thead className={"bg-info text-white"}>
                                                    <tr>
                                                        <th>Artiste</th>
                                                        <th>Tarif</th>
                                                        <th>Duree</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.oneValue.devisartistes !== null ?
                                                        this.state.oneValue.devisartistes.map((dev,index) => (
                                                            <tr className={dev.sum >= 25 ? "bg-info text-white" : ""}>
                                                                <td
                                                                    onClick={() => {
                                                                        window.location.replace("/devis/artiste/"+dev.id)
                                                                    }}
                                                                    className={dev.sum >= 25 ? "":"text-info hlink"}>{dev.artisteid.nom}</td>
                                                                <td>{dev.sum >= 25 ? "":  OwnService.format(dev.artisteid.tarif)}</td>
                                                                <td>{dev.duree}h</td>
                                                                <td>{OwnService.format(dev.val) }</td>
                                                            </tr>
                                                            ))
                                                        :<></>
                                                }

                                                </tbody>
                                            </table>
                                            <button className={"btn btn-success"}
                                                onClick={()=>{
                                                    window.location.replace("/devis/artiste/form")
                                                }
                                                }
                                            >Ajouter artiste</button>

                                            <h2 className="text-dark mb-3 mt-5">Les depenses fixes</h2>

                                            <table className={"table"}>
                                                <thead className={"bg-info text-white"}>
                                                <tr>
                                                    <th>Depense</th>
                                                    <th>Tarif</th>
                                                    <th>Duree</th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.oneValue.devisfixes !== null ?
                                                        this.state.oneValue.devisfixes.map((dev,index) => (
                                                            <tr className={dev.sum >= 25 ? "bg-info text-white" : ""}>
                                                                <td
                                                                    onClick={() => {
                                                                        window.location.replace("/devis/fixe/"+dev.id)
                                                                    }}
                                                                    className={dev.sum >= 25 ? "":"text-info hlink"}>{dev.depensefixeid.titre}</td>
                                                                <td>{dev.sum >= 25 ? "":  OwnService.format(dev.depensefixeid.tarif)}</td>
                                                                <td>{dev.duree}h</td>
                                                                <td>{OwnService.format(dev.val) }</td>
                                                            </tr>
                                                        ))
                                                        :<></>
                                                }

                                                </tbody>
                                            </table>
                                            <button className={"btn btn-success"}
                                                    onClick={()=>{
                                                        window.location.replace("/devis/fixe/form")
                                                    }
                                                    }
                                            >Ajouter dépense fixe</button>
                                            <h2 className="text-dark mb-3 mt-5">Les autres depenses</h2>

                                            <table className={"table"}>
                                                <thead className={"bg-info text-white"}>
                                                <tr>
                                                    <th>Depense</th>
                                                    <th>Prix</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.oneValue.devisautres !== null ?
                                                        this.state.oneValue.devisautres.map((dev,index) => (
                                                            <tr className={dev.sum >= 25 ? "bg-info text-white" : ""}>
                                                                <td
                                                                    onClick={() => {
                                                                        window.location.replace("/devis/autre/"+dev.id)
                                                                    }}
                                                                    className={dev.sum >= 25 ? "":"text-info hlink"}>{dev.autreid.titre}</td>
                                                                <td>{dev.sum >= 25 ? "":  OwnService.format(dev.prix)}</td>
                                                            </tr>
                                                        ))
                                                        :<></>
                                                }

                                                </tbody>
                                            </table>
                                            <button className={"btn btn-success"}
                                                    onClick={()=>{
                                                        window.location.replace("/devis/autre/form")
                                                    }
                                                    }
                                            >Ajouter dépense variable</button>

                                        </>
                                        : <></>
                                }
                            </div>
                        </div>
                    </Border>
                </div>
            </HContainer>
        );
    }
}
