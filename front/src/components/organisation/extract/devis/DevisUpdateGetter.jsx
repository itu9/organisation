import { useParams } from "react-router-dom";

import DevisUpdate from "./DevisUpdate";

export default function DevisUpdateGetter() {
    const {id} = useParams();
    return (
        <DevisUpdate id={id}>
        </DevisUpdate>
    );
}