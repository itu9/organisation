import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import ListePage from "@/components/utils/ListePage";
import GeneralListe from "@/components/utils/GeneralListe";
import React from "react";
import OwnService from "../../../../service/OwnService.jsx";

export default class DevisListe extends GeneralListe {
    constructor(params) {
        super(params);
        this.lieuid = React.createRef(null);
        this.prixlieuhmin = React.createRef(null);
        this.prixlieuhmax = React.createRef(null);
        this.keys = React.createRef(null);
        this.urlData = "/devis/filter";
        this.urlUtils = "/devis/utils";
        this.baseUrl = "/devis"
    }

    componentDidMount() {
        this.init();
    }

    prepareDataListe = () => {
        let data = {
            keys: this.prepare(this.keys)
            ,
            lieuid: this.checkRefNull(this.lieuid, {id: this.prepare(this.lieuid)}, null),
            prixlieuhmin: this.prepare(this.prixlieuhmin),
            prixlieuhmax: this.prepare(this.prixlieuhmax),
            etathmax : 0
        }
        return data;
    }
    findData = () => {
        this.search(this.prepareDataListe());
    }
    onChange = (event, page) => {
        let data = this.prepareDataListe();
        data = {
            ...data,
            page: page
        }
        this.search(data);
    }

    toPdf = () => {
        let data = this.prepareDataListe();
        data = {
            ...data,
            page: -1
        }
        let ans = {
            ...data,
            pdf: {
                titre: "Liste des devis",
                fields: [
                    {
                        field: "titre",
                        titre: "Titre"
                    }, {
                        field: "lieuid",
                        titre: "Lieu"
                    }, {
                        field: "prixlieu",
                        titre: "Prix du lieu"
                    },
                ]
            }
        }
        // console.log('ans',ans);
        this.pdf(ans);

    }

    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                    <div className="col-12">
                        <Border>
                            <ListePage
                                title="Liste des devis"
                                count={this.state.count}
                                onChange={this.onChange}
                                search={
                                    <>
                                        <div className="col-6">
                                            <h3 className="text-info mt-3">Recherche</h3>
                                            <input className="form-control" type="text"
                                                   placeholder="recherche par mot clé" ref={this.keys}/>
                                            <h3 className="text-info mt-3">Lieu</h3>
                                            <select className="form-control" ref={this.lieuid}>
                                                <option value=""></option>
                                                {
                                                    this.state.utils !== undefined && this.state.utils !== null ?
                                                        this.state.utils.lieuid.map((data, index) => (
                                                            <option key={index} value={data.id}>{data.titre}</option>
                                                        ))
                                                        : <></>
                                                }
                                            </select><h3 className="text-info mt-3">Prix du lieu</h3>
                                            <div className="row">
                                                <div className="col-6">
                                                    <input type="text" className="form-control" placeholder="Minimum"
                                                           ref={this.prixlieuhmin}/>
                                                </div>
                                                <div className="col-6">
                                                    <input type="text" className="form-control" placeholder="Maximum"
                                                           ref={this.prixlieuhmax}/>
                                                </div>
                                            </div>
                                            <button className="btn btn-block btn-info mt-3"
                                                    onClick={this.findData}>Rechercher
                                            </button>
                                        </div>
                                        <div className="col-12 mt-5">
                                            <div className="row">
                                                <div className="col-6"></div>
                                                <div className="col-6">
                                                    <div className="row">
                                                        <div className="col-2"></div>
                                                        <div className="col-4">
                                                            <div className="btn-list w-100">
                                                                <div className="btn-group">
                                                                    <button type="button"
                                                                            className="btn btn-info dropdown-toggle w-100"
                                                                            data-toggle="dropdown" aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                        Exporter
                                                                    </button>
                                                                    <div className="dropdown-menu w-100">
                                                                        <a className="dropdown-item " href="#a"
                                                                           onClick={this.toPdf}><i
                                                                            className="fas fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;&nbsp;PDF
                                                                        </a>
                                                                        <a className="dropdown-item " href="#a"><i
                                                                            className="fas fa-file-excel"></i>&nbsp;&nbsp;&nbsp;&nbsp;CSV</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-6">
                                                            <button className="btn btn-block btn-success"
                                                                    onClick={() => {
                                                                        window.location.replace("/devis/form");
                                                                    }}
                                                            >Ajouter un nouveau
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                                head={
                                    <tr>
                                        <td>Titre</td>
                                        <td>Lieu</td>
                                        <td>Prix du lieu</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                }
                            >
                                {this.state.data !== undefined && this.state.data !== null ? this.state.data.map((data, index) => (
                                    <tr>
                                        <td>{data.titre}</td>
                                        <td>{data.lieuid.titre}</td>
                                        <td>{OwnService.format(data.prixlieu) }</td>
                                        <td>
                                            <button className="btn btn-block btn-info" onClick={() => {
                                                window.location.replace("/devis/affichage/" + data.id)
                                            }}>
                                                Affichage
                                            </button>
                                        </td>
                                        <td>
                                            <button className="btn" onClick={() => {
                                                window.location.replace("/devis/update/" + data.id)
                                            }}>
                                                <i className="fas fa-pencil-alt text-warning"></i>
                                            </button>
                                            <button className="btn ml-3" onClick={() => {
                                                this.delete(data)
                                            }}>
                                                <i className="fas fa-trash-alt text-danger"></i>
                                            </button>
                                            <button className="btn ml-3" onClick={() => {
                                                window.location.replace("/devis/" + data.id)
                                            }}>
                                                <i className="fas fa-plus text-info"></i>
                                            </button>
                                        </td>
                                    </tr>)) : <></>}
                            </ListePage>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}