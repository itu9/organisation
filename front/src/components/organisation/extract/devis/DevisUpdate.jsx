import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class DevisUpdate extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.lieuid = React.createRef(null);
        this.prixlieu = React.createRef(null);
        this.date = React.createRef(null);
        this.hour = React.createRef(null);
        this.urlSend = "/devis";
        this.urlUtils = "/devis/utils";
        this.afterValidation = "/devis";
        this.id = React.createRef(null);
    }

    componentDidMount() {
        this.initUpdate();
    }

    actionUpdate = () => {
        this.titre.current.value = this.state.oneValue.titre;
        this.lieuid.current.value = this.state.oneValue.lieuid.id;
        this.prixlieu.current.value = this.state.oneValue.prixlieu;
        this.date.current.value = this.state.oneValue.date;
        this.hour.current.value = this.state.oneValue.hour;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            titre: this.prepare(this.titre),
            lieuid: this.checkRefNull(this.lieuid, {id: this.prepare(this.lieuid)}, null),
            prixlieu: this.prepare(this.prixlieu),
            date: this.prepare(this.date),
            hour: this.prepare(this.hour),
            etat: this.state.oneValue.etat
        }
        this.validate(data)
    }
    //
    // prepareHour = () => {
    //     let hour = ;
    //     if (hour === null) {
    //         return null;
    //     } else {
    //         return hour;
    //     }
    // }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire de modification de devis</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Titre</h3>
                            <input type="text" className="form-control" ref={this.titre}></input>
                            <h3 className="text-info mt-3">Date</h3>
                            <input type="date" className="form-control" ref={this.date}></input>
                            <h3 className="text-info mt-3">Heur</h3>
                            <input type="text" className="form-control" ref={this.hour}></input>
                            <h3
                            className="text-info mt-3">Lieu</h3>
                            <select className="form-control" ref={this.lieuid}>
                                <option value=""></option>
                                {
                                    this.state.utils !== undefined && this.state.utils !== null ?
                                        this.state.utils.lieuid.map((data, index) => (
                                            <option key={index} value={data.id}>{data.titre}</option>
                                        ))
                                        : <></>
                                }
                            </select><h3 className="text-info mt-3">Prix du lieu</h3>
                            <input type="text" className="form-control" ref={this.prixlieu}></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}