import { useParams } from "react-router-dom";
import DepensefixeInfo from "./DepensefixeInfo";
export default function DepensefixeFicheGetter() {
    const {id} = useParams();
    return (
        <DepensefixeInfo id={id}>
        </DepensefixeInfo>
    );
}