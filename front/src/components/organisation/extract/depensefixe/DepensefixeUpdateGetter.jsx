import { useParams } from "react-router-dom";

import DepensefixeUpdate from "./DepensefixeUpdate";

export default function DepensefixeUpdateGetter() {
    const {id} = useParams();
    return (
        <DepensefixeUpdate id={id}>
        </DepensefixeUpdate>
    );
}