import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class DepensefixeUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);this.tarif = React.createRef(null);this.typeid = React.createRef(null);this.typedepenseid = React.createRef(null);this.duree = React.createRef(null);
        this.urlSend = "/depensefixe";
        this.urlUtils = "/depensefixe/utils";
        this.afterValidation = "/depensefixe";
        this.id = React.createRef(null);
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.titre.current.value = this.state.oneValue.titre;this.tarif.current.value = this.state.oneValue.tarif;this.typeid.current.value = this.state.oneValue.typeid.id;this.typedepenseid.current.value = this.state.oneValue.typedepenseid.id;this.duree.current.value = this.state.oneValue.duree;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            titre : this.prepare(this.titre),tarif : this.prepare(this.tarif),typeid : this.checkRefNull(this.typeid,{id:this.prepare(this.typeid)},null),typedepenseid : this.checkRefNull(this.typedepenseid,{id:this.prepare(this.typedepenseid)},null),duree : this.prepare(this.duree)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire de modification de depense fixe</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Titre du depense</h3>
<input type="text" className="form-control" ref={this.titre} ></input><h3 className="text-info mt-3">Tarif periodique</h3>
<input type="text" className="form-control" ref={this.tarif} ></input><h3 className="text-info mt-3" >Type</h3>
<select className="form-control" ref={this.typeid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.typeid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3" >Type de depense</h3>
<select className="form-control" ref={this.typedepenseid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.typedepenseid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Duree</h3>
<input type="text" className="form-control" ref={this.duree} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}