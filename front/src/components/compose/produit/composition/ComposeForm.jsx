import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class ComposeForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.produitid = React.createRef(null);
    this.ingredientid = React.createRef(null);
    this.quantite = React.createRef(null);
    this.urlSend = "/compose";
    this.urlUtils = "/compose/utils";
    this.afterValidation = "/compose";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      produitid: this.checkRefNull(
        this.produitid,
        { id: this.prepare(this.produitid) },
        null
      ),
      ingredientid: this.checkRefNull(
        this.ingredientid,
        { id: this.prepare(this.ingredientid) },
        null
      ),
      quantite: this.prepare(this.quantite),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire de composition</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Produit</h3>
              <select className="form-control" ref={this.produitid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.produitid.map((data, index) => (
                    <option key={index} value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Ingredient</h3>
              <select className="form-control" ref={this.ingredientid}>
                <option value=""></option>
                {this.state.utils !== undefined && this.state.utils !== null ? (
                  this.state.utils.ingredientid.map((data, index) => (
                    <option key={index} value={data.id}>{data.titre}</option>
                  ))
                ) : (
                  <></>
                )}
              </select>
              <h3 className="text-info mt-3">Quantite</h3>
              <input
                type="text"
                className="form-control"
                ref={this.quantite}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
