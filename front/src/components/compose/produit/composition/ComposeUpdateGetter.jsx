import { useParams } from "react-router-dom";

import ComposeUpdate from "./ComposeUpdate";

export default function ComposeUpdateGetter() {
    const {id} = useParams();
    return (
        <ComposeUpdate id={id}>
        </ComposeUpdate>
    );
}