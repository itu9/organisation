import { useParams } from "react-router-dom";
import ComposeInfo from "./ComposeInfo";
export default function ComposeFicheGetter() {
    const {id} = useParams();
    return (
        <ComposeInfo id={id}>
        </ComposeInfo>
    );
}