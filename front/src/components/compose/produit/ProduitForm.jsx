import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class ProduitForm extends GeneralForm {
  constructor(params) {
    super(params);
    this.titre = React.createRef(null);
    this.pu = React.createRef(null);
    this.pvente = React.createRef(null);
    this.quantite = React.createRef(null);
    this.urlSend = "/produit";
    this.urlUtils = "/produit/utils";
    this.afterValidation = "/produit";
  }
  componentDidMount() {
    this.init();
  }

  validateData = () => {
    let data = {
      titre: this.prepare(this.titre),
      pu: this.prepare(this.pu),
      pvente: this.prepare(this.pvente),
      quantite: this.prepare(this.quantite),
    };
    this.validate(data);
  };

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Formulaire de produit</h1>
        <div className="row mt-3">
          <div className="col-8">
            <Border>
              <h3 className="text-info mt-3">Titre</h3>
              <input
                type="text"
                className="form-control"
                ref={this.titre}
              ></input>
              <h3 className="text-info mt-3">Prix</h3>
              <input type="text" className="form-control" ref={this.pu}></input>
              <h3 className="text-info mt-3">Prix de vente</h3>
              <input type="text" className="form-control" ref={this.pvente}></input>
              <h3 className="text-info mt-3">Quantite</h3>
              <input
                type="text"
                className="form-control"
                ref={this.quantite}
              ></input>
              <button
                onClick={this.validateData}
                className="mt-3 btn btn-success btn-block"
              >
                Valider
              </button>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
