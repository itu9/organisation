import { useParams } from "react-router-dom";
import ProduitInfo from "./ProduitInfo";
export default function ProduitFicheGetter() {
    const {id} = useParams();
    return (
        <ProduitInfo id={id}>
        </ProduitInfo>
    );
}