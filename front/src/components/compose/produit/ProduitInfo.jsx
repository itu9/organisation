import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../service/OwnService";
import React from "react";
import '../stock/Stock.css';

export default class ProduitInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/produit";
    this.urlUtils = "/produit/utils";
    this.quantite = React.createRef(null);
    this.demande = React.createRef(null);
    this.percent = React.createRef(null);
  }

  componentDidMount() {
    this.initUpdate();
    this.setState({
      compose : [],
      propose : 0
    })
  }

  actionUpdate = ()=> {
    this.getQuantite(this.state.oneValue.quantite)
    this.setState({
      propose : this.state.oneValue.pvente
    })
  }

  getQuantite = (quantite) => {
    this.getListe(this.url+"/compose/produit/"+this.props.id+"/quantite/"+quantite,
        data => {
          this.verifData(data,
              response => {
                this.setState({
                  compose : response.data
                })
              }
          )
      },
      // eslint-disable-next-line no-unused-vars
      header => {

      }
    )
  }

  equilibrer = () => {
    this.getListe(this.url+this.urlSend+"/"+this.props.id+"/equilibrer",
        data => {
          this.verifData(data,
              // eslint-disable-next-line no-unused-vars
              response => {
                  window.location.reload();
              }
          )
      },
      // eslint-disable-next-line no-unused-vars
      header => {

      }
    )
  }

  creer = () => {
    if (this.quantite.current.value !== '') {
      this.getListe(this.url+this.urlSend+"/"+this.props.id+"/quantite/"+this.quantite.current.value,
          data => {
            this.verifData(data,
                // eslint-disable-next-line no-unused-vars
                response => {
                    window.location.replace("/stock");
                }
            )
        },
        // eslint-disable-next-line no-unused-vars
        header => {

        }
      )
    }
  }

  load = () => {
    let quantite = this.demande.current.value
    if (quantite === '') 
      quantite = this.state.oneValue.quantite
    this.getQuantite(quantite)
    
  }

  evalPercent = (price,base) => {
    if (base === 0)
      return 100
    let diff = price-base
    return diff / base * 100
  }

  askPrice = () => {
    let percent = this.percent.current.value;
    if (percent !== '') {
      let iPercent = parseFloat(percent, 10)
      if (!isNaN(iPercent)) {
        let plus = (this.state.oneValue.pu !== 0) ? (this.state.oneValue.pu * iPercent / 100.) : 0;
        let val = this.state.oneValue.pu + plus
        this.setState({
          propose: val
        })
      }
    }
  }

  equilibrerPvente = () => {
    if (this.percent.current.value !== '') {
      this.getListe(this.url+this.urlSend+"/"+this.props.id+"/equilibrer/percent/"+this.percent.current.value,
          data => {
            this.verifData(data,
                // eslint-disable-next-line no-unused-vars
                response => {
                  window.location.reload()
                }
            )
          },
          // eslint-disable-next-line no-unused-vars
          header => {

          }
      )
    }
  }
  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Fiche de details de produit</h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-warning btn-block"
                      onClick={() => {
                        window.location.replace(
                          "/produit/update/" + this.props.id
                        );
                      }}
                    >
                      Modifier
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/produit");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-success btn-block"
                      onClick={() => {
                        window.location.replace("/produit/form/");
                      }}
                    >
                      Ajouter
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={() => {
                        this.delete();
                      }}
                    >
                      Suprimer
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-outline-success btn-block"
                      onClick={() => {
                        this.equilibrer();
                      }}
                    >
                      Equilibrer
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-dark mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Titre</h3>
                    <p className="text-dark">{this.state.oneValue.titre}</p>
                    <h3 className="text-info">Prix</h3>
                    <p className="text-dark">{OwnService.format(this.state.oneValue.pu) }</p>
                    <h3 className="text-info">Prix de vente</h3>
                    <p className="text-dark">{OwnService.format(this.state.oneValue.pvente) }</p>
                    <h3 className="text-info">Bénéfice</h3>
                    <p className="text-dark">{OwnService.format(this.state.oneValue.benefice) }</p>
                    <h3 className="text-info">Quantite</h3>
                    <p className="text-dark">{this.state.oneValue.quantite}</p>

                    <h3 className="text-info">Stock</h3>
                    <p className="text-dark">{
                      this.state.oneValue.stock != null ?
                      this.state.oneValue.stock.quantite : 0
                    }</p>
                    <h3 className="text-info">Valeur en stock</h3>
                    <p className="text-dark">{
                      OwnService.format(this.state.oneValue.stock != null ?
                        this.state.oneValue.stock.prix : 0)
                      
                    }</p>
                    <h3 className="text-info">Limite de creation</h3>
                    <p className="text-dark">{
                      this.state.oneValue.possible
                    }</p>
                    <h2 className="text-dark mt-5">Equilibrer le prix de vente</h2>
                    <div className="row">
                      <div className="col-6">
                        <h3 className="text-info">Prix equivalent</h3>
                        <p className="text-dark">{this.state.propose}</p>
                        <h3 className="text-info">Pourcentage</h3>
                        <input className="form-control" placeholder="quantité à demander  "
                               defaultValue={this.evalPercent(this.state.oneValue.pvente,this.state.oneValue.pu)} ref={this.percent}
                               onChange={() => {this.askPrice()}}/>
                        <button className="btn btn-block btn-success mt-3" onClick={()=>{this.equilibrerPvente()}}>Equilibrer</button>
                      </div>
                    </div>
                    <h2 className="text-dark mt-5">Compositions de {this.state.oneValue.quantite} de
                      {this.state.oneValue.titre}: </h2>
                    <table className="table mt-3">
                      <thead className="text-white bg-info">
                        <tr>
                          <th>Produit</th>
                          <th>Quantité</th>
                          <th>Valeur</th>
                          <th>Stock</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.state.oneValue.compositions.map((comp,index) => (
                            <tr key={index}>
                              <td className="hstock text-info" onClick={()=>{window.location.replace("/produit/"+comp.ingredientid.id)}}>{comp.ingredientid.titre}</td>
                              <td>{comp.quantite}</td>
                              <td>{OwnService.format(comp.prix)}</td>
                              <td>{comp.stock !== null ? comp.stock.quantite:0}</td>

                            </tr>
                          ))
                        }
                        
                      </tbody>
                    </table>
                    {
                          this.state.compose !== undefined && this.state.compose !== null ?
                    <>
                    <h2 className="text-dark mt-5">Demande de composition </h2>
                    <div className="row">
                      <div className="col-6">
                        <h3 className="text-info">Pour quantité</h3>
                        <input className="form-control" placeholder="quantité à demander  " defaultValue={this.state.oneValue.quantite} ref={this.demande} onChange={() => {this.load()}}/>
                      </div>
                      </div>
                    <table className="table mt-3">
                      <thead className="text-white bg-info">
                        <tr>
                          <th>Produit</th>
                          <th>Quantité</th>
                          <th>Valeur</th>
                          <th>Stock</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.state.compose.map((comp,index) => (
                            <tr key={index}>
                              <td className="hstock text-info" onClick={()=>{window.location.replace("/produit/"+comp.ingredientid.id)}}>{comp.ingredientid.titre}</td>
                              <td className={!comp.enougth ? "text-danger":"" }>{comp.quantite}</td>
                              <td className={comp.sum >= 50 ? "text-info":""}>{OwnService.format(comp.prix)}</td>
                              <td>{comp.stock !== null ? comp.stock.quantite:(comp.sum >= 50 ? null : 0)}</td>

                            </tr>
                          )) 
                        }
                        
                      </tbody>
                    </table>
                    </>
                    : <></>}
                    <button className="btn btn-block btn-info" onClick={()=>{window.location.replace("/compose/form")}}>Ajouter</button>
                    {this.state.oneValue.compositions.length > 0 ? (
                      <div className="row">
                      <div className="col-6">
                        <h2 className="text-dark mt-5">Création </h2>
                        <h3 className="text-info">Quantite</h3>
                        <input className="form-control" placeholder="quantité à créer" ref={this.quantite}/>
                        <button className="btn btn-block btn-success mt-3" onClick={()=>{this.creer()}}>Creer</button>
                      </div>

                      </div>
                    ) : <></>}
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
