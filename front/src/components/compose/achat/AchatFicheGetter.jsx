import { useParams } from "react-router-dom";
import AchatInfo from "./AchatInfo";
export default function AchatFicheGetter() {
    const {id} = useParams();
    return (
        <AchatInfo id={id}>
        </AchatInfo>
    );
}