import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";

export default class ProduitInfo extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.urlSend = "/produit";
        this.urlUtils = "/produit/utils";
    }

    componentDidMount() {
        this.initUpdate();
    }

    render() {
        return (
             <HContainer>
             
                <h1 className="text-dark">Fiche de details de produit</h1>
                <div className="mt-5">
                    <Border>
                    <div className="row">
                        <div className="col-3">
                            <h2 className="text-info">Alternatives</h2>
                            <div className="row mt-3 mb-3">
                                <div className="col-12 mb-3">
                                    <button className="btn btn-warning btn-block"
                                        onClick={
                                            () => {
                                                window.location.replace("/produit/update/"+this.props.id)
                                            }
                                        }
                                    >Modifier</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-info btn-block"
                                        onClick={
                                            () => {
                                                window.location.replace("/produit")
                                            }
                                        }
                                    >Liste</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-success btn-block" 
                                        onClick={
                                            () => {
                                                window.location.replace("/produit/form/")
                                            }
                                        }
                                    >Ajouter</button>
                                </div>
                                <div className="col-12 mb-3">
                                    <button className="btn btn-danger btn-block" 
                                        onClick={
                                            () => {
                                                this.delete();
                                            }
                                        }
                                    >Suprimer</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-8">
                            <h2 className="text-info mb-3">Information</h2>
                            {
                                this.state.oneValue !== null && this.state.oneValue !== undefined ?
                                <>
                                    <h3 className="text-info">Titre</h3>
<p className="text-dark">{this.state.oneValue.titre}</p><h3 className="text-info">Prix</h3>
<p className="text-dark">{this.state.oneValue.pu}</p><h3 className="text-info">Quantite</h3>
<p className="text-dark">{this.state.oneValue.quantite}</p><h3 className="text-info">Prix de vente</h3>
<p className="text-dark">{this.state.oneValue.pvente}</p><h3 className="text-info">Possibilite de creation</h3>
<p className="text-dark">{this.state.oneValue.possible}</p>
                                </>
                                :<></>
                            }
                        </div>
                    </div>
                    </Border>
                </div>
             </HContainer>
        );
    }
}
