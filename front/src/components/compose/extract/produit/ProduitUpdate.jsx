import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class ProduitUpdate extends GeneralUpdate {
    constructor(params) {
        super(params);
        this.titre = React.createRef(null);
        this.pu = React.createRef(null);
        this.quantite = React.createRef(null);
        this.pvente = React.createRef(null);
        this.urlSend = "/produit";
        this.urlUtils = "/produit/utils";
        this.afterValidation = "/produit";
        this.id = React.createRef(null);
    }

    componentDidMount() {
        this.initUpdate();
    }

    actionUpdate = () => {
        this.titre.current.value = this.state.oneValue.titre;
        this.pu.current.value = this.state.oneValue.pu;
        this.quantite.current.value = this.state.oneValue.quantite;
        this.pvente.current.value = this.state.oneValue.pvente;
    }
    validateData = () => {
        let data = {
            id: this.prepare(this.id),
            titre: this.prepare(this.titre),
            pu: this.prepare(this.pu),
            quantite: this.prepare(this.quantite),
            pvente: this.prepare(this.pvente),
        }
        this.validate(data)
    }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark">Formulaire de modification de produit</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3">Titre</h3>
                            <input type="text" className="form-control" ref={this.titre}></input><h3
                            className="text-info mt-3">Prix</h3>
                            <input type="text" className="form-control" ref={this.pu}></input><h3
                            className="text-info mt-3">Quantite</h3>
                            <input type="text" className="form-control" ref={this.quantite}></input><h3
                            className="text-info mt-3">Prix de vente</h3>
                            <input type="text" className="form-control" ref={this.pvente}></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider
                            </button>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}