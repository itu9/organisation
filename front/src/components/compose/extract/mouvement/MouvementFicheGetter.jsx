import { useParams } from "react-router-dom";
import MouvementInfo from "./MouvementInfo";
export default function MouvementFicheGetter() {
    const {id} = useParams();
    return (
        <MouvementInfo id={id}>
        </MouvementInfo>
    );
}