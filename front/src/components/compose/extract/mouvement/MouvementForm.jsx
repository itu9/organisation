import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class MouvementForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.ENTREE = React.createRef(null);this.SORTIE = React.createRef(null);this.produitid = React.createRef(null);this.quantite = React.createRef(null);this.date = React.createRef(null);this.etat = React.createRef(null);
        this.urlSend = "/mouvement";
        this.urlUtils = "/mouvement/utils";
        this.afterValidation = "/mouvement";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            ENTREE : this.prepare(this.ENTREE),SORTIE : this.prepare(this.SORTIE),produitid : this.checkRefNull(this.produitid,{id:this.prepare(this.produitid)},null),quantite : this.prepare(this.quantite),date : this.prepare(this.date),etat : this.prepare(this.etat)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire d'ajout de mouvement</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3">ENTREE</h3>
<input type="text" className="form-control" ref={this.ENTREE} ></input><h3 className="text-info mt-3">SORTIE</h3>
<input type="text" className="form-control" ref={this.SORTIE} ></input><h3 className="text-info mt-3" >Produit</h3>
<select className="form-control" ref={this.produitid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.produitid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Quantite</h3>
<input type="text" className="form-control" ref={this.quantite} ></input><h3 className="text-info mt-3">Date</h3>
<input type="date" className="form-control" ref={this.date} ></input><h3 className="text-info mt-3">Etat</h3>
<input type="text" className="form-control" ref={this.etat} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}