import { useParams } from "react-router-dom";

import MouvementUpdate from "./MouvementUpdate";

export default function MouvementUpdateGetter() {
    const {id} = useParams();
    return (
        <MouvementUpdate id={id}>
        </MouvementUpdate>
    );
}