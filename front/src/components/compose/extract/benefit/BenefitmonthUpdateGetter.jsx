import { useParams } from "react-router-dom";

import BenefitmonthUpdate from "./BenefitmonthUpdate";

export default function BenefitmonthUpdateGetter() {
    const {id} = useParams();
    return (
        <BenefitmonthUpdate id={id}>
        </BenefitmonthUpdate>
    );
}