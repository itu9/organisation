import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralForm from "@/components/utils/GeneralForm";
import React from "react";

export default class BenefitmonthForm  extends GeneralForm  {
    constructor(params) {
        super(params);
        this.produitid = React.createRef(null);this.vente = React.createRef(null);this.depense = React.createRef(null);this.benefit = React.createRef(null);this.annee = React.createRef(null);this.mois = React.createRef(null);
        this.urlSend = "/benefit/month";
        this.urlUtils = "/benefit/month/utils";
        this.afterValidation = "/benefit/month";
    }
    componentDidMount() {
        this.init();
    }

    validateData = () => {
        let data = {
            produitid : this.checkRefNull(this.produitid,{id:this.prepare(this.produitid)},null),vente : this.prepare(this.vente),depense : this.prepare(this.depense),benefit : this.prepare(this.benefit),annee : this.prepare(this.annee),mois : this.prepare(this.mois)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <h3 className="text-info mt-3" >Produit</h3>
<select className="form-control" ref={this.produitid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.produitid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Vente</h3>
<input type="text" className="form-control" ref={this.vente} ></input><h3 className="text-info mt-3">Depense</h3>
<input type="text" className="form-control" ref={this.depense} ></input><h3 className="text-info mt-3">Benefice</h3>
<input type="text" className="form-control" ref={this.benefit} ></input><h3 className="text-info mt-3">Annee</h3>
<input type="text" className="form-control" ref={this.annee} ></input><h3 className="text-info mt-3">Mois</h3>
<input type="text" className="form-control" ref={this.mois} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}