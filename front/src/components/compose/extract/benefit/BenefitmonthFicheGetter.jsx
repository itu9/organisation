import { useParams } from "react-router-dom";
import BenefitmonthInfo from "./BenefitmonthInfo";
export default function BenefitmonthFicheGetter() {
    const {id} = useParams();
    return (
        <BenefitmonthInfo id={id}>
        </BenefitmonthInfo>
    );
}