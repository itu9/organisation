import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import ListePage from "@/components/utils/ListePage";
import GeneralListe from "@/components/utils/GeneralListe";
import React from "react";

export default class BenefitmonthListe extends GeneralListe {
    constructor(params) {
        super(params);
        this.produitid = React.createRef(null);this.ventehmin = React.createRef(null);this.ventehmax = React.createRef(null);this.depensehmin = React.createRef(null);this.depensehmax = React.createRef(null);this.benefithmin = React.createRef(null);this.benefithmax = React.createRef(null);this.anneehmin = React.createRef(null);this.anneehmax = React.createRef(null);this.moishmin = React.createRef(null);this.moishmax = React.createRef(null);
        this.keys = React.createRef(null);
        this.urlData = "/benefit/month/filter";
        this.urlUtils = "/benefit/month/utils";
        this.baseUrl = "/benefit/month"
    }
    componentDidMount() {
        this.init();
    }
    prepareDataListe = () => {
        let data = {
            keys : this.prepare(this.keys)
            ,produitid : this.checkRefNull(this.produitid,{id:this.prepare(this.produitid)},null),ventehmin : this.prepare(this.ventehmin),ventehmax : this.prepare(this.ventehmax),depensehmin : this.prepare(this.depensehmin),depensehmax : this.prepare(this.depensehmax),benefithmin : this.prepare(this.benefithmin),benefithmax : this.prepare(this.benefithmax),anneehmin : this.prepare(this.anneehmin),anneehmax : this.prepare(this.anneehmax),moishmin : this.prepare(this.moishmin),moishmax : this.prepare(this.moishmax)
        }
        return data;
    }
    findData = () => {
        this.search(this.prepareDataListe());
    }
    onChange = (event,page) => {
        let data = this.prepareDataListe();
        data = {
        ...data,
        page : page
        }
        this.search(data);
    }

    toPdf = ()=> {
        let data = this.prepareDataListe();
        data = {
            ...data,
            page : -1
        }
        let ans = {
            ...data,
            pdf : {
                titre : "Titre de la liste",
                fields : [
                    {
    field : "produitid",
    titre : "Produit"
},{
    field : "vente",
    titre : "Vente"
},{
    field : "depense",
    titre : "Depense"
},{
    field : "benefit",
    titre : "Benefice"
},{
    field : "annee",
    titre : "Annee"
},{
    field : "mois",
    titre : "Mois"
},
                ]
            }
        }
        // console.log('ans',ans);
        this.pdf(ans);

    }
    render() {
        return (
            <HContainer>
                <div className="row mt-3">
                    <div className="col-12">
                        <Border>
                            <ListePage
                                title="Titre de la liste"
                                count={this.state.count}
                                onChange={this.onChange}
                                search = {
                                    <>
                                        <div className="col-6">
                                            <h3 className="text-info mt-3">Recherche</h3>
                                            <input className="form-control" type="text" placeholder="recherche par mot clé" ref={this.keys}/>
                                            <h3 className="text-info mt-3" >Produit</h3>
<select className="form-control" ref={this.produitid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.produitid.map((data,index) => (
            <option key={index} value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Vente</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.ventehmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.ventehmax} />
    </div>
</div><h3 className="text-info mt-3">Depense</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.depensehmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.depensehmax} />
    </div>
</div><h3 className="text-info mt-3">Benefice</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.benefithmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.benefithmax} />
    </div>
</div><h3 className="text-info mt-3">Annee</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.anneehmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.anneehmax} />
    </div>
</div><h3 className="text-info mt-3">Mois</h3>
<div className="row">
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Minimum" ref={this.moishmin} />
    </div>
    <div className="col-6">
        <input type="text" className="form-control" placeholder="Maximum" ref={this.moishmax} />
    </div>
</div>
                                            <button className="btn btn-block btn-info mt-3" onClick={this.findData}>Rechercher</button>
                                        </div>
                                        <div className="col-12 mt-5">
                                            <div className="row">
                                                <div className="col-6"></div>
                                                <div className="col-6">
                                                    <div className="row">
                                                        <div className="col-2"></div>
                                                        <div className="col-4">
                                                        <div className="btn-list w-100">
                                                            <div className="btn-group">
                                                                <button type="button" className="btn btn-info dropdown-toggle w-100"
                                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Exporter
                                                                </button>
                                                                <div className="dropdown-menu w-100">
                                                                    <a className="dropdown-item " href="#a" onClick={this.toPdf}><i className="fas fa-file-pdf"></i>&nbsp;&nbsp;&nbsp;&nbsp;PDF </a>
                                                                    <a className="dropdown-item " href="#a"><i className="fas fa-file-excel"></i>&nbsp;&nbsp;&nbsp;&nbsp;CSV</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <div className="col-6">
                                                            <button className="btn btn-block btn-success"
                                                                onClick={() => {
                                                                    window.location.replace("/benefit/month/form");
                                                                }}
                                                            >Ajouter un nouveau</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                                head = {
                                    <tr><td>Produit</td><td>Vente</td><td>Depense</td><td>Benefice</td><td>Annee</td><td>Mois</td><td></td></tr>
                                }
                            >
                            {this.state.data !== undefined && this.state.data !== null ?this.state.data.map((data,index) =>(<tr><td>{data.produitid.titre}</td><td>{data.vente}</td><td>{data.depense}</td><td>{data.benefit}</td><td>{data.annee}</td><td>{data.mois}</td> <td>
    <button className="btn" onClick={()=> {
        window.location.replace("/benefit/month/update/"+data.id)
    }}>
        <i className="fas fa-pencil-alt text-warning"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        this.delete(data)
    }}>
        <i className="fas fa-trash-alt text-danger"></i>
    </button>
    <button className="btn ml-3" onClick={()=> {
        window.location.replace("/benefit/month/"+data.id)
    }}>
        <i className="fas fa-plus text-info"></i>
    </button>
</td></tr>)):<></>}
                            </ListePage>
                        </Border>
                    </div>
                </div>
            </HContainer>
        );
    }
}