import { useParams } from "react-router-dom";
import VenteInfo from "./VenteInfo";
export default function VenteFicheGetter() {
    const {id} = useParams();
    return (
        <VenteInfo id={id}>
        </VenteInfo>
    );
}