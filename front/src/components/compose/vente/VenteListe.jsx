import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import ListePage from "@/components/utils/ListePage";
import GeneralListe from "@/components/utils/GeneralListe";
import React from "react";

export default class VenteListe extends GeneralListe {
  constructor(params) {
    super(params);
    this.produitid = React.createRef(null);
    this.datehmin = React.createRef(null);
    this.datehmax = React.createRef(null);
    this.quantitehmin = React.createRef(null);
    this.quantitehmax = React.createRef(null);
    this.keys = React.createRef(null);
    this.urlData = "/vente/filter";
    this.urlUtils = "/vente/utils";
    this.baseUrl = "/vente";
  }
  componentDidMount() {
    this.init();
  }
  prepareDataListe = () => {
    let data = {
      keys: this.prepare(this.keys),
      produitid: this.checkRefNull(
        this.produitid,
        { id: this.prepare(this.produitid) },
        null
      ),
      datehmin: this.prepare(this.datehmin),
      datehmax: this.prepare(this.datehmax),
      quantitehmin: this.prepare(this.quantitehmin),
      quantitehmax: this.prepare(this.quantitehmax),
    };
    return data;
  };
  findData = () => {
    this.search(this.prepareDataListe());
  };
  onChange = (event, page) => {
    let data = this.prepareDataListe();
    data = {
      ...data,
      page: page,
    };
    this.search(data);
  };
  render() {
    return (
      <HContainer>
        <div className="row mt-3">
          <div className="col-12">
            <Border>
              <ListePage
                title="Liste des ventes"
                count={this.state.count}
                onChange={this.onChange}
                search={
                  <>
                    <div className="col-6">
                      <h3 className="text-info mt-3">Recherche</h3>
                      <input
                        className="form-control"
                        type="text"
                        placeholder="recherche par mot clé"
                        ref={this.keys}
                      />
                      <h3 className="text-info mt-3">Produit</h3>
                      <select className="form-control" ref={this.produitid}>
                        <option value=""></option>
                        {this.state.utils !== undefined &&
                        this.state.utils !== null ? (
                          this.state.utils.produitid.map((data, index) => (
                            <option key={index} value={data.id}>{data.titre}</option>
                          ))
                        ) : (
                          <></>
                        )}
                      </select>
                      <h3 className="text-info mt-3">Date</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.datehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.datehmax}
                          />
                        </div>
                      </div>
                      <h3 className="text-info mt-3">Quantite</h3>
                      <div className="row">
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Minimum"
                            ref={this.quantitehmin}
                          />
                        </div>
                        <div className="col-6">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Maximum"
                            ref={this.quantitehmax}
                          />
                        </div>
                      </div>
                      <button
                        className="btn btn-block btn-info mt-3"
                        onClick={this.findData}
                      >
                        Rechercher
                      </button>
                    </div>
                    <div className="col-12 mt-5">
                      <div className="row">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <div className="row">
                            <div className="col-2"></div>
                            <div className="col-4">
                              <div className="btn-list w-100">
                                <div className="btn-group">
                                  <button
                                    type="button"
                                    className="btn btn-info dropdown-toggle w-100"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    Exporter
                                  </button>
                                  <div className="dropdown-menu w-100">
                                    <a className="dropdown-item " href="#a">
                                      <i className="fas fa-file-pdf"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;PDF{" "}
                                    </a>
                                    <a className="dropdown-item " href="#a">
                                      <i className="fas fa-file-excel"></i>
                                      &nbsp;&nbsp;&nbsp;&nbsp;CSV
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6">
                              <button
                                className="btn btn-block btn-success"
                                onClick={() => {
                                  window.location.replace("/vente/form");
                                }}
                              >
                                Ajouter un nouveau
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                }
                head={
                  <tr>
                    <td>Produit</td>
                    <td>Date</td>
                    <td>Quantite</td>
                    <td></td>
                  </tr>
                }
              >
                {this.state.data !== undefined && this.state.data !== null ? (
                  this.state.data.map((data, index) => (
                    <tr key={index}>
                      <td>{data.produitid.titre}</td>
                      <td>{data.date}</td>
                      <td>{data.quantite}</td>
                      <td>
                        <button
                          className="btn"
                          onClick={() => {
                            window.location.replace("/vente/update/" + data.id);
                          }}
                        >
                          <i className="fas fa-pencil-alt text-warning"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            this.delete(data);
                          }}
                        >
                          <i className="fas fa-trash-alt text-danger"></i>
                        </button>
                        <button
                          className="btn ml-3"
                          onClick={() => {
                            window.location.replace("/vente/" + data.id);
                          }}
                        >
                          <i className="fas fa-plus text-info"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <></>
                )}
              </ListePage>
            </Border>
          </div>
        </div>
      </HContainer>
    );
  }
}
