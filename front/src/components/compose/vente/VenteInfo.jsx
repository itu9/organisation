import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import OwnService from "../../../service/OwnService";

export default class VenteInfo extends GeneralUpdate {
  constructor(params) {
    super(params);
    this.urlSend = "/vente";
    this.urlUtils = "/vente/utils";
  }

  componentDidMount() {
    this.initUpdate();
  }

  render() {
    return (
      <HContainer>
        <h1 className="text-dark">Fiche de details de vente</h1>
        <div className="mt-5">
          <Border>
            <div className="row">
              <div className="col-3">
                <h2 className="text-info">Alternatives</h2>
                <div className="row mt-3 mb-3">
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-warning btn-block"
                      onClick={() => {
                        window.location.replace(
                          "/vente/update/" + this.props.id
                        );
                      }}
                    >
                      Modifier
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-info btn-block"
                      onClick={() => {
                        window.location.replace("/vente");
                      }}
                    >
                      Liste
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-success btn-block"
                      onClick={() => {
                        window.location.replace("/vente/form/");
                      }}
                    >
                      Ajouter
                    </button>
                  </div>
                  <div className="col-12 mb-3">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={() => {
                        this.delete();
                      }}
                    >
                      Suprimer
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-8">
                <h2 className="text-info mb-3">Information</h2>
                {this.state.oneValue !== null &&
                this.state.oneValue !== undefined ? (
                  <>
                    <h3 className="text-info">Produit</h3>
                    <p className="text-dark">
                      {this.state.oneValue.produitid.titre}
                    </p>
                    <h3 className="text-info">Date</h3>
                    <p className="text-dark">{OwnService.formatDate( this.state.oneValue.date)}</p>
                    <h3 className="text-info">Quantite</h3>
                    <p className="text-dark">{this.state.oneValue.quantite}</p>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </Border>
        </div>
      </HContainer>
    );
  }
}
