import HContainer from "@/components/client/nav/HContainer";
import Border from "@/components/utils/Border";
import GeneralUpdate from "@/components/utils/GeneralUpdate";
import React from "react";

export default class StockUpdate  extends GeneralUpdate  {
    constructor(params) {
        super(params);
        this.produitid = React.createRef(null);this.quantite = React.createRef(null);this.prix = React.createRef(null);this.id = React.createRef(null);
        this.urlSend = "/stock";
        this.urlUtils = "/stock/utils";
        this.afterValidation = "/stock";
    }
    componentDidMount() {
        this.initUpdate();
    }
    actionUpdate = ()=> {
        this.produitid.current.value = this.state.oneValue.produitid.id;this.quantite.current.value = this.state.oneValue.quantite;this.prix.current.value = this.state.oneValue.prix;this.id.current.value = this.state.oneValue.id;
    }
    validateData = () => {
        let data = {
            produitid : this.checkRefNull(this.produitid,{id:this.prepare(this.produitid)},null),quantite : this.prepare(this.quantite),prix : this.prepare(this.prix),id : this.prepare(this.id)
        }
        this.validate(data)
    }

    render() {
        return (
             <HContainer>
                <h1 className="text-dark">Formulaire</h1>
                <div className="row mt-3">
                    <div className="col-8">
                        <Border>
                            <input type="hidden" ref={this.id} value={this.props.id}/>
                            <h3 className="text-info mt-3" >Produit</h3>
<select className="form-control" ref={this.produitid}>
    <option value=""></option>
    {
        this.state.utils !== undefined && this.state.utils !== null?
        this.state.utils.produitid.map((data,index) => (
            <option value={data.id} >{data.titre}</option>
        ))
        :<></>
    }
</select><h3 className="text-info mt-3">Quantite</h3>
<input type="text" className="form-control" ref={this.quantite} ></input><h3 className="text-info mt-3">Prix</h3>
<input type="text" className="form-control" ref={this.prix} ></input><h3 className="text-info mt-3">Id</h3>
<input type="text" className="form-control" ref={this.id} ></input>
                            <button onClick={this.validateData} className="mt-3 btn btn-success btn-block">Valider</button>
                        </Border>
                    </div>
                </div>
             </HContainer>
        );
    }
}