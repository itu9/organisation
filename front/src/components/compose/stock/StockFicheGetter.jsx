import { useParams } from "react-router-dom";
import StockInfo from "./StockInfo";
export default function StockFicheGetter() {
    const {id} = useParams();
    return (
        <StockInfo id={id}>
        </StockInfo>
    );
}