import AuthAdmin from "@/components/authentification/AuthAdmin";
import AuthClient from "./components/authentification/AuthClient";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import './App.css';
import AffClassGetter from "./service/assistant/AffClassGetter";
import AfficheGetter from "./service/assistant/AfficheGetter";
import GetPreview from "./service/assistant/GetPreview";
import Histant from "./service/assistant/Histant";
import LieuListe from "./components/organisation/extract/lieu/LieuListe";
import LieuFicheGetter from "./components/organisation/extract/lieu/LieuFicheGetter";
import LieuForm from "./components/organisation/extract/lieu/LieuForm";
import LieuUpdateGetter from "./components/organisation/extract/lieu/LieuUpdateGetter";
import DepensefixeListe from "./components/organisation/extract/depensefixe/DepensefixeListe";
import DepensefixeFicheGetter from "./components/organisation/extract/depensefixe/DepensefixeFicheGetter";
import DepensefixeForm from "./components/organisation/extract/depensefixe/DepensefixeForm";
import DepensefixeUpdateGetter from "./components/organisation/extract/depensefixe/DepensefixeUpdateGetter";
import TypeListe from "./components/organisation/extract/type/TypeListe";
import TypeFicheGetter from "./components/organisation/extract/type/TypeFicheGetter";
import TypeForm from "./components/organisation/extract/type/TypeForm";
import TypeUpdateGetter from "./components/organisation/extract/type/TypeUpdateGetter";
import TypedepenseListe from "./components/organisation/extract/type/TypedepenseListe";
import TypedepenseFicheGetter from "./components/organisation/extract/type/TypedepenseFicheGetter";
import TypedepenseForm from "./components/organisation/extract/type/TypedepenseForm";
import TypedepenseUpdateGetter from "./components/organisation/extract/type/TypedepenseUpdateGetter";
import AutredepenseListe from "./components/organisation/extract/autre/AutredepenseListe";
import AutredepenseFicheGetter from "./components/organisation/extract/autre/AutredepenseFicheGetter";
import AutredepenseForm from "./components/organisation/extract/autre/AutredepenseForm";
import AutredepenseUpdateGetter from "./components/organisation/extract/autre/AutredepenseUpdateGetter";
import DevisListe from "./components/organisation/extract/devis/DevisListe";
import DevisFicheGetter from "./components/organisation/extract/devis/DevisFicheGetter";
import DevisAffichageGetter from "./components/organisation/extract/devis/DevisAffichageGetter";
import DevisForm from "./components/organisation/extract/devis/DevisForm";
import DevisUpdateGetter from "./components/organisation/extract/devis/DevisUpdateGetter";
import DevisartisteListe from "./components/organisation/extract/devis/artiste/DevisartisteListe";
import DevisartisteFicheGetter from "./components/organisation/extract/devis/artiste/DevisartisteFicheGetter";
import DevisartisteForm from "./components/organisation/extract/devis/artiste/DevisartisteForm";
import DevisartisteUpdateGetter from "./components/organisation/extract/devis/artiste/DevisartisteUpdateGetter";
import ArtisteListe from "./components/organisation/extract/artiste/ArtisteListe";
import ArtisteFicheGetter from "./components/organisation/extract/artiste/ArtisteFicheGetter";
import ArtisteForm from "./components/organisation/extract/artiste/ArtisteForm";
import ArtisteUpdateGetter from "./components/organisation/extract/artiste/ArtisteUpdateGetter";
import DevisautreListe from "./components/organisation/extract/devis/autre/DevisautreListe";
import DevisautreFicheGetter from "./components/organisation/extract/devis/autre/DevisautreFicheGetter";
import DevisautreForm from "./components/organisation/extract/devis/autre/DevisautreForm";
import DevisautreUpdateGetter from "./components/organisation/extract/devis/autre/DevisautreUpdateGetter";
import DevisfixeListe from "./components/organisation/extract/devis/fixe/DevisfixeListe";
import DevisfixeFicheGetter from "./components/organisation/extract/devis/fixe/DevisfixeFicheGetter";
import DevisfixeForm from "./components/organisation/extract/devis/fixe/DevisfixeForm";
import DevisfixeUpdateGetter from "./components/organisation/extract/devis/fixe/DevisfixeUpdateGetter";
import CategoriesListe from "./components/organisation/extract/categories/CategoriesListe";
import CategoriesFicheGetter from "./components/organisation/extract/categories/CategoriesFicheGetter";
import CategoriesForm from "./components/organisation/extract/categories/CategoriesForm";
import CategoriesUpdateGetter from "./components/organisation/extract/categories/CategoriesUpdateGetter";
import CategorieslieuListe from "./components/organisation/extract/categories/lieu/CategorieslieuListe";
import CategorieslieuFicheGetter from "./components/organisation/extract/categories/lieu/CategorieslieuFicheGetter";
import CategorieslieuForm from "./components/organisation/extract/categories/lieu/CategorieslieuForm";
import CategorieslieuUpdateGetter from "./components/organisation/extract/categories/lieu/CategorieslieuUpdateGetter";
import DevisbilletListe from "./components/organisation/extract/billet/DevisbilletListe";
import DevisbilletFicheGetter from "./components/organisation/extract/billet/DevisbilletFicheGetter";
import DevisbilletForm from "./components/organisation/extract/billet/DevisbilletForm";
import DevisbilletUpdateGetter from "./components/organisation/extract/billet/DevisbilletUpdateGetter";
import LoginListe from "./components/organisation/extract/auth/LoginListe";
import LoginFicheGetter from "./components/organisation/extract/auth/LoginFicheGetter";
import LoginForm from "./components/organisation/extract/auth/LoginForm";
import LoginUpdateGetter from "./components/organisation/extract/auth/LoginUpdateGetter";
import PercentListe from "./components/organisation/extract/percent/PercentListe";
import PercentFicheGetter from "./components/organisation/extract/percent/PercentFicheGetter";
import PercentForm from "./components/organisation/extract/percent/PercentForm";
import PercentUpdateGetter from "./components/organisation/extract/percent/PercentUpdateGetter";
import SpectacleListe from "./components/organisation/extract/spectacle/SpectacleListe";
import SpectacleFicheGetter from "./components/organisation/extract/spectacle/SpectacleFicheGetter";
import SpectacleForm from "./components/organisation/extract/spectacle/SpectacleForm";
import SpectacleUpdateGetter from "./components/organisation/extract/spectacle/SpectacleUpdateGetter";
import BisGetter from "./components/organisation/extract/devis/BisGetter";

function App() {
    return (
        <BrowserRouter>
            <Routes>
                {/*--------------assistant--------------*/}
                <Route path="/assistant" element={ <Histant /> }></Route>
                <Route path="/assistant/preview/:table" element={ <GetPreview /> }></Route>
                <Route path="/assistant/affichage/tables/:table" element={ <AfficheGetter /> }></Route>
                <Route path="/assistant/affichage/class/:className" element={ <AffClassGetter /> }></Route>
                {/*--------------Client-----------------*/}
                <Route path="/" element={ <AuthClient /> }></Route>
                <Route path="/admin" element={ <AuthAdmin /> }></Route>
                {/* Lieu */}
                <Route path="/lieu" element={ <LieuListe /> }></Route>
                <Route path="/lieu/:id" element={ <LieuFicheGetter /> }></Route>
                <Route path="/lieu/form" element={ <LieuForm /> }></Route>
                <Route path="/lieu/update/:id" element={ <LieuUpdateGetter /> }></Route>
                {/* Depense fixe */}
                <Route path="/depensefixe" element={ <DepensefixeListe /> }></Route>
                <Route path="/depensefixe/:id" element={ <DepensefixeFicheGetter /> }></Route>
                <Route path="/depensefixe/form" element={ <DepensefixeForm /> }></Route>
                <Route path="/depensefixe/update/:id" element={ <DepensefixeUpdateGetter /> }></Route>
                {/* Type */}
                <Route path="/type" element={ <TypeListe /> }></Route>
                <Route path="/type/:id" element={ <TypeFicheGetter /> }></Route>
                <Route path="/type/form" element={ <TypeForm /> }></Route>
                <Route path="/type/update/:id" element={ <TypeUpdateGetter /> }></Route>
                {/* Type de depense */}
                <Route path="/typedepense" element={ <TypedepenseListe /> }></Route>
                <Route path="/typedepense/:id" element={ <TypedepenseFicheGetter /> }></Route>
                <Route path="/typedepense/form" element={ <TypedepenseForm /> }></Route>
                <Route path="/typedepense/update/:id" element={ <TypedepenseUpdateGetter /> }></Route>
                {/* Autre depense */}
                <Route path="/autre/depense" element={ <AutredepenseListe /> }></Route>
                <Route path="/autre/depense/:id" element={ <AutredepenseFicheGetter /> }></Route>
                <Route path="/autre/depense/form" element={ <AutredepenseForm /> }></Route>
                <Route path="/autre/depense/update/:id" element={ <AutredepenseUpdateGetter /> }></Route>
                {/* Devis */}
                <Route path="/devis" element={ <DevisListe /> }></Route>
                <Route path="/devis/:id" element={ <DevisFicheGetter /> }></Route>
                <Route path="/devis/affichage/:id" element={ <DevisAffichageGetter /> }></Route>
                <Route path="/devis/form" element={ <DevisForm /> }></Route>
                <Route path="/devis/update/:id" element={ <DevisUpdateGetter /> }></Route>
                
                {/* Devis artiste */}
                <Route path="/devis/artiste" element={ <DevisartisteListe /> }></Route>
                <Route path="/devis/artiste/:id" element={ <DevisartisteFicheGetter /> }></Route>
                <Route path="/devis/artiste/form" element={ <DevisartisteForm /> }></Route>
                <Route path="/devis/artiste/update/:id" element={ <DevisartisteUpdateGetter /> }></Route>
                {/* Artiste */}
                <Route path="/artiste" element={ <ArtisteListe /> }></Route>
                <Route path="/artiste/:id" element={ <ArtisteFicheGetter /> }></Route>
                <Route path="/artiste/form" element={ <ArtisteForm /> }></Route>
                <Route path="/artiste/update/:id" element={ <ArtisteUpdateGetter /> }></Route>
                {/* Autre devis */}
                <Route path="/devis/autre" element={ <DevisautreListe /> }></Route>
                <Route path="/devis/autre/:id" element={ <DevisautreFicheGetter /> }></Route>
                <Route path="/devis/autre/form" element={ <DevisautreForm /> }></Route>
                <Route path="/devis/autre/update/:id" element={ <DevisautreUpdateGetter /> }></Route>
                {/* Devis fixe */}
                <Route path="/devis/fixe" element={ <DevisfixeListe /> }></Route>
                <Route path="/devis/fixe/:id" element={ <DevisfixeFicheGetter /> }></Route>
                <Route path="/devis/fixe/form" element={ <DevisfixeForm /> }></Route>
                <Route path="/devis/fixe/update/:id" element={ <DevisfixeUpdateGetter /> }></Route>
                {/* Categories */}
                <Route path="/categories" element={ <CategoriesListe /> }></Route>
                <Route path="/categories/:id" element={ <CategoriesFicheGetter /> }></Route>
                <Route path="/categories/form" element={ <CategoriesForm /> }></Route>
                <Route path="/categories/update/:id" element={ <CategoriesUpdateGetter /> }></Route>
                {/* Nombre de place */}
                <Route path="/categories/lieu" element={ <CategorieslieuListe /> }></Route>
                <Route path="/categories/lieu/:id" element={ <CategorieslieuFicheGetter /> }></Route>
                <Route path="/categories/lieu/form" element={ <CategorieslieuForm /> }></Route>
                <Route path="/categories/lieu/update/:id" element={ <CategorieslieuUpdateGetter /> }></Route>
                {/* Devis billet */}
                <Route path="/devis/billet" element={ <DevisbilletListe /> }></Route>
                <Route path="/devis/billet/:id" element={ <DevisbilletFicheGetter /> }></Route>
                <Route path="/devis/billet/form" element={ <DevisbilletForm /> }></Route>
                <Route path="/devis/billet/update/:id" element={ <DevisbilletUpdateGetter /> }></Route>
                {/* Role */}
                <Route path="/role" element={ <LoginListe /> }></Route>
                <Route path="/role/:id" element={ <LoginFicheGetter /> }></Route>
                <Route path="/role/form" element={ <LoginForm /> }></Route>
                <Route path="/role/update/:id" element={ <LoginUpdateGetter /> }></Route>
                {/* Taxe */}
                <Route path="/percent" element={ <PercentListe /> }></Route>
                <Route path="/percent/:id" element={ <PercentFicheGetter /> }></Route>
                <Route path="/percent/form" element={ <PercentForm /> }></Route>
                <Route path="/percent/update/:id" element={ <PercentUpdateGetter /> }></Route>
                {/* Spectacle */}
                <Route path="/spectacle" element={ <SpectacleListe /> }></Route>
                <Route path="/spectacle/:id" element={ <SpectacleFicheGetter /> }></Route>
                <Route path="/spectacle/form" element={ <SpectacleForm /> }></Route>
                <Route path="/spectacle/update/:id" element={ <SpectacleUpdateGetter /> }></Route>
                <Route path="/bis/:id" element={ <BisGetter /> }></Route>

                {/* Benefice
                <Route path="/benefice" element={ <Benefice /> }></Route>
                {/* Statistique global */}
                {/* <Route path="/statistique/global" element={ <VenteGlobal /> }></Route> */}
                {/* Statistique par point de vente */}
                {/* <Route path="/statistique/vente" element={ <VenteParPoint /> }></Route> */}
                {/* Stock */}
                {/* <Route path="/commission/vente" element={ <VcommissionListe /> }></Route>
                <Route path="/commission/vente/:id" element={ <VcommissionFicheGetter /> }></Route> */}
                {/* Stock */}
                {/* <Route path="/stock/magasin" element={ <StockmagasinListe /> }></Route>
                <Route path="/stock/magasin/:id" element={ <StockmagasinFicheGetter /> }></Route> */}
                {/* Historique Vente */}
                {/* <Route path="/vente/details" element={ <VenteDetailsListe /> }></Route>
                <Route path="/vente/details/:id" element={ <VenteDetailsFicheGetter /> }></Route> */}
                {/* Arrivage */}
                {/* <Route path="/arrivage" element={ <ArrivageListe /> }></Route>
                <Route path="/arrivage/:id" element={ <ArrivageFicheGetter /> }></Route> */}
                {/* Vente */}
                {/* <Route path="/vente" element={ <VenteListe /> }></Route>
                <Route path="/vente/:id" element={ <VenteFicheGetter /> }></Route>
                <Route path="/vente/form" element={ <VenteForm /> }></Route>
                <Route path="/vente/update/:id" element={ <VenteUpdateGetter /> }></Route> */}

                {/* Commission */}

                {/* <Route path="/commission" element={ <CommissionListe /> }></Route>
                <Route path="/commission/:id" element={ <CommissionFicheGetter /> }></Route>
                <Route path="/commission/form" element={ <CommissionForm /> }></Route>
                <Route path="/commission/update/:id" element={ <CommissionUpdateGetter /> }></Route> */}
                {/* Transfert */}
                {/* <Route path="/transfert" element={ <TransfertListe /> }></Route>
                <Route path="/transfert/:id" element={ <TransfertFicheGetter /> }></Route>
                <Route path="/transfert/form" element={ <TransfertForm /> }></Route>
                <Route path="/transfert/update/:id" element={ <TransfertUpdateGetter /> }></Route> */}
                {/* Achat */}
                {/* <Route path="/achat" element={ <AchatListe /> }></Route>
                <Route path="/achat/:id" element={ <AchatFicheGetter /> }></Route>
                <Route path="/achat/form" element={ <AchatForm /> }></Route>
                <Route path="/achat/update/:id" element={ <AchatUpdateGetter /> }></Route> */}
                {/* Role */}
                {/* <Route path="/role" element={ <LoginListe /> }></Route>
                <Route path="/role/:id" element={ <LoginFicheGetter /> }></Route>
                <Route path="/role/form" element={ <LoginForm /> }></Route>
                <Route path="/role/update/:id" element={ <LoginUpdateGetter /> }></Route> */}
                {/* Crud marque */}
                {/* <Route path="/marque" element={ <MarqueListe /> }></Route>
                <Route path="/marque/:id" element={ <MarqueFicheGetter /> }></Route>
                <Route path="/marque/form" element={ <MarqueForm /> }></Route>
                <Route path="/marque/update/:id" element={ <MarqueUpdateGetter /> }></Route> */}

                {/* Crud reference */}
                {/* <Route path="/reference" element={ <ReferenceListe /> }></Route>
                <Route path="/reference/:id" element={ <ReferenceFicheGetter /> }></Route>
                <Route path="/reference/form" element={ <ReferenceForm /> }></Route>
                <Route path="/reference/update/:id" element={ <ReferenceUpdateGetter /> }></Route> */}
                {/* Crud processeur */}
                {/* <Route path="/processeur" element={ <ProcesseurListe /> }></Route>
                <Route path="/processeur/:id" element={ <ProcesseurFicheGetter /> }></Route>
                <Route path="/processeur/form" element={ <ProcesseurForm /> }></Route>
                <Route path="/processeur/update/:id" element={ <ProcesseurUpdateGetter /> }></Route> */}
                {/* Crud ram */}
                {/* <Route path="/ram" element={ <RamListe /> }></Route>
                <Route path="/ram/:id" element={ <RamFicheGetter /> }></Route>
                <Route path="/ram/form" element={ <RamForm /> }></Route>
                <Route path="/ram/update/:id" element={ <RamUpdateGetter /> }></Route> */}
                {/* Crud ecran */}
                {/* <Route path="/ecran" element={ <EcranListe /> }></Route>
                <Route path="/ecran/:id" element={ <EcranFicheGetter /> }></Route>
                <Route path="/ecran/form" element={ <EcranForm /> }></Route>
                <Route path="/ecran/update/:id" element={ <EcranUpdateGetter /> }></Route> */}
                {/* Crud laptop */}
                {/* <Route path="/laptop" element={ <LaptopListe /> }></Route>
                <Route path="/laptop/:id" element={ <LaptopFicheGetter /> }></Route>
                <Route path="/laptop/form" element={ <LaptopForm /> }></Route>
                <Route path="/laptop/update/:id" element={ <LaptopUpdateGetter /> }></Route> */}
                {/* Crud type de place */}
                {/* <Route path="/typeplace" element={ <TypeplaceListe /> }></Route>
                <Route path="/typeplace/:id" element={ <TypeplaceFicheGetter /> }></Route>
                <Route path="/typeplace/form" element={ <TypeplaceForm /> }></Route>
                <Route path="/typeplace/update/:id" element={ <TypeplaceUpdateGetter /> }></Route> */}
                {/* Crud place */}
                {/* <Route path="/place" element={ <PlaceListe /> }></Route>
                <Route path="/place/:id" element={ <PlaceFicheGetter /> }></Route>
                <Route path="/place/form" element={ <PlaceForm /> }></Route>
                <Route path="/place/update/:id" element={ <PlaceUpdateGetter /> }></Route> */} 
                {/* Crud Mouvement */}                
                {/* <Route path="/mouvement" element={ <MouvementListe /> }></Route>
                <Route path="/mouvement/:id" element={ <MouvementFicheGetter /> }></Route>
                <Route path="/mouvement/form" element={ <MouvementForm /> }></Route>
                <Route path="/mouvement/update/:id" element={ <MouvementUpdateGetter /> }></Route> */}
            </Routes>
        </BrowserRouter>
    );
}

export default App;
