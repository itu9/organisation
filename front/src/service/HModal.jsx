import { Component } from "react";
import Modal from "react-modal";
import './HModal.css'
export default class HModal extends Component {
    render() {
        return (
             <>
                <Modal isOpen={this.props.isOpen} 
                    onRequestClose={this.props.onClose}
                    className = 'addModal'
                    style={
                        {
                            overlay : {
                                zIndex : 600,
                                backgroundColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                >
                    {this.props.children}
                </Modal>
             </>
        );
    }
}