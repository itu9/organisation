import { Checkbox, FormControl, InputLabel, ListItemText, MenuItem, OutlinedInput, Select } from "@mui/material";
import { Component } from "react";

export default class HMultSelect extends Component {
    constructor(params) {
        super(params);
        this.state = {
            data : []
        }
    }

    getValue = (data,key) => {
        let dt = []
        for (let i = 0; i < data.length; i++) {
            dt.push(data[i][key])
        }
        return dt;
    }

    onChange = (event) => {
        const {
            target: { value },  
        } = event;
        this.setState({
            data : value
        })
    }

    onClick = (id) => {
        // console.log(this.props.result);
        let data = this.props.result.filter(res => res !== id)
        if (data.length === this.props.result.length) {
            data.push(id)
        }
        this.props.onChange(data);
    }



    render() {
                
        const ITEM_HEIGHT = 48;
        const ITEM_PADDING_TOP = 8;
        const MenuProps = {
            PaperProps: {
              style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
              }
            }
          };
        return (
            <>
            <FormControl sx={{ mt: 1,mb:1, width: '100%' }}>
                <InputLabel id="demo-multiple-checkbox-label">{this.props.title}</InputLabel>
                <Select
                        labelId="demo-multiple-checkbox-label"
                        id="demo-multiple-checkbox"
                        multiple
                        value={this.state.data}
                        onChange={this.onChange}
                        input={<OutlinedInput label={this.props.label} />}
                        renderValue={(selected) => selected.join(", ")}
                        MenuProps={MenuProps}
                        >
                            {
                                this.props.data.map((data) =>(
                                    <MenuItem key={data.id} value={data[this.props.keyVal]} onClick={()=>{
                                        this.onClick(data.id)
                                    }}>
                                        <Checkbox checked={this.state.data.indexOf(data[this.props.keyVal]) > -1} onChange={()=>{
                                            this.onClick(data.id)
                                        }} />
                                        <ListItemText primary={data[this.props.keyVal]} />
                                    </MenuItem>
                                ))
                            }
                    </Select>

            </FormControl>
            </>
        );
    }
}