export default class OwnService {
    static url = "http://localhost:8000/";
    static startTime = (start,setH,setM,setS,action,h,m,s) => {
        setInterval(() => {
            let ans = OwnService.between(start,h,m,s);
            setH(ans.hours)
            setM(ans.minute)
            setS(ans.second)
            action()
        },
        1000)
    }

    static between = (start,h,m,s) => {
        console.log(start)
        let end = new Date(Date.now())
        let total = end.getTime() - start.getTime();
        total += (h*3600*1000)+ (m *60 * 1000)+(s * 1000);
        let totalHours = Math.floor(total / (1000 * 60 * 60 ))
        let hours = totalHours;
        let minute = Math.floor((total / (1000 * 60)) % 60)
        let second = Math.floor((total / (1000 ))%60);
        return {
            hours : hours,
            minute : minute,
            second : second
        }
    }

    static getPdf = (url,data,action) => {
        fetch(url,{
            crossDomain:true,
            method:'POST', 
            body: JSON.stringify(data), 
            headers: {'Content-Type': 'application/json'}
        }).then(response=>{
            response.blob().then(blob=>{
                const fileURL = window.URL.createObjectURL(blob);
                let alink = document.createElement('a');
                alink.href = fileURL;
                alink.download = 'liste.pdf';
                alink.click();
                action();
            })
        })
    }
    static getData = (url,onReady,readHeader) => {
        fetch(url)
        .then(response => {
            readHeader(response.headers)
            return response.json()
        })
        .then(data => {
            onReady(data);
        });
    }

    static send = (url,data,onReady,methode,header=()=>{}) => {
        fetch(url, {
            method: methode,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(response => {
            header(response.headers)
            return response.json()
        } )
        .then(data => {
            onReady(data);
        })
    }

    static sendData = (url,data, onReady,header=()=>{}) => {
        OwnService.send(url,data,onReady,'POST',header);
    }

    static update = (url,data,onReady) => {
        OwnService.send(url,data,onReady,'PUT');
    }

    static delete = (url,data,onReady) => {
        fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(response => onReady(response));
    }

    static moneyFormat = new Intl.NumberFormat('fr-FR', {
        style: 'currency',
        currency: 'MGA',
        minimumFractionDigits: 2
    });

    static formatDate = (date) => {
        let dObjt = new Date(date);
        let ans = dObjt.toLocaleDateString("fr-FR", {
            day : "numeric",
            month : "long",
            year : "numeric"
        });
        return ans;
    }

    static format = (data) => {
        return OwnService.moneyFormat.format(data);
    }

    static getPLaceNbByType = (avion,type) => {
        for (let i = 0; i < avion.classes.length; i++) {
            if (avion.classes[i].type.id === type.id) {
                return OwnService.getPLaceNumber(avion,avion.classes[i]);
            }
        }
        return 0;
    }

    static getPLaceNumber = (avion,cls) => {
        let rang = parseInt(avion.rang,10);
        let siege = parseInt(cls.rfin,10)-parseInt(cls.rdebut,10)+1;
        return rang * siege;
    }
    
    static getTotalPlace =(avion) => {
        let ans = 0;
        for (let i = 0; i < avion.classes.length; i++) {
            ans += OwnService.getPLaceNumber(avion,avion.classes[i]);
        }
        return ans;
    }

    static capitaliseFirstLetter = (str) => {
        return str[0].toUpperCase()+str.slice(1)
    }
}