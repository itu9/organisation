import { useParams } from "react-router-dom";
import Preview from "./Preview";

export default function GetPreview() {
    const {table} = useParams();
    return (
        <Preview table={table}>

        </Preview>
    )
}