import React from "react";
import HContainer from "../../components/client/nav/HContainer";
import Border from "../../components/utils/Border";
import General from "../../components/utils/General";
import ListePage from "../../components/utils/ListePage";
import HAvatar from "./HAvatar";

export default class Histant extends General {
    componentDidMount() {
        this.setState({
            tables : [],
            panier : [],
            clazz : [],
            totSel : 0.,
        }, this.getTablesData);
        this.chat = React.createRef()
    }

    getTablesData = () => {
        this.getListe(this.url+"/tables",
            data => {
                // console.log(data.data);
                this.setState( {
                    tables : data.data
                })
            }
            // eslint-disable-next-line no-unused-vars
            , header => {}
        )
        this.getListe(this.url+"/affichage",
            data => {
                this.setState({
                    clazz : data.data
                })
            },
            // eslint-disable-next-line no-unused-vars
            header => {

            }
        )
    }

    add = (data) => {
        let panier = this.state.panier.filter(pan => pan !== data.tableName);
        if (panier.length === this.state.panier.length) {
            panier = [
                ...panier,
                data.tableName
            ]
        }
        this.setState({
            panier : panier
        })
    }

    formatUrl = (clazz) => {
        let splt = clazz.split(".")
        return splt.join("-");
    }

    valider = () => {
        console.log(this.state.panier);
    }
    render() {
        return (
            <HContainer>
                <h1><HAvatar/></h1>
                <Border>
                    <h3 className="text-info">Generer l&apos;affichage à partir de la classe</h3>
                    <ListePage
                        head={
                                <tr>
                                    <th>Classe</th>
                                    <th>Package</th>
                                </tr>
                        }
                    >
                        {
                            this.state.clazz !== undefined && this.state.clazz !== null ?
                            this.state.clazz.map((cls,index) => (
                                <tr key={index}>
                                    <td><a href="#a" onClick={()=>{ window.location.replace("/assistant/affichage/class/"+this.formatUrl(cls.clazz) )}}>{cls.name}</a></td>
                                    <td>{cls.packageName}</td>
                                </tr>
                            )):<></>
                        }
                        <tr></tr>
                    </ListePage>
                </Border>
                <Border>
                <ListePage
                    title="Les tables existantes"
                    size="8"
                    head={
                        <tr>
                            <td>Nom de table</td>
                            <td>Etat</td>
                            <td>Action</td>
                            <td></td>
                        </tr>
                    }
                >
                {
                    this.state.tables !== undefined? this.state.tables.map((table,index) => (
                        <tr key={index}>
                            <td>{table.tableName}</td>
                            <td>
                                {
                                    table.created ? <span className="text-success">Associée</span > : <span className="text-danger">Pas de model</span>
                                }
                            </td>
                            <td>
                                {
                                    table.created ? <button className="btn btn-info btn-block"  onClick={()=>{
                                        window.location.replace("/assistant/preview/"+table.tableName)
                                    }} >Modifier</button> :
                                    table.block ? <button className="btn btn-danger btn-block" onClick={this.valider} >En attente</button>:
                                    <button className="btn btn-success btn-block" onClick={()=>{
                                        window.location.replace("/assistant/preview/"+table.tableName)
                                    }} >Preview</button>
                                }
                                
                                
                            </td>     
                            <td>
                                <input type="checkbox" onChange={() => {this.add(table)}}></input>
                            </td>   
                        </tr>
                    )) : <></>
                }
                </ListePage>

                {/* <div className="col-8">

                </div>
                <div className="col-8">
                    <button className="btn btn-success mt-3 btn-block" onClick={this.valider} >Preview</button>
                </div> */}
                </Border>
            </HContainer>
        );
    }
}