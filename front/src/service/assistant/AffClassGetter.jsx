import { useParams } from "react-router-dom";
import Affichage from "./Affichage";

export default function AffClassGetter() {
    const {className} = useParams();
    let splt = className.split("-")
    var clazz = splt.join(".")
    return (
        <Affichage className={clazz}>
        </Affichage>
    );
}