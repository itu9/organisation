import React from "react";
import HContainer from "../../components/client/nav/HContainer";
import Border from "../../components/utils/Border";
import General from "../../components/utils/General";
import './Preview.css';
import Swal from "sweetalert2";
import HAvatar from "./HAvatar";

export default class Preview extends General {
    constructor(params) {
        super(params);
        this.packageName = React.createRef(null);
        this.mapping = React.createRef(null);
    }
    componentDidMount () {
        this.setState({
            data : null,
            panier : []
        },()=> {
            let pack =  "extract."
            this.getData(pack,false,[]);
        }
        );
    }

    
    add = (data) => {
        let panier = this.state.panier.filter(pan => pan.column !== data.name);
        if (panier.length === this.state.panier.length) {
            panier = [
                ...panier,
                {
                    column :data.name
                }
            ]
        }
        this.setState({
            panier : panier
        },() => {
            let packageName = this.packageName.current.value
            this.getData(packageName,false,this.state.panier)
        })
    }
    getData = (packageName,valid,onetoone,action = (data)=>{})=> {
        let data = {
            table : this.props.table,
            packageName : packageName,
            valid : valid,
            oneToOne : onetoone
        }
        this.sendData(this.url+"/tables",data,
            data => {
                console.log(data);
                if ( data.code !== undefined && data.code !== null) {
                    Swal.fire({
                        icon:'error',
                        title:'Erreur',
                        text:`Cette action n'est pas permise.`,
                        timer:3000
                    })
                } else {
                    action(data);
                    this.setState({
                        data : data.data
                    })

                }
            }
        )
    }

    valider = () => {
        let packageName = this.packageName.current.value
        this.getData(packageName,true,this.state.panier,
            data => {
                Swal.fire({
                    icon:'success',
                        title:'Felicitation',
                        text:`Le model est mis en place`,
                        timer:5000
                }).then(() => {window.location.reload()})
            }
        )
    }

    changePackage = () => {
        let packageName = this.packageName.current.value
        this.getData(packageName,false,this.state.panier)
    }

    createController = () => {
        let data = {
            tableName : this.props.table,
            url : this.mapping.current.value
        }
        this.sendData(this.url+"/tables/makecontroller",data,
            (data) => {
                if ( data.code !== undefined && data.code !== null) {
                    Swal.fire({
                        icon:'error',
                        title:'Erreur',
                        text:`Cette action n'est pas permise.`,
                        timer:3000
                    })
                } else {
                    Swal.fire({
                        icon:'success',
                        title : 'Félicitation',
                        text : 'Votre controlleur est mis en place.'
                    })

                }
            }
        )
    }

    render() {
        return (
            <HContainer>
                <div className="row">
                    <div className="col-8">
                        <h1 className="text-dark"><HAvatar/> | {this.firstTop( this.props.table)}</h1>
                    </div>
                    <div className="col-4 mb-5">
                        <button className="btn btn-info btn-block mt-3" onClick={
                            () => {
                                window.location.replace("/assistant/affichage/tables/"+this.props.table)
                            }
                        } >{"Partie 2 >> Affichage"}</button>
                    </div>
                </div>
                <Border>
                    {
                        this.state.data !== undefined && this.state.data !== null ?
                        <>
                        <div className="row">
                            <div className="col-6">
                                <h3 className="text-info ">Package</h3>
                                <input type="text" defaultValue={"extract."} className="form-control" onChange={this.changePackage} ref={this.packageName} />
                            </div>
                            {
                                this.state.data.table.created ?
                                <div className="col-6">
                                    <h3 className="text-info">Url mapping</h3>
                                    <input ref={this.mapping} className="form-control" defaultValue={"/"+this.props.table}/>
                                    <button className="btn btn-success btn-block mt-3" onClick={this.createController}>Metre en place un controller</button>
                                </div>:
                                <></>
                            }
                            <div className="col-12 mb-3"></div>
                            <div className="col-6">
                                <h3 className="text-info">Les columns de la table</h3>
                                <table className="table mt-3">
                                    <thead className="bg-info text-white">
                                        <tr>
                                            <td>Colonne</td>
                                            <td>Type</td>
                                            <td>Clé étrangère</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.data.table.cols.map((col,index) => (
                                                <tr>
                                                    <td>{col.name}</td>
                                                    <td>{col.type}</td>
                                                    <td>{col.foreign ? "Oui" : "Non"}</td>
                                                    <td>
                                                        {
                                                            col.foreign ?
                                                            <input type="checkbox" onChange={() => {this.add(col)}} ></input> : <></>
                                                        }
                                                    </td>
                                                </tr>
                                            ) )
                                        }
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-6">
                                <h3 className="text-info">Les tables associées</h3>
                                <table className="table mt-3">
                                    <thead className="bg-info text-white">
                                        <tr>
                                            <td>Colonne</td>
                                            <td>Table de reference</td>
                                            <td>Clé étrangère</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.data.table.fks.map((fk,index) => (
                                                <tr>
                                                    <td>{fk.name}</td>
                                                    <td>{fk.tableRef}</td>
                                                    <td>
                                                        <input type="checkbox" ></input>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-8">
                                <pre className="form-control hcode">
                                {this.state.data.content}</pre>
                            </div>
                            {
                                // !this.state.data.table.created ?
                                true?
                                <div className="col-6">
                                    <button className="btn btn-success btn-block mt-3" onClick={this.valider}>Valider</button>
                                </div>:
                                <></>
                            }
                            
                        </div>
                        </> : <></>
                    }
                </Border>
            </HContainer>
        );
    }
}