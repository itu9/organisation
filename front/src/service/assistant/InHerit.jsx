import React from "react";
import General from "../../components/utils/General";
import Swal from "sweetalert2";
import OwnService from "../OwnService.jsx";

export default class InHerit extends General {
    constructor(params) {
        super(params);
        this.valeur = React.createRef(null);
        this.label = React.createRef(null);
        this.title = React.createRef(null);
        this.checked = React.createRef(null);
    }


    addData = () => {
        if (this.title.current.value === "") {
            Swal.fire({
                icon : 'error',
                title : 'Blocké',
                text : 'Le titre ne peut pas être vide',
                timer : 3000
            }).then(() => {
                this.checked.current.checked = false;
            })  
        } else {
            let data = {
                field : this.props.fields.name,
                value : this.valeur.current.value,
                label : this.label.current.value,
                title : this.title.current.value
            }
            if (this.checked.current.checked) {
                this.props.add(data);
            } else {
                this.props.remove(data);
            }
        }
    }

    change = () => {
        let data = {
            field : this.props.fields.name,
            value : this.valeur.current.value,
            label : this.label.current.value,
            title : this.title.current.value
        }
        if (this.title.current.value === "") {
            this.props.remove(data);
            this.checked.current.checked = false;
        } else {
            this.props.remove(data);
            this.addData();
        }
    }

    render() {
        return (
            <tr>
                <td>{this.props.fields.name}</td>
                <td>
                    <select className="form-control" ref={this.valeur} onChange={this.change}>
                        {
                            this.props.fields.fields.map(field => (
                                <option>{field.name}</option>
                            ))
                        }
                    </select>
                </td>
                <td>
                    <select className="form-control" ref={this.label} onChange={this.change}>
                        {
                            this.props.fields.fields.map(field => (
                                <option>{field.name}</option>
                            ))
                        }
                    </select>
                </td>
                <td>
                    <input className="form-control" ref={this.title} onChange={this.change} defaultValue={OwnService.capitaliseFirstLetter(this.props.fields.name)}/>
                </td>
                <td>
                    <input ref={this.checked} type="checkbox" onClick={this.addData} />
                </td>
            </tr>
        );
    }
}