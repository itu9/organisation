import HContainer from "/src/components/client/nav/HContainer";
import Border from "/src/components/utils/Border";
import General from "/src/components/utils/General";
import ListePage from "/src/components/utils/ListePage";
import './Affichage.css';
import HAvatar from "./HAvatar";
import InHerit from "./InHerit";
import Swal from "sweetalert2";
import SimpleField from "./SimpleField";
import React from "react";
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { copy } from 'clipboard';
import Editor, { DiffEditor, useMonaco, loader } from '@monaco-editor/react';

export default class Affichage extends General {
    constructor(props) {
        super(props);
        this.listeTitle = React.createRef(null);
        this.formTitle = React.createRef(null);
        this.updateTitle = React.createRef(null);
        this.infoTitle = React.createRef(null);
        this.app = React.createRef(null);
        this.container = React.createRef(null);
    }

    componentDidMount = () => {
        this.setState({
            class: "truncate",
            content : "",
            imports : "",
            container : "",
            app : "",
            valid : false,
            fields: null,
            listeData: [],
            simpleData: []
        }, this.getData)
    }

    getData = () => {
        let data = {
            clazz: this.props.className
        };
        this.sendData(this.url + "/affichage", data,
            (result) => {
                this.verifData(result,
                    res => {
                        this.setState({
                            fields: res.data
                        })
                    }
                )
            }
        )
    }

    change = () => {
        this.setState({
            class: this.state.class === "" ? "truncate" : ""
        })
    }

    addFrom = (source, data) => {
        let ans = source.filter(li => li.field !== data.field);
        ans = [
            ...ans,
            data
        ]
        return ans
    }

    add = (data) => {
        this.setState({
            listeData: this.addFrom(this.state.listeData, data)
        })
    }

    addSimple = (data) => {
        this.setState({
            simpleData: this.addFrom(this.state.simpleData, data)
        })
    }

    remove = (data) => {
        let ans = this.state.listeData.filter(li => li.field !== data.field);
        this.setState({
            listeData: ans
        })
    }

    removeSimple = (data) => {
        let ans = this.state.simpleData.filter(li => li.field !== data.field);
        this.setState({
            simpleData: ans
        })
    }

    validate = () => {
        let data = {
            clazz: this.state.fields.clazz,
            inheritParams: this.state.listeData,
            simpleParams : this.state.simpleData,
            listeTitle : this.listeTitle.current.value,
            formTitle : this.formTitle.current.value,
            updateTitle : this.updateTitle.current.value,
            infoTitle : this.infoTitle.current.value
        }
        console.log(data)
        this.sendData(this.url + "/tables/makefront",
            data,
            (result) => {
                this.verifData(result,
                    // eslint-disable-next-line no-unused-vars
                    ans => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Bravo',
                            text: 'Le front a été créé',
                            timer: 3000
                        }).then(() => {
                            console.log(result.data)
                            this.setState({
                                content : result.data.content,
                                imports : result.data.import,
                                app : result.data.app
                            },()=>{
                                this.app.current.setValue(result.data.app)
                                this.container.current.setValue(result.data.container)
                            })
                        })
                    }
                )
            }
        )
    }

    handleCopyComp = () => {
        copy(this.state.content);
    }

    handleCopyImport = () => {
        copy(this.state.imports)
    }
    handleEditorDidMount = (editor, monaco) =>{
        this.app.current = editor
        this.app.current.setValue(this.state.app)
    }


    handleContainerEditorDidMount = (editor, monaco) =>{
        this.container.current = editor
        this.container.current.setValue(this.state.container)
    }

    saveData = () => {
        let data = {
            app : this.app.current.getValue(),
            container : this.container.current.getValue()
        }
        this.sendData(this.url + "/tables/saveModel",
            data,
            (result) => {
                this.verifData(result,
                    // eslint-disable-next-line no-unused-vars
                    ans => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Bravo',
                            text: 'Le front a été créé',
                            timer: 3000
                        })
                    }
                )
            }
        )
    }

    render() {
        return (
            <HContainer>
                <h1 className="text-dark mb-5"><HAvatar/> | Mise en place de la partie affichage</h1>
                <Border>
                    <h2 className="text-info">Liste,Supression avec recherche et export PDF|CSV (Retrieve|Delete)</h2>
                    <p className={this.state.class !== undefined ? this.state.class : "truncate"} onClick={this.change}>
                        <HAvatar></HAvatar> génère un fichier <span className="text-info">javascript</span> avec
                        un <span className="text-dark">composant</span> de liste dedans.<br/>
                        Les fonctionnalités de <span className="text-dark">recherche</span> et de <span
                        className="text-dark">supression</span> y sont déjà intégrées <br/>
                        mais pour la modification et l&apos;ajout, un bouton est mise en place pour<br/>
                        y rediriger.<br/><br/>
                        Pour les <span className="text-info">recherches avancées</span>, la première entrée est
                        une <span className="text-info">&quot;elastic search&quot;</span><br/>
                        ou <span className="text-dark">recherche par mot clé</span>. Les recherches par <span
                        className="text-info">intervalles</span> (ex: prix minimum <br/>
                        et prix maximum) sont pour les données <span className="text-dark">quantifiables</span>. Pour
                        les données<br/> en relation avec d&apos;autres données,
                        un <span className="text-dark">select</span> est mise en place avec la liste des <br/>données
                        existantes.
                        <br/><br/>
                        <HAvatar></HAvatar> offre aussi un export <span className="text-info">pdf et csv</span> du
                        resultat de recherche.
                    </p>
                </Border>
                <Border>
                    <h2 className="text-info">Atelier</h2>
                    <div className="row mt-3">
                        <div className="col-6">
                            <h3 className="text-info">Titre de la liste</h3>
                            <input type="text" className="form-control" defaultValue="Titre de la liste" ref={this.listeTitle}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <h3 className="text-info">Titre du fiche de detail</h3>
                            <input type="text" className="form-control" defaultValue="Fiche de details" ref={this.infoTitle}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <h3 className="text-info">Titre du formulaire d'ajout</h3>
                            <input type="text" className="form-control" defaultValue="Formulaire" ref={this.formTitle}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <h3 className="text-info">Titre du formulaire de modification</h3>
                            <input type="text" className="form-control" defaultValue="Formulaire de modification" ref={this.updateTitle}/>
                        </div>
                    </div>
                    <ListePage
                        size="10"
                        head={
                            <tr>
                                <th>Field</th>
                                <th>Valeur</th>
                                <th>Label</th>
                                <th>Titre</th>
                                <th>Etat</th>
                            </tr>
                        }
                    >
                        {
                            this.state.fields !== null && this.state.fields !== undefined ?
                                this.state.fields.fields.map((fields, index) => {
                                    if (fields.fields.length > 0) {
                                        return (
                                            <InHerit key={index} add={this.add} remove={this.remove}
                                                     fields={fields}></InHerit>
                                        )
                                    } else {
                                        return <SimpleField key={index} add={this.addSimple}
                                                            remove={this.removeSimple}
                                                            fields={fields}
                                                    ></SimpleField>
                                    }

                                }) : <></>
                        }
                    </ListePage>
                    <div className="row">
                        <div className="col-6">
                            <button className="btn btn-block btn-success mt-3" onClick={this.validate}>Generer liste
                            </button>
                        </div>
                    </div>
                    <>
                        <div className={"row mt-5"} >
                            <div className={"col-12"}>
                                <h3 className="text-dark mb-3">Copier le code ci-dessous parmis les <span className={"text-dark"}>"Route"</span> dans App.jsx ou utiliser la fenetre vscode de la page</h3>
                                <SyntaxHighlighter language="javascript">
                                    {this.state.content}
                                </SyntaxHighlighter>
                                <button className={"btn btn-info"} onClick={this.handleCopyComp} disabled={false}>
                                    Copier
                                </button>
                            </div>
                        </div>
                        <div className={"row mt-5"} >
                            <div className={"col-12"}>
                                <h3 className="text-dark mb-3">Copier le code ci-dessous parmis les <span className={"text-dark"}> importations</span> dans App.jsx ou utiliser la fenetre vscode de la page</h3>
                                <SyntaxHighlighter language="javascript">
                                    {this.state.imports}
                                </SyntaxHighlighter>
                                <button className={"btn btn-info"} onClick={this.handleCopyImport} disabled={false}>
                                    Copier
                                </button>
                            </div>
                        </div>
                        <div className={"row mt-3"} >
                            <div className={"col-12"}>
                                <h3 className="text-dark mb-3">App.jsx</h3>
                                <Editor onMount={this.handleEditorDidMount} theme={"vs-dark"} height="90vh" defaultLanguage="javascript" defaultValue={this.state.app}
                                />
                            </div>
                        </div>
                        <div className={"row mt-3"} >
                            <div className={"col-12"}>
                                <h3 className="text-dark mb-3">HContainer.jsx</h3>
                                <Editor onMount={this.handleContainerEditorDidMount} theme={"vs-dark"} height="90vh" defaultLanguage="javascript" defaultValue={this.state.container}
                                />
                                <button className={"btn btn-info mt-3"} onClick={this.saveData}>
                                    Valider
                                </button>
                            </div>
                        </div>
                    </>
                </Border>
            </HContainer>
        );
    }
}