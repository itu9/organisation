import { Card, CardContent } from "@mui/material";
import { Component } from "react";

export default class HCard extends Component {
    render() {
        return (
             <>
             <Card sx={{maxWidth : '100%',boxShadow: '2px 3px 10px'}}>
             <CardContent>
                {this.props.children}
             </CardContent>
             </Card>
             </>
        );
    }
}